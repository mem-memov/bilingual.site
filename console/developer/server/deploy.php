<?php
$user = 'root';
$server = '5.63.157.150';
$remote = new RemoteServer($user, $server);

$remote->rsync('/var/www/bilingual.site/web/' , '/var/www/bilingual.site/web');
$remote->rsync('/var/www/bilingual.site/php/' , '/var/www/bilingual.site/php');
$remote->rsync('/var/www/bilingual.site/console/' , '/var/www/bilingual.site/console');
$remote->rsync('/var/www/bilingual.site/migration/' , '/var/www/bilingual.site/migration');

$remote->ssh('chown -R www-data:www-data /var/www/bilingual.site');

$remote->ssh('mkdir -p /var/www/bilingual.site/dump');
$dumpFile = 'dump_' . date('Y_m_d_H_i_s') . '_' . $server . '_bilingual_site.gz';
$remote->ssh('mysqldump --quick bilingual_site | gzip > /var/www/bilingual.site/dump/' . $dumpFile);
$remote->scp('/var/www/bilingual.site/dump/' . $dumpFile, '/var/www/bilingual.site/dump/' . $dumpFile);

$localMigration = trim(file_get_contents('/var/www/bilingual.site/migration/current'));
$remote->ssh('php /var/www/bilingual.site/console/developer/database/migration/apply.php "' . $localMigration . '"');

final class RemoteServer
{
    private $address;
    
    public function __construct(string $user, string $server)
    {
        $this->address = $user . '@' . $server;
    }
    
    function ssh(string $command) {
        $message = 'ssh ' . $this->address . ' "' . str_replace('"', '\"', $command) . '"';
        echo $message . "\n";
        passthru($message);
    }

    function rsync(string $localOrigin, string $remoteDestination) {
        $message = 'rsync --recursive --times "' . $localOrigin . '" ' . $this->address . ':"' . $remoteDestination . '"';
        echo $message . "\n";
        passthru($message);
    }

    function scp(string $remoteOrigin, string $localDestination) {
        $message = 'scp ' . $this->address . ':"' . $remoteOrigin . '" "' . $localDestination . '"';
        echo $message . "\n";
        passthru($message);
    }
}
