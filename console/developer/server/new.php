<?php

if ($argc < 2) {
    $lines =[
        'User and remote server are not provided.',
        'This script will destroy remote site!',
        'Configure ssh:',
        "\t" . 'ssh -v',
        "\t" . 'ls -a ~/.ssh',
        "\t" . 'ssh-keygen',
        "\t" . 'ssh-copy-id root@111.111.111.111',
        'Then start like this:',
        "\t" . 'php ' . __FILE__ . ' root 111.111.111.111'
    ];
    echo implode("\n", $lines) . "\n";
    exit;
}

$user = $argv[1];
$server = $argv[2];

$remote = new RemoteServer($user, $server);

$remote->ssh('rm -rf /var/www/bilingual.site');
$remote->ssh('mkdir /var/www/bilingual.site');

$remote->ssh('mkdir /var/www/bilingual.site/web');
$remote->rsync('/var/www/bilingual.site/web/' , '/var/www/bilingual.site/web');

$remote->ssh('mkdir /var/www/bilingual.site/php');
$remote->rsync('/var/www/bilingual.site/php/' , '/var/www/bilingual.site/php');

$remote->ssh('mkdir /var/www/bilingual.site/console');
$remote->rsync('/var/www/bilingual.site/console/' , '/var/www/bilingual.site/console');

$remote->ssh('mkdir /var/www/bilingual.site/migration');
$remote->rsync('/var/www/bilingual.site/migration/' , '/var/www/bilingual.site/migration');

$remote->ssh('chown -R www-data:www-data /var/www/bilingual.site');

$remote->ssh('mkdir /var/www/bilingual.site/dump');
$dumpFile = 'dump_' . date('Y_m_d_H_i_s') . '_' . $server . '_bilingual_site.gz';
$remote->ssh('mysqldump --quick bilingual_site | gzip > /var/www/bilingual.site/dump/' . $dumpFile);
$remote->scp('/var/www/bilingual.site/dump/' . $dumpFile, '/var/www/bilingual.site/dump/' . $dumpFile);
$remote->ssh('mysqladmin --force drop bilingual_site');
$remote->ssh('mysqladmin create bilingual_site');
$remote->ssh('echo start > /var/www/bilingual.site/migration/current');
$localMigration = trim(file_get_contents('/var/www/bilingual.site/migration/current'));
$remote->ssh('php /var/www/bilingual.site/console/developer/database/migration/apply.php "' . $localMigration . '"');



final class RemoteServer
{
    private $address;
    
    public function __construct(string $user, string $server)
    {
        $this->address = $user . '@' . $server;
    }
    
    function ssh(string $command) {
        $message = 'ssh ' . $this->address . ' "' . str_replace('"', '\"', $command) . '"';
        echo $message . "\n";
        passthru($message);
    }

    function rsync(string $localOrigin, string $remoteDestination) {
        $message = 'rsync --recursive --times "' . $localOrigin . '" ' . $this->address . ':"' . $remoteDestination . '"';
        echo $message . "\n";
        passthru($message);
    }

    function scp(string $remoteOrigin, string $localDestination) {
        $message = 'scp ' . $this->address . ':"' . $remoteOrigin . '" "' . $localDestination . '"';
        echo $message . "\n";
        passthru($message);
    }
}
