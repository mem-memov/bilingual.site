<?php
$phpDirectory = __DIR__ . '/../../php';
require $phpDirectory . '/vendor/autoload.php';

$configuration = require_once($phpDirectory . 'configuration/developer.php');
$factory = new \Pool(new \Factory($configuration));
$mailer = $factory->mailer();
$mailer->send();
