INSERT INTO `source` ( `source`.`file` ) VALUES ( "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies.mkv" );
# a query
INSERT INTO `video` ( `video`.`source_id`, `video`.`language_id`, `video`.`file`, `video`.`width`, `video`.`height`, `video`.`duration` ) VALUES ( 42, 1, "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/movie.mp4", 768, 432, 1363405 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 42, 2, "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.mp3", 1363402 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 42, 83 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 42, 1, "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.mp3", 1363402 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 42, 84 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 42, 1, "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 2169, 5172, "<i>-Oh, Drake.\r\n-I\'m sorry, Amber.</i>", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28691, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28691, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28691, 15879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28691, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28691, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28691, 268 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "amber" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28691, 18205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28691, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 5380, 7925, "<i>It\'s just like Brad to have\r\nto have the last word.</i>", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 60 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "brad" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 18206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 615 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28692, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 8967, 12054, "-I\'m sorry I\'m late. What happened?\r\n-We wanna see the end.", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 1710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 452 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28693, 1793 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 12221, 15015, "<i>-I want you, Drake.\r\n-I know you do.</i>", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 15879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28694, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 15182, 17351, "<i>But you and I can never\r\nbe together that way.</i>", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28695, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 17976, 21146, "<i>-What?</i>\r\n<i>-There\'s something I never told you.</i>", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28696, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 21313, 24233, "<i>I\'m actually your half-brother.</i>", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28697, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28697, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28697, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28697, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28697, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28697, 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28697, 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28697, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 24399, 26193, "-What?\r\n-Huh?", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28698, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28698, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 29530, 31198, "So, what happens next?", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28699, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28699, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28699, 5672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28699, 4022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 31365, 34868, "Well, I get the medical award\r\nfor separating the Siamese twins.", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "medical" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 18207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 13910 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 202 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "separating" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 18208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "siamese" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 18209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28700, 1682 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 35035, 39331, "Then Amber and I go to Venezuela\r\nto meet our other half-brother, Ramon...", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 18205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 4 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "venezuela" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 18210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 1707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 1520 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 195 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "ramon" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28701, 18211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 40541, 44336, "...and that\'s where I find the world\'s\r\nbiggest emerald. It\'s really big.", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 456 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 1616 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "emerald" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 18212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28702, 263 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 44503, 45796, "But it\'s cursed.", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28703, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28703, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28703, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28703, 10330 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 47214, 50008, "-So cool!\r\n-God, that is good TV.", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28704, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28704, 2531 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28704, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28704, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28704, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28704, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28704, 403 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 99099, 102019, "-Pheebs, play with me!\r\n-No!", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28705, 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28705, 3127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28705, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28705, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28705, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 102185, 103562, "This game is grotesque.", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28706, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28706, 2469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28706, 50 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "grotesque" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28706, 18213 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 103770, 107691, "Twenty armless guys joined\r\nat the waist by a steel bar...", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 5772 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "armless" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 18214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 388 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "joined" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 18215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 4062 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 7707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28707, 3208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 108025, 111278, "...forced to play soccer forever? Ugh!", 18 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "forced" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28708, 18216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28708, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28708, 3127 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "soccer" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28708, 18217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28708, 3058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28708, 3985 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 111445, 113655, "Hello? Human rights violation.", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28709, 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28709, 8584 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "rights" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28709, 18218 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "violation" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28709, 18219 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 114448, 116700, "Don\'t feel so bad.\r\nAfter they\'re done playing...", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 1674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28710, 1639 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 116867, 121038, "...I break out the plastic women\r\nand everybody has a pretty good time.", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 593 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 3182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 1521 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 1517 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 1485 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28711, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 122372, 124249, "Why don\'t you play with your roommate?", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28712, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28712, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28712, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28712, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28712, 3127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28712, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28712, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28712, 5209 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 124416, 126043, "Eh, he\'s, uh....", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28713, 363 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28713, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28713, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28713, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 126209, 128170, "He\'s not a big fan of foosball.", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28714, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28714, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28714, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28714, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28714, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28714, 4618 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28714, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28714, 17763 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 128337, 131131, "Uh-oh. Ooh. Are we not getting along\r\nwith the new boy?", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 5763 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 585 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28715, 528 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 131298, 135260, "No, he\'s all right. He just, uh,\r\nspends most of his time in his room.", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 10331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 2468 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28716, 213 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 135427, 138055, "Maybe because you haven\'t\r\ntaken time to get to know him.", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 471 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 1749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28717, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 138221, 139806, "Let\'s remedy that, shall we?", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28718, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28718, 2 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "remedy" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28718, 18220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28718, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28718, 6228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28718, 200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 140140, 143060, "-We don\'t need to remedy that.\r\n-Oh, yeah, it\'ll be fun!", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 18220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28719, 2418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 145645, 147564, "-What was that?\r\n-Hi!", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28720, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28720, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28720, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28720, 103 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 147731, 150484, "Um, I thought it\'d be fun\r\nif the three of us had beers...", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 2418 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 1546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28721, 12624 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 150651, 152194, "...and got to know each other.", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28722, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28722, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28722, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28722, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28722, 3072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28722, 1520 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 152361, 155489, "-All right. That sounds all right.\r\n-Oh, good! Okay.", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28723, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 155656, 160202, "Oh, no, I have to go,\r\nbecause I\'m late for my, urn...", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 1710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28724, 470 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 160369, 162454, "<i>...Green Eggs and Ham\r\n</i>discussion group.", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28725, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28725, 3221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28725, 14465 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28725, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28725, 5752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28725, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28725, 10738 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28725, 2449 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 163538, 167542, "Um, tonight, it\'s \"Why he would not\r\neat them on a train.\"", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28726, 8161 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 168043, 170212, "-Have fun! Bye!\r\n-That was so lame.", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28727, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28727, 2418 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28727, 3933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28727, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28727, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28727, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28727, 10268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 170379, 172339, "I know. Yeah, heh.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28728, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28728, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28728, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28728, 360 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 172506, 174800, "-Okay, talk to him.\r\n-Ahem.", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28729, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28729, 364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28729, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28729, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28729, 1669 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 177844, 182974, "So you, uh, think that <i>Speed Racer </i>guy\r\ngets a lot of tickets or...?", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 16190 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "racer" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 18221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 1768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 3082 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28730, 199 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 185060, 187771, "Ha-ha-ha. That\'s good.\r\nSo who broke up with who?", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28731, 247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 187938, 189690, "You kidding? I broke up with her.", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28732, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28732, 546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28732, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28732, 422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28732, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28732, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28732, 39 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 189940, 193527, "She actually thought Sean Penn\r\nwas the capital of Cambodia.", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 567 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "sean" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 18222 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "penn" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 18223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 10771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 69 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "cambodia" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28733, 18224 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 195028, 198156, "When everybody knows that\r\nthe, uh, capital of Cambodia is....", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 1771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 10771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 18224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28734, 50 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 198323, 200117, "Is, uh....", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28735, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28735, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 200283, 202536, "It\'s not Sean Penn.", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28736, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28736, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28736, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28736, 18222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28736, 18223 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 202703, 204996, "All right, I got a funny one. All right?", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 1737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28737, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 205163, 207958, "My last girlfriend, Tilly.\r\nWe\'re eating breakfast, right?", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 3922 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "tilly" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 18225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 6791 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 4665 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28738, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 208166, 210502, "I made these pancakes.\r\nLike, 50 pancakes.", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28739, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28739, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28739, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28739, 8168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28739, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28739, 582 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28739, 8168 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 210752, 213839, "All of a sudden,\r\nshe turns to me and says, \"Eddie...", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28740, 13241 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 214047, 216049, "...I don\'t wanna see you anymore.\"\r\nHa, ha.", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 3880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28741, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 216216, 219094, "It was literally like she had\r\nreached into my chest...", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 13496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 6184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28742, 12343 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 219344, 222681, "...ripped out my heart\r\nand smeared it all over my life!", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 4070 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 1586 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 30 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "smeared" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 18226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28743, 277 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 223849, 226768, "There was this incredible abyss\r\nand I\'m falling.", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 3269 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "abyss" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 18227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28744, 4008 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 227018, 229729, "I keep falling and\r\nI don\'t think I\'m ever gonna stop!", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 4008 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28745, 133 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 235026, 237028, "That wasn\'t such a funny story, was it?", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 1737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 4046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28746, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 242784, 245287, "<i>And a crusty old man\r\nSaid I\'ll do what I can</i>", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "crusty" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 18228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 1787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 1645 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28747, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 245454, 247747, "<i>And the rest of the rats\r\nPlayed maracas</i>", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 2545 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 15097 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 3314 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "maracas" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 18229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28748, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 250417, 252627, "That\'s it! Thanks! Good night.", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28749, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28749, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28749, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28749, 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28749, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28749, 507 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 254379, 256465, "-Phoebe\'s got another job, right?\r\n-Heh.", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28750, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28750, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28750, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28750, 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28750, 543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28750, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28750, 360 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 257841, 259676, "-Great set tonight, Pheebs.\r\n-Yes.", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28751, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28751, 5704 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28751, 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28751, 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28751, 339 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 259843, 260969, "I know.", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28752, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28752, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 261970, 263889, "-We should get going.\r\n-Mm.", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28753, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28753, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28753, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28753, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28753, 423 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 264055, 266850, "We should too.\r\nI\'ve got patients at 8 in the morning.", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 231 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "patients" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 18230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 454 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28754, 504 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 267058, 269311, "You know how we always stay\r\nat your apartment?", 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28755, 5195 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 269561, 271772, "I thought tonight\r\nwe\'d stay at my place.", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28756, 3063 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 272314, 274524, "I don\'t know.\r\nI don\'t have my jammies.", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28757, 4058 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 274691, 275859, "Heh.", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28758, 360 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 276026, 277360, "You don\'t need them.", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28759, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28759, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28759, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28759, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28759, 447 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 277903, 280447, "My baby sister, ladies and gentlemen.", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28760, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28760, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28760, 5220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28760, 4067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28760, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28760, 4068 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 282199, 283283, "Shut up. I\'m happy.", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28761, 509 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28761, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28761, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28761, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28761, 142 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 285368, 288246, "Oh, this is so nice!\r\nI have to make a speech.", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28762, 8969 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 288413, 289790, "Ahem. I just want to say...", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28763, 1669 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28763, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28763, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28763, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28763, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28763, 159 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 289956, 293543, "...of all the guys that Monica\r\nhas been with, and that is a lot...", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 1517 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28764, 163 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 295879, 298590, "-...I like you the best.\r\n-Aw.", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28765, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28765, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28765, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28765, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28765, 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28765, 5684 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 298757, 301426, "Thank you, Pheebs.\r\nThat\'s very sweet.", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28766, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28766, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28766, 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28766, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28766, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28766, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28766, 222 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 301927, 303094, "You hear that?", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28767, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28767, 564 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28767, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 303345, 306848, "She likes me best. And apparently,\r\nthere have been a lot.", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 3274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 1574 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28768, 163 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 308600, 309768, "Not a lot.", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28769, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28769, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28769, 163 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 310018, 312479, "Phoebe\'s kidding. Phoebe\'s crazy.", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28770, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28770, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28770, 546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28770, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28770, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28770, 5758 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 314231, 316024, "Phoebe\'s dead.", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28771, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28771, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28771, 1559 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 321863, 323031, "-Hi.\r\n-Hi.", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28772, 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28772, 103 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 323198, 324741, "I\'m looking for Eddie Manoick.", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28773, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28773, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28773, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28773, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28773, 13241 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "manoick" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28773, 18231 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 324908, 327619, "Oh, uh, he\'s not here right now.\r\nI\'m Chandler.", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28774, 193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 327869, 330705, "Can I take a message or....\r\nOr a fish tank?", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 9965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 8997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28775, 17114 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 330914, 332082, "Thanks.", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28776, 130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 332582, 333959, "Come on in.", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28777, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28777, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28777, 64 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 334334, 335377, "I\'m Tilly.", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28778, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28778, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28778, 18225 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 340507, 341550, "Oh.", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28779, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 341716, 344511, "I gather by that \"Oh,\"\r\nhe told you about me.", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 10 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "gather" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 18232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28780, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 344719, 347556, "Yeah, your, uh, name came up...", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28781, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28781, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28781, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28781, 348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28781, 597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28781, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 347806, 351268, "...in a conversation that\r\nterrified me to my very soul.", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 7308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 77 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "terrified" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 18233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28782, 8635 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 352686, 354062, "He\'s kind of intense, huh?", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28783, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28783, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28783, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28783, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28783, 8197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28783, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 354271, 357065, "Yes! Hey, can I ask you,\r\nis Eddie a little...", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 400 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 13241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28784, 386 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 357274, 359943, "-A little what?\r\n-...bit country?", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28785, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28785, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28785, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28785, 4667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28785, 10715 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 363113, 364406, "Come on in, you roomie!", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28786, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28786, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28786, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28786, 15 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "roomie" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28786, 18234 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 365615, 366908, "Hello, Tilly.", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28787, 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28787, 18225 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 367158, 369578, "Eddie, I just came by\r\nto drop off your tank.", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 13241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 3292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28788, 17114 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 370036, 371955, "That was very thoughtful of you.", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28789, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28789, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28789, 98 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "thoughtful" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28789, 18235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28789, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28789, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 372163, 373707, "It\'s very thoughtful.", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28790, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28790, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28790, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28790, 18235 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 378044, 379588, "Well, okay then.", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28791, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28791, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28791, 79 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 380297, 382257, "I\'m gonna go. Bye.", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28792, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28792, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28792, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28792, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28792, 3933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 382465, 384134, "-Bye.\r\n-Bye.", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28793, 3933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28793, 3933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 386678, 387887, "So we getting a fish?", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28794, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28794, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28794, 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28794, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28794, 8997 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 388513, 390599, "You had sex with her, didn\'t you?", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28795, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28795, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28795, 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28795, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28795, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28795, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28795, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28795, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 393101, 395729, "Pheebs! Check it out, check it out!", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28796, 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28796, 3080 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28796, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28796, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28796, 3080 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28796, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28796, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 395937, 400066, "Oh, ooh, <i>Soap Opera Digest!\r\n</i>That\'s one of my favorite digests!", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 15818 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 15819 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "digest" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 18236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 396 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "digests" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28797, 18237 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 401568, 403862, "-Page 42! Page 42!\r\n-Okay, okay.", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28798, 7211 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "42" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28798, 18238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28798, 7211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28798, 18238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28798, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28798, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 404029, 405905, "Okay. Ooh. Hey!", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28799, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28799, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28799, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 406072, 408950, "\"New Doc on the Block,\r\n<i>Days of our Lives </i>Joey Tribbiani.\"", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 585 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "doc" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 18239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 4574 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 1597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 1739 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28800, 4721 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 409117, 411620, "-Yeah. Yeah.\r\n-Oh, cool picture!", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28801, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28801, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28801, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28801, 2531 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28801, 4526 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 411786, 414039, "-Ah.\r\n-Oh, I look good.", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28802, 3190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28802, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28802, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28802, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28802, 346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 415707, 419002, "Is this true? That you write\r\na lot of your own lines?", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 2532 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 3152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 595 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28803, 9106 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 419669, 421171, "Well, kind of, yeah.", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28804, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28804, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28804, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28804, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 422464, 425342, "Remember last week\r\nwhen Alex was in the accident?", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 411 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 221 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "alex" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 18240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28805, 5740 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 425592, 427385, "The line in the script was:", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28806, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28806, 552 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28806, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28806, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "script" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28806, 18241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28806, 146 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 427594, 431056, "\"If we don\'t get this woman\r\nto a hospital, she\'s going to die.\"", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 431 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 7305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28807, 2353 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 431306, 434309, "But I made it, \"If this woman\r\ndoesn\'t get to a hospital...", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 431 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28808, 7305 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 434517, 436436, "...she\'s not gonna live.\"", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28809, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28809, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28809, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28809, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28809, 320 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 438063, 441524, "Oh, okay.\r\nI see what you did there.", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28810, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28810, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28810, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28810, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28810, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28810, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28810, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28810, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 444027, 448031, "Aren\'t you afraid the writers will be\r\nkind of mad when they read this?", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 3090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 11216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "writers" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 18242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 1600 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 1652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28811, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 448239, 449532, "Huh.", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28812, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 449699, 454079, "I never thought about the writers.\r\nThe scripts just come to my house.", 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 18242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "scripts" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 18243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28813, 3247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 457040, 458416, "But you know what?", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28814, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28814, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28814, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28814, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 458625, 462045, "This makes me look good,\r\nwhich makes the show look good...", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 1500 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28815, 346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 462295, 465966, "...which makes the writers look good.\r\nSo how could they be mad?", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 18242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28816, 1600 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 467467, 469511, "<i>Makes up most of his lines.</i>", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28817, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28817, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28817, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28817, 2468 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28817, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28817, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28817, 9106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28817, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 469719, 470970, "<i>Son of a....</i>", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28818, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28818, 7229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28818, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28818, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28818, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 472764, 475725, "<i>Yeah. Write this, jerk-weed.</i>", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28819, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28819, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28819, 3152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28819, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28819, 6778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28819, 9067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28819, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 475934, 477686, "I fall down an elevator shaft?", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28820, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28820, 1708 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28820, 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28820, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28820, 15877 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "shaft" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28820, 18244 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 477852, 480855, "What does this mean?\r\nI fall down an elevator shaft?", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 1708 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 15877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28821, 18244 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 481064, 484526, "Uh, I don\'t know.\r\nI just bring the scripts.", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 1509 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28822, 18243 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 486069, 489489, "They can\'t kill me!\r\nI\'m Francesca\'s long-lost son!", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 62 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "francesca" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 18245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 1596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28823, 7229 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 489656, 491741, "Right.", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28824, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 492033, 493702, "Uh, could you sign?", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28825, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28825, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28825, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28825, 4566 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 493952, 496871, "No! No way! I\'m not signing that!", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28826, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28826, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28826, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28826, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28826, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28826, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28826, 13488 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28826, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 497038, 500375, "I don\'t think that\'s gonna affect\r\nthe plot of the show.", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 12324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 9105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28827, 1500 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 501584, 503795, "How can they do this to me?", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28828, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28828, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28828, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28828, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28828, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28828, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28828, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 506756, 509134, "All right, uh, I\'m just gonna go.", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28829, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28829, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28829, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28829, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28829, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28829, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28829, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28829, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 509843, 510885, "Sorry.", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28830, 268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 517142, 519227, "Well, it wasn\'t that many guys.", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28831, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28831, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28831, 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28831, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28831, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28831, 3869 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28831, 388 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 519477, 522397, "If you consider how many guys\r\nthere actually are...", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 14476 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 3869 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28832, 109 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 522772, 525150, "-...it\'s a very small percentage.\r\n-Hey.", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28833, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28833, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28833, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28833, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28833, 114 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "percentage" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28833, 18246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28833, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 525316, 528111, "It\'s not that big a deal.\r\nI was just curious.", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 1671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28834, 11500 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 528361, 530029, "-Good night.\r\n-Good night, Richard.", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28835, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28835, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28835, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28835, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28835, 4602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 530280, 532115, "Good luck, Mon.", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28836, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28836, 542 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28836, 1609 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 532323, 536578, "All right, before I tell you, uh, you tell me\r\nhow many women you\'ve been with.", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 3869 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 1521 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28837, 12 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 538788, 543043, "-Two.\r\n-Two? Two? Heh.", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28838, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28838, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28838, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28838, 360 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 543209, 545462, "How is that possible? I mean...", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28839, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28839, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28839, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28839, 2471 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28839, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28839, 239 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 545670, 547756, "...have you seen you?", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28840, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28840, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28840, 519 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28840, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 548923, 551259, "Well, I mean, what can I say?", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28841, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28841, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28841, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28841, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28841, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28841, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28841, 159 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 551468, 555638, "I was married to Barbara for 30 years,\r\nmy high-school sweetheart.", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 10360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 2536 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 476 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28842, 5718 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 555889, 557807, "Now you. That\'s two.", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28843, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28843, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28843, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28843, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28843, 54 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 558516, 560810, "-Two it is. Heh.\r\n-Mm-hm.", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28844, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28844, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28844, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28844, 360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28844, 423 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28844, 634 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 560977, 563313, "Okay, time for bed.\r\nI\'m gonna brush my teeth.", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 556 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 4678 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28845, 413 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 563480, 566024, "Whoa, no, wait a minute now!", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28846, 2366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28846, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28846, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28846, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28846, 335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28846, 165 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 566232, 567984, "Come on, it\'s your turn.", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28847, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28847, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28847, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28847, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28847, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28847, 3214 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 568193, 570028, "Oh, come on!", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28848, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28848, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28848, 14 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 571279, 574991, "You know, I don\'t need the\r\nactual number. Just a ballpark.", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 6182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 3205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "ballpark" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28849, 18247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 576451, 579496, "Okay, it is definitely\r\nless than a ballpark.", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28850, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28850, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28850, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28850, 3253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28850, 3076 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28850, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28850, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28850, 18247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 584292, 586711, "Wow, I am so glad\r\nI\'m not Monica right now.", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28851, 165 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 586961, 588671, "Tell me about it.", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28852, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28852, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28852, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28852, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 588880, 591508, "So, what\'s your magic number?", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28853, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28853, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28853, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28853, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28853, 3191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28853, 3205 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 592884, 594052, "Ann....", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28854, 4621 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 594219, 598598, "Come on, you know everyone\r\nI\'ve been with. All both of them.", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 3067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28855, 447 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 599182, 601518, "Well, ahem, there\'s you.", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28856, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28856, 1669 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28856, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28856, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28856, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 602018, 603770, "Better not be doing these in order.", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28857, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28857, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28857, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28857, 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28857, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28857, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28857, 4649 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 605271, 609025, "Okay, uh, Billy Dreskin, Pete Carny.", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28858, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28858, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28858, 492 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28858, 9935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28858, 8549 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "carny" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28858, 18248 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 609192, 610860, "-Ah.\r\n-Barry.", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28859, 3190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28859, 230 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 611027, 613154, "And, uh, oh, Paolo.", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28860, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28860, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28860, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28860, 5285 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 613321, 616533, "Oh, yes, the weenie from Turinie.", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28861, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28861, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28861, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28861, 15860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28861, 2359 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "turinie" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28861, 18249 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 617867, 621955, "Oh, honey, are you jealous of Paolo?", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28862, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28862, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28862, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28862, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28862, 9540 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28862, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28862, 5285 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 622163, 625792, "I\'m so much happier with you\r\nthan I ever was with him.", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 16203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28863, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 626000, 628044, "-Really?\r\n-Oh, please.", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28864, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28864, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28864, 327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 628294, 631297, "That Paolo thing was\r\nbarely a relationship.", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28865, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28865, 5285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28865, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28865, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28865, 4576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28865, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28865, 3102 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 631631, 635218, "All it really was\r\nwas just meaningless, animal sex.", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 7 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "meaningless" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 18250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 3267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28866, 58 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 639305, 644060, "Okay, you know, that sounded\r\nso much better in my head.", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 11201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28867, 238 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 645270, 647438, "Eddie! I didn\'t sleep with\r\nyour ex-girlfriend.", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 13241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 1533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28868, 3922 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 647605, 650900, "That\'s exactly what someone\r\nwho slept with her would say.", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 614 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 535 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28869, 159 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 651067, 654195, "This is nuts! Crazy!\r\nShe came over for two minutes...", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 449 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 5758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28870, 2355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 654362, 656739, "...dropped off a fish tank and left!\r\nEnd of story.", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 17171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 8997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 17114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 1793 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28871, 4046 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 656906, 659367, "-Where\'s Buddy?\r\n-Buddy?", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28872, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28872, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28872, 2437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28872, 2437 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 659534, 661161, "My fish, Buddy.", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28873, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28873, 8997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28873, 2437 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 662203, 664497, "There was no fish\r\nwhen she dropped it off.", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 8997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 17171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28874, 429 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 664664, 666416, "Oh, this is unbelievable!", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28875, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28875, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28875, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28875, 2347 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 667167, 671171, "You sleep with my ex-girlfriend,\r\ninsult my intelligence by lying...", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 1533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 3922 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "insult" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 18251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 9001 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28876, 10284 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 671421, 673339, "...then you kill my fish?\r\nMy Buddy?", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28877, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28877, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28877, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28877, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28877, 8997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28877, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28877, 2437 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 673756, 675383, "I didn\'t kill your fish!", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28878, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28878, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28878, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28878, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28878, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28878, 8997 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 675633, 677427, "Look, Eddie-", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28879, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28879, 13241 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 680847, 683057, "Would you look at what I\'m doing?", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28880, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28880, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28880, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28880, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28880, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28880, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28880, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28880, 246 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 683558, 685059, "Now that can\'t be smart.", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28881, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28881, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28881, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28881, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28881, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28881, 3920 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 688646, 691649, "So we\'re just gonna\r\ntake this guy right off you...", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28882, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 691858, 694319, "...and put him here in Mr. Pocket.", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28883, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28883, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28883, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28883, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28883, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28883, 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28883, 4587 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 696404, 697739, "Tangelo?", 194 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "tangelo" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28884, 18252 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 707582, 710710, "That\'s it? That\'s the giant number\r\nyou were afraid to tell me?", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 2407 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 3205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 11216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28885, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 710877, 713963, "-Well, yeah.\r\n-Well, that\'s not bad at all.", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 1674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28886, 90 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 714214, 716883, "You had me thinking it was,\r\nlike, a fleet.", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 3930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "fleet" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28887, 18253 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 718343, 719427, "You\'re okay with it?", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28888, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28888, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28888, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28888, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28888, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 719594, 721346, "Oh, honey, I\'m fine.", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28889, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28889, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28889, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28889, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28889, 139 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 721512, 722680, "Oh, yay!", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28890, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28890, 14829 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 726017, 727894, "Okay, about that two.", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28891, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28891, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28891, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28891, 54 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 728311, 729395, "What?", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28892, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 729562, 730605, "All right.", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28893, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28893, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 731481, 732649, "What about my two?", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28894, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28894, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28894, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28894, 54 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 732857, 735485, "It just seems like\r\na really small number.", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28895, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28895, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28895, 3903 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28895, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28895, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28895, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28895, 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28895, 3205 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 735693, 736736, "Right.", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28896, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 737237, 739364, "-And?\r\n-And, well....", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28897, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28897, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28897, 206 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 739572, 742492, "Don\'t you have a lot\r\nof wild oats to sow?", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 3124 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "oats" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 18254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 4 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "sow" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28898, 18255 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 743201, 747497, "Or is that what you\'re doing with me?\r\nOh, my God! Am I an oat?", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 259 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "oat" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28899, 18256 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 748414, 750124, "Honey, you are not an oat.", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28900, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28900, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28900, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28900, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28900, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28900, 18256 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 750375, 754170, "I don\'t know, I guess\r\nI\'m just not an oat guy.", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 18256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28901, 9 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 754379, 756339, "I\'ve only slept with women I love.", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28902, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28902, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28902, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28902, 535 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28902, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28902, 1521 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28902, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28902, 269 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 756965, 758758, "You\'ve only slept with two people.", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28903, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28903, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28903, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28903, 535 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28903, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28903, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28903, 55 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 763721, 765431, "Right.", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28904, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 767892, 769185, "Wow.", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28905, 477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 770061, 771854, "Oh, wow.", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28906, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28906, 477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 772772, 774857, "You know I love you too, right?", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28907, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28907, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28907, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28907, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28907, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28907, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28907, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 775733, 777360, "Now I do.", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28908, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28908, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28908, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 781531, 782991, "Ugh, Ross!", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28909, 3985 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28909, 161 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 783157, 784701, "Ross, please listen to me.", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28910, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28910, 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28910, 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28910, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28910, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 784867, 789080, "Ross, you are so much better for me\r\nthan Paolo ever was.", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 5285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28911, 146 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 789330, 792375, "You care about me, you\'re loving,\r\nyou make me laugh.", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 2561 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 8192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28912, 3291 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 792625, 794919, "If I make you laugh, here\'s an idea.", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 3291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28913, 392 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 795086, 798589, "Invite Paolo to have a romp in the sack,\r\nand I\'ll stand in the corner...", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 4659 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 5285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "romp" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 18257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 9878 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 1488 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28914, 17771 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 798756, 801426, "-...and tell knock-knock jokes.\r\n-Ross! Agh!", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28915, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28915, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28915, 1777 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28915, 1777 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28915, 4658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28915, 161 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "agh" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28915, 18258 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 801592, 804012, "God, Ross!", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28916, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28916, 161 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 804178, 807265, "What you and I have is special!\r\nAll Paolo and I ever had was-", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 1717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 5285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28917, 146 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 807432, 808683, "Animal sex, animal sex?", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28918, 3267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28918, 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28918, 3267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28918, 58 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 812020, 813688, "So, what are you saying?", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28919, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28919, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28919, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28919, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28919, 435 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 813938, 816816, "There\'s nothing between us\r\n\"animal\" at all?", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28920, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28920, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28920, 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28920, 1670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28920, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28920, 3267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28920, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28920, 90 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 817066, 820987, "There\'s not even,\r\nlike, um, a little animal?", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28921, 3267 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 821237, 822822, "Not even, like...", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28922, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28922, 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28922, 60 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 823197, 824449, "...chipmunk sex?", 233 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "chipmunk" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28923, 18259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28923, 58 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 826492, 827952, "Okay, Ross....", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28924, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28924, 161 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 828202, 830955, "Try to hear me, okay?", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28925, 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28925, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28925, 564 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28925, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28925, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 831122, 833291, "I— Hey. I\'m not gonna lie to you.", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 1780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28926, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 833916, 837211, "-It was good with Paolo.\r\n-Knock-knock.", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28927, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28927, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28927, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28927, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28927, 5285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28927, 1777 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28927, 1777 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 839464, 842425, "But what you and I have\r\nis so much better.", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28928, 312 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 842675, 847013, "We have tenderness,\r\nwe have intimacy, we connect.", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28929, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28929, 28 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "tenderness" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28929, 18260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28929, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28929, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28929, 8096 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28929, 200 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "connect" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28929, 18261 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 847221, 848681, "You know? I swear.", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28930, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28930, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28930, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28930, 3122 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 848931, 851017, "This is the best...", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28931, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28931, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28931, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28931, 315 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 851267, 853936, "...I have ever had.", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28932, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28932, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28932, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28932, 76 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 855563, 857523, "-Until now.\r\n-Aah!", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28933, 4630 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28933, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28933, 6718 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 862278, 864906, "-Oh, hi.\r\n-Hi.", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28934, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28934, 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28934, 103 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 865156, 866657, "Richard told me he loved me.", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28935, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28935, 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28935, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28935, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28935, 488 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28935, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 866908, 867992, "-Oh, my God!\r\n-I know!", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28936, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28936, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28936, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28936, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28936, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 868242, 869577, "Honey, that\'s great!", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28937, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28937, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28937, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28937, 505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 869786, 870995, "-I know!\r\n-Ha, ha.", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28938, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28938, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28938, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28938, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 871204, 872914, "-I just can\'t find—\r\n-Top drawer.", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28939, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28939, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28939, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28939, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28939, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28939, 3976 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "drawer" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28939, 18262 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 873122, 875208, "-Hurry.\r\n-Why? You need one too?", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28940, 1544 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28940, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28940, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28940, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28940, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28940, 576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 875375, 877794, "Oh, yeah.", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28941, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28941, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 878252, 880338, "-Found them!\r\n-Whoo.", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28942, 577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28942, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28942, 7311 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 884217, 885259, "There\'s only one.", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28943, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28943, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28943, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28943, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 885843, 888221, "-Monica!\r\n-Hi.", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28944, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28944, 103 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 888388, 892475, "Uh, we\'ll be right there.\r\nWe\'re just trying to decide something.", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 1506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 3089 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28945, 22 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 896104, 897396, "Rachel?", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28946, 180 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 908074, 909909, "-Hey.\r\n-Hey.", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28947, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28947, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 910576, 913955, "-They\'re trying to decide something.\r\n-Good. Good, good.", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 1506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 3089 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28948, 346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 920253, 924465, "So is, urn- So was your mustache-\r\nDid it used to be different?", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 470 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 12013 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 3179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28949, 4051 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 926884, 928719, "-No.\r\n-Oh.", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28950, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28950, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 931556, 934600, "How do you, uh, you know,\r\nkeep it so neat?", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28951, 4069 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 936561, 938688, "-I have a little comb.\r\n-Oh.", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28952, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28952, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28952, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28952, 386 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "comb" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28952, 18263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28952, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 942233, 944068, "-What do you call that?\r\n-A mustache comb.", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28953, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28953, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28953, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28953, 1622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28953, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28953, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28953, 12013 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28953, 18263 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 944235, 945653, "Thank you.", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28954, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28954, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 947029, 950867, "Okay, I will do your laundry\r\nfor one month.", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 3910 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28955, 1755 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 951450, 953244, "-No!\r\n-Okay, okay!", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28956, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28956, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28956, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 953411, 955079, "I will, I will, I will....", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28957, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28957, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28957, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28957, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28957, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28957, 529 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 955288, 957957, "I will clean the apartment\r\nfor two months.", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28958, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28958, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28958, 412 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28958, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28958, 5195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28958, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28958, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28958, 3087 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 958166, 962461, "I\'ll give this to you now, if you\r\ntell me where we keep the dustpan.", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "dustpan" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28959, 18264 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 972221, 973848, "So were you in Nam?", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28960, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28960, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28960, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28960, 64 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "nam" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28960, 18265 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 978477, 980688, "-Rock, paper, scissors?\r\n-Gotcha.", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28961, 8094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28961, 1554 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28961, 9541 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28961, 9453 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 981272, 983024, "One, two, three.", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28962, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28962, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28962, 1546 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 983274, 984859, "-Yes!\r\n-Agh!", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28963, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28963, 18258 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 985026, 988321, "-Ah!\r\n-Fine. Go have sex.", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28964, 3190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28964, 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28964, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28964, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28964, 58 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 990781, 993159, "No! You have got it completely wrong!", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28965, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28965, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28965, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28965, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28965, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28965, 3206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28965, 23 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 993326, 996204, "John Savage was <i>Deer Hunter,\r\n</i>no legs.", 276 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "john" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 18266 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "savage" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 18267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 10 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "deer" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 18268 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hunter" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 18269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28966, 391 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 996454, 999332, "Jon Voight was <i>Coming Home,\r\n</i>couldn\'t feel his legs.", 277 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "jon" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 18270 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "voight" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 18271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 2565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 620 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 3088 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28967, 391 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 999540, 1003711, "No, you\'ve got it totally the other way\r\naround, my friend. Jon Voight-", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 1520 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 1691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 18270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28968, 18271 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1003878, 1005713, "-Honey? Ahem.\r\n-What? Oh.", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28969, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28969, 1669 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28969, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28969, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1014430, 1016807, "-Shall we?\r\n-It\'s not gonna happen.", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28970, 6228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28970, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28970, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28970, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28970, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28970, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28970, 4006 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1017058, 1020311, "They\'re doing it tonight.\r\nWe can do it tomorrow.", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28971, 1583 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1022647, 1026567, "Uh, in the future, if I could see\r\nthe schedule beforehand....", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 2462 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 7733 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28972, 13922 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1030363, 1032156, "So when I woke up this morning...", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28973, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28973, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28973, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28973, 3193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28973, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28973, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28973, 504 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1032365, 1035534, "...he\'d stolen all the insoles\r\nout of my shoes!", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 13249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "insoles" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 18272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28974, 2402 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1035701, 1036827, "Why?", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28975, 148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1036994, 1040873, "He thinks I slept with his\r\nex-girlfriend and killed his fish.", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 5694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 535 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 1533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 3922 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 600 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28976, 8997 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1041123, 1042917, "Why would you kill his fish?", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28977, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28977, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28977, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28977, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28977, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28977, 8997 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1046337, 1049006, "Because sometimes\r\nafter you sleep with someone...", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28978, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28978, 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28978, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28978, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28978, 533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28978, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28978, 112 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1049257, 1051592, "...you have to kill a fish.", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28979, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28979, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28979, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28979, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28979, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28979, 8997 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1053719, 1055137, "Chandler, honey...", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28980, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28980, 2523 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1055388, 1056514, "...I\'m sorry.", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28981, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28981, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28981, 268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1056722, 1059058, "Can we watch Joey\'s show now, please?", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28982, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28982, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28982, 421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28982, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28982, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28982, 1500 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28982, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28982, 327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1059225, 1061102, "-Yeah!\r\n-Wait. He\'s not here yet.", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28983, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28983, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28983, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28983, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28983, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28983, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28983, 573 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1061310, 1063604, "He\'s on the show,\r\nhe knows what happens.", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 1500 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 1771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28984, 5672 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1064814, 1067233, "I\'m fine about my problem now,\r\nby the way.", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 1494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28985, 420 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1070444, 1071696, "<i>Amber...</i>", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28986, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28986, 18205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28986, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1072321, 1074740, "<i>...I want you to know\r\nI\'ll always be there for you...</i>", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28987, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1074949, 1076993, "<i>...as a friend and as your brother.</i>", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 1691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28988, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1077702, 1079245, "<i>Oh, Drake.</i>", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28989, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28989, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28989, 15879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28989, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1079954, 1081247, "<i>Hard day, huh?</i>", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28990, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28990, 594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28990, 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28990, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28990, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1081497, 1084166, "-First the medical award, now this?\r\n-Yeah.", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28991, 596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28991, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28991, 18207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28991, 13910 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28991, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28991, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28991, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1084333, 1087670, "-Some guys are just lucky, I guess. Ha, ha.\r\n-Ha-ha-ha.", 302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 4009 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28992, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1087837, 1089505, "<i>Dr. Ramoray...</i>", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28993, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28993, 1715 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28993, 15880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28993, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1089672, 1092383, "<i>-...report to first floor Emergency stat.\r\n</i>-Hmm.", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 11955 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 3209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 9918 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28994, 623 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1093217, 1094885, "Well, then...", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28995, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28995, 79 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1095678, 1097388, "...I, uh, guess that\'s me.", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28996, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28996, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28996, 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28996, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28996, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28996, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1099849, 1103227, "Anyone else need to go on the elevator?\r\nDr. Horton? Dr. Wong?", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 1537 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 541 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 15877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 1715 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "horton" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 18273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 1715 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "wong" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28997, 18274 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1103394, 1106480, "No, no. They only said you.", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28998, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28998, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28998, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28998, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28998, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28998, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1108733, 1111235, "Oh, okay. All right.", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28999, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28999, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28999, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 28999, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1111402, 1114071, "-I love you, Drake.\r\n-Yeah, whatever.", 310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29000, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29000, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29000, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29000, 15879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29000, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29000, 3309 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 310   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1115489, 1118075, "-Oh, no!\r\n-Drake, look out!", 311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29001, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29001, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29001, 15879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29001, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29001, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 311   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1122705, 1125041, "-Did they just kill off Joey?\r\n-No!", 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29002, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29002, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29002, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29002, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29002, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29002, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29002, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 312   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1127460, 1129170, "Now maybe.", 313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29003, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29003, 288 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 313   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1131464, 1133758, "Come on! Open up!\r\nWe want to talk to you!", 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 2389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29004, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 314   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1133966, 1135426, "I don\'t feel like talking!", 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29005, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29005, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29005, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29005, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29005, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29005, 434 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 315   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1135635, 1137428, "Come on! We care about you.", 316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29006, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29006, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29006, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29006, 2561 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29006, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29006, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 316   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1137595, 1141891, "-We\'re worried about you.\r\n-And some of us have to pee!", 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 2525 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29007, 1680 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 317   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1146062, 1147563, "I\'m sorry, Joey.", 318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29008, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29008, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29008, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29008, 126 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 318   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1149690, 1153152, "Listen, sorry about your death.\r\nThat really sucks.", 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29009, 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29009, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29009, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29009, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29009, 3163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29009, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29009, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29009, 617 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 319   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1153819, 1157156, "-We came over as soon as we saw.\r\n-How could you not tell us?", 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 5247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29010, 198 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 320   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1157365, 1161118, "I don\'t know. I was kind of hoping\r\nno one would ever find out.", 321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29011, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 321   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1162578, 1165331, "Well, maybe they could find\r\na way to bring you back.", 322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 1509 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29012, 63 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 322   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1165498, 1171003, "No, they said that when they found\r\nmy body, my brain was so smashed in...", 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 2527 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 7309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 479 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29013, 64 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 323   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1171253, 1174840, "...that the only doctor\r\nwho could\'ve saved me was me.", 324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 5661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 3974 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29014, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 324   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1176592, 1178886, "It\'s supposed to be\r\nsome kind of irony.", 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29015, 11221 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 325   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1180846, 1182556, "But Joey, you\'re gonna be fine.", 326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29016, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29016, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29016, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29016, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29016, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29016, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29016, 139 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 326   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1182765, 1186185, "You don\'t need that show.\r\nIt was just a dumb soap opera.", 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 1500 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 9434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 15818 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29017, 15819 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 327   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1186394, 1189563, "This was the greatest thing\r\nthat ever happened to me.", 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 5745 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 452 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29018, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 328   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1189772, 1192400, "Yes! I was going to incorporate that.", 329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29019, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29019, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29019, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29019, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29019, 4 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "incorporate" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29019, 18275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29019, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 329   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1193901, 1196987, "Oh, good, here\'s Monica.\r\nShe\'ll have something nice to say.", 330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29020, 159 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 330   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1198406, 1202076, "Um, I straightened out your shower curtain\r\nso you won\'t get mildew.", 331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 10 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "straightened" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 18276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 4654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "curtain" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 18277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 128 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "mildew" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29021, 18278 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 331   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1203452, 1205121, "What? To me, that\'s nice.", 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29022, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29022, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29022, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29022, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29022, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29022, 299 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 332   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1206580, 1208499, "It\'s gonna be okay. You know that.", 333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29023, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29023, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29023, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29023, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29023, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29023, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29023, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29023, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 333   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1209083, 1210751, "No, I don\'t.", 334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29024, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29024, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29024, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29024, 37 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 334   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1212044, 1215339, "It\'s like you work\r\nyour whole life for something...", 335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 592 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29025, 22 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 335   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1215589, 1217716, "...and you think\r\nthat when you get it...", 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29026, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29026, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29026, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29026, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29026, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29026, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29026, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29026, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 336   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1217967, 1220302, "...it\'ll never be\r\nas good as you thought.", 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29027, 567 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 337   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1220511, 1222263, "But this so was.", 338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29028, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29028, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29028, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29028, 146 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 338   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1222638, 1224223, "It changed everything.", 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29029, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29029, 8210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29029, 1482 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 339   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1224432, 1227643, "Like, the other day,\r\nI got this credit card application...", 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 1520 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 547 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 588 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "application" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29030, 18279 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 340   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1227893, 1230187, "...and I was pre-approved!", 341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29031, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29031, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29031, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29031, 9014 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "approved" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29031, 18280 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 341   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1231731, 1236110, "Huh? I\'ve never been pre-approved\r\nfor anything in my life!", 342 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 9014 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 18280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29032, 277 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 342   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1236318, 1237862, "I\'m sorry, man.", 343 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29033, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29033, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29033, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29033, 1645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 343   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1238070, 1241365, "I don\'t know if this\'ll mean\r\nanything to you...", 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29034, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 344   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1241574, 1244201, "...but you\'ll always be\r\npre-approved with us.", 345 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 9014 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 18280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29035, 198 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 345   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1245494, 1247163, "No, that means nothing to me.", 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29036, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29036, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29036, 2332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29036, 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29036, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29036, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 346   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1251500, 1253002, "Ann!", 347 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29037, 4621 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 347   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1254378, 1256255, "Pecan sandy? Just made them.", 348 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "pecan" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29038, 18281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29038, 6770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29038, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29038, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29038, 447 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 348   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1257882, 1259300, "Yeah, all right.", 349 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29039, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29039, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29039, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 349   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1260968, 1262595, "Are these raisins?", 350 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29040, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29040, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29040, 16466 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 350   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1262761, 1264221, "Uh, sure, why not?", 351 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29041, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29041, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29041, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29041, 51 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 351   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1271645, 1273814, "Listen, Eddie. Um, ahem.", 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29042, 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29042, 13241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29042, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29042, 1669 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 352   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1274565, 1277860, "I\'ve been thinking about\r\nour living situation and, uh....", 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 3930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 605 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 1718 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29043, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 353   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1278027, 1279195, "Why are you smiling?", 354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29044, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29044, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29044, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29044, 534 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 354   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1279361, 1282781, "I got a little surprise.\r\nLook. There\'s a new fishy.", 355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 5258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 585 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "fishy" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29045, 18282 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 355   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1283532, 1284867, "I named him, uh...", 356 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29046, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29046, 1672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29046, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29046, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 356   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1285075, 1287620, "...Chandler, you know, after you.", 357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29047, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29047, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29047, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29047, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29047, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 357   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1296796, 1299548, "Well, that\'s not even a real fish.", 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29048, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29048, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29048, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29048, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29048, 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29048, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29048, 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29048, 8997 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 358   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1300841, 1303302, "No, that\'s a Goldfish cracker.", 359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29049, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29049, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29049, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29049, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "goldfish" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29049, 18283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29049, 5749 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 359   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1305179, 1306847, "So, what\'s your point, man?", 360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29050, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29050, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29050, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29050, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29050, 3200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29050, 1645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 360   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1311018, 1312895, "Okay, good night.", 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29051, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29051, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29051, 507 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 361   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1313813, 1316232, "You big freak of nature.", 362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29052, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29052, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29052, 3294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29052, 69 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "nature" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29052, 18284 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 362   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1332331, 1334208, "-Aah.\r\n-Hey.", 363 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29053, 6718 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29053, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 363   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1338629, 1339672, "Hey.", 364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29054, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 364   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1339964, 1341715, "-Hey.\r\n-Hey.", 365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29055, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29055, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 365   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1341882, 1345219, "-Whoo. Brisk tonight.\r\n-Oh, man.", 366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29056, 7311 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "brisk" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29056, 18285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29056, 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29056, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29056, 1645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 366   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 83, 1346262, 1348764, "-Let\'s never speak of this.\r\n-You got it.", 367 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 1684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29057, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/English.srt" ,  `subtitle`.`phrase_number` = 367   WHERE  `subtitle`.`id` = 83 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 42, 83 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 42, 2, "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1830, 4830, "- О, Дрейк.\r\n- Извини, Эмбер.", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29058, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29058, 16260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29058, 1447 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "эмбер" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29058, 18286 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 5380, 8720, "Похоже, последнее слово\r\nосталось за Брэдом", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29059, 1899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29059, 9790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29059, 2907 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29059, 4513 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29059, 870 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "брэдом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29059, 18287 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 8970, 10470, "Извините, опоздал. Что было?", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29060, 1042 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29060, 9668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29060, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29060, 742 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 10720, 12060, "Мы хотим досмотреть конец", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29061, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29061, 4916 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "досмотреть" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29061, 18288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29061, 3044 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 12310, 13560, "Ты нужен мне, Дрейк", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29062, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29062, 4461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29062, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29062, 16260 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 13810, 14980, "Я это знаю", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29063, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29063, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29063, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 15230, 17730, "Но мы не сможем быть парой", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29064, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29064, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29064, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29064, 3542 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29064, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29064, 5901 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 17980, 18810, "Что?", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29065, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 19110, 21190, "Я тебе не говорил", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29066, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29066, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29066, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29066, 4806 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 21440, 25740, "На самом деле\r\nя твой сводный брат", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29067, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29067, 1979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29067, 1980 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29067, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29067, 1330 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сводный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29067, 18289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29067, 1455 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 29570, 31160, "И что будет дальше?", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29068, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29068, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29068, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29068, 3632 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 31410, 34750, "Я получаю профессиональную премию\r\nза разделение сиамских близнецов", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29069, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29069, 16799 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "профессиональную" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29069, 18290 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "премию" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29069, 18291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29069, 870 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разделение" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29069, 18292 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сиамских" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29069, 18293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29069, 10183 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 35000, 40250, "Потом мы с Эмбер едем в Венесуэлу\r\nк нашему сводному брату... Рамону", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 18286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 7036 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "венесуэлу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 18294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 1187 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сводному" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 18295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 12887 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рамону" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29070, 18296 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 40500, 44260, "...и там я нахожу крупнейший\r\nв мире изумруд. Он огромный...", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нахожу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 18297 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "крупнейший" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 18298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 3319 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "изумруд" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 18299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29071, 2718 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 44510, 46840, "...но на нем лежит проклятие", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29072, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29072, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29072, 1321 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "лежит" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29072, 18300 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "проклятие" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29072, 18301 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 47090, 47840, "Классно!", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29073, 3818 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 48090, 51510, "Господи, вот это сериал!", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29074, 1121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29074, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29074, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сериал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29074, 18302 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 52260, 57440, "\"Смерть доктора Раморе\"\r\n(сезон 2, серия 18)", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29075, 2635 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29075, 10041 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "раморе" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29075, 18303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29075, 1849 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29075, 1850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29075, 1851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29075, 10398 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 99060, 102020, "- Фибс, сыграй со мной!\r\n- Нет!", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29076, 2577 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сыграй" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29076, 18304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29076, 676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29076, 1368 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29076, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 102150, 103480, "Это абсурдная игра!", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29077, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "абсурдная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29077, 18305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29077, 2848 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 103730, 107740, "Двадцать мужиков без рук\r\nнанизаны на стальной прут...", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29078, 6384 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29078, 3650 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29078, 841 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29078, 5565 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нанизаны" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29078, 18306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29078, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стальной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29078, 18307 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прут" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29078, 18308 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 107990, 111240, "...и обречены на вечный футбол?", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29079, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обречены" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29079, 18309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29079, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вечный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29079, 18310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29079, 5970 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 111490, 114160, "Это же нарушение прав человека!", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29080, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29080, 778 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нарушение" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29080, 18311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29080, 3769 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29080, 1014 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 114410, 116580, "Зачем так сразу. После игры...", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29081, 909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29081, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29081, 1124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29081, 1824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29081, 3045 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 116830, 122330, "...я кидаю им пластмассовых баб,\r\nи всем становится весело", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кидаю" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 18312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 1921 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пластмассовых" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 18313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 1862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 1444 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 1968 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29082, 5885 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 122330, 126090, "Почему ты не играешь с соседом?", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29083, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29083, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29083, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29083, 4811 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29083, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29083, 6837 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 126170, 128090, "Он не очень-то любит играть", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29084, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29084, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29084, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29084, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29084, 1236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29084, 3572 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 128340, 131010, "У нас не складывается\r\nс новым парнем?", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29085, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29085, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29085, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29085, 9415 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29085, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29085, 7133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29085, 4394 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 131260, 135180, "Да нет, он нормальный.\r\nПросто сидит в своей комнате", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 13717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 4777 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 2094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29086, 2095 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 135430, 137930, "Просто ты с ним не подружился", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29087, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29087, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29087, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29087, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29087, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подружился" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29087, 18314 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 138180, 139940, "Давай это поправим, а?", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29088, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29088, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поправим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29088, 18315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29088, 666 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 140190, 143690, "- Да не нужно.\r\n- Давай, будет классно!", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29089, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29089, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29089, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29089, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29089, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29089, 3818 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 145610, 147940, "- Что это было?\r\n- Привет!", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29090, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29090, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29090, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29090, 729 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 148280, 152410, "Было бы неплохо взять пива\r\nи познакомиться поближе", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29091, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29091, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29091, 1887 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29091, 1141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29091, 1167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29091, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29091, 8276 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поближе" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29091, 18316 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 152700, 153530, "Ладно", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29092, 1037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 153740, 155410, "Вот и отлично!", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29093, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29093, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29093, 1422 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 155620, 160080, "Ой! Мне надо бежать,\r\nя опаздываю...", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29094, 1217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29094, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29094, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29094, 912 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29094, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29094, 5094 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 160370, 163960, "...на собрание дискуссионного\r\nклуба \"Яичница с ветчиной\"", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29095, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29095, 9671 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дискуссионного" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29095, 18317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29095, 13729 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "яичница" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29095, 18318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29095, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ветчиной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29095, 18319 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 164380, 167710, "Сегодня тема\r\n\"Почему ее не едят в поезде\"", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29096, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29096, 15699 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29096, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29096, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29096, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "едят" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29096, 18320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29096, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29096, 17832 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 168000, 170130, "- Счастливо!\r\n- Какая подлянка.", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29097, 7664 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29097, 1353 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подлянка" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29097, 18321 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 170380, 172300, "Я такая", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29098, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29098, 2616 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 172550, 175470, "Поговори с ним", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29099, 9970 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29099, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29099, 649 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 177810, 184480, "Так ты думаешь, у этого \"Гонщика\"\r\nмного билетов или...?", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 1209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 1220 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гонщика" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 18322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 2200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 14387 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29100, 862 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 185900, 188230, "Отлично. Так кто кого бросил?", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29101, 1422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29101, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29101, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29101, 918 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29101, 3618 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 188480, 189650, "Я ее бросил", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29102, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29102, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29102, 3618 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 189900, 194570, "Она думала, что Шон Пенн -\r\nэто столица Камбоджи", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29103, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29103, 1453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29103, 643 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шон" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29103, 18323 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пенн" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29103, 18324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29103, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "столица" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29103, 18325 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "камбоджи" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29103, 18326 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 194820, 199660, "Когда все знают, что столица\r\nКамбоджи...", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29104, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29104, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29104, 2126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29104, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29104, 18325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29104, 18326 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 200250, 202580, "Не Шон Пенн", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29105, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29105, 18323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29105, 18324 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 202830, 204080, "Вот у меня смешно", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29106, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29106, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29106, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29106, 4466 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 204330, 207840, "Моя последняя девушка, Тилли.\r\nМы завтракаем, так?", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29107, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29107, 5513 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29107, 1958 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тилли" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29107, 18327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29107, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "завтракаем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29107, 18328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29107, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 208090, 210420, "Я напек блинов. Штук 50", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29108, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "напек" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29108, 18329 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "блинов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29108, 18330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29108, 8046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29108, 6273 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 210670, 213760, "И вдруг она поворачивается ко мне\r\nи говорит: \"Эдди...", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 702 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 771 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поворачивается" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 18331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 1070 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 4254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29109, 13355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 214010, 215930, "...я не хочу тебя больше видеть.\"", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29110, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29110, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29110, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29110, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29110, 888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29110, 6609 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 216180, 219010, "Она как будто вспорола мне грудь...", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29111, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29111, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29111, 739 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вспорола" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29111, 18332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29111, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29111, 8251 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 219260, 223520, "...вырвала сердце и размазала его!", 55 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вырвала" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29112, 18333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29112, 2954 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29112, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29112, 16036 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29112, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 223770, 226690, "Огромная бездна, и я падаю", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29113, 8009 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бездна" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29113, 18334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29113, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29113, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29113, 4377 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 226940, 232190, "Все падаю и падаю без остановки!", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29114, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29114, 4377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29114, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29114, 4377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29114, 841 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29114, 3490 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 235030, 238530, "Не такая уж смешная история", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29115, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29115, 2616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29115, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29115, 16612 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29115, 3793 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 242700, 245120, "\"А сухой старикан сказал:\r\nсделаю, что смогу\"", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29116, 666 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сухой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29116, 18335 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "старикан" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29116, 18336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29116, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29116, 4518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29116, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29116, 3411 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 245370, 249210, "\"А остальные крысы играли\r\nна маракасах\"", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29117, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29117, 12422 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "крысы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29117, 18337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29117, 2070 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29117, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "маракасах" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29117, 18338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 250380, 254130, "Вот и все! Спасибо!\r\nПриятного вечера", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29118, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29118, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29118, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29118, 756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29118, 16051 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29118, 7523 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 254300, 257550, "У Фиби есть на что жить, а?", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29119, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29119, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29119, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29119, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29119, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29119, 744 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29119, 666 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 257800, 259470, "Отличное выступление, Фибс", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29120, 3447 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выступление" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29120, 18339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29120, 2577 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 259720, 261640, "Сама знаю", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29121, 791 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29121, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 261890, 263730, "Нам надо идти", 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29122, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29122, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29122, 1955 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 263980, 266730, "И нам. У меня пациенты в 8 утра", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29123, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29123, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29123, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29123, 711 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пациенты" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29123, 18340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29123, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29123, 1184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29123, 7534 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 266980, 269230, "Знаешь, мы всегда остаемся у тебя", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29124, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29124, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29124, 706 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "остаемся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29124, 18341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29124, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29124, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 269480, 271980, "Может, сегодня побудем у меня?", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29125, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29125, 749 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "побудем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29125, 18342 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29125, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29125, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 272230, 275990, "Ну не знаю. Я пижаму забыл", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29126, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29126, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29126, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29126, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пижаму" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29126, 18343 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29126, 1239 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 275990, 277570, "Обойдешься", 70 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обойдешься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29127, 18344 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 277820, 281910, "Моя сестричка, дамы и господа", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29128, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29128, 5402 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29128, 4509 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29128, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29128, 4510 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 282120, 284750, "Заткнись. Я счастлива", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29129, 17404 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29129, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29129, 773 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 285330, 288580, "Ой как здорово! Я скажу речь", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29130, 1217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29130, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29130, 1261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29130, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29130, 2072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29130, 3333 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 288830, 295010, "Из всех парней, которые были\r\nу Моники, а их было ого-го...", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 2833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 2834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 2098 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 1935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 4169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29131, 12854 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 295840, 298430, "...ты мне нравишься больше всех", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29132, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29132, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29132, 12770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29132, 888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29132, 2833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 298680, 301640, "Спасибо, Фибс. Очень приятно", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29133, 756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29133, 2577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29133, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29133, 2956 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 301850, 303010, "Слышала?", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29134, 9389 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 303270, 308350, "Я - лучший. А по-видимому,\r\nих было немало", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29135, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29135, 2850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29135, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29135, 819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29135, 11722 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29135, 1935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29135, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29135, 13166 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 308520, 309690, "Не так уж много", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29136, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29136, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29136, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29136, 2200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 309980, 313940, "Фиби шутит. Она чокнутая", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29137, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29137, 12366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29137, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29137, 12726 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 314150, 317530, "Фиби - труп", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29138, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29138, 5500 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 323120, 325040, "Мне нужен Эдди Маноик", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29139, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29139, 4461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29139, 13355 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "маноик" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29139, 18345 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 325330, 327540, "Его сейчас нет. Я Чендлер", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29140, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29140, 752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29140, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29140, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29140, 2148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 327790, 330630, "Передать что-нибудь?\r\nНапример, аквариум?", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29141, 4128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29141, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29141, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29141, 2211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29141, 18109 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 330830, 332290, "Спасибо", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29142, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 332540, 334050, "Заходите", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29143, 8419 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 334250, 336880, "Меня зовут Тилли", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29144, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29144, 1045 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29144, 18327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 341800, 344390, "Из этого \"О\" я поняла,\r\nчто он обо мне рассказывал", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 885 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 5798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 695 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рассказывал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29145, 18346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 344640, 347480, "Да, твое имя упоминалось...", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29146, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29146, 4249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29146, 2212 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "упоминалось" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29146, 18347 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 347730, 352400, "...в разговоре, который\r\nперепугал меня до смерти", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29147, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разговоре" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29147, 18348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29147, 1024 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "перепугал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29147, 18349 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29147, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29147, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29147, 5851 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 352650, 353980, "Он довольно темпераментный", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29148, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29148, 8894 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "темпераментный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29148, 18350 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 354230, 356990, "Ага! Можно тебя спросить,\r\nЭдди случайно не...", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29149, 2880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29149, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29149, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29149, 1457 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29149, 13355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29149, 9568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29149, 672 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 357240, 358070, "\"Случайно не\" что?", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29150, 9568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29150, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29150, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 358320, 361410, "...поет кантри?", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29151, 14608 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кантри" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29151, 18351 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 362990, 365330, "Заходи, соседушка!", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29152, 1001 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "соседушка" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29152, 18352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 365580, 366830, "Привет, Тилли", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29153, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29153, 18327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 367080, 369750, "Эдди, я просто занесла аквариум", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29154, 13355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29154, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29154, 646 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "занесла" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29154, 18353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29154, 18109 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 370000, 371830, "Очень предусмотрительно", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29155, 723 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "предусмотрительно" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29155, 18354 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 372080, 375170, "Просто очень предусмотрительно", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29156, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29156, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29156, 18354 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 378010, 380010, "Ну тогда хорошо", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29157, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29157, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29157, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 380260, 383760, "- Я пойду. Пока.\r\n- Пока. Пока!", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29158, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29158, 2300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29158, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29158, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29158, 1859 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 386600, 388270, "Так у нас теперь рыбка?", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29159, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29159, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29159, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29159, 810 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рыбка" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29159, 18355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 388520, 391270, "Ты с ней трахался", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29160, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29160, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29160, 2016 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "трахался" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29160, 18356 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 394020, 395610, "Зацени, зацени!", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29161, 3365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29161, 3365 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 395860, 401280, "\"Дайджест мыльных опер\"!\r\nЭто мои любимые оперы!", 105 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дайджест" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29162, 18357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29162, 15982 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "опер" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29162, 18358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29162, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29162, 1097 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "любимые" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29162, 18359 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "оперы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29162, 18360 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 401530, 405530, "Страница 42! Страница 42!", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29163, 7410 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29163, 11900 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29163, 7410 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29163, 11900 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 406030, 409790, "\"Новое лицо, новая звезда.\r\n\'Дни нашей жизни\' - Джоуи Трибиани.\"", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 4921 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 6339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 9996 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "звезда" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 18361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 16057 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 848 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 1160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29164, 5377 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 410040, 412040, "Классная фотка!", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29165, 2015 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29165, 8822 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 412290, 415540, "Да, я красивый", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29166, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29166, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29166, 7519 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 415630, 419380, "Это правда? Ты сам пишешь\r\nбольшинство своих реплик?", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29167, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29167, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29167, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29167, 1039 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29167, 15278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29167, 5106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29167, 2747 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "реплик" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29167, 18362 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 419630, 422130, "Ну, типа того", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29168, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29168, 1809 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29168, 1151 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 422380, 425300, "Помнишь, на той неделе\r\nАлекс попала в аварию?", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29169, 859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29169, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29169, 3491 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29169, 7104 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "алекс" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29169, 18363 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29169, 4846 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29169, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29169, 16238 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 425550, 427310, "В сценарии было...", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29170, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сценарии" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29170, 18364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29170, 742 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 427560, 430980, "...\"Если не доставить ее\r\nв больницу, она умрет.\"", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29171, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29171, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "доставить" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29171, 18365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29171, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29171, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29171, 7600 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29171, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29171, 12795 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 431230, 434230, "А я переделал: \"Если она\r\nне попадет в больницу...", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "переделал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 18366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "попадет" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 18367 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29172, 7600 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 434480, 437900, "...то ей не жить.\"", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29173, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29173, 840 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29173, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29173, 744 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 437980, 442990, "А, поняла. Большую работу\r\nты проделал", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29174, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29174, 885 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29174, 6625 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29174, 1318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29174, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29174, 2021 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 443990, 449490, "Не боишься, что сценаристы\r\nрассердятся, прочитав это?", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29175, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29175, 13826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29175, 643 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сценаристы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29175, 18368 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рассердятся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29175, 18369 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прочитав" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29175, 18370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29175, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 449740, 455580, "Я не задумывался о сценаристах.\r\nСценарии просто приносят на дом", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "задумывался" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 18371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 825 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сценаристах" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 18372 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 18364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 646 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "приносят" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 18373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29176, 5568 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 457000, 458340, "Знаешь что?", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29177, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29177, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 458590, 462010, "Это улучшает мою репутацию,\r\nа значит, репутацию сериала...", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29178, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "улучшает" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29178, 18374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29178, 760 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "репутацию" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29178, 18375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29178, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29178, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29178, 18375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29178, 17799 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 462260, 467180, "...и репутацию сценаристов тоже.\r\nНа что им сердиться?", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29179, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29179, 18375 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сценаристов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29179, 18376 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29179, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29179, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29179, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29179, 1921 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сердиться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29179, 18377 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 467430, 469430, "Пишет большинство реплик", 123 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пишет" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29180, 18378 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29180, 5106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29180, 18362 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 469680, 472430, "Сукин с...", 124 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сукин" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29181, 18379 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29181, 648 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 473850, 475600, "Попиши теперь у меня, кретин", 125 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "попиши" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29182, 18380 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29182, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29182, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29182, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29182, 14722 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 475850, 477610, "Я падаю в шахту лифта?", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29183, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29183, 4377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29183, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шахту" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29183, 18381 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "лифта" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29183, 18382 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 477860, 480780, "То есть как это?\r\nЯ падаю в шахту лифта?", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 4377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 18381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29184, 18382 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 481030, 486030, "Не знаю. Я просто разношу сценарии", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29185, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29185, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29185, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29185, 646 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разношу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29185, 18383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29185, 18364 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 486030, 489530, "Меня нельзя убивать!\r\nЯ - потерянный сын Франчески!", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29186, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29186, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29186, 18183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29186, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "потерянный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29186, 18384 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29186, 7656 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "франчески" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29186, 18385 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 489780, 492370, "Ага", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29187, 2880 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 492620, 493620, "Подпишете?", 131 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подпишете" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29188, 18386 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 493870, 496790, "Нет! Ни за что! Не подпишу!", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29189, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29189, 3824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29189, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29189, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29189, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подпишу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29189, 18387 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 497040, 501300, "Вряд ли это повлияет\r\nна развитие сюжета", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29190, 2764 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29190, 1234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29190, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "повлияет" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29190, 18388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29190, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "развитие" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29190, 18389 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сюжета" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29190, 18390 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 501550, 505300, "Как они могут так со мной?", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29191, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29191, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29191, 959 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29191, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29191, 676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29191, 1368 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 506720, 509550, "Ладно, я пойду", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29192, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29192, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29192, 2300 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 509810, 512390, "Извините", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29193, 1042 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 517060, 519150, "Ну, не так уж много было парней", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29194, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29194, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29194, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29194, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29194, 2200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29194, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29194, 2834 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 519400, 522480, "По сравнению с общим количеством\r\nмужчин...", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29195, 819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29195, 8700 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29195, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "общим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29195, 18391 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "количеством" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29195, 18392 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29195, 7388 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 522730, 525070, "...процент очень маленький", 139 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "процент" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29196, 18393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29196, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29196, 4258 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 525320, 528070, "Это не так уж важно.\r\nПростое любопытство", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29197, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29197, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29197, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29197, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29197, 935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29197, 13364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29197, 5522 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 528320, 529990, "- Спокойной ночи.\r\n- Спокойной ночи, Ричард.", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29198, 1445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29198, 1446 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29198, 1445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29198, 1446 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29198, 4917 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 530240, 531990, "Держись, Мон", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29199, 9341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29199, 5167 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 532240, 538080, "Прежде чем я скажу, скажи,\r\nсколько у тебя было женщин", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 9659 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 2072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 1164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 1132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29200, 3773 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 538750, 539750, "Две", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29201, 6005 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 540000, 543000, "Две? ДВЕ?", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29202, 6005 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29202, 6005 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 543260, 545340, "Как это может быть? В смысле...", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29203, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29203, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29203, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29203, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29203, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29203, 1932 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 545590, 549260, "...ты сам себя видел?", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29204, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29204, 1039 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29204, 947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29204, 4265 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 549850, 551180, "А что тут сказать?", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29205, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29205, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29205, 644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29205, 1212 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 551390, 555560, "Я 30 лет был женат на Барбаре,\r\nмоей школьной пассии", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 2974 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 2968 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 1182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "барбаре" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 18394 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 2056 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "школьной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 18395 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пассии" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29206, 18396 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 555850, 558190, "А теперь ты. Получается две", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29207, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29207, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29207, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29207, 1215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29207, 6005 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 558440, 560730, "Две так две", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29208, 6005 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29208, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29208, 6005 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 560940, 564030, "Пора в постель. Пойду почищу зубы", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29209, 1273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29209, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29209, 5607 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29209, 2300 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "почищу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29209, 18397 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "зубы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29209, 18398 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 564320, 565940, "Нет, минутку!", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29210, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29210, 2206 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 566190, 567900, "Давай, твоя очередь", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29211, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29211, 1879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29211, 5436 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 568110, 570950, "Ну давай!", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29212, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29212, 1427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 571200, 576120, "Мне не нужна точная цифра.\r\nХотя бы примерно", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29213, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29213, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29213, 955 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "точная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29213, 18399 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "цифра" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29213, 18400 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29213, 2078 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29213, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29213, 1873 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 576370, 581000, "Точно меньше, чем \"примерно\"", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29214, 2128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29214, 2290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29214, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29214, 1873 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 584300, 586670, "Как я рада,\r\nчто я сейчас не Моника", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29215, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29215, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29215, 5827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29215, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29215, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29215, 752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29215, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29215, 826 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 586880, 588550, "И не говори", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29216, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29216, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29216, 3041 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 588800, 592970, "Как насчет твоих\r\nзарубок на прикладе?", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29217, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29217, 2157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29217, 970 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "зарубок" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29217, 18401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29217, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прикладе" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29217, 18402 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 594220, 598890, "Да ладно, ты уже знаешь всех,\r\nс кем я был. Всех двоих", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 2833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 2144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 2833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29218, 9300 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 599140, 601730, "Ну, есть ты", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29219, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29219, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29219, 654 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 601980, 605230, "Лучше по порядку", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29220, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29220, 819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29220, 3569 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 606150, 609570, "Билли Дрескин, Пит Карни", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29221, 10163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29221, 10164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29221, 8703 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29221, 8702 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 609780, 611990, "Барри", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29222, 893 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 612280, 613200, "Паоло", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29223, 5558 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 613410, 618000, "Ах да, \"туринская плакса...ница\"", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29224, 4118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29224, 652 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "туринская" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29224, 18403 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29224, 8704 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ница" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29224, 18404 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 618330, 621830, "Милый, ты ревнуешь к Паоло?", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29225, 4138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29225, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ревнуешь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29225, 18405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29225, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29225, 5558 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 622130, 625710, "Я намного счастливее с тобой,\r\nчем была с ним", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 1969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 16427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 735 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29226, 649 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 625920, 628010, "- Правда?\r\n- О чём речь!", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29227, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29227, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29227, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29227, 3333 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 628220, 631340, "С Паоло была вообще не любовь,\r\nа непонятно что", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 5558 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 5455 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 666 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "непонятно" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 18406 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29228, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 631590, 636720, "Это был не более чем\r\nгрубый животный секс", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29229, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29229, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29229, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29229, 1346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29229, 890 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "грубый" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29229, 18407 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "животный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29229, 18408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29229, 1845 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 639270, 644940, "Да, у меня в голове\r\nэто звучало лучше", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29230, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29230, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29230, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29230, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29230, 906 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29230, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "звучало" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29230, 18409 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29230, 991 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 645190, 647360, "Я не спал с твоей бывшей подружкой", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29231, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29231, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29231, 3771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29231, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29231, 1964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29231, 3402 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29231, 8382 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 647610, 650950, "Именно это сказал бы тот,\r\nкто переспал с ней", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 1805 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 1023 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 925 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29232, 2016 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 651150, 654280, "Идиотизм! Безумие!\r\nОна зашла на две минуты...", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29233, 17776 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29233, 10050 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29233, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29233, 830 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29233, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29233, 6005 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29233, 6015 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 654530, 656790, "...отдала аквариум и ушла! Всё!", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29234, 10100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29234, 18109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29234, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29234, 751 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29234, 681 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 657040, 658200, "Где Бадди?", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29235, 875 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бадди" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29235, 18410 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 658450, 659450, "Бадди?", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29236, 18410 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 659700, 661960, "Моя рыбка, Бадди", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29237, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29237, 18355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29237, 18410 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 662210, 664460, "Там не было рыбки", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29238, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29238, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29238, 742 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рыбки" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29238, 18411 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 664750, 666920, "С ума сойти!", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29239, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29239, 6035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29239, 12483 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 667170, 671170, "Трахнул мою бывшую,\r\nоскорбил мои чувства ложью...", 183 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "трахнул" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29240, 18412 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29240, 760 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бывшую" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29240, 18413 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "оскорбил" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29240, 18414 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29240, 1097 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29240, 1315 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ложью" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29240, 18415 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 671420, 673510, "...еще и рыбку убил?\r\nМоего Бадди?", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29241, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29241, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рыбку" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29241, 18416 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29241, 2982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29241, 856 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29241, 18410 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 673760, 675340, "Не убивал я твою рыбку!", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29242, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "убивал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29242, 18417 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29242, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29242, 7425 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29242, 18416 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 675590, 678930, "Слушай, Эдди...", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29243, 2085 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29243, 13355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 680850, 683270, "Я правильно понял твой взгляд?", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29244, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29244, 5133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29244, 903 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29244, 1330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29244, 4098 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 683520, 686610, "То есть не надо было так", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29245, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29245, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29245, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29245, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29245, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29245, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 688610, 691610, "Сейчас мы ее уберем с плеча...", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29246, 752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29246, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29246, 926 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "уберем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29246, 18418 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29246, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29246, 14992 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 691860, 695860, "...и положим в Мальчик-Карманчик", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29247, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "положим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29247, 18419 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29247, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29247, 1296 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "карманчик" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29247, 18420 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 696360, 699280, "Мандаринчик?", 191 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мандаринчик" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29248, 18421 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 707960, 712050, "- И все? И эту огромную цифру\r\nты боялась назвать мне?  - Ага.", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 3012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 12843 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "цифру" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 18422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 1954 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 14936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29249, 2880 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 712300, 714380, "Тут вообще не о чем говорить", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29250, 644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29250, 897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29250, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29250, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29250, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29250, 1202 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 714630, 718470, "Я уж подумал, это был целый флот", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29251, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29251, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29251, 2950 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29251, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29251, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29251, 7123 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "флот" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29251, 18423 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 718720, 719720, "Тебя это не беспокоит?", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29252, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29252, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29252, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29252, 5175 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 719970, 723230, "- Нет, милая. Нисколько.\r\n- Ура!", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29253, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29253, 2944 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нисколько" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29253, 18424 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29253, 3645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 726010, 728100, "Теперь насчет твоих двоих", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29254, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29254, 2157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29254, 970 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29254, 9300 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 728350, 729350, "Что?", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29255, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 729600, 731270, "Ладно", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29256, 1037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 731520, 732600, "Что насчет них?", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29257, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29257, 2157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29257, 1388 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 732860, 735440, "Как-то их слишком мало", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29258, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29258, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29258, 1935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29258, 1323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29258, 1893 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 735690, 738280, "Ну да. И...?", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29259, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29259, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29259, 688 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 739610, 742950, "Разве мужчине не хочется\r\nснять побольше тёлок?", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29260, 2177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29260, 7607 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29260, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29260, 745 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29260, 5072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29260, 3325 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тёлок" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29260, 18425 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 743200, 748200, "Или получается... О боже!\r\nЭто я - такая тёлка?", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29261, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29261, 1215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29261, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29261, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29261, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29261, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29261, 2616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29261, 17301 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 748450, 750120, "Нет, милая, ты не тёлка", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29262, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29262, 2944 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29262, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29262, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29262, 17301 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 750370, 754130, "Не знаю, может,\r\nя не такой уж и гуляка", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 1310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гуляка" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29263, 18426 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 754380, 756710, "Я сплю только с женщинами,\r\nв которых влюблен", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29264, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сплю" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29264, 18427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29264, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29264, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29264, 8301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29264, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29264, 1205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29264, 1451 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 756960, 760300, "Ты спал аж с двумя", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29265, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29265, 3771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29265, 2985 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29265, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29265, 8299 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 763720, 766220, "Именно", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29266, 1805 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 772730, 775480, "Ты знаешь, что я тебя тоже люблю?", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29267, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29267, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29267, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29267, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29267, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29267, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29267, 934 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 775730, 778900, "Теперь да", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29268, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29268, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 782740, 785820, "Росс, выслушай же меня", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29269, 798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29269, 940 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29269, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29269, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 786070, 789080, "С тобой мне намного лучше,\r\nчем было с Паоло", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 735 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 1969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29270, 5558 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 789330, 792330, "Ты заботишься обо мне, любишь,\r\nвеселишь меня", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29271, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заботишься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29271, 18428 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29271, 5798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29271, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29271, 2243 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "веселишь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29271, 18429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29271, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 792580, 795080, "Раз уж я весельчак, как тебе идея?", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29272, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29272, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29272, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29272, 7116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29272, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29272, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29272, 4515 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 795330, 797420, "Завяжись с Паоло в мешок\r\nи повозись немного...", 216 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "завяжись" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29273, 18430 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29273, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29273, 5558 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29273, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29273, 6884 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29273, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "повозись" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29273, 18431 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29273, 1119 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 797670, 802260, "...а я постою в углу\r\nи в самый разгар скажу \"тук-тук\"!", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 14950 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "углу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 18432 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 2835 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разгар" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 18433 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 2072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 2293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29274, 2293 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 804180, 807180, "Между нами что-то особенное!\r\nА у меня и Паоло был только...", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 1173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 1174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 13330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 5558 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29275, 757 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 807430, 810180, "\"Животный секс\"?", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29276, 18408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29276, 1845 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 812020, 813690, "Что ты хочешь сказать?", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29277, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29277, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29277, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29277, 1212 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 813940, 816770, "Между нами нет совсем\r\nничего \"животного\"?", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29278, 1173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29278, 1174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29278, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29278, 1997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29278, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29278, 10999 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 817020, 820940, "Пусть хотя бы\r\nкак у маленьких животных!", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29279, 4948 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29279, 2078 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29279, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29279, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29279, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29279, 8747 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29279, 3451 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 821190, 822950, "Ну, например...", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29280, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29280, 2211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 823200, 825950, "...бурундучковый секс?", 224 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бурундучковый" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29281, 18434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29281, 1845 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 828200, 830950, "Попробуй меня выслушать, ладно?", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29282, 2651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29282, 711 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выслушать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29282, 18435 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29282, 1037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 831200, 833620, "Я не буду тебе врать", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29283, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29283, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29283, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29283, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29283, 6968 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 833870, 838710, "- С Паоло было неплохо.\r\n- Тук-тук.", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29284, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29284, 5558 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29284, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29284, 1887 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29284, 2293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29284, 2293 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 839460, 842380, "Но! То, что между нами -\r\nгораздо лучше", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29285, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29285, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29285, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29285, 1173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29285, 1174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29285, 4817 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29285, 991 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 842630, 846970, "У нас нежная, глубокая связь.\r\nМы чувствуем друг друга", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 1210 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нежная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 18436 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "глубокая" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 18437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 12081 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 7390 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 2152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29286, 2773 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 847220, 848640, "Понимаешь? Я клянусь", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29287, 1213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29287, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29287, 3448 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 848890, 850970, "Это лучшее...", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29288, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29288, 7984 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 851220, 855480, "...что у меня было в жизни", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29289, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29289, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29289, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29289, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29289, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29289, 1160 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 855560, 857980, "До этих пор", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29290, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29290, 804 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29290, 1200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 865150, 866660, "Ричард сказал, что он меня любит", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29291, 4917 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29291, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29291, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29291, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29291, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29291, 1236 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 866910, 867990, "- О боже!\r\n- Именно!", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29292, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29292, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29292, 1805 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 868240, 869490, "Это же чудесно!", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29293, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29293, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29293, 6000 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 869740, 870910, "Я знаю!", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29294, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29294, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 871160, 872830, "- Не могу найти...\r\n- Верхний ящик.", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29295, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29295, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29295, 5532 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "верхний" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29295, 18438 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ящик" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29295, 18439 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 873080, 876540, "- Быстрее.\r\n- А что? Тебе тоже надо?", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29296, 6534 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29296, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29296, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29296, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29296, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29296, 758 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 876540, 877540, "Еще бы!", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29297, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29297, 936 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 878250, 881170, "Нашла, вот они!", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29298, 1365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29298, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29298, 2022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 884340, 886760, "Точнее, он один", 242 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "точнее" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29299, 18440 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29299, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29299, 1110 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 888590, 894020, "Мы сейчас. Нам надо решить\r\nодин вопрос", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29300, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29300, 752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29300, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29300, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29300, 3380 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29300, 1110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29300, 1112 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 910530, 915450, "- Им нужно что-то решить.\r\n- Хорошо. Хор-рошо.", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29301, 1921 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29301, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29301, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29301, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29301, 3380 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29301, 833 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "хор" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29301, 18441 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рошо" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29301, 18442 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 920210, 925960, "А твои усы... Они когда-нибудь\r\nбыли другими?", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29302, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29302, 1961 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "усы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29302, 18443 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29302, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29302, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29302, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29302, 876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29302, 11051 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 926760, 928050, "Нет", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29303, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 931550, 936140, "Как это они у тебя...\r\nтакие аккуратные?", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29304, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29304, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29304, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29304, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29304, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29304, 1386 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "аккуратные" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29304, 18444 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 936560, 938980, "Специальной расчесочкой", 248 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "специальной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29305, 18445 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "расчесочкой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29305, 18446 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 942230, 944070, "- Как она называется?\r\n- Расческа для усов.", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29306, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29306, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29306, 12148 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "расческа" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29306, 18447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29306, 768 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "усов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29306, 18448 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 944320, 946740, "Спасибо", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29307, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 946990, 951030, "Ладно, я буду месяц\r\nходить за тебя в прачечную", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 2232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 1130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прачечную" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29308, 18449 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 951030, 952320, "Нет!", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29309, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 953410, 954990, "Тогда я, я, я...", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29310, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29310, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29310, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29310, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 955240, 957910, "Буду два месяца убирать квартиру", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29311, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29311, 1223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29311, 1066 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29311, 2147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29311, 10488 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 958160, 964000, "Хорошо, отдам - если ты скажешь,\r\nгде мы держим совок", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 12685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 1461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "держим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 18450 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29312, 7675 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 972180, 975350, "Ты был во Вьетнаме?", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29313, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29313, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29313, 1155 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вьетнаме" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29313, 18451 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 978430, 981020, "- Кинем на пальцах?\r\n- Давай.", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29314, 14736 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29314, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29314, 7380 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29314, 1427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 981270, 983020, "Раз, два, три", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29315, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29315, 1223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29315, 1244 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 983270, 985940, "Ура!", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29316, 3645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 986190, 989860, "Ладно. Иди трахайся", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29317, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29317, 1034 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "трахайся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29317, 18452 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 990780, 993120, "Нет! Ты все не так понял!", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29318, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29318, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29318, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29318, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29318, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29318, 903 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 993370, 996200, "Джон Сэвидж  был без ног\r\nв \"Охотнике на оленей\"", 262 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "джон" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 18453 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сэвидж" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 18454 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 841 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 8739 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "охотнике" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 18455 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "оленей" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29319, 18456 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 996450, 999290, "А Джон Войт в \"Пути домой\"\r\nне чувствовал ног!", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 18453 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "войт" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 18457 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 2996 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 832 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 13169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29320, 8739 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 999540, 1003630, "Все наоборот, друг мой.\r\nКак раз Джон Войт...", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29321, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29321, 15273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29321, 2152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29321, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29321, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29321, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29321, 18453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29321, 18457 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1003880, 1007210, "- Милый?\r\n- Что?", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29322, 4138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29322, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1014300, 1015140, "Прошу!", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29323, 2744 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1015390, 1016810, "Ничего не будет", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29324, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29324, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29324, 966 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1017060, 1021810, "Сегодня их очередь.\r\nА мы будем завтра", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29325, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29325, 1935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29325, 5436 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29325, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29325, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29325, 863 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29325, 2024 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1023310, 1028070, "В будущем мне хотелось бы\r\nзаранее знать расписание...", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29326, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29326, 5100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29326, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29326, 5636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29326, 936 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заранее" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29326, 18458 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29326, 789 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29326, 11368 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1030320, 1032070, "И когда я проснулся утром...", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29327, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29327, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29327, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29327, 3609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29327, 1982 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1032320, 1035740, "...он украл все стельки\r\nиз моей обуви!", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29328, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29328, 13375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29328, 681 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стельки" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29328, 18459 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29328, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29328, 2056 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обуви" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29328, 18460 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1035990, 1036740, "Почему?", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29329, 661 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1036990, 1040830, "Он думает, я переспал\r\nс его бывшей и убил его рыбку", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 8232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 925 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 3402 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 2982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29330, 18416 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1041080, 1044420, "А рыбку зачем убивать?", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29331, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29331, 18416 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29331, 909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29331, 18183 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1046340, 1049000, "Иногда, когда с кем-то\r\nпереспишь...", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29332, 793 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29332, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29332, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29332, 2144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29332, 835 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "переспишь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29332, 18461 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1049250, 1053090, "...хочется убить хотя бы рыбку", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29333, 745 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29333, 5107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29333, 2078 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29333, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29333, 18416 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1053680, 1055090, "Чендлер, милый...", 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29334, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29334, 4138 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1055340, 1056430, "...мне очень жаль", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29335, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29335, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29335, 2965 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1056680, 1059260, "Теперь можно посмотреть\r\nсериал Джоуи?", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29336, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29336, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29336, 2899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29336, 18302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29336, 854 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1059520, 1061020, "Подожди. Его еще нет", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29337, 972 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29337, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29337, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29337, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1061270, 1064350, "Он в курсе дела. Он там играет", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29338, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29338, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29338, 3667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29338, 1817 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29338, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29338, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29338, 5014 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1064770, 1068770, "Кстати, моя проблема\r\nуже разрешилась", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29339, 1877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29339, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29339, 1822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29339, 990 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разрешилась" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29339, 18462 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1070440, 1072070, "Эмбер...", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29340, 18286 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1072320, 1074660, "...я всегда буду рядом с тобой...", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29341, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29341, 706 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29341, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29341, 4122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29341, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29341, 735 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1074910, 1077450, "...как друг и брат", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29342, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29342, 2152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29342, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29342, 1455 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1077660, 1079700, "О, Дрейк", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29343, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29343, 16260 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1079950, 1081200, "Трудный день, а?", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29344, 8873 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29344, 1032 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29344, 666 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1081450, 1084120, "Вначале премия, теперь это?", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29345, 4892 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "премия" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29345, 18463 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29345, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29345, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1084370, 1087710, "Да, некоторым просто везет", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29346, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29346, 13448 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29346, 646 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "везет" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29346, 18464 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1087960, 1092630, "Д-р Раморе, вызов -\r\nна первый этаж в реанимацию", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 11370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 9126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 18303 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вызов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 18465 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 2807 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 10941 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "реанимацию" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29347, 18466 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1093380, 1095380, "Ну ладно...", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29348, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29348, 1037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1095630, 1098890, "...это вроде меня", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29349, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29349, 1806 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29349, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1099810, 1104310, "Еще кому-нибудь надо в лифт?\r\nД-р Хортон? Д-р Уолл?", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 2251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "лифт" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 18467 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 11370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 9126 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "хортон" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 18468 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 11370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 9126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29350, 6907 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1104560, 1107980, "Звали только тебя", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29351, 12460 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29351, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29351, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1108730, 1111280, "Ладно уже", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29352, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29352, 990 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1111570, 1112730, "Я люблю тебя, Дрейк", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29353, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29353, 934 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29353, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29353, 16260 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1112990, 1115200, "Какая разница", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29354, 1353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29354, 7641 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1115450, 1116490, "Ой, нет!", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29355, 1217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29355, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1116740, 1119580, "Дрейк, берегись!", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29356, 16260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29356, 6455 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1122660, 1126540, "- Только что убили Джоуи?\r\n- Нет!", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29357, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29357, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29357, 10916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29357, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29357, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1127460, 1130710, "Теперь, возможно, да", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29358, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29358, 1026 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29358, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1130710, 1133710, "Давай открывай!\r\nМы хотим поговорить с тобой!", 302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29359, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29359, 6382 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29359, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29359, 4916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29359, 2957 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29359, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29359, 735 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1133920, 1135340, "Я не хочу разговаривать!", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29360, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29360, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29360, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29360, 6369 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1135590, 1137470, "Да ладно тебе! Мы волнуемся", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29361, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29361, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29361, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29361, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "волнуемся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29361, 18469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1137760, 1143430, "- Мы беспокоимся за тебя.\r\n- И кое-кто хочет ссать!", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "беспокоимся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 18470 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 2876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 1466 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29362, 4765 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1146020, 1149100, "Сочувствую, Джоуи", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29363, 16391 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29363, 854 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1149690, 1153530, "Нам жаль, что тебя убили.\r\nДа еще так паскудно", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 2965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 10916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 679 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "паскудно" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29364, 18471 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1153780, 1157110, "- Мы пришли, как только увидели\r\n- Как ты мог не сказать нам?", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 10107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 6515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 788 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 1212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29365, 1083 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1157360, 1162280, "Не знаю. Надеялся, что\r\nникто не узнает", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29366, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29366, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29366, 6874 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29366, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29366, 1279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29366, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29366, 12707 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1162530, 1166370, "Может быть, тебя удастся\r\nкак-то оживить", 310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29367, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29367, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29367, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29367, 1297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29367, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29367, 835 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "оживить" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29367, 18472 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 310   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1166620, 1171000, "Сказали, что когда нашли тело,\r\nмозг был так поврежден...", 311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 3700 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 5546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 5891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 14610 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 679 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поврежден" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29368, 18473 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 311   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1171250, 1176380, "...что единственный врач,\r\nкоторый бы меня спас, был я", 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 2149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 5825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 1024 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 10243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29369, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 312   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1176590, 1180390, "Это у них типа ирония такая", 313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29370, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29370, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29370, 1388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29370, 1809 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ирония" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29370, 18474 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29370, 2616 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 313   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1180800, 1182470, "Джоуи, все будет хорошо", 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29371, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29371, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29371, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29371, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 314   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1182720, 1186100, "Зачем тебе этот сериал.\r\nТупая мыльная опера", 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29372, 909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29372, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29372, 717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29372, 18302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29372, 1901 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мыльная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29372, 18475 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "опера" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29372, 18476 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 315   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1186350, 1189480, "Фиби, это было лучшее,\r\nчто я имел в своей жизни", 316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 7984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 5092 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 2094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29373, 1160 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 316   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1189770, 1193650, "Я как раз собиралась добавить", 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29374, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29374, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29374, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29374, 10161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29374, 1978 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 317   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1193900, 1198490, "А вот Моника.\r\nОна тебя чем-нибудь утешит", 318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29375, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29375, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29375, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29375, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29375, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29375, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29375, 658 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "утешит" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29375, 18477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 318   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1198570, 1203160, "Я расправила занавеску в душе,\r\nчтобы не заплесневела", 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29376, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "расправила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29376, 18478 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "занавеску" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29376, 18479 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29376, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29376, 5080 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29376, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29376, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заплесневела" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29376, 18480 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 319   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1203450, 1206290, "А что? Меня бы это утешило", 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29377, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29377, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29377, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29377, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29377, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "утешило" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29377, 18481 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 320   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1206580, 1208830, "Все будет хорошо. Сам знаешь", 321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29378, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29378, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29378, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29378, 1039 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29378, 737 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 321   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1209040, 1211790, "Нет, не знаю", 322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29379, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29379, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29379, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 322   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1212040, 1215300, "Как будто всю жизнь работал\r\nнад чем-то...", 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29380, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29380, 739 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29380, 942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29380, 943 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29380, 1415 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29380, 2281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29380, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29380, 835 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 323   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1215550, 1217670, "...и думал, что когда\r\nдобьешься своего...", 324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29381, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29381, 2255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29381, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29381, 1126 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "добьешься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29381, 18482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29381, 2771 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 324   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1217920, 1220260, "...все равно не будет\r\nтак хорошо, как ожидал", 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29382, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29382, 1818 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29382, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29382, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29382, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29382, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29382, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29382, 11317 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 325   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1220510, 1222340, "Но оказалось еще лучше", 326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29383, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29383, 3437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29383, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29383, 991 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 326   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1222590, 1224180, "Все изменилось", 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29384, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29384, 5829 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 327   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1224430, 1227600, "На днях я написал заявление\r\nна открытие кредитной карточки...", 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 8002 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 1914 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заявление" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 18483 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "открытие" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 18484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 1390 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29385, 11619 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 328   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1227850, 1231730, "...и его \"приняли к рассмотрению\"!", 329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29386, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29386, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29386, 11851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29386, 831 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рассмотрению" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29386, 18485 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 329   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1232690, 1236020, "Меня раньше никогда и нигде\r\nне \"принимали к рассмотрению\"!", 330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 900 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 5569 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "принимали" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 18486 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29387, 18485 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 330   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1236280, 1237780, "Извини, друг", 331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29388, 1447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29388, 2152 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 331   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1238030, 1241280, "Не знаю, значит ли это\r\nчто-нибудь для тебя...", 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 1234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29389, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 332   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1241530, 1245200, "...но у нас ты всегда будешь\r\n\"принят к рассмотрению\"", 333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 706 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 747 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "принят" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 18487 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29390, 18485 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 333   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1245450, 1248660, "Для меня это ничего не значит", 334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29391, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29391, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29391, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29391, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29391, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29391, 692 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 334   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1254420, 1257800, "Орехово-песочного? Сам испек", 335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29392, 4144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29392, 9738 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29392, 1039 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "испек" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29392, 18488 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 335   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1257880, 1260800, "Ага, давай", 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29393, 2880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29393, 1427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 336   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1261050, 1263140, "Это изюм?", 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29394, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "изюм" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29394, 18489 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 337   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1263390, 1265720, "Вполне возможно", 338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29395, 3443 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29395, 1026 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 338   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1271600, 1274480, "Слушай, Эдди", 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29396, 2085 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29396, 13355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 339   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1274560, 1277980, "Я тут думал о том, как мы живем", 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29397, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29397, 644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29397, 2255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29397, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29397, 1823 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29397, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29397, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29397, 5035 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 340   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1278230, 1279320, "Ты чего улыбаешься?", 341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29398, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29398, 1000 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "улыбаешься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29398, 18490 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 341   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1279570, 1283240, "У меня сюрприз. Смотри -\r\nновая рыбулька", 342 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29399, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29399, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29399, 5505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29399, 1439 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29399, 9996 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рыбулька" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29399, 18491 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 342   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1283490, 1284820, "Я назвал его...", 343 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29400, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29400, 4155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29400, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 343   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1285070, 1289160, "...Чендлером. Ну, в честь тебя", 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29401, 12186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29401, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29401, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29401, 10990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29401, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 344   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1296750, 1300590, "Это же даже не живая рыбка", 345 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29402, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29402, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29402, 684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29402, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "живая" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29402, 18492 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29402, 18355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 345   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1300800, 1304840, "Нет, это пластмассовая наживка", 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29403, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29403, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пластмассовая" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29403, 18493 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "наживка" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29403, 18494 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 346   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1305180, 1308350, "Ты это к чему, чувак?", 347 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29404, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29404, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29404, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29404, 6040 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29404, 3361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 347   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1311020, 1313520, "Ну все, спокойной ночи", 348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29405, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29405, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29405, 1445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29405, 1446 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 348   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1313770, 1317730, "Хренов любитель природы!", 349 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "хренов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29406, 18495 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "любитель" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29406, 18496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29406, 11835 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 349   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1341760, 1344760, "- Прохладно сегодня.\r\n- Ага.", 350 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29407, 15719 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29407, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29407, 2880 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 350   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 84, 1346250, 1350300, "- Давай об этом забудем.\r\n- Договорились.", 351 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29408, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29408, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29408, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29408, 12842 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 29408, 686 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 42 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 02/s02e18 - The One Where Dr. Ramoray Dies/Russian.srt" ,  `subtitle`.`phrase_number` = 351   WHERE  `subtitle`.`id` = 84 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 42, 84 );