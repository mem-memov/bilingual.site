INSERT INTO `source` ( `source`.`file` ) VALUES ( "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device.mkv" );
# a query
INSERT INTO `video` ( `video`.`source_id`, `video`.`language_id`, `video`.`file`, `video`.`width`, `video`.`height`, `video`.`duration` ) VALUES ( 56, 1, "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/movie.mp4", 768, 432, 1369411 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 56, 2, "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.mp3", 1369418 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 56, 111 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 56, 1, "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.mp3", 1369418 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 56, 112 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 56, 1, "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 2127, 3503, "Here you go, Pheebs.", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38708, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38708, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38708, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38708, 375 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 3712, 6340, "Who wants one of my\r\nspecial home-baked brownies?", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 1627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 1717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 620 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 18891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38709, 15390 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 6548, 7591, "I\'ll have one.", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38710, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38710, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38710, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38710, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 8550, 9968, "Agh! Ugh.", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38711, 18258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38711, 3985 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 10135, 12179, "-That\'s okay.\r\n-I\'m not having one.", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38712, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38712, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38712, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38712, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38712, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38712, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38712, 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38712, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 12888, 14890, "No, no. It\'s just my tooth.", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38713, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38713, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38713, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38713, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38713, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38713, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38713, 6819 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 15140, 16975, "-All right.\r\n-I\'ll have one.", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38714, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38714, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38714, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38714, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38714, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38714, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 18101, 20687, "So you need a dentist?\r\nI\'ve got a good one.", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38715, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 20854, 25108, "Thanks. I have a good one too.\r\nI just can\'t see him.", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38716, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 25359, 28195, "See, that is the problem\r\nwith invisible dentists.", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38717, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38717, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38717, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38717, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38717, 1494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38717, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38717, 15452 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "dentists" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38717, 22238 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 30030, 31281, "Why can\'t you go to him?", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38718, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38718, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38718, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38718, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38718, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38718, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38718, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 31448, 36370, "Because every time I go\r\nto the dentist, somebody dies.", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 1570 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 2537 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38719, 18888 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 37663, 40499, "That\'s so weird.\r\nEvery time I go to the dentist...", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 1570 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38720, 408 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 40707, 42876, "...I look down the hygienist\'s blouse.", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38721, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38721, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38721, 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38721, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hygienist" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38721, 22239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38721, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38721, 12666 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 43919, 46296, "Phoebe, what, urn...?", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38722, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38722, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38722, 470 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 46463, 47547, "What?", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38723, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 47714, 49299, "First, it was my Aunt Mary.", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38724, 596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38724, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38724, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38724, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38724, 1789 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38724, 10329 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 49549, 52094, "And then there was, um,\r\nJohn, my mailman.", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38725, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38725, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38725, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38725, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38725, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38725, 18266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38725, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38725, 15392 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 52261, 54888, "And then my cowboy friend, Albino Bob.", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38726, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38726, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38726, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38726, 13234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38726, 1691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38726, 606 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38726, 3919 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 55889, 58308, "Ahem. And all these people actually died?", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38727, 1669 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38727, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38727, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38727, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38727, 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38727, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38727, 5670 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 58475, 60686, "Yes. While I was in the chair.", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38728, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38728, 499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38728, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38728, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38728, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38728, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38728, 11192 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 60852, 64022, "That\'s why I take care of my teeth.\r\nIt\'s not about oral hygiene.", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 2561 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 413 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 208 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "oral" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 22240 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hygiene" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38729, 22241 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 64273, 66316, "I floss to save lives.", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38730, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38730, 11191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38730, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38730, 3978 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38730, 1739 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 67317, 69152, "Come on, you didn\'t kill anybody.", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38731, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38731, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38731, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38731, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38731, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38731, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38731, 553 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 69403, 73073, "These people just happened to die\r\nwhen you went to the dentist.", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 452 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 2353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38732, 408 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 73282, 74741, "It\'s just a coincidence.", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38733, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38733, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38733, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38733, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38733, 3909 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 74950, 77744, "Tell that to them.\r\nOh, you can\'t! They\'re dead!", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38734, 1559 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 128879, 129921, "Thanks, Gunther.", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38735, 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38735, 15403 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 130088, 131381, "-Hi.\r\n-Hey.", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38736, 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38736, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 131548, 133008, "Um, can I get a napkin too?", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38737, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38737, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38737, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38737, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38737, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38737, 15476 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38737, 576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 133175, 135844, "Oh, like you don\'t already\r\nhave everything.", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38738, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38738, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38738, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38738, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38738, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38738, 1549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38738, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38738, 1482 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 137637, 138680, "Ow!", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38739, 4042 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 138847, 140015, "Ow!", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38740, 4042 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 141183, 144853, "You\'re in pain. Would you just\r\ngo to the dentist? Just go.", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38741, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 145020, 147147, "Fine. But if you\'re my next victim...", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38742, 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38742, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38742, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38742, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38742, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38742, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38742, 4022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38742, 6674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 147314, 150651, "...don\'t come back as a poltergeist\r\nwho sucks me into the TV set.", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "poltergeist" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 22242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 617 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 403 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38743, 5704 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 151443, 152486, "I promise.", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38744, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38744, 2435 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 152653, 154571, "Although, don\'t feel like you can\'t visit.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38745, 17772 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 157240, 159701, "Hey, is Chandler here?", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38746, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38746, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38746, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38746, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 161119, 162454, "No. No, he\'s not.", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38747, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38747, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38747, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38747, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38747, 51 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 163955, 168585, "You guys, Joey just saw\r\nJanice kissing her ex-husband.", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 3874 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 1478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 1533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38748, 486 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 168794, 170003, "-Huh? Oh!\r\n-What?", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38749, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38749, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38749, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 170170, 173215, "What are you gonna do?\r\nHow are you gonna tell Chandler?", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38750, 193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 173382, 176635, "I was thinking about that.\r\nI think the best way would be...", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 3930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38751, 21 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 176802, 177844, "...to not.", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38752, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38752, 51 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 178762, 182015, "Joey, if you know about this,\r\nyou have to tell him.", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38753, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 182182, 184476, "It\'ll kill him. It\'ll just kill him.", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38754, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 184643, 187521, "Wait till I go to the dentist.\r\nMaybe I\'ll kill him.", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 4578 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38755, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 191608, 193026, "Ew!", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38756, 131 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 193193, 198115, "Ugly Naked Guy\'s using a hammock.\r\nIt\'s like a Play-Doh fat factory.", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 1577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hammock" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 22243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 3127 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "doh" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 22244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 3224 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "factory" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38757, 22245 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 199116, 201201, "I\'m going to the dentist. So listen...", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38758, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38758, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38758, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38758, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38758, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38758, 408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38758, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38758, 276 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 201368, 205622, "...just be on the lookout for anything\r\nthat you could fall into...", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "lookout" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 22246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 1708 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38759, 555 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 205789, 208458, "...or that can fall on you...", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38760, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38760, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38760, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38760, 1708 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38760, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38760, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 208625, 210001, "...or....", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38761, 199 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 210168, 211545, "All right, just look out!", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38762, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38762, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38762, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38762, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38762, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 212045, 215549, "Okay? And, um, I also just want to....", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38763, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38763, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38763, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38763, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38763, 2494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38763, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38763, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38763, 4 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 215882, 218969, "I just want to tell you all that, urn....", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38764, 470 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 224266, 227436, "I have a problem. I have to go\r\ninto work for a few hours.", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 1494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 8609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38765, 1605 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 227602, 229980, "Some kids messed up\r\nthe Homo sapien display.", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38766, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38766, 512 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "messed" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38766, 22247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38766, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38766, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38766, 17514 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "sapien" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38766, 22248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38766, 15798 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 230147, 231565, "What did they do?", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38767, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38767, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38767, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38767, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 231731, 234860, "They painted over the word\r\n\"sapien,\" for one thing.", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 20462 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 615 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 22248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38768, 381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 235735, 240615, "Then they rearranged the figures.\r\nLet\'s just leave it at that.", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 584 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "rearranged" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 22249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "figures" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 22250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38769, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 240782, 242576, "Do you want me to watch Ben for you?", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38770, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 242742, 244536, "Yes, that\'s what I was gonna ask.\r\nThank you.", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 400 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38771, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 244703, 247539, "Woah, wait, hello! Heh, what about me?", 65 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "woah" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38772, 22251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38772, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38772, 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38772, 360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38772, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38772, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38772, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 247706, 251126, "You. You want to watch Ben?", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38773, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38773, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38773, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38773, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38773, 421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38773, 12337 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 255547, 257966, "That would be great!\r\nI just asked Monica...", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38774, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38774, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38774, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38774, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38774, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38774, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38774, 338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38774, 85 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 258133, 260427, "...because I know how empty her life is.", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38775, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38775, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38775, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38775, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38775, 3093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38775, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38775, 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38775, 50 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 264848, 267517, "-Hey, Ross? I got a science question.\r\n-Yeah. Mm-hm.", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 8613 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 423 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38776, 634 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 267684, 273482, "If the Homo sapiens were, in fact,\r\n\"homo\" sapiens...", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 17514 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "sapiens" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 22252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 3140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 17514 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38777, 22252 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 274858, 276401, "...is that why they\'re extinct?", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38778, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38778, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38778, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38778, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38778, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38778, 11939 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 280322, 283450, "Joey, Homo sapiens are people.", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38779, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38779, 17514 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38779, 22252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38779, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38779, 55 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 283617, 284951, "Hey, I\'m not judging.", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38780, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38780, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38780, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38780, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38780, 11951 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 287954, 290165, "Look, Benny! Spoon!", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38781, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38781, 20207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38781, 458 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 290624, 292459, "Spoon!", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38782, 458 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 293835, 296797, "Come on. All right, you know what?\r\nI think he\'s bored.", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 2 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bored" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38783, 22253 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 297214, 299508, "Here. You wanna play\r\nthe airplane game?", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38784, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38784, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38784, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38784, 3127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38784, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38784, 10371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38784, 2469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 299716, 301635, "You wanna show Rachel? Come here.", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38785, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38785, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38785, 1500 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38785, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38785, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38785, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 301801, 303887, "You wanna do something fun? Okay.", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38786, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38786, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38786, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38786, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38786, 2418 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38786, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 304054, 305222, "Whee!", 80 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "whee" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38787, 22254 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 307766, 310227, "Whee! Ha, ha, oh!", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38788, 22254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38788, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38788, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38788, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 315232, 318360, "You\'re so brave.\r\nYou\'re so brave! Yes, you are.", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 11558 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 11558 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38789, 109 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 318527, 320445, "-You\'re so brave.\r\n-You\'re okay.", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38790, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38790, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38790, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38790, 11558 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38790, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38790, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38790, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 320612, 322864, "He\'s fine. Let\'s just put him down.", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38791, 81 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 323031, 325951, "Come here, babe.\r\nYes, you\'re such a good boy.", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 8159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38792, 528 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 326117, 330372, "How could you do that to him?\r\nRoss trusted me! What\'s he gonna say?", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 15086 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38793, 159 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 330539, 332582, "Nothing, because we\'re not telling him.", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38794, 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38794, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38794, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38794, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38794, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38794, 1660 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38794, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 332749, 333917, "-We\'re not?\r\n-No.", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38795, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38795, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38795, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38795, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 334084, 336086, "-All right, I like that.\r\n-Okay.", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38796, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38796, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38796, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38796, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38796, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38796, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 336836, 339172, "So we\'re okay. We\'re okay, aren\'t we?", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 3090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38797, 200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 339339, 341049, "No, we\'re not okay. There\'s a bump.", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38798, 6200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 341216, 342926, "-There\'s a bump.\r\n-What bump?", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38799, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38799, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38799, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38799, 6200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38799, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38799, 6200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 343093, 345637, "Oh, my God! Well, push it in! Push it in!", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38800, 64 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 345804, 348598, "I can not push it in!", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38801, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38801, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38801, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38801, 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38801, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38801, 64 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 349432, 351560, "-Okay, we\'re gonna need a distraction.\r\n-Okay.", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38802, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38802, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38802, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38802, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38802, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38802, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "distraction" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38802, 22255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38802, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 351726, 352978, "-That\'s it.\r\n-Okay.", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38803, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38803, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38803, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38803, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 353144, 354312, "-I got it!\r\n-Okay.", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38804, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38804, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38804, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38804, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 354479, 356189, "-The second Ross walks in...\r\n-Yeah.", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38805, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38805, 1728 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38805, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38805, 7781 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38805, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38805, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 356356, 359651, "...take him to your bedroom and do\r\nwhatever you do that makes him go:", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 6808 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 3309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38806, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 362946, 364781, "Or...", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38807, 199 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 366283, 368118, "...we could put a hat on his head.", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38808, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38808, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38808, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38808, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38808, 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38808, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38808, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38808, 238 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 368285, 373123, "A hat! Yes! We need a hat! Where\r\nare we gonna find a tiny little hat?", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 2501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38809, 283 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 373290, 374958, "Oh, oh, oh! I\'ll get Rainy Day Bear!", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 128 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "rainy" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 22256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38810, 3946 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 375125, 377460, "Because he\'ll know what to do?", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38811, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38811, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38811, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38811, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38811, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38811, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38811, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 377794, 379379, "Oh, my God. You\'re a genius!", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38812, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38812, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38812, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38812, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38812, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38812, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "genius" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38812, 22257 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 379546, 381464, "Oh, God. It\'s kind of sewn on.", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38813, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38813, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38813, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38813, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38813, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38813, 69 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "sewn" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38813, 22258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38813, 14 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 381631, 383091, "-Give it. Give it.\r\n-Okay.", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38814, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38814, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38814, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38814, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38814, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 383466, 384676, "-Oh!\r\n-Oh.", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38815, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38815, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 387304, 390807, "Agh, it\'s just like a bloodbath\r\nin here today!", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 18258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bloodbath" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 22259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38816, 125 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 396146, 398440, "Hey, hold on a second.\r\nHold on a second.", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 2413 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 1728 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 2413 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38817, 1728 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 400108, 402402, "Do you think these pearls are nice?", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38818, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38818, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38818, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38818, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38818, 21259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38818, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38818, 299 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 402694, 404446, "I\'d prefer a mountain bike.", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38819, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38819, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38819, 6161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38819, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38819, 8212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38819, 16137 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 405905, 408992, "Janice\'s birthday\'s coming.\r\nI wanna get her something special.", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 3874 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 3079 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 2565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38820, 1717 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 410410, 413204, "-Come in here with me.\r\n-Woah, woah, whoa, wait. Woah.", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 22251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 22251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 2366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38821, 22251 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 415290, 417667, "You wanna get her something special?\r\nGet her flowers.", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 1717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38822, 3155 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 417834, 420587, "Get her candy. Get her gum.\r\nGirls love gum.", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 3207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 2542 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 1501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38823, 2542 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 421296, 424758, "That\'s a good idea. \"Dear Janice:\r\nHave a Hubba-Bubba birthday.\"", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 392 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 3874 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hubba" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 22260 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bubba" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 22261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38824, 3079 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 426134, 428345, "I\'d like to get her something serious.", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38825, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38825, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38825, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38825, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38825, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38825, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38825, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38825, 1481 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 428511, 431097, "Oh, you want something serious.", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38826, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38826, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38826, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38826, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38826, 1481 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 431264, 434601, "You know what you should do?\r\nYou should get her one of those, um...", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38827, 590 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 434768, 436227, "...barium enemas.", 121 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "barium" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38828, 22262 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "enemas" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38828, 22263 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 438438, 440273, "Those are dead serious.", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38829, 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38829, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38829, 1559 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38829, 1481 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 440440, 445111, "Look, I\'m gonna go in here.\r\nAnd you don\'t buy me anything, ever!", 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38830, 451 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 445278, 447948, "You can\'t! You can\'t.\r\nYou can\'t buy her pearls.", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38831, 21259 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 448114, 450533, "You just can\'t, you can\'t, you can\'t!", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38832, 37 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 450700, 452452, "Why not?", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38833, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38833, 51 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 453995, 456289, "Oh, God. Uh, okay.", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38834, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38834, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38834, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38834, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 457040, 458958, "Here\'s the thing. Ahem.", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38835, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38835, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38835, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38835, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38835, 1669 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 459626, 461670, "This is the thing. Okay, the thing is—", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38836, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38836, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38836, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38836, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38836, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38836, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38836, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38836, 50 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 461836, 462962, "-What is the thing?\r\n-Okay.", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38837, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38837, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38837, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38837, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38837, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 464714, 467926, "I went down to the\r\nMattress King\'s showroom...", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 21962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 10292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38838, 21966 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 468176, 471012, "...and I saw Janice kissing\r\nher ex-husband.", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38839, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38839, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38839, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38839, 3874 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38839, 1478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38839, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38839, 1533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38839, 486 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 472931, 474307, "What?", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38840, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 474474, 476101, "They were in his office.", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38841, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38841, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38841, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38841, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38841, 8937 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 479270, 482315, "Well, she- She wouldn\'t do that.\r\nShe\'s with me.", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38842, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 482482, 484150, "I\'m telling you, I saw it.", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38843, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38843, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38843, 1660 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38843, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38843, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38843, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38843, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 484317, 486236, "Well, you\'re wrong. Okay? You\'re wrong.", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38844, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38844, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38844, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38844, 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38844, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38844, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38844, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38844, 23 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 486403, 488071, "I\'m not wrong.", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38845, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38845, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38845, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38845, 23 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 488238, 490448, "I wish I was.", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38846, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38846, 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38846, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38846, 146 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 492200, 493952, "I\'m sorry.", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38847, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38847, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38847, 268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 497706, 500375, "Bet that barium enema\r\ndoesn\'t sound so bad now, huh?", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 7191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 22262 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "enema" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 22264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 1674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38848, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 503169, 508049, "It makes more sense as an ensemble.\r\nAnd it takes the focus off the hat.", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 4530 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 259 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "ensemble" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 22265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 8086 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 5206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38849, 283 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 508842, 511970, "Oh! You\'re alive! You\'re alive!", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38850, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38850, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38850, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38850, 2543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38850, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38850, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38850, 2543 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 513179, 515807, "See? I promised you\r\nno one would die, didn\'t I?", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 9054 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 2353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38851, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 515974, 519519, "We\'ll see about that. Can I use your\r\nphone to call everyone I know?", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 2378 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 1622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38852, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 519686, 522480, "Sure. We have no money. Go ahead.", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38853, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38853, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38853, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38853, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38853, 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38853, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38853, 9035 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 523773, 526985, "Hey, you\'re not dead! Okay, see you.", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38854, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38854, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38854, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38854, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38854, 1559 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38854, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38854, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38854, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 529195, 530739, "Monica.", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38855, 85 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 531072, 533575, "Oh, my God! He just said my name!", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38856, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38856, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38856, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38856, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38856, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38856, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38856, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38856, 348 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 533742, 535577, "-Did you hear that?\r\n-Yeah.", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38857, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38857, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38857, 564 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38857, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38857, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 536286, 538163, "Monica bang!", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38858, 85 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bang" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38858, 22266 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 541041, 543793, "Ahem. Okay, I heard that.", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38859, 1669 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38859, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38859, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38859, 2450 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38859, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 543960, 546296, "-Did he just say, \"Monica bang\"?\r\n-Mm-hm.", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38860, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38860, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38860, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38860, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38860, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38860, 22266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38860, 423 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38860, 634 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 546463, 549132, "Oh, my God! He\'s gonna rat me out!", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 3912 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38861, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 549799, 552427, "-Monica bang!\r\n-Oh, ho, ho.", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38862, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38862, 22266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38862, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38862, 7721 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38862, 7721 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 552927, 556347, "Sweetie, you need to stop\r\nsaying that now.", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38863, 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38863, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38863, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38863, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38863, 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38863, 435 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38863, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38863, 165 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 556514, 559100, "It\'s no big deal.\r\nIt\'s not even worth mentioning.", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 1671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 2386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38864, 20467 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 559267, 561352, "You see? We all do it all the time.", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38865, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 561519, 562645, "See? Watch this.", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38866, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38866, 421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38866, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 562812, 564773, "Ben, Ben, Ben. Ow.", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38867, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38867, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38867, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38867, 4042 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 564939, 567650, "Monica bang!\r\nEverybody bang!", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38868, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38868, 22266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38868, 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38868, 22266 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 567817, 569152, "Ben bang!", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38869, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38869, 22266 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 569319, 570945, "Rachel bang!", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38870, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38870, 22266 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 571112, 573114, "Bang, Rachel! Bang!", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38871, 22266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38871, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38871, 22266 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 573656, 576159, "-Oh, isn\'t it fun?\r\n-Look at that! Look at that!", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 426 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 2418 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38872, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 576826, 578203, "We all do it!", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38873, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38873, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38873, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38873, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 578369, 580663, "-Okay, I\'m stopping now.\r\n-Are you okay?", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38874, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38874, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38874, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38874, 21801 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38874, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38874, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38874, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38874, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 581122, 584918, "Oh, yeah. You know, if it\'s not\r\na headboard, it\'s just not worth it.", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "headboard" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 22267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 2386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38875, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 596012, 598098, "How\'s my Bing-a-ling?", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38876, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38876, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38876, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38876, 444 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38876, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38876, 20955 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 599766, 601267, "Uh....", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38877, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 601976, 603019, "I don\'t know.", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38878, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38878, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38878, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38878, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 603812, 605230, "You tell me.", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38879, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38879, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38879, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 606231, 609359, "Anything you, uh, want to tell me?", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38880, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38880, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38880, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38880, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38880, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38880, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38880, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 609526, 611277, "Because if you, uh- You should-", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38881, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38881, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38881, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38881, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38881, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38881, 155 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 611444, 613571, "You would tell me.", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38882, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38882, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38882, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38882, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 615031, 618034, "Why are your eyes so wide?", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38883, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38883, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38883, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38883, 1786 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38883, 25 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "wide" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38883, 22268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 618827, 620495, "You tell me.", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38884, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38884, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38884, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 620870, 623498, "Maybe it\'s because I was just\r\nfooling around with my ex.", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 16863 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38885, 1533 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 623665, 626126, "Oh, no. No, no, no. That was you!", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38886, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38886, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38886, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38886, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38886, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38886, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38886, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38886, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 626292, 627669, "Oh, my God!", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38887, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38887, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38887, 181 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 627836, 629671, "-All right!\r\n-How did you know?", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38888, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38888, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38888, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38888, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38888, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38888, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 629838, 632006, "Joey told me. He saw you two kissing.", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38889, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38889, 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38889, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38889, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38889, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38889, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38889, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38889, 1478 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 632173, 633716, "In the park?", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38890, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38890, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38890, 2569 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 634884, 637679, "No. In his office.\r\nHow many kisses were there?", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 8937 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 3869 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 20195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38891, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 637846, 640598, "-Just those two!\r\n-Wh~? Why? Why?", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38892, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38892, 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38892, 54 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "wh" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38892, 22269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38892, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38892, 148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 640765, 643560, "Why was there kissing?\r\nThere should be no kissing!", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 1478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38893, 1478 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 643726, 646479, "Oh, I\'m sorry, honey. I\'m so, so....", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38894, 25 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 647856, 649440, "I\'m so, so sorry. I just....", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38895, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38895, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38895, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38895, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38895, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38895, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38895, 7 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 651818, 654571, "What happened was I....\r\nI can\'t breathe.", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38896, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38896, 452 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38896, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38896, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38896, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38896, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38896, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38896, 296 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 654737, 658491, "Can you get me a bag or something?\r\nAah.", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 6137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38897, 6718 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 658658, 660159, "-Here. Here.\r\n-Aah.", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38898, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38898, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38898, 6718 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 660326, 661411, "There.", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38899, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 664914, 667167, "-It\'s the receipt.\r\n-I\'ll take it.", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38900, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38900, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38900, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38900, 8603 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38900, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38900, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38900, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38900, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 668626, 670461, "Look, I\'ve got to know.", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38901, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38901, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38901, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38901, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38901, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38901, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 673214, 674757, "Are you finished with me?", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38902, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38902, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38902, 395 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38902, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38902, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 677719, 679387, "Are you finished with him?", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38903, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38903, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38903, 395 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38903, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38903, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 683641, 685768, "Do you still love him?", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38904, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38904, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38904, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38904, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38904, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 688730, 690148, "Do you still love me?", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38905, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38905, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38905, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38905, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38905, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 692275, 693484, "All right, look...", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38906, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38906, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38906, 80 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 694611, 697238, "...I\'m gonna need an actual\r\nanswer here, okay?", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 6182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38907, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 697405, 699324, "So which is it?", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38908, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38908, 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38908, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38908, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 700658, 702035, "Him or me?", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38909, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38909, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38909, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 711169, 713254, "I don\'t know.", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38910, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38910, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38910, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38910, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 715214, 718927, "Okay, if you\'re alive,\r\nyou answer your phone!", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 2543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38911, 82 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 723348, 727769, "Ben, I won\'t tell your daddy\r\nthat you had ice cream before dinner...", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 438 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38912, 56 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 727936, 731773, "...if you won\'t tell him about\r\nour little bonking incident.", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 386 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bonking" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 22270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38913, 10309 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 731940, 735401, "Number one, I don\'t think Ben\r\nunderstands the concept of bribery.", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 3205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 14830 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 4598 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 69 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bribery" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38914, 22271 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 735568, 737278, "And number two, I-", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38915, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38915, 3205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38915, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38915, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 739489, 740490, "What?", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38916, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 740657, 743117, "You said \"number two.\"", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38917, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38917, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38917, 3205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38917, 54 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 746120, 747413, "I also said \"number one.\"", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38918, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38918, 2494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38918, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38918, 3205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38918, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 747580, 749165, "Ha-ha-ha, I know.", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38919, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38919, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38919, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38919, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38919, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 752251, 753336, "-Hey, everyone!\r\n-Hi!", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38920, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38920, 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38920, 103 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 753503, 755505, "How\'s my little boy?", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38921, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38921, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38921, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38921, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38921, 528 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 755672, 757674, "He\'s perfect. Aren\'t you, Ben?", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38922, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38922, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38922, 3923 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38922, 3090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38922, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38922, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38922, 12337 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 757840, 760551, "-He\'s never been better.\r\n-What did you do, take him whaling?", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 24 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "whaling" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38923, 22272 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 763429, 765056, "Monica.", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38924, 85 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 765223, 769602, "Oh, my God! He just said your name!\r\nThat\'s great! Good job, Ben!", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38925, 12337 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 770186, 772021, "Monica bang!", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38926, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38926, 22266 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 772188, 773690, "Ha-ha-ha. That\'s right.", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38927, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38927, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38927, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38927, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38927, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38927, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 773856, 776776, "That\'s what I\'d sound like\r\nif I exploded.", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38928, 6779 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 778945, 781406, "Woo-hoo! The curse is broken!\r\nI called everybody I know...", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 6680 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 7189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "curse" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 22273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 1701 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 3891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38929, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 781572, 783449, "...and everyone is alive!", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38930, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38930, 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38930, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38930, 2543 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 786452, 787620, "-Huh.\r\n-What?", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38931, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38931, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 788871, 792041, "Ugly Naked Guy looks awfully still.", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38932, 1577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38932, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38932, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38932, 235 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "awfully" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38932, 22274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38932, 480 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 797672, 799424, "Oh, my God, I killed another one!", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38933, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38933, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38933, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38933, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38933, 600 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38933, 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38933, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 799590, 803678, "The curse is getting stronger too,\r\nto bring down something that big.", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 22273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 12009 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 1509 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38934, 263 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 803845, 806097, "Maybe he\'s just taking a nap.", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38935, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38935, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38935, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38935, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38935, 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38935, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38935, 14844 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 806264, 808808, "I\'m telling you,\r\nhe hasn\'t moved since this morning.", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 1660 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 6119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 463 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38936, 504 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 808975, 810476, "We should call somebody.", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38937, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38937, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38937, 1622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38937, 2537 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 810643, 813896, "Tell them the naked guy we stare at\r\nisn\'t moving?", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 7786 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 426 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38938, 5262 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 814063, 816399, "We\'ve got to find out if he\'s alive.", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38939, 2543 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 816566, 819027, "How are we gonna do that?\r\nThere\'s no way.", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38940, 420 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 819193, 820987, "Well, there is one way.", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38941, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38941, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38941, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38941, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38941, 420 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 821154, 824073, "His window\'s open. I say...", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38942, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38942, 3108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38942, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38942, 2389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38942, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38942, 159 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 824240, 825742, "...we poke him.", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38943, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38943, 7700 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38943, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 829370, 830997, "Hey!", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38944, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 831164, 833750, "You know how we, uh,\r\nsave all those chopsticks...", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 3978 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 272 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "chopsticks" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38945, 22275 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 833916, 836377, "...for no reason\r\nwhen we get Chinese food?", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38946, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38946, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38946, 3061 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38946, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38946, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38946, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38946, 12623 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38946, 1629 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 836544, 837670, "Yeah?", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38947, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 837837, 840256, "Well, now we got a reason.", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38948, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38948, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38948, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38948, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38948, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38948, 3061 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 841090, 842216, "What?", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38949, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 842383, 845303, "Well, we\'re fashioning a very long\r\npoking device.", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 16 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "fashioning" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 22276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 15870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38950, 4707 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 847263, 848765, "All right.", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38951, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38951, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 851434, 853895, "Hey, uh, what\'s the matter?", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38952, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38952, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38952, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38952, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38952, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38952, 1499 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 855980, 858024, "I talked to Janice.", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38953, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38953, 1764 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38953, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38953, 3874 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 858191, 861277, "Oh, my God. Is she going back to him?", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38954, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 861611, 863112, "She doesn\'t know.", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38955, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38955, 515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38955, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38955, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 863279, 864781, "Says she loves us both.", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38956, 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38956, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38956, 14458 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38956, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38956, 3067 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 866449, 869452, "I woke up this morning, I was in love.", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 3193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 504 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38957, 269 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 869619, 871412, "I was happy!", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38958, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38958, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38958, 142 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 872955, 875583, "It serves me right for buying\r\nthat 12-pack of condoms.", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 21246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 15847 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 571 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 5693 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38959, 7233 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 877960, 881798, "And now I can\'t even return them,\r\nbecause she choked on the receipt!", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 1589 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 101 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "choked" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 22277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38960, 8603 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 885009, 887178, "What are you, uh-?\r\nWhat are you gonna do?", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38961, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 889972, 891808, "I don\'t know.", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38962, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38962, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38962, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38962, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 892475, 894435, "You know? What would you do?", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38963, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38963, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38963, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38963, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38963, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38963, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 896979, 898856, "Uh, it doesn\'t matter what I\'d do.", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 1499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38964, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 899023, 900691, "Come on. Come on. Tell me.", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38965, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38965, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38965, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38965, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38965, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38965, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 903611, 905113, "All right, you, uh...", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38966, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38966, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38966, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38966, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 906572, 909742, "...you probably won\'t want\r\nto hear this, but, uh...", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 564 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38967, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 911744, 915498, "...if it was me,\r\nand this is just me, I would, uh....", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38968, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 915665, 917834, "-I would bow out.\r\n-What?", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38969, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38969, 518 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bow" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38969, 22278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38969, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38969, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 920211, 922046, "What are you talking about?", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38970, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38970, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38970, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38970, 434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38970, 208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 925216, 926968, "They have a kid together...", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38971, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38971, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38971, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38971, 1649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38971, 494 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 927135, 929554, "...you know? They\'re like-", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38972, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38972, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38972, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38972, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38972, 60 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 929846, 932014, "They\'re, like, a family.", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38973, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38973, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38973, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38973, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38973, 1540 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 932557, 934517, "And if, I don\'t know...", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38974, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38974, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38974, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38974, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38974, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38974, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 934684, 938020, "...if there\'s a chance\r\nthey can make that work...", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 1644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38975, 11 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 938187, 941023, "...I wouldn\'t want to be the guy\r\nwho stood in the way.", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 9939 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38976, 420 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 945903, 947363, "You okay?", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38977, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38977, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 948906, 950408, "You, uh...?", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38978, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38978, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 951033, 952702, "You wanna come poke a nude guy?", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38979, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38979, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38979, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38979, 7700 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38979, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38979, 16172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38979, 9 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 956122, 959083, "-Well, he\'s finally asleep.\r\n-Mm.", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38980, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38980, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38980, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38980, 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38980, 9907 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38980, 423 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 959250, 962420, "About that, uh, bump on his head....", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38981, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38981, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38981, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38981, 6200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38981, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38981, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38981, 238 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 963212, 965756, "Are you, uh, sure it\'s a new bump?", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 585 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38982, 6200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 965923, 970178, "No offense, but I\'ve always thought\r\nof Ben as a fairly bumpy-headed child.", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 15049 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 21800 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 14215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38983, 1566 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 970344, 973848, "It\'s okay if he bumps his head.\r\nKids bump their heads all the time.", 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 27 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bumps" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 22279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 512 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 6200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 1676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 4043 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38984, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 974015, 977143, "It was your first time babysitting.\r\nYou did your best.", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 361 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "babysitting" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 22280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38985, 315 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 977727, 979729, "-I did!\r\n-I know.", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38986, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38986, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38986, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38986, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 979896, 982231, "You just have to watch them\r\nall the time.", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38987, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 982398, 985234, "I did! I watched! I watched!", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38988, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38988, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38988, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38988, 6753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38988, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38988, 6753 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 985401, 988905, "I watched Monica bang his head\r\nagainst that thing!", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 6753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 22266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 2461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38989, 381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 989906, 991449, "Monica did it?", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38990, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38990, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38990, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 994577, 996412, "-Monica? Urn....\r\n-Yeah.", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38991, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38991, 470 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38991, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 996579, 1000583, "Did you notice anything weird\r\nabout Ben today?", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38992, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38992, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38992, 2454 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38992, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38992, 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38992, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38992, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38992, 125 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1001626, 1002710, "No, why?", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38993, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38993, 148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1002877, 1006964, "I was just playing with him, and we\r\nwere doing the \"Alphabet Song\"...", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 1639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 2490 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38994, 4023 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1007131, 1010176, "...which he\'s good at.\r\nBut he left out \"E\" and \"F.\"", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 18886 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38995, 21502 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1010343, 1014764, "It\'s like they just, uh, I don\'t know,\r\nfell out of his head.", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 9861 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38996, 238 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1016349, 1017433, "Really?", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38997, 140 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1017600, 1020811, "Oh, and also, he\'s walking...", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38998, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38998, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38998, 2494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38998, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38998, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38998, 3254 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1021395, 1022605, "...kind of funny.", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38999, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38999, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 38999, 1737 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1024232, 1028778, "It\'s like his left leg is moving\r\na lot faster than his right leg.", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 419 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 5262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 6206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39000, 419 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1028945, 1032114, "Yeah, he\'s in there,\r\njust sort of, you know....", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 557 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39001, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1034659, 1037787, "Oh, my God! I wrecked your baby!", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39002, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39002, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39002, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39002, 10 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "wrecked" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39002, 22281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39002, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39002, 1636 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1040289, 1043125, "I hope it\'s still funny\r\nwhen you\'re in hell.", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 1737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39003, 143 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1044210, 1045336, "You jerk!", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39004, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39004, 6778 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1045503, 1050633, "-You know I love that kid!\r\n-Monica bang! Monica bang!", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 1649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 22266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39005, 22266 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1051509, 1052927, "Ow.", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39006, 4042 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1053719, 1055680, "I\'ll get the hat.", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39007, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39007, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39007, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39007, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39007, 283 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1059642, 1061727, "I have something to tell you.", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39008, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39008, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39008, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39008, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39008, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39008, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1061894, 1064063, "And I want you to let me\r\nget through it...", 302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39009, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1064230, 1066148, "...because...", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39010, 100 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1066524, 1069026, "...it\'s not gonna be easy.", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39011, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39011, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39011, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39011, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39011, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39011, 1593 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1069819, 1071070, "Okay.", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39012, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1073447, 1075533, "I think you should go back with Gary.", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39013, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39013, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39013, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39013, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39013, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39013, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39013, 12 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "gary" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39013, 22282 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1077535, 1080413, "I don\'t want to be the guy\r\nthat breaks up a family.", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 4705 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39014, 1540 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1081247, 1084709, "You know, when my parents split up,\r\nit was because of that guy.", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 586 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 624 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39015, 9 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1084959, 1087128, "Whenever I\'d see him,\r\nI\'d always think:", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 4727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39016, 298 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1087378, 1090881, "\"You\'re the reason.\r\nYou are why they\'re not together.\"", 310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 3061 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39017, 494 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 310   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1091132, 1094051, "And I hated that guy.", 311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39018, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39018, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39018, 5750 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39018, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39018, 9 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 311   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1095219, 1100141, "And it didn\'t matter how nice he was\r\nor how happy he made my dad.", 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 1499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39019, 3129 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 312   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1104895, 1106689, "Wow!", 313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39020, 477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 313   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1107440, 1109317, "Yeah, wow.", 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39021, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39021, 477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 314   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1110943, 1113362, "-It\'s the right thing to do.\r\n-Ugh.", 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39022, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39022, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39022, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39022, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39022, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39022, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39022, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39022, 3985 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 315   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1113529, 1115448, "You\'re right.", 316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39023, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39023, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39023, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 316   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1116324, 1121662, "Oh, God.\r\nBut before I can say goodbye...", 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39024, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39024, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39024, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39024, 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39024, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39024, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39024, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39024, 5327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 317   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1122204, 1125458, "...there\'s something I really\r\nneed you to know.", 318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39025, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 318   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1125958, 1128586, "The way I feel about you...", 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39026, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39026, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39026, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39026, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39026, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39026, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 319   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1128878, 1131964, "...it\'s like I finally understand...", 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39027, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39027, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39027, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39027, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39027, 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39027, 1477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 320   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1132214, 1134550, "...what Lionel Richie\'s\r\nbeen singing about.", 321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39028, 42 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "lionel" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39028, 22283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39028, 21960 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39028, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39028, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39028, 6212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39028, 208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 321   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1136177, 1138054, "You know? I mean, what we have...", 322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39029, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39029, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39029, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39029, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39029, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39029, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39029, 28 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 322   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1138220, 1141140, "...it\'s like movie love.", 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39030, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39030, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39030, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39030, 4647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39030, 269 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 323   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1142391, 1144310, "You\'re my soul mate.", 324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39031, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39031, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39031, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39031, 8635 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39031, 17112 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 324   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1145353, 1149648, "I can\'t believe we\'re not gonna\r\nspend the rest of our lives together.", 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 1738 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 2545 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 1739 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39032, 494 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 325   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1151484, 1153152, "Then don\'t leave me.", 326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39033, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39033, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39033, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39033, 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39033, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 326   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1154445, 1155488, "What?", 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39034, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 327   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1155655, 1158616, "Forget what I said! I was babbling!\r\nPick me!", 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 1713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 20480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 2492 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39035, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 328   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1158783, 1162495, "No, you were right!\r\nI gotta give my marriage another chance.", 329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 8129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39036, 1644 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 329   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1162661, 1164664, "Pssh! No, you don\'t!", 330 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "pssh" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39037, 22284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39037, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39037, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39037, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39037, 37 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 330   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1164830, 1168668, "No, I say you have to give your divorce\r\nanother chance!", 331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 6236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39038, 1644 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 331   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1171545, 1172880, "I\'m sorry.", 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39039, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39039, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39039, 268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 332   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1177468, 1179595, "Oh, don\'t go.", 333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39040, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39040, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39040, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39040, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 333   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1180262, 1181931, "No, I gotta go.", 334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39041, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39041, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39041, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39041, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 334   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1182765, 1186310, "No. No. No, no.", 335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39042, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39042, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39042, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39042, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 335   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1186477, 1188521, "Honey. Honey. Ha, ha.", 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39043, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39043, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39043, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39043, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 336   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1188688, 1191065, "-People are looking.\r\n-I don\'t care.", 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39044, 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39044, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39044, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39044, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39044, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39044, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39044, 2561 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 337   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1191982, 1193609, "I don\'t care!", 338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39045, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39045, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39045, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39045, 2561 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 338   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1194151, 1196987, "Yeah, um, okay. I\'m-", 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39046, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39046, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39046, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39046, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39046, 62 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 339   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1197154, 1198697, "I\'m leaving now.", 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39047, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39047, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39047, 4695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39047, 165 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 340   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1201158, 1203744, "You can\'t leave! I have your shoe!", 341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39048, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39048, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39048, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39048, 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39048, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39048, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39048, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39048, 279 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 341   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1207498, 1209834, "Goodbye, Chandler Bing.", 342 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39049, 5327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39049, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39049, 444 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 342   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1221303, 1223097, "Rachel has those in burgundy.", 343 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39050, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39050, 1517 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39050, 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39050, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39050, 5709 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 343   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1226851, 1231564, "Remember, something this big and long\r\nwill be very difficult to maneuver.", 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 6143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39051, 3860 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 344   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1231772, 1233816, "Fortunately,\r\nI have experience in that area.", 345 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39052, 11604 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39052, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39052, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39052, 8581 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39052, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39052, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39052, 7290 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 345   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1235192, 1239989, "Can we please focus here?\r\nA naked man\'s life hangs in the balance.", 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 5206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 1645 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 277 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hangs" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 22285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39053, 7769 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 346   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1240239, 1241615, "I\'m telling you, he\'s dead.", 347 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39054, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39054, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39054, 1660 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39054, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39054, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39054, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39054, 1559 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 347   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1241824, 1245327, "What we\'re about to have here\r\nis a dead fat guy on a stick!", 348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 1559 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 3224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39055, 2541 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 348   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1246245, 1249123, "All right, ladies and gentlemen,\r\nlet\'s poke.", 349 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39056, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39056, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39056, 4067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39056, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39056, 4068 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39056, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39056, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39056, 7700 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 349   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1249457, 1250499, "Steady.", 350 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "steady" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39057, 22286 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 350   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1250666, 1252251, "Steady.", 351 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39058, 22286 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 351   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1252418, 1254336, "Okay, a little higher.", 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39059, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39059, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39059, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39059, 18882 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 352   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1254628, 1256213, "Careful of the angle.", 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39060, 1747 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39060, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39060, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39060, 19934 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 353   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1256380, 1259133, "Okay, okay.\r\nWe\'re approaching the window.", 354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39061, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39061, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39061, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39061, 16 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "approaching" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39061, 22287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39061, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39061, 3108 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 354   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1263012, 1265514, "Thread the needle. Thread the needle.", 355 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "thread" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39062, 22288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39062, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39062, 14504 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39062, 22288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39062, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39062, 14504 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 355   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1271187, 1273898, "He\'s alive! He\'s alive!", 356 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39063, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39063, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39063, 2543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39063, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39063, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39063, 2543 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 356   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1275983, 1278402, "And yet we\'re still poking him.", 357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39064, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39064, 573 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39064, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39064, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39064, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39064, 15870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39064, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 357   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1278569, 1281238, "Okay, retract the device!\r\nRetract the device!", 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39065, 46 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "retract" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39065, 22289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39065, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39065, 4707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39065, 22289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39065, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39065, 4707 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 358   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1281739, 1284617, "-He does not look happy.\r\n-Hey, hey.", 359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39066, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39066, 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39066, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39066, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39066, 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39066, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39066, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 359   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1284784, 1287786, "Now he\'s showing us his poking device.", 360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39067, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39067, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39067, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39067, 7268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39067, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39067, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39067, 15870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39067, 4707 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 360   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1288788, 1293334, "Hey, that\'s never gonna make it\r\nall the way over here, buddy! Ha.", 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 2437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39068, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 361   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1298547, 1303844, "<i>I\'ll hold you close in my arms</i>", 362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 2413 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 1531 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 6803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39069, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 362   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1304470, 1309099, "<i>I can\'t resist your charms</i>", 363 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39070, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39070, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39070, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39070, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39070, 9916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39070, 183 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "charms" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39070, 22290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39070, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 363   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1309642, 1311393, "<i>And love</i>", 364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39071, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39071, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39071, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39071, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 364   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1311560, 1313896, "<i>Love</i>", 365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39072, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39072, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39072, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 365   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1314730, 1319818, "<i>I\'ll be a fool for you</i>", 366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 17142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39073, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 366   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1319985, 1322822, "<i>I\'m sure</i>", 367 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39074, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39074, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39074, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39074, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39074, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 367   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1323405, 1326617, "<i>You know I don\'t mind</i>", 368 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39075, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39075, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39075, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39075, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39075, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39075, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39075, 3105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39075, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 368   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1326784, 1329495, "<i>Oh, you know I don\'t mind</i>", 369 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 3105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39076, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 369   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1329662, 1331830, "<i>Yes</i>", 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39077, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39077, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39077, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 370   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1331997, 1336544, "<i>You mean the world to me</i>", 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39078, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39078, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39078, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39078, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39078, 456 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39078, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39078, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39078, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 371   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1336710, 1339922, "<i>-Oh, I know\r\n-I know</i>", 372 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39079, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39079, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39079, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39079, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39079, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39079, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39079, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 372   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1340089, 1342842, "<i>-I\'ve found\r\n-I\'ve found</i>", 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39080, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39080, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39080, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39080, 577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39080, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39080, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39080, 577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39080, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 373   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1343008, 1345261, "<i>In you</i>", 374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39081, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39081, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39081, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39081, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 374   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1345719, 1349265, "<i>My endless love</i>", 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39082, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39082, 95 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "endless" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39082, 22291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39082, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39082, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 375   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1351851, 1355729, "<i>My endless love</i>", 376 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39083, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39083, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39083, 22291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39083, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39083, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 376   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1357565, 1359400, "<i>Love</i>", 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39084, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39084, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39084, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 377   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 111, 1359567, 1362695, "<i>-Lo- Love\r\n-Love</i>", 378 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39085, 10 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "lo" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39085, 22292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39085, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39085, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39085, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/English.srt" ,  `subtitle`.`phrase_number` = 378   WHERE  `subtitle`.`id` = 111 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 56, 111 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 56, 2, "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 2120, 3490, "Пожалуйста, Фибс.", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39086, 1048 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39086, 2577 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 3700, 6410, "Кто хочет моих особых\r\nшоколадно-маковых печеньев?", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39087, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39087, 1466 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39087, 1937 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "особых" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39087, 22293 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шоколадно" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39087, 22294 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "маковых" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39087, 22295 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "печеньев" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39087, 22296 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 6540, 8250, "Я возьму одно.", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39088, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39088, 4305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39088, 908 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 10130, 12670, "- Ну ладно.\r\n- Я не буду.", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39089, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39089, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39089, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39089, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39089, 2591 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 12880, 15010, "Нет - нет. Это мой зуб.", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39090, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39090, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39090, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39090, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39090, 6395 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 15130, 17920, "- Тогда хорошо.\r\n- Я возьму одно.", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39091, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39091, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39091, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39091, 4305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39091, 908 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 18130, 20720, "Так тебе нужен дантист?\r\nУ меня есть один хороший.", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 4461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 16360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 1110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39092, 4752 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 20890, 25270, "Спасибо. У меня тоже хороший есть.\r\nПросто я не могу видеть его.", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 4752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 6609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39093, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 25390, 29390, "Видишь, та же проблема со всеми\r\nневидимыми дантистами.", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39094, 2233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39094, 1957 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39094, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39094, 1822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39094, 676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39094, 6896 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "невидимыми" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39094, 22297 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дантистами" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39094, 22298 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 29980, 31400, "Почему это ты не можешь\r\nсходить к нему?", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39095, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39095, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39095, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39095, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39095, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39095, 13451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39095, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39095, 6966 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 31480, 36740, "Потому, что каждый раз, когда\r\nя иду к дантисту - кто-то умирает.", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 5470 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 1317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 831 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дантисту" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 22299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39096, 5795 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 37650, 40570, "Это очень странно.\r\nВот когда я иду к дантисту...", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 6512 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 1317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39097, 22299 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 40740, 43370, "...то всегда заглядываю\r\nпод блузку гигиенистке.", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39098, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39098, 706 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заглядываю" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39098, 22300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39098, 5782 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39098, 12877 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гигиенистке" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39098, 22301 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 46370, 47580, "Что?", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39099, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 47750, 49410, "Первой была моя тётя Мери.", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39100, 3442 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39100, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39100, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39100, 7808 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мери" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39100, 22302 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 49580, 52170, "А потом был Джон, почтальон.", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39101, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39101, 707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39101, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39101, 18453 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "почтальон" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39101, 22303 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 52250, 55710, "А потом друг ковбой - Альбайно Боб.", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39102, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39102, 707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39102, 2152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39102, 13428 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "альбайно" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39102, 22304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39102, 4201 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 56090, 58420, "И все эти люди уже умерли?", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39103, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39103, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39103, 1094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39103, 1871 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39103, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39103, 17873 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 58510, 60720, "Да. Пока я была в кресле.", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39104, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39104, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39104, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39104, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39104, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39104, 11355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 60840, 64180, "Вот почему я так забочусь о зубах.\r\nВовсе не ради оральной гигиены.", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 679 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "забочусь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 22305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 18627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 1246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 762 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "оральной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 22306 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гигиены" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39105, 22307 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 64300, 67020, "Зубная нитка сохраняет жизни.", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39106, 11353 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нитка" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39106, 22308 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сохраняет" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39106, 22309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39106, 1160 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 67350, 69350, "Да ладно, ты же никого не убила.", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39107, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39107, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39107, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39107, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39107, 1410 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39107, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "убила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39107, 22310 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 69430, 73230, "Так уж случилось, что эти люди\r\nумирали, когда ты была у дантиста.", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 868 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 1094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 1871 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "умирали" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 22311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 710 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дантиста" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39108, 22312 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 73310, 74900, "Это просто совпадение.", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39109, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39109, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39109, 4185 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 74980, 78490, "Ты это им скажи.\r\nО, не можешь! Они же умерли!", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 1921 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 1164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39110, 17873 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 79610, 82780, "Сезон 3, серия 8.\r\nТа, с гигантским тыкательным прибором.", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 1849 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 2622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 1851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 1184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 1957 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 22174 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тыкательным" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 22313 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прибором" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39111, 22314 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 82910, 86030, "Русские субтитры:\r\nDais 2010", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39112, 13306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39112, 13307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39112, 13308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39112, 20269 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 128850, 130720, "Спасибо, Гантер.", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39113, 756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39113, 18701 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 131390, 132930, "Можно мне тоже салфетку?", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39114, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39114, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39114, 1071 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "салфетку" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39114, 22315 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 133060, 136270, "О, как будто тебе\r\nещё чего-то не хватает.", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 739 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 1000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39115, 7469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 141150, 144740, "Фибс, тебе же больно. Может,\r\nвсё-же сходишь к зубному? Сходи.", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 2577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 800 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 22163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 1131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39116, 11678 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 144900, 146950, "Прекрасно. Но если ты будешь\r\nмоей следующей жертвой...", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39117, 2686 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39117, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39117, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39117, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39117, 747 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39117, 2056 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39117, 4419 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "жертвой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39117, 22316 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 147070, 151240, "...не являйся ко мне полтергейстом,\r\nзасасывающим меня в телевизор.", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "являйся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 22317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 1070 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 695 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "полтергейстом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 22318 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "засасывающим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 22319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39118, 1116 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 151490, 152490, "Я обещаю.", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39119, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39119, 7650 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 152620, 155410, "Всё-же, я не уверена,\r\nчто ты не явишься.", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 5160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "явишься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39120, 22320 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 157080, 159290, "Эй, Чендлер здесь?", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39121, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39121, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39121, 827 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 160960, 163090, "Нет. Нет, его нет.", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39122, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39122, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39122, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39122, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 163960, 168760, "Ребята, Джоуи видел, как Дженис\r\nцеловалась с бывшим мужем.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 843 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 4265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 4117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 7648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 21351 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39123, 13943 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 168890, 170050, "Что?", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39124, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 170140, 173220, "Что же ты собираешься делать?\r\nКак ты об этом расскажешь Чендлеру?", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 2060 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 2025 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 861 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39125, 7524 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 173390, 176690, "Я думал об этом. Думаю,\r\nчто лучше всего будет если...", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 2255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39126, 790 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 176810, 178190, "...не говорить.", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39127, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39127, 1202 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 178770, 182110, "Джоуи, если ты знаешь об этом,\r\nтебе следует сказать ему.", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 5028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 1212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39128, 2171 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 182150, 184490, "Это его убьёт.\r\nЭто его просто убьёт.", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39129, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39129, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39129, 3699 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39129, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39129, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39129, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39129, 3699 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 184650, 187780, "Погоди, пока я пойду к зубному.\r\nМожет, я его убью.", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 6820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 2300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 1131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39130, 17688 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 193080, 198460, "Кажется, Голый Парниша ложится в гамак.\r\nОн очень похож на жир-трест.", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 1046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 705 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 20335 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ложится" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 22321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гамак" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 22322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 1152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 20761 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "трест" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39131, 22323 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 198960, 201290, "Я пошла к зубному.\r\nТак что слушайте...", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39132, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39132, 4292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39132, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39132, 1131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39132, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39132, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39132, 3672 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 201420, 205590, "...берегитесь всего,\r\nво что можно упасть...", 48 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "берегитесь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39133, 22324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39133, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39133, 1155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39133, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39133, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39133, 22065 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 205670, 208380, "...или что может упасть на вас...", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39134, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39134, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39134, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39134, 22065 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39134, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39134, 1044 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 208510, 209640, "...или...", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39135, 862 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 209840, 212220, "Хорошо, просто берегитесь!", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39136, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39136, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39136, 22324 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 213470, 215640, "И ещё я хотела бы...", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39137, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39137, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39137, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39137, 1082 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39137, 936 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 215770, 219100, "Просто хочу сказать вам, что...", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39138, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39138, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39138, 1212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39138, 952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39138, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 224280, 227440, "Так. У меня проблема.\r\nНадо сходить на работу на пару часов.", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 1822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 13451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 1318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 3680 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39139, 2201 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 227610, 230450, "Какие-то детки порезвились в макете\r\nгомо сапиенс.", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39140, 1055 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39140, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39140, 1272 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "порезвились" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39140, 22325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39140, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39140, 17670 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гомо" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39140, 22326 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сапиенс" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39140, 22327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 230530, 231620, "Что они сделали?", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39141, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39141, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39141, 11084 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 231700, 235540, "Они написали поверх \"сапиенс\"\r\nчто-то другое.", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39142, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39142, 7481 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поверх" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39142, 22328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39142, 22327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39142, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39142, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39142, 5938 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 235790, 240880, "И изменили позы манекенов.\r\nБольше я вам ничего не скажу.", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 20908 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "позы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 22329 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "манекенов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 22330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39143, 2072 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 241040, 243130, "Хочешь, чтобы я присмотрела за Беном?", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39144, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39144, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39144, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "присмотрела" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39144, 22331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39144, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39144, 14621 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 243250, 245380, "Только что собирался попросить.\r\nСпасибо.", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39145, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39145, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39145, 4807 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39145, 12543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39145, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 245460, 247510, "Эй! Погоди, алё!\r\nА как же я?", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39146, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39146, 6820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39146, 14931 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39146, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39146, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39146, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39146, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 247630, 251430, "Ты. Ты хочешь присмотреть за Беном?", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39147, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39147, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39147, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39147, 6964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39147, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39147, 14621 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 255470, 258060, "Это было бы великолепно!\r\nЯ попросил Монику...", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39148, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39148, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39148, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39148, 5016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39148, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "попросил" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39148, 22332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39148, 11880 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 258180, 260940, "...потому, что знаю, насколько\r\nпуста её жизнь.", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39149, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39149, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39149, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39149, 923 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пуста" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39149, 22333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39149, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39149, 943 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 265440, 267480, "Эй, Росс. У меня научный вопрос.", 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39150, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39150, 798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39150, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39150, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39150, 13638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39150, 1112 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 267650, 273370, "Если гомо сапиенс были\r\nна самом деле... \"гомо\" - сапиенс...", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 22326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 22327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 1979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 1980 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 22326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39151, 22327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 274700, 277910, "...значит они от этого вымерли?", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39152, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39152, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39152, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39152, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39152, 12064 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 280160, 283540, "Джоуи, гомо сапиенс - это люди.", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39153, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39153, 22326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39153, 22327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39153, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39153, 1871 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 283630, 285790, "Эй, да я же не осуждаю.", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39154, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39154, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39154, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39154, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39154, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39154, 3817 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 287880, 289720, "Смотри, Бени! Ложка!", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39155, 1439 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бени" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39155, 22334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39155, 17783 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 290380, 292010, "Ложка!", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39156, 17783 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 293680, 297060, "Всё. Ладно, знаешь что?\r\nЯ думаю, ему скучно.", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39157, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39157, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39157, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39157, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39157, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39157, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39157, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39157, 4140 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 297220, 299560, "Так. Хочешь сыграть\r\nв аэроплан?", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39158, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39158, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39158, 3849 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39158, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39158, 10698 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 299730, 301600, "Давай покажем Рейчел?\r\nИди сюда.", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39159, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39159, 17445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39159, 844 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39159, 1034 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39159, 1357 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 301730, 304310, "Хочешь повеселиться? Давай.", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39160, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39160, 4895 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39160, 1427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 315200, 318410, "Ты такой смелый.\r\nТы такой смелый! Да, смелый.", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39161, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39161, 1310 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "смелый" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39161, 22335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39161, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39161, 1310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39161, 22335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39161, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39161, 22335 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 318490, 320410, "- Ты такой смелый.\r\n- Всё хорошо.", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39162, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39162, 1310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39162, 22335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39162, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39162, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 320580, 322830, "Всё у него прекрасно.\r\nПросто посадим его.", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39163, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39163, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39163, 748 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39163, 2686 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39163, 646 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "посадим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39163, 22336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39163, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 323000, 325960, "Давай сюда, малыш.\r\nДа, ты такой хороший мальчик.", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39164, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39164, 1357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39164, 6514 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39164, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39164, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39164, 1310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39164, 4752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39164, 1296 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 326090, 330340, "Как ты могла так поступить с ним?!\r\nРосс доверил мне! Что он скажет?", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 3016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 7138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 798 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "доверил" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 22337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39165, 5164 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 330510, 332630, "Ничего,..\r\nмы же не расскажем ему.", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39166, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39166, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39166, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39166, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "расскажем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39166, 22338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39166, 2171 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 332760, 333840, "- Не расскажем?\r\n- Нет.", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39167, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39167, 22338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39167, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 333930, 336220, "Хорошо, мне это нравится.", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39168, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39168, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39168, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39168, 3639 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 336850, 339060, "Значит, всё в порядке.\r\nВсё в порядке, так ведь?", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 766 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 766 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39169, 726 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 339180, 341480, "Нет, не в порядке.\r\nВот, шишка.", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39170, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39170, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39170, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39170, 766 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39170, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39170, 12123 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 341600, 342770, "Какая шишка?", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39171, 1353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39171, 12123 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 342940, 345650, "О, боже мой! Затолкай её обратно!\r\nЗатолкай обратно!", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39172, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39172, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39172, 891 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "затолкай" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39172, 22339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39172, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39172, 12851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39172, 22339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39172, 12851 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 345770, 348110, "Я не могу затолкать её обратно!", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39173, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39173, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39173, 930 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "затолкать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39173, 22340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39173, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39173, 12851 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 349280, 351490, "Ладно, нам нужно отвлечь внимание.", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39174, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39174, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39174, 1966 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отвлечь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39174, 22341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39174, 7576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 351610, 352860, "Точно.", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39175, 2128 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 353030, 354240, "Я придумала!", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39176, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39176, 3638 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 354360, 356070, "Только Росс зайдёт сюда...", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39177, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39177, 798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39177, 7962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39177, 1357 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 356200, 358990, "...хватай его и тащи в спальню,\r\nа там делай что хочешь...", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 1190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 15120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 19678 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 2000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39178, 1035 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 359120, 361580, "...чтобы он аж застонал!", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39179, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39179, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39179, 2985 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "застонал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39179, 22342 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 363040, 364370, "Или...", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39180, 862 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 366040, 368090, "...можем надеть шляпу на голову.", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39181, 4091 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39181, 3001 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39181, 16580 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39181, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39181, 2311 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 368250, 373130, "Шляпу! Да! Нам нужна шляпа!\r\nГде нам взять маленькую шляпку?", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 16580 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 955 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 954 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 1141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 10637 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шляпку" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39182, 22343 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 373260, 374970, "Я возьму Дождливого медвежонка!", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39183, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39183, 4305 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дождливого" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39183, 22344 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "медвежонка" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39183, 22345 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 375050, 377470, "Ведь он знает, что делать?", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39184, 726 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39184, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39184, 2192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39184, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39184, 2025 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 377760, 379600, "О, боже мой.\r\nТы - гений!", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39185, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39185, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39185, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39185, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гений" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39185, 22346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 379640, 381520, "Она, вроде, пришита.", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39186, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39186, 1806 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пришита" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39186, 22347 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 381640, 383600, "Дай мне. Дай мне.", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39187, 1358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39187, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39187, 1358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39187, 695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 387730, 391440, "Здесь просто\r\nкровавая баня сегодня!", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39188, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39188, 646 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кровавая" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39188, 22348 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "баня" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39188, 22349 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39188, 749 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 395990, 399160, "Эй, погоди-ка секунду.\r\nПогоди секунду.", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39189, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39189, 6820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39189, 2653 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39189, 3499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39189, 6820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39189, 3499 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 399990, 402490, "Тебе не кажется, что эти\r\nбусы прекрасны?", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39190, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39190, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39190, 1046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39190, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39190, 1094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39190, 21477 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прекрасны" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39190, 22350 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 402620, 405500, "По мне, так лучше\r\nгорный велосипед.", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39191, 819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39191, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39191, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39191, 991 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "горный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39191, 22351 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39191, 22128 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 405790, 410340, "Скоро день рождения Дженис.\r\nЯ хочу подарить ей что-то особенное.", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 2625 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 1032 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 3373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 4117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 4148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 840 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39192, 13330 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 410590, 413210, "- Пошли зайдём вместе.\r\n- Погоди!", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39193, 3780 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "зайдём" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39193, 22352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39193, 650 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39193, 6820 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 415260, 417840, "Хочешь подарить что-то особенное?\r\nПодари ей цветы.", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39194, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39194, 4148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39194, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39194, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39194, 13330 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подари" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39194, 22353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39194, 840 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39194, 3513 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 418010, 420970, "Подари леденцов. Подари жвачку.\r\nДевчонки любят жвачку.", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39195, 22353 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "леденцов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39195, 22354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39195, 22353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39195, 2981 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39195, 3453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39195, 12118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39195, 2981 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 421100, 425770, "Хорошая идея. \"Дорогая Дженис:\r\nВот тебе Хуба-Буба день рожденья\".", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 4490 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 4515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 11698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 4117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 753 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "хуба" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 22355 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "буба" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 22356 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 1032 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39196, 14366 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 426100, 428690, "Хотелось бы подарить ей\r\nчто-то серьёзное.", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39197, 5636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39197, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39197, 4148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39197, 840 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39197, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39197, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39197, 16346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 428850, 431020, "Хочешь чего-то серьёзного.", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39198, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39198, 1000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39198, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39198, 5600 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 431190, 434400, "Знаешь, что сделай?\r\nПодари ей одну из этих...", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39199, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39199, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39199, 4228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39199, 22353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39199, 840 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39199, 2837 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39199, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39199, 804 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 434530, 436860, "...бариевых клизм.", 115 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бариевых" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39200, 22357 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "клизм" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39200, 22358 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 438360, 440370, "Это чертовски серьёзно.", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39201, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39201, 12021 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39201, 774 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 440530, 445040, "Так, я пойду туда.\r\nА ты никогда ничего мне не покупай!", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 2300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 4077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "покупай" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39202, 22359 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 445200, 447920, "Тебе нельзя! Тебе нельзя.\r\nТебе нельзя покупать ей бусы.", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 7887 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 840 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39203, 21477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 448000, 450540, "Просто нельзя, нельзя,\r\nнельзя!", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39204, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39204, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39204, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39204, 1393 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 450630, 452290, "Почему нет?", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39205, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39205, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 453880, 455550, "О, боже.", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39206, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39206, 860 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 457010, 458340, "Такое дело.", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39207, 715 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39207, 1154 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 459470, 461430, "Такое дело.\r\nЛадно, дело в том...", 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39208, 715 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39208, 1154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39208, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39208, 1154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39208, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39208, 1823 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 461550, 463260, "Что за дело?", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39209, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39209, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39209, 1154 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 464560, 467850, "Я ходил в магазин\r\nМатрассного Короля...", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39210, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39210, 1302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39210, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39210, 4971 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "матрассного" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39210, 22360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39210, 15900 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 468020, 471310, "...и видел, как Дженис целовалась\r\nс бывшим мужем.", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39211, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39211, 4265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39211, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39211, 4117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39211, 7648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39211, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39211, 21351 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39211, 13943 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 472820, 474020, "Что?", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39212, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 474400, 477110, "Они были в его кабинете.", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39213, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39213, 876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39213, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39213, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39213, 9153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 479240, 482240, "Она не могла этого сделать.\r\nОна же со мной.", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 3016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 2250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39214, 1368 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 482410, 484120, "Говорю тебе, я видел.", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39215, 956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39215, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39215, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39215, 4265 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 484240, 486200, "Чтож, ты ошибся.\r\nТы ошибся.", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39216, 14090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39216, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39216, 4233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39216, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39216, 4233 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 486370, 487960, "Я не ошибся.", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39217, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39217, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39217, 4233 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 488080, 491040, "Хотел бы я ошибиться.", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39218, 1016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39218, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39218, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ошибиться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39218, 22361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 492040, 493670, "Сожалею.", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39219, 14879 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 497670, 501890, "А сейчас бариевая клизма\r\nуже кажется в самый раз. А?", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 752 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бариевая" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 22362 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "клизма" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 22363 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 1046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 2835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39220, 666 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 502890, 508350, "Теперь это кажется чем-то целым.\r\nИ отводит внимание от шляпы.", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 1046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 7891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отводит" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 22364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 7576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39221, 5426 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 508770, 511730, "О! Вы живы!\r\nВы живы!", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39222, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39222, 783 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "живы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39222, 22365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39222, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39222, 22365 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 513190, 515820, "Я же обещала тебе, что никто\r\nне умрёт, правда?", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 9319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 1279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 12795 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39223, 780 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 515940, 519490, "Это мы ещё посмотрим. Могу я позвонить\r\nс вашего телефона всем своим знакомым?", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 3562 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 2033 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 3552 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 10944 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 1444 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 677 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "знакомым" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39224, 22366 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 519610, 522450, "Конечно. Денег у нас всё равно нет.\r\nДавай.", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39225, 775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39225, 971 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39225, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39225, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39225, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39225, 1818 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39225, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39225, 1427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 523620, 526830, "Эй, вы не умерли!\r\nЛадно, увидимся.", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39226, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39226, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39226, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39226, 17873 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39226, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39226, 2581 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 529080, 530920, "Моника.", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39227, 826 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 531080, 533670, "О, боже мой! Он сказал моё имя!", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39228, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39228, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39228, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39228, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39228, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39228, 1948 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39228, 2212 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 533790, 535630, "Ты слышала?", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39229, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39229, 9389 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 536250, 538380, "Моника бум!", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39230, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39230, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 541340, 543550, "Ладно, я слышала.", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39231, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39231, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39231, 9389 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 543970, 546220, "Он сказал \"Моника бум\"?", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39232, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39232, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39232, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39232, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 546310, 549730, "О, боже мой! Он меня сдаст!", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39233, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39233, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39233, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39233, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39233, 711 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сдаст" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39233, 22367 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 549810, 551850, "Моника бум!", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39234, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39234, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 552940, 556320, "Дорогой, тебе нужно перестать\r\nговорить это.", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39235, 736 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39235, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39235, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39235, 18181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39235, 1202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39235, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 556440, 559030, "Ничего не случилось.\r\nДаже вспоминать не о чём.", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39236, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39236, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39236, 868 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39236, 684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39236, 20348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39236, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39236, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39236, 890 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 559150, 561280, "Видишь ли, мы все так делаем.", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39237, 2233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39237, 1234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39237, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39237, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39237, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39237, 9750 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 561490, 563200, "Понимаешь? Смотри.", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39238, 1213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39238, 1439 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 564820, 567910, "Моника бум!\r\nВсе кругом бум!", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39239, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39239, 4475 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39239, 681 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кругом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39239, 22368 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39239, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 567990, 569160, "Бен бум!", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39240, 12546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39240, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 569200, 570960, "Рейчел бум!", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39241, 844 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39241, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 571120, 573420, "Бум, Рейчел! Бум!", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39242, 4475 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39242, 844 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39242, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 573670, 576420, "- О, разве не смешно?\r\n- Только посмотри!", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39243, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39243, 2177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39243, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39243, 4466 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39243, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39243, 1992 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 576670, 577880, "Мы все так делаем!", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39244, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39244, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39244, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39244, 9750 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 578170, 580970, "- Так, с меня хватит.\r\n- Ты в порядке?", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39245, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39245, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39245, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39245, 951 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39245, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39245, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39245, 766 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 581090, 585340, "Знаешь, это всё-таки больно,\r\nоно того не стоит.", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 1029 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 800 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 1974 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 1151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39246, 4096 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 595860, 598440, "Как мой Бингелинг?", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39247, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39247, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39247, 21100 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 601940, 603530, "Не знаю.", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39248, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39248, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 603820, 605780, "Ты мне расскажи.", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39249, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39249, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39249, 3568 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 606200, 609330, "Что-нибудь хочешь рассказать мне?", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39250, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39250, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39250, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39250, 2113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39250, 695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 609450, 611250, "Потому что если ты...\r\nТебе следовало бы...", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39251, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39251, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39251, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39251, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39251, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39251, 7616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39251, 936 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 611370, 614080, "Тебе следовало бы рассказать мне.", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39252, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39252, 7616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39252, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39252, 2113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39252, 695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 615040, 618380, "Почему у тебя такие\r\nбольшие глаза?", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39253, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39253, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39253, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39253, 1386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39253, 2076 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39253, 709 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 618630, 620670, "Это ты скажи мне.", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39254, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39254, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39254, 1164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39254, 695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 620840, 624050, "Может от того, что я заигрывал\r\nс моим бывшим.", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 1151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заигрывал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 22369 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 764 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39255, 21351 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 624220, 626300, "О, нет-нет! Это же была ты!", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39256, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39256, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39256, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39256, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39256, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39256, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39256, 654 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 626390, 627430, "О, боже мой!", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39257, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39257, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39257, 891 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 627550, 629600, "- Хорошо!\r\n- Как ты узнал?", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39258, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39258, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39258, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39258, 8264 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 629720, 632060, "Джоуи сказал мне.\r\nОн видел, как вы целовались.", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39259, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39259, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39259, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39259, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39259, 4265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39259, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39259, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39259, 15160 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 632140, 633810, "В парке?", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39260, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39260, 1290 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 634640, 637650, "Нет. В кабинете.\r\nСколько же всего было поцелуев?", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39261, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39261, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39261, 9153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39261, 1132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39261, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39261, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39261, 742 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поцелуев" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39261, 22370 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 637810, 639020, "Только эти два!", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39262, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39262, 1094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39262, 1223 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 639060, 640570, "Почему?", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39263, 661 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 640730, 643570, "Почему вобще целовались?\r\nНельзя было вобще целоваться!", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39264, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39264, 13329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39264, 15160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39264, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39264, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39264, 13329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39264, 11381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 643690, 647280, "О, извини, дорогой.\r\nЯ так, так...", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39265, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39265, 1447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39265, 736 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39265, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39265, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39265, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 647570, 650240, "Я так извиняюсь. Я просто... а!..", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39266, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39266, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39266, 1218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39266, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39266, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39266, 666 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 651740, 654710, "То, что случилось, было... Я...\r\nЯ не могу дышать.", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 868 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 930 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дышать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39267, 22371 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 654830, 658040, "Дай мне мешок какой-нибудь!", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39268, 1358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39268, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39268, 6884 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39268, 657 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39268, 658 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 658420, 660250, "Держи. Держи.", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39269, 6386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39269, 6386 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 664670, 665630, "Это чек.", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39270, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39270, 3418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 665760, 667260, "Дай сюда!", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39271, 1358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39271, 1357 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 668510, 670800, "Слушай, я должен знать.", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39272, 2085 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39272, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39272, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39272, 789 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 673180, 675350, "Ты разлюбила меня?", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39273, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разлюбила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39273, 22372 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39273, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 677560, 679810, "Ты разлюбила его?", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39274, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39274, 22372 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39274, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 683440, 686200, "Ты ещё любишь его?", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39275, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39275, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39275, 2243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39275, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 688610, 690820, "Ты ещё любишь меня?", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39276, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39276, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39276, 2243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39276, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 692360, 694360, "Хорошо, слушай...", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39277, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39277, 2085 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 694610, 697320, "...мне нужен ясный ответ, понятно?", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39278, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39278, 4461 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ясный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39278, 22373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39278, 6979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39278, 4409 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 697490, 699570, "Каков он?", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39279, 16900 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39279, 667 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 700780, 702700, "Он или я?", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39280, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39280, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39280, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 711210, 713380, "Я не знаю.", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39281, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39281, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39281, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 715460, 719130, "Так, если ты жив,\r\nто бери трубку!", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39282, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39282, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39282, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "жив" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39282, 22374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39282, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39282, 7976 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39282, 721 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 723370, 727840, "Ладно, Бен, я не скажу папе, что\r\nты ел мороженое перед обедом...", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 12546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 2072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 16379 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ел" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 22375 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мороженое" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 22376 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 1962 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обедом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39283, 22377 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 727960, 731840, "...если ты не расскажешь ему о\r\nнашем стукательном инциденте.", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 861 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 4731 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стукательном" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 22378 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "инциденте" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39284, 22379 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 731970, 735430, "Моника, первое - я не думаю, что Бен\r\nпонимает сам принцип подкупа.", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 7981 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 12546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 13200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 1039 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "принцип" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 22380 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подкупа" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39285, 22381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 735550, 737680, "А второе - я...", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39286, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39286, 1211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39286, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 739640, 740560, "Что?", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39287, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 740720, 743350, "Ты сказала \"второе\".", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39288, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39288, 973 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39288, 1211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 745940, 747940, "Я также говорила и \"первое\".", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39289, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39289, 13957 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39289, 1027 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39289, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39289, 7981 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 748150, 749570, "Я знаю.", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39290, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39290, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 752030, 753190, "Привет всем!", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39291, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39291, 1444 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 753320, 755530, "Как мой маленький мальчик?", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39292, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39292, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39292, 4258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39292, 1296 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 755660, 757450, "Он прекрасно.\r\nПравда, Бен?", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39293, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39293, 2686 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39293, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39293, 12546 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 757570, 762000, "- Никогда не было лучше.\r\n- Чем занимались, играли в китобоев?", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 12574 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 2070 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "китобоев" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39294, 22382 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 763460, 764620, "Моника.", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39295, 826 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 764710, 769420, "О, боже мой! Он сказал твоё имя!\r\nВеликолепно! Молодец, Бен!", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 4249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 2212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 5016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 1225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39296, 12546 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 770050, 771670, "Моника бум!", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39297, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39297, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 772630, 773720, "Правильно.", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39298, 5133 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 773840, 777550, "Такой звук был бы, если бы\r\nя взорвалась.", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39299, 1310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39299, 7002 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39299, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39299, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39299, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39299, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39299, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39299, 7077 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 778680, 781350, "Проклятье снято!\r\nЯ обзвонила всех, кого знаю...", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39300, 16270 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "снято" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39300, 22383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39300, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обзвонила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39300, 22384 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39300, 2833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39300, 918 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39300, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 781470, 784140, "...и все живы!", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39301, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39301, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39301, 22365 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 788650, 792440, "Голый Уродец\r\nуж слишком неподвижен.", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39302, 705 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39302, 10101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39302, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39302, 1323 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "неподвижен" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39302, 22385 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 797660, 799530, "Боже мой, я убила ещё одного!", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39303, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39303, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39303, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39303, 22310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39303, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39303, 6953 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 799620, 803790, "Проклятье стало сильнее, раз смогло\r\nсвалить кого-то таких размеров.", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 16270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 4139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 16092 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 1038 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "смогло" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 22386 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "свалить" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 22387 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 918 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 1019 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "размеров" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39304, 22388 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 803910, 805910, "Может, он просто задремал.", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39305, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39305, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39305, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39305, 12677 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 806080, 808710, "Я говорю вам, он не двигается\r\nуже с самого утра.", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "двигается" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 22389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 2941 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39306, 7534 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 808880, 810500, "Нам нужно позвонить куда-нибудь.", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39307, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39307, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39307, 2033 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39307, 911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39307, 658 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 810590, 813880, "Сказать им, что голый парень, на\r\nкоторого мы пялимся - не двигается?", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 1212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 1921 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 705 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 836 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 2160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пялимся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 22390 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39308, 22389 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 814010, 816380, "Нам надо узнать жив ли он.", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39309, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39309, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39309, 7376 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39309, 22374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39309, 1234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39309, 667 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 816470, 818930, "Как мы это сделаем?\r\nЭто невозможно.", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39310, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39310, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39310, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39310, 4132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39310, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39310, 2861 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 819050, 820850, "Ну, есть один способ.", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39311, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39311, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39311, 1110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39311, 8312 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 821010, 823890, "Его окно открыто.\r\nЯ предлагаю...", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39312, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39312, 3762 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39312, 13941 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39312, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39312, 2283 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 824020, 825850, "...потыкать его.", 228 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "потыкать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39313, 22391 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39313, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 831150, 833690, "Мы же собирали палочки...", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39314, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39314, 778 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "собирали" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39314, 22392 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "палочки" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39314, 22393 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 833820, 836610, "...безо всякого повода,\r\nкогда брали китайскую еду?", 230 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "безо" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39315, 22394 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39315, 2758 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "повода" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39315, 22395 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39315, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39315, 5597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39315, 12764 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39315, 5889 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 837820, 840200, "Вот, теперь появился повод.", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39316, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39316, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39316, 20677 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "повод" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39316, 22396 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 841030, 842080, "Что?", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39317, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 842200, 846660, "Мы собираем очень длинный\r\nтыкательный прибор.", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39318, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "собираем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39318, 22397 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39318, 723 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "длинный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39318, 22398 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тыкательный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39318, 22399 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39318, 11433 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 847160, 848790, "Хорошо.", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39319, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 851250, 853340, "Эй, что такое?", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39320, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39320, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39320, 715 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 855960, 857880, "Я говорил с Дженис.", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39321, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39321, 4806 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39321, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39321, 4117 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 858010, 861180, "О, боже мой. Она хочет\r\nвернуться к нему?", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39322, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39322, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39322, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39322, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39322, 1466 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39322, 2638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39322, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39322, 6966 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 861340, 863050, "Она не знает.", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39323, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39323, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39323, 2192 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 863220, 865520, "Говорит, что любит нас обоих.", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39324, 4254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39324, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39324, 1236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39324, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39324, 5126 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 866430, 869270, "Проснувшись этим утром,\r\nя был влюблён.", 240 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "проснувшись" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39325, 22400 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39325, 1138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39325, 1982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39325, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39325, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39325, 1451 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 869390, 871690, "Я был счастлив!", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39326, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39326, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39326, 2257 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 872730, 876530, "Из-за этого я купил\r\n12 пачек презиков.", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39327, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39327, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39327, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39327, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39327, 11259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39327, 3776 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пачек" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39327, 22401 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "презиков" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39327, 22402 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 877820, 882660, "А теперь я не могу их вернуть,\r\nведь она чуть не подавилась чеком!", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 1935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 5183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 726 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 664 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подавилась" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 22403 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "чеком" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39328, 22404 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 884830, 887580, "И что ты собираешься делать?", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39329, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39329, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39329, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39329, 2060 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39329, 2025 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 889910, 891710, "Я не знаю.", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39330, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39330, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39330, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 892460, 895380, "Знаешь? А что бы ты сделал?", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39331, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39331, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39331, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39331, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39331, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39331, 2854 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 896920, 898760, "Это не важно, что я бы сделал.", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39332, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39332, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39332, 935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39332, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39332, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39332, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39332, 2854 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 898880, 901590, "Да, ладно. Скажи мне.", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39333, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39333, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39333, 1164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39333, 695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 903550, 905720, "Хорошо...", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39334, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 906510, 909730, "...ты, наверное не захочешь\r\nэто услышать...", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39335, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39335, 1940 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39335, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39335, 4304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39335, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39335, 5484 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 911650, 915440, "...но если бы это был я,\r\nа это только я...", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39336, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 915520, 916480, "...я бы отошёл.", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39337, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39337, 936 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отошёл" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39337, 22405 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 916610, 917690, "Что?", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39338, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 920070, 922200, "О чём это ты?", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39339, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39339, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39339, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39339, 654 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 925160, 926990, "У них же есть ребёнок...", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39340, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39340, 1388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39340, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39340, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39340, 2150 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 927120, 928750, "...понимаешь?", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39341, 1213 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 929790, 932370, "Они же, вроде, семья.", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39342, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39342, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39342, 1806 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39342, 1894 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 932500, 934420, "И раз уж,.. я не знаю...", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39343, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39343, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39343, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39343, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39343, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39343, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 934500, 937960, "...есть шанс, что у них\r\nвсё сладится...", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39344, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39344, 5152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39344, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39344, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39344, 1388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39344, 681 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сладится" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39344, 22406 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 938090, 941840, "...я бы не хотел быть тем,\r\nкто им помешает.", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 1016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 1975 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 1921 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39345, 5141 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 945640, 947470, "Ты в порядке?", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39346, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39346, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39346, 766 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 950980, 954190, "Хочешь пойти потыкать\r\nголого парнишу?", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39347, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39347, 2023 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39347, 22391 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39347, 6606 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "парнишу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39347, 22407 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 955940, 958860, "Чтож, он наконец-то заснул.", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39348, 14090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39348, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39348, 3436 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39348, 835 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заснул" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39348, 22408 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 959110, 962780, "Кстати о той шишке на его голове...", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39349, 1877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39349, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39349, 3491 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шишке" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39349, 22409 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39349, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39349, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39349, 906 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 963240, 965700, "Ты уверен, что это новая шишка?", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39350, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39350, 6588 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39350, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39350, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39350, 9996 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39350, 12123 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 965820, 970250, "Я не настаиваю, но я не думаю, что\r\nБен очень шишкоголовый ребёнок.", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 13094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 12546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 723 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шишкоголовый" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 22410 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39351, 2150 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 970410, 974000, "Это нормально, если он стукается головой.\r\nДети часто бьются головой.", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 2198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 667 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стукается" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 22411 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 7098 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 6882 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 3630 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бьются" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 22412 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39352, 7098 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 974120, 977420, "Это был твой первый раз присмотра за ним.\r\nТы сделала, что смогла.", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 1330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 2807 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 1038 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "присмотра" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 22413 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 7627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39353, 7934 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 977540, 978670, "Конечно!", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39354, 775 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 978800, 979710, "Я понимаю.", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39355, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39355, 799 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 979840, 982220, "За ними нужно следить всегда.", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39356, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39356, 2701 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39356, 1966 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "следить" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39356, 22414 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39356, 706 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 982340, 985220, "Я так и делала! Я следила!", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39357, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39357, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39357, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39357, 4350 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39357, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39357, 21153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 985390, 989430, "Я следила, как Моника стукнула\r\nего головой об эту штуку!", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 21153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 826 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стукнула" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 22415 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 7098 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 3012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39358, 6064 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 989680, 991470, "Моника стукнула?", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39359, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39359, 22415 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 995900, 1000480, "Моника. Ты не замечала сегодня\r\nстранностей у Бена?", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39360, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39360, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39360, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39360, 14847 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39360, 749 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "странностей" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39360, 22416 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39360, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39360, 13387 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1001480, 1002690, "Нет, а что?", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39361, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39361, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39361, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1002820, 1006870, "Просто я игрался с ним сегодня,\r\nмы пели Алфавитную песенку...", 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "игрался" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 22417 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пели" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 22418 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "алфавитную" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 22419 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39362, 20778 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1006950, 1009950, "...которая у него получается хорошо.\r\nНо он пропустил \"И\" и \"Ф\".", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 1959 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 748 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 1215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 7076 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39363, 16989 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1010120, 1014500, "Вроде как,.. не знаю,\r\nони выпали у него из головы.", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 1806 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 2022 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выпали" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 22420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 748 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39364, 8805 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1016290, 1017460, "Правда?", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39365, 780 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1017580, 1021170, "А,.. и ещё, он ходит...", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39366, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39366, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39366, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39366, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39366, 7022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1021340, 1023210, "...как-то смешно.", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39367, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39367, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39367, 4466 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1024220, 1028760, "Похоже, что его левая нога движется\r\nнамного быстрее, чем правая.", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 1899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 11055 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 3847 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 8651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 1969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 6534 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39368, 3846 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1028890, 1031600, "Да,.. он там,.. так...", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39369, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39369, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39369, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39369, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1034560, 1038360, "О, боже мой! Я искалечила\r\nтвоего ребёнка!", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39370, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39370, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39370, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39370, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "искалечила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39370, 22421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39370, 4217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39370, 2213 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1040270, 1043740, "Надеюсь будет так же смешно,\r\nкогда ты будешь в аду.", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 855 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 4466 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 747 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39371, 11449 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1044110, 1045360, "Придурок!", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39372, 1456 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1045490, 1051370, "- Ты знаешь, как я люблю это дитя!\r\n- Моника бум! Моника бум!", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 934 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дитя" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 22422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 4475 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39373, 4475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1053660, 1055750, "Пойду за шляпой.", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39374, 2300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39374, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39374, 16583 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1059630, 1061630, "Дженис, я должен сказать тебе что-то.", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39375, 4117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39375, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39375, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39375, 1212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39375, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39375, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39375, 835 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1061670, 1063960, "И я хочу, чтобы ты дала\r\nмне высказаться...", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39376, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39376, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39376, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39376, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39376, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39376, 1988 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39376, 695 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "высказаться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39376, 22423 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1064130, 1065880, "...потому...", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39377, 1305 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1066470, 1069390, "...что это не будет легко.", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39378, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39378, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39378, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39378, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39378, 7550 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1073310, 1076520, "Я думаю, ты должна вернуться к Гари.", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39379, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39379, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39379, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39379, 1058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39379, 2638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39379, 831 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гари" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39379, 22424 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1077480, 1081110, "Я не хочу быть тем,\r\nкто разрушает семьи.", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39380, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39380, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39380, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39380, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39380, 1975 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39380, 915 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разрушает" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39380, 22425 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39380, 19361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1081230, 1084780, "Понимаешь, мои родители\r\nрасстались из-за одного парня.", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39381, 1213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39381, 1097 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39381, 1923 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39381, 3791 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39381, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39381, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39381, 6953 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39381, 2752 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1084900, 1087150, "И тогда, когда я видел его,\r\nЯ всгда думал...", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 4265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "всгда" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 22426 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39382, 2255 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1087320, 1090950, "...\"Ты причина всего.\r\nИз-за тебя они не вместе\".", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 11893 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39383, 650 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1091070, 1093950, "И я ненавидел того парня.", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39384, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39384, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39384, 13660 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39384, 1151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39384, 2752 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1095120, 1101420, "И было не важно, насколько он вежлив\r\nили как счаслив с ним был мой отец.", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 923 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 667 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вежлив" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 22427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 787 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "счаслив" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 22428 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39385, 4147 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1110720, 1113050, "Это будет правильно.", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39386, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39386, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39386, 5133 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1113600, 1115640, "Ты прав.", 302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39387, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39387, 3769 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1116270, 1121690, "О, боже.\r\nНо перед тем, как я скажу прощай...", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 1962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 1975 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 2072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39388, 1423 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1122190, 1125690, "...я должна сказать тебе\r\nвот что.", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39389, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39389, 1058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39389, 1212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39389, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39389, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39389, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1125940, 1128650, "То, что я чувствую к тебе...", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39390, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39390, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39390, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39390, 4412 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39390, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39390, 753 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1128820, 1131990, "...похоже,.. я наконец, поняла...", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39391, 1899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39391, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39391, 3436 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39391, 885 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1132160, 1135330, "...о чём пел Лайонел Ричи.", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39392, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39392, 890 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пел" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39392, 22429 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "лайонел" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39392, 22430 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39392, 22043 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1136120, 1138000, "Понимаешь?\r\nЯ думаю, что у нас...", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39393, 1213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39393, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39393, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39393, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39393, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39393, 1210 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1138160, 1141710, "...любовь как в кино.", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39394, 5455 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39394, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39394, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39394, 5013 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1142330, 1145210, "Ты вторая половинка моей души.", 310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39395, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39395, 13303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39395, 11078 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39395, 2056 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "души" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39395, 22431 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 310   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1145300, 1151050, "Я не могу поверить в то, что мы\r\nне проживём остаток дней вместе.", 311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 4993 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "проживём" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 22432 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "остаток" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 22433 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 3390 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39396, 650 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 311   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1151430, 1154050, "Тогда не покидай меня.", 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39397, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39397, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "покидай" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39397, 22434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39397, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 312   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1154510, 1155510, "Что?", 313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39398, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 313   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1155600, 1158680, "Забудь, что я говорил! Я бредил!\r\nВыбери меня!", 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39399, 10230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39399, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39399, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39399, 4806 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39399, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бредил" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39399, 22435 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39399, 7960 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39399, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 314   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1158770, 1162940, "Нет, ты был прав! Я должна дать\r\nсвоему браку ещё один шанс.", 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 3769 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 1058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 2881 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 20875 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "браку" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 22436 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 1110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39400, 5152 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 315   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1163100, 1165110, "Нет, не должна!", 316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39401, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39401, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39401, 1058 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 316   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1165360, 1169740, "Предлагаю дать ещё один\r\nшанс твоему разводу!", 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39402, 2283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39402, 2881 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39402, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39402, 1110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39402, 5152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39402, 4790 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разводу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39402, 22437 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 317   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1171490, 1173240, "Извини.", 318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39403, 1447 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 318   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1178200, 1179830, "Не уходи.", 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39404, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39404, 5095 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 319   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1180080, 1182370, "Нет, я должна идти.", 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39405, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39405, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39405, 1058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39405, 1955 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 320   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1186340, 1188210, "Дорогой. Дорогой.", 321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39406, 736 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39406, 736 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 321   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1188670, 1191760, "- Люди смотрят.\r\n- Мне плевать.", 322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39407, 1871 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39407, 11685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39407, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39407, 3043 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 322   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1191930, 1193800, "Мне плевать!", 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39408, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39408, 3043 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 323   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1194140, 1195800, "Да, ладно.", 324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39409, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39409, 1037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 324   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1196970, 1198850, "Я ухожу.", 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39410, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39410, 8078 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 325   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1201100, 1204810, "Ты не можешь уйти!\r\nУ меня твоя туфля!", 326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39411, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39411, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39411, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39411, 7882 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39411, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39411, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39411, 1879 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "туфля" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39411, 22438 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 326   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1207480, 1210360, "Прощай, Чендлер Бинг.", 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39412, 1423 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39412, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39412, 7365 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 327   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1221250, 1223960, "Рейчел такие одевает к бургундскому.", 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39413, 844 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39413, 1386 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "одевает" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39413, 22439 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39413, 831 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бургундскому" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39413, 22440 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 328   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1226790, 1231630, "Запомните, такими большими и длинными\r\nвещами очень трудно маневрировать.", 329 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "запомните" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 22441 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 2109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 8493 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "длинными" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 22442 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 18720 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 1206 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "маневрировать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39414, 22443 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 329   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1231720, 1235010, "К счастью, у меня есть опыт\r\nв этой области.", 330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 4844 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 671 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "опыт" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 22444 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 5026 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39415, 8228 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 330   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1235260, 1240140, "Можете не отвлекаться там? Жизнь\r\nголого человека зависит от балланса.", 331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 2804 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отвлекаться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 22445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 943 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 6606 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 1014 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 8669 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 730 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "балланса" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39416, 22446 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 331   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1240180, 1241640, "Говорю же, он мёртв.", 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39417, 956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39417, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39417, 667 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мёртв" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39417, 22447 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 332   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1241770, 1246150, "Скоро у нас появится толстенький\r\nмертвячок на палочке!", 333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39418, 2625 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39418, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39418, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39418, 8402 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "толстенький" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39418, 22448 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мертвячок" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39418, 22449 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39418, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39418, 9118 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 333   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1246230, 1249270, "Хорошо, леди и джентльмены,\r\nначинаем тыкать.", 334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39419, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39419, 16002 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39419, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "джентльмены" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39419, 22450 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39419, 5075 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39419, 9628 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 334   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1249360, 1250320, "Осторожно.", 335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39420, 2239 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 335   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1250440, 1251860, "Осторожно.", 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39421, 2239 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 336   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1252320, 1254320, "Так,.. немного выше.", 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39422, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39422, 1119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39422, 19042 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 337   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1254570, 1256200, "Следите за углом.", 338 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "следите" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39423, 22451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39423, 870 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "углом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39423, 22452 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 338   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1256320, 1259530, "Хорошо, хорошо.\r\nМы достигли окна.", 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39424, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39424, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39424, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "достигли" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39424, 22453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39424, 4926 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 339   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1263000, 1265670, "Направляем наконечник.\r\nНаправляем наконечник.", 340 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "направляем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39425, 22454 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "наконечник" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39425, 22455 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39425, 22454 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39425, 22455 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 340   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1271170, 1274050, "Он жив! Он жив!", 341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39426, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39426, 22374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39426, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39426, 22374 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 341   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1275930, 1278350, "А мы его тычем до сих пор.", 342 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39427, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39427, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39427, 933 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тычем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39427, 22456 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39427, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39427, 3498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39427, 1200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 342   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1278510, 1281600, "Ладно, убираем прибор!\r\nУбираем прибор!", 343 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39428, 1037 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "убираем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39428, 22457 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39428, 11433 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39428, 22457 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39428, 11433 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 343   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1281770, 1284480, "Он не выглядит счастливым.", 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39429, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39429, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39429, 4262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39429, 14308 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 344   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1284560, 1288650, "Теперь он показывает нам\r\nсвой тыкательный прибор.", 345 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39430, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39430, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39430, 12863 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39430, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39430, 2928 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39430, 22399 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39430, 11433 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 345   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1288770, 1293990, "Эй, ты этим сюда никогда\r\nне достанешь, приятель!", 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39431, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39431, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39431, 1138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39431, 1357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39431, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39431, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "достанешь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39431, 22458 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39431, 3647 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 346   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1298740, 1303330, "Я обнимаю тебя,", 347 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39432, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обнимаю" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39432, 22459 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39432, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 347   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1304450, 1308580, "Не устою пред тобой,", 348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39433, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "устою" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39433, 22460 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пред" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39433, 22461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39433, 735 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 348   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1309580, 1311130, "и любовь,", 349 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39434, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39434, 5455 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 349   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1311340, 1313260, "Любовь...", 350 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39435, 5455 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 350   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1314670, 1319640, "Я ведь сойду... с ума,", 351 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39436, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39436, 726 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сойду" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39436, 22462 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39436, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39436, 6035 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 351   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1319970, 1322100, "Уверен я,", 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39437, 6588 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39437, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 352   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1323310, 1326480, "Знаешь, я не против,", 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39438, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39438, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39438, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39438, 4904 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 353   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1326770, 1329440, "О, знаешь, я не против,", 354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39439, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39439, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39439, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39439, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39439, 4904 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 354   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1329980, 1331610, "Да,", 355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39440, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 355   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1331860, 1335940, "Ты мне дороже всего мира,", 356 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39441, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39441, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39441, 14445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39441, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39441, 3318 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 356   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1336650, 1339700, "О, я знаю, я знаю.", 357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39442, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39442, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39442, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39442, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39442, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 357   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1339990, 1342620, "Я нашёл,", 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39443, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39443, 1011 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 358   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1342830, 1344160, "В тебе,", 359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39444, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39444, 753 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 359   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 112, 1345660, 1349210, "Мою бесконечную любовь,", 360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39445, 760 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бесконечную" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39445, 22463 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 39445, 5455 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 56 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 03/s03e08 - The One with the Giant Poking Device/Russian.srt" ,  `subtitle`.`phrase_number` = 360   WHERE  `subtitle`.`id` = 112 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 56, 112 );