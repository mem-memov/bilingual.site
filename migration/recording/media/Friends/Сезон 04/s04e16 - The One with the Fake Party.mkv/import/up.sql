INSERT INTO `source` ( `source`.`file` ) VALUES ( "Friends/Сезон 04/s04e16 - The One with the Fake Party.mkv" );
# a query
INSERT INTO `video` ( `video`.`source_id`, `video`.`language_id`, `video`.`file`, `video`.`width`, `video`.`height`, `video`.`duration` ) VALUES ( 89, 1, "Friends/Сезон 04/s04e16 - The One with the Fake Party/movie.mp4", 768, 432, 1334376 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 89, 2, "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.mp3", 1334388 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 89, 177 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 89, 1, "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.mp3", 1334388 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 89, 178 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 89, 1, "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 4504, 5631, "What is it?", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61540, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61540, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61540, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 5797, 8050, "Nothing. I can\'t find anything\r\nthat I want to eat.", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61541, 34 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 8217, 9551, "Everything makes me nauseous.", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61542, 1482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61542, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61542, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61542, 7708 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 9760, 12554, "I\'m telling you, being pregnant\r\nis no piece of cake. Oh, cake.", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 1660 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 1543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 3884 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61543, 3884 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 14348, 15766, "No.", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61544, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 16558, 18894, "-Aw, honey, I\'m sorry.\r\n-God.", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61545, 5684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61545, 2523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61545, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61545, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61545, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61545, 181 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 19061, 23732, "Ooh, what is that smell?\r\nIt\'s coming from the bathroom.", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 1525 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 2565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61546, 4662 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 24024, 27152, "Wow, pregnancy does give you\r\nsome weird cravings.", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61547, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61547, 28937 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61547, 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61547, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61547, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61547, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61547, 99 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "cravings" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61547, 29796 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 28529, 29863, "-Yeah.\r\n-It\'s me. It\'s Phoebe.", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61548, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61548, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61548, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61548, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61548, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61548, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61548, 88 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 30030, 32991, "Listen, there\'s something in here\r\nI want to eat. What...?", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61549, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 33951, 36119, "What smells so good?", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61550, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61550, 9058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61550, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61550, 346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 36912, 39373, "Is it the shampoo? It\'s guava.", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61551, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61551, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61551, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61551, 25093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61551, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61551, 2 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "guava" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61551, 29797 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 39540, 40749, "No.", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61552, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 40916, 42918, "-Ugh.\r\n-Oh, wait, wait.", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61553, 3985 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61553, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61553, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61553, 33 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 43919, 45963, "Is it my bologna sandwich?", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61554, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61554, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61554, 95 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bologna" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61554, 29798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61554, 19200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 48298, 50175, "Yes, yes, yes.", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61555, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61555, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61555, 339 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 50342, 53345, "I can\'t believe it. The baby wants bologna.\r\nThe baby wants me to eat meat.", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 1627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 29798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 1627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61556, 7685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 53512, 56265, "-I can\'t eat meat.\r\n-Uh, wait, wait.", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61557, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61557, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61557, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61557, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61557, 7685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61557, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61557, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61557, 33 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 57307, 59059, "Maybe it\'s the pickle.", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61558, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61558, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61558, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61558, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61558, 23197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 111320, 112529, "What are you writing?", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61559, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61559, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61559, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61559, 7348 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 112696, 114156, "Well, Joshua\'s coming in tomorrow...", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61560, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61560, 29171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61560, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61560, 2565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61560, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61560, 1583 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 114323, 116867, "...and since I don\'t have the guts\r\nto ask him out...", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 463 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 8170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 400 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61561, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 117034, 119703, "...I\'m gonna sell him a coat\r\nand put this note in the pocket.", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 7269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 8545 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 1557 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61562, 4587 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 119870, 121330, "-Oh, yeah?\r\n-Oh.", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61563, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61563, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61563, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 121496, 125751, "\"Joshua, give me a call sometime.\r\nGuys like you...", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 29171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 1622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 621 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61564, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 125917, 128337, "...never go out of style.\"", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61565, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61565, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61565, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61565, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61565, 14216 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 129254, 131506, "What did you throw away?", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61566, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61566, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61566, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61566, 5324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61566, 323 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 133508, 136553, "-Hi, guys.\r\n-Hey, what have you guys been up to?", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61567, 4 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 136720, 140807, "Oh, we went to see a collection of Victorian\r\ndoorknobs at the Cooper-Hewitt museum.", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 17140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 25287 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "doorknobs" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 29799 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 19496 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hewitt" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 29800 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61568, 1656 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 141058, 142434, "Without me?", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61569, 1590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61569, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 144144, 147230, "My uncle dragged us there,\r\nbut actually it turned out to be interesting.", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 7696 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "dragged" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 29801 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61570, 4615 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 147397, 149733, "-Yeah.\r\n-They were so ornate and beautiful.", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61571, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61571, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61571, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61571, 25 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "ornate" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61571, 29802 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61571, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61571, 1580 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 149900, 150942, "I mean, look at that.", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61572, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61572, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61572, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61572, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61572, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 151109, 152778, "I don\'t know how museums work\r\nin England...", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 25712 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61573, 21982 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 152944, 155530, "...but here,\r\nyou\'re not supposed to take stuff.", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61574, 124 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 155906, 158450, "I, uh- I got it from the gift shop.", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 2447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61575, 8182 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 158617, 160494, "They have really lax security there.", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61576, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61576, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61576, 140 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "lax" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61576, 29803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61576, 5240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61576, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 162204, 163664, "It\'s a joke.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61577, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61577, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61577, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61577, 11557 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 165082, 166375, "Bye.", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61578, 3933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 168835, 171296, "Right, I\'ve got to be off. I\'ll see you.\r\nBye-bye, then.", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 3933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 3933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61579, 79 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 171463, 173131, "Bye.", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61580, 3933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 173799, 176301, "Wow. Hey, you guys seem\r\nto be having a good time.", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 1516 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61581, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 176468, 180764, "Oh, yeah, she is amazing,\r\nand she\'s so much fun.", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61582, 2418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 180931, 184643, "And you know what?\r\nWhen I\'m with her, I\'m fun.", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61583, 2418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 186311, 188897, "I even signed up for helicopter classes.", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61584, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61584, 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61584, 22011 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61584, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61584, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61584, 27911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61584, 11511 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 190232, 192317, "She\'s leaving in two days.\r\nI don\'t have to do it.", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 4695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 1597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61585, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 193610, 195737, "-Aw, two days? You must be bummed.\r\n-Yeah.", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61586, 5684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61586, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61586, 1597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61586, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61586, 1679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61586, 21 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "bummed" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61586, 29804 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61586, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 195904, 197489, "She\'s got to go back to London.", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61587, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61587, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61587, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61587, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61587, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61587, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61587, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61587, 7218 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 197656, 200325, "But you know what?\r\nI\'ve been prepared for this from the start.", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 14831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61588, 2453 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 200492, 204037, "We both knew we had two weeks\r\ntogether and then that\'s it, you know?", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 3067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 3180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61589, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 204204, 206790, "Hey, that\'s what all\r\nmy relationships are like.", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 9947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61590, 60 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 207624, 210919, "Yes, but in Ross\' case,\r\nthey both know in two weeks that\'s it.", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 433 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 3067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 3180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61591, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 212963, 215090, "Pheebs, hey.", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61592, 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61592, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 215257, 217843, "-Hello, hello.\r\n-Yeah, I know. Yeah.", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61593, 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61593, 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61593, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61593, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61593, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61593, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 219720, 222055, "So the baby is totally craving meat.", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61594, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61594, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61594, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61594, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61594, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61594, 7225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61594, 7685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 222222, 223765, "This afternoon, I tried tricking it.", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61595, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61595, 3964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61595, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61595, 4617 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "tricking" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61595, 29805 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61595, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 223932, 226810, "I made it a soy burger\r\nso that maybe it\'d think it was getting meat.", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 21234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 23721 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61596, 7685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 226977, 229187, "You know? And I got nauseous.", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61597, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61597, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61597, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61597, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61597, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61597, 7708 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 229354, 232524, "Well, maybe that\'s because\r\nsoy burgers suck.", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61598, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61598, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61598, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61598, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61598, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61598, 21234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61598, 12628 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61598, 7773 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 233692, 235777, "Being pregnant is hard on your tummy.", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61599, 366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61599, 1543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61599, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61599, 594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61599, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61599, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61599, 26846 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 235944, 238822, "Hey, but at least you got\r\nthat cool, pregnant-lady glow.", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 1643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 2531 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 1543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 3893 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61600, 3270 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 238989, 242284, "Oh, that\'s sweat. You throw up all morning,\r\nyou\'ll have that glow too.", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 19937 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 5324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 504 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 3270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61601, 576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 251042, 253795, "-Okay.\r\n-Uh, here\'s that trench coat you wanted.", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 77 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "trench" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 29806 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 8545 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61602, 1754 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 253962, 255839, "Oh, great. Great.", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61603, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61603, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61603, 505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 256256, 258800, "-Wow, yeah, it\'s comfortable.\r\n-Yeah.", 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61604, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61604, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61604, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61604, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61604, 1613 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61604, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 258967, 262304, "Man, you could really\r\nflash somebody in this thing.", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 1645 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 14803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 2537 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61605, 381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 262471, 265056, "Ha, ha. They don\'t want you to put\r\nyour hands in the pockets...", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 6751 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "pockets" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61606, 29807 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 265223, 267350, "...until you are out of the store.", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61607, 4630 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61607, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61607, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61607, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61607, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61607, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61607, 6662 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 268435, 269478, "Why not?", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61608, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61608, 51 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 269644, 271480, "Well, because...", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61609, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61609, 100 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 271646, 273899, "...we get a lot of—", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61610, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61610, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61610, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61610, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61610, 69 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 276276, 277903, "You know, they ruin it for everybody.", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61611, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61611, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61611, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61611, 19538 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61611, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61611, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61611, 47 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 278069, 279446, "I know.", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61612, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61612, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 279613, 282073, "You know, I wore that cashmere sweater\r\non a date last night.", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 15018 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 77 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "cashmere" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 29808 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 5191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61613, 507 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 283033, 286244, "-Oh.\r\n-It was the first date since the divorce.", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 463 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61614, 6236 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 287037, 290332, "Well, congratulations.\r\nSo do you love her?", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61615, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61615, 498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61615, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61615, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61615, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61615, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61615, 39 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 291082, 293752, "No, no, no. She\'s nice,\r\nbut, you know, it made me realize...", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61616, 71 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 293919, 297005, "...I\'m just not ready to be dating,\r\nyou know?", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 2333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61617, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 298131, 299382, "Huh.", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61618, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 299633, 301927, "Well, uh....", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61619, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61619, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 302093, 304179, "That\'s, uh- That\'s interesting.", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61620, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61620, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61620, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61620, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61620, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61620, 4615 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 304346, 305722, "Hey, whoa, hey.", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61621, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61621, 2366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61621, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 305889, 308934, "-What was that?\r\n-Oh, that\'s just an antitheft device.", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 259 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "antitheft" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 29809 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61622, 4707 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 309809, 311311, "Then what\'s this?", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61623, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61623, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61623, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61623, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 311478, 315732, "Well, you need that too, because\r\nobviously a thief could just tear this up.", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 3899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "thief" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 29810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 7 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "tear" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 29811 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61624, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 321071, 322656, "Listen, we have to have a party tonight.", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61625, 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61625, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61625, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61625, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61625, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61625, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61625, 3167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61625, 353 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 322822, 325867, "We have to have one in five minutes,\r\nso everybody cancel your plans.", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 409 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 2355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 345 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61626, 3873 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 326785, 328245, "-What?\r\n-Yeah, what\'s going on?", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61627, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61627, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61627, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61627, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61627, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61627, 14 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 328411, 331498, "We have to have a surprise\r\nbon-voyage party for Emily.", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 5258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 25077 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "voyage" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 29812 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 3167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61628, 18911 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 331665, 333875, "But it\'s actually for Joshua.", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61629, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61629, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61629, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61629, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61629, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61629, 29171 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 334834, 337295, "Look, he said he\'s not ready to date.", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61630, 53 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 337462, 340632, "So I had to invite him to a party\r\nif I wanted to see him outside work.", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 4659 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 3167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 1754 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61631, 11 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 340799, 345637, "And now I have created\r\nthe perfect opportunity to seduce him.", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 28 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "created" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 29813 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 3923 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 4682 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 4 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "seduce" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 29814 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61632, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 346137, 349641, "Well, as much as I\'d like\r\nto meet Josh and warn him...", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 1707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 27530 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 30 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "warn" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 29815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61633, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 351601, 353562, "...Emily and I aren\'t gonna be here,\r\nall right?", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 18911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 3090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61634, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 353728, 355689, "She\'s gonna come by first\r\nto say goodbye.", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61635, 5327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 355855, 359943, "And then I\'ve got a whole special\r\nevening planned. So sorry. No party.", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 592 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 1717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 4579 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 6234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61636, 3167 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 360110, 361736, "Aw.", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61637, 5684 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 362946, 365532, "-Hello?\r\n-Surprise!", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61638, 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61638, 5258 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 366908, 368785, "Surprise!", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61639, 5258 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 370453, 373039, "No one\'s ever thrown me\r\na surprise party before.", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 11492 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 5258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 3167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61640, 211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 373206, 376167, "Well, it was all Ross\' idea.", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61641, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61641, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61641, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61641, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61641, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61641, 392 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 377711, 379838, "You\'re so sweet.", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61642, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61642, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61642, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61642, 222 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 380338, 382299, "And I\'m so surprised.", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61643, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61643, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61643, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61643, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61643, 5257 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 386595, 387929, "You really didn\'t know?", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61644, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61644, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61644, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61644, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61644, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 394686, 396438, "<i>There\'s dancin\'</i>", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61645, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61645, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61645, 2 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "dancin" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61645, 29816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61645, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 396646, 398106, "<i>Behind movie scenes</i>", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61646, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61646, 5228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61646, 4647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61646, 27524 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61646, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 398273, 401067, "Why are you over here\r\nif Joshua\'s all the way over there?", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 29171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61647, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 401234, 403987, "Uh, because I\'m trying to play\r\nhard-to-get.", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 1506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 3127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61648, 128 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 405238, 408074, "Oh, quick. He\'s looking over here.\r\nSay something funny.", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 5659 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61649, 1737 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 408241, 410285, "-Like what?\r\n-Ha-ha-ha.", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61650, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61650, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61650, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61650, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61650, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 411911, 413496, "What\'s so funny?", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61651, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61651, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61651, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61651, 1737 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 415457, 416541, "I said, \"Like what?\"", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61652, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61652, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61652, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61652, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 418543, 420337, "Now that\'s a thinker.", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61653, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61653, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61653, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61653, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "thinker" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61653, 29817 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 421504, 424424, "Okay, you know what? This\r\nplaying-hard-to-get thing is not working.", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 1639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61654, 3971 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 424591, 427594, "Uh, hand me those cherries. Okay.", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61655, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61655, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61655, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61655, 272 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "cherries" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61655, 29818 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61655, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 427761, 428928, "Okay.", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61656, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 433683, 434809, "-Hi.\r\n-Hey.", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61657, 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61657, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 434976, 437312, "-Care for a cherry?\r\n-No, thanks.", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61658, 2561 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61658, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61658, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61658, 445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61658, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61658, 130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 437479, 441691, "You know, I can tie one of these into a knot\r\nusing just my tongue.", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 3240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 17531 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61659, 4037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 454996, 456206, "You okay?", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61660, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61660, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 456831, 458249, "You all right?", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61661, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61661, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61661, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 463213, 465465, "So we should probably get going soon.", 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61662, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61662, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61662, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61662, 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61662, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61662, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61662, 5247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 465632, 468301, "Oh, but the party\'s only\r\njust getting started.", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 3167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61663, 207 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 468468, 472097, "Yeah, but we have to be at The Four\r\nSeasons for drinks in, oh, 15 minutes.", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 203 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "seasons" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 29819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 9547 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 10326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61664, 2355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 472263, 474474, "And then, uh, you know,\r\nthen the Plaza for dinner.", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "plaza" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 29820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61665, 56 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 474933, 477394, "So why did you plan a party\r\nat the same time?", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 3198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 3167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 2372 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61666, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 478603, 480605, "No, no, no. Ha, ha.", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61667, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61667, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61667, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61667, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61667, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 482232, 486486, "Actually, American surprise parties\r\nare very short.", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61668, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61668, 20975 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61668, 5258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61668, 2467 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61668, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61668, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61668, 3948 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 486653, 490991, "It\'s usually, \"Surprise!\" Then,\r\n\"Oh, my God, I\'m so surprised. Goodbye.\"", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 7344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 5258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 5257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61669, 5327 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 493118, 495370, "Ross, I\'m having such a great time.", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61670, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61670, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61670, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61670, 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61670, 405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61670, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61670, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61670, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 495537, 499249, "Your sister has just been telling me that\r\nyou used to dressed up like old ladies...", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 5220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 1517 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 1660 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 3179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 3145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 1787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61671, 4067 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 499416, 501876, "...and host make-believe tea parties.", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61672, 30 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "host" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61672, 29821 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61672, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61672, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61672, 2384 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61672, 2467 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 505255, 507674, "Monica said that, did she?", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61673, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61673, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61673, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61673, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61673, 101 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 508633, 511928, "Oh, yeah. Then what are you going\r\nto put on top of that?", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 3976 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61674, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 512095, 515432, "-A little salami.\r\n-Ooh, yeah. Yeah.", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61675, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61675, 386 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "salami" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61675, 29822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61675, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61675, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61675, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 516891, 518977, "Then, um, what goes on top\r\nof the salami?", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 3229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 3976 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61676, 29822 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 519644, 521563, "-Pastrami.\r\n-Oh, yeah.", 138 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "pastrami" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61677, 29823 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61677, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61677, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 524441, 526609, "You\'re a genius.", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61678, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61678, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61678, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61678, 22257 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 529320, 530989, "Can someone give me a hand\r\nwith the zipper.", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61679, 8573 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 533742, 535118, "Up.", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61680, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 537912, 541541, "-You changed?\r\n-Yeah, I just needed my lucky dress.", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 8210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 4073 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 4009 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61681, 3904 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 541708, 544335, "-And \"lucky\" means \"more cleavage\"?\r\n-Does for me.", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61682, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61682, 4009 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61682, 2332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61682, 226 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "cleavage" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61682, 29824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61682, 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61682, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61682, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 555805, 558016, "Oh, God, look at him. He\'s so cute.", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61683, 3251 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 558183, 560852, "I just want to go over there and grab him\r\nand kiss him.", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 457 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 1483 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61684, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 561019, 563480, "How could I kiss him\r\nwithout letting him know that I like him?", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 1483 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 1590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 5325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61685, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 563646, 566900, "Hey, I know how you can get him.\r\nTake off your bra.", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61686, 3862 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 567817, 568860, "What?", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61687, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 569027, 571070, "-There was a scene in <i>Footloose-\r\n-Flashdance.</i>", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 9892 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 10 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "footloose" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 29825 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "flashdance" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 29826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61688, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 571237, 574824, "-Yeah, yeah. Yeah, with that plumber girl?\r\n-She was a welder.", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 77 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "plumber" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 29827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 1584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "welder" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61689, 29828 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 577368, 579162, "Were you, like, in the movie?", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61690, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61690, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61690, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61690, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61690, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61690, 4647 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 581539, 584709, "Anyway, she takes off her bra\r\nunder the shirt...", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 8086 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 3862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 5641 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61691, 8572 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 584876, 586419, "...and pulls it out the sleeve.", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61692, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61692, 18849 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61692, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61692, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61692, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61692, 3863 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 586586, 589130, "Very sexy. And classy.", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61693, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61693, 2421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61693, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61693, 25695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 592926, 595553, "Or if you wanna kiss him,\r\nurn, you could use mistletoe.", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 1483 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 470 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 2378 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61694, 15482 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 595720, 596805, "It\'s not Christmas.", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61695, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61695, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61695, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61695, 6243 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 596971, 598807, "-Or spin the bottle.\r\n-And he\'s not 11.", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 27341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 8183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61696, 7209 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 602727, 605688, "Thank you so much for this.\r\nIt was really so thoughtful of you.", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 18235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61697, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 605855, 607106, "You\'re leaving?", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61698, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61698, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61698, 4695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 607273, 611110, "-Yes, we have something we have to get to.\r\n-Uh, yeah, I think I\'m gonna take off too.", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61699, 576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 611277, 614197, "No, you guys, you can\'t leave yet.\r\nYou have to stay.", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 573 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61700, 289 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 614364, 617033, "We\'ve got the whole big thing planned.", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61701, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61701, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61701, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61701, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61701, 592 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61701, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61701, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61701, 6234 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 617659, 618743, "What big thing?", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61702, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61702, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61702, 381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 618910, 621037, "So, Spin-The-Bottle\r\nworks like this.", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61703, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61703, 27341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61703, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61703, 8183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61703, 1792 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61703, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61703, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 621204, 622705, "I spin the bottle.", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61704, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61704, 27341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61704, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61704, 8183 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 622872, 625625, "Lands on Gunther,\r\nso I would have to kiss Gunther.", 166 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "lands" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 29829 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 15403 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 1483 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61705, 15403 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 627210, 628419, "All right.", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61706, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61706, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 629879, 631464, "Who wants to go first?", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61707, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61707, 1627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61707, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61707, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61707, 596 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 631631, 632924, "I\'ll go.", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61708, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61708, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61708, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 638137, 640265, "Welcome to America.", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61709, 455 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61709, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61709, 9881 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 642392, 643643, "Oh.", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61710, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 644644, 646020, "All right.", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61711, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61711, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 646980, 648523, "Oh, my God.", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61712, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61712, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61712, 181 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 648690, 652360, "Oh, my gosh. Two in a row.\r\nYou gotta use your tongues now.", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 6777 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 2572 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 2378 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 183 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "tongues" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 29830 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61713, 165 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 658074, 659784, "All right.", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61714, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61714, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 661369, 663121, "What are the odds? What are the odds?", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61715, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61715, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61715, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61715, 2432 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61715, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61715, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61715, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61715, 2432 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 665039, 667542, "Okay, that\'s enough, you know. Ha, ha.", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61716, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61716, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61716, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61716, 495 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61716, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61716, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61716, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61716, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 668459, 670753, "Let\'s let someone else play.", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61717, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61717, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61717, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61717, 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61717, 541 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61717, 3127 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 672088, 674924, "Hey, if you didn\'t want to play,\r\nthen why\'d you come to the party?", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 3127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61718, 3167 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 676885, 678428, "Okay, my turn.", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61719, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61719, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61719, 3214 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 681347, 683683, "Oh! Look at that.", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61720, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61720, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61720, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61720, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 683850, 685476, "Oh, my God.", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61721, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61721, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61721, 181 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 689314, 690732, "Oh, my God. The baby just kicked.", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61722, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61722, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61722, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61722, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61722, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61722, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61722, 7777 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 692108, 694944, "It\'s okay, it\'s okay.\r\nIf it kicked once, it\'ll kick again.", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 7777 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 531 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 12940 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61723, 177 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 698781, 703286, "All right, well, everybody just remember\r\nwhere they were sitting.", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61724, 1513 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 712503, 714047, "It was a bug.", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61725, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61725, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61725, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61725, 8982 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 716799, 718593, "It doesn\'t matter\r\nhow much I\'m craving it.", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 1499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 7225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61726, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 718760, 722222, "You know why I\'m never gonna eat meat?\r\nBecause it\'s cold-blooded murder.", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 7685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 4636 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "blooded" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 29831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61727, 10770 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 723139, 724557, "Okay.", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61728, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 729896, 732357, "There\'s a Phoebe on my sandwich.", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61729, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61729, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61729, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61729, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61729, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61729, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61729, 19200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 734150, 735526, "Phoebe, what are you doing?", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61730, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61730, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61730, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61730, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61730, 246 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 735693, 737946, "I can\'t help it.", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61731, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61731, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61731, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61731, 376 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61731, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 738112, 741032, "I need the meat.\r\nThe baby needs the meat.", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 7685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 6176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61732, 7685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 741491, 743242, "You know how when\r\nyou\'re dating someone...", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61733, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61733, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61733, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61733, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61733, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61733, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61733, 2333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61733, 112 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 743409, 746996, "...and you don\'t want to cheat on them\r\nunless it\'s with someone really hot?", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 15457 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61734, 4637 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 749582, 752001, "-Yeah, totally.\r\n-All right, okay.", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61735, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61735, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61735, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61735, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61735, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 752168, 753753, "Well, this is the same kind of deal.", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61736, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61736, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61736, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61736, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61736, 2372 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61736, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61736, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61736, 1671 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 753920, 756673, "If you\'re gonna do something wrong...", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61737, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61737, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61737, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61737, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61737, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61737, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61737, 23 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 758341, 759384, "...do it right.", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61738, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61738, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61738, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 766182, 767642, "Feel better?", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61739, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61739, 312 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 767809, 769978, "Yeah, but at what cost?", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61740, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61740, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61740, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61740, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61740, 9865 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 770937, 772981, "Six more months, three meals a day?", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61741, 4720 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61741, 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61741, 3087 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61741, 1546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61741, 21217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61741, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61741, 325 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 773147, 777110, "I\'m gonna eat like, you know,\r\nmillions of cows.", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 13476 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61742, 11579 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 778903, 782573, "Hey, what if I said I could even things out\r\nfor you, meat-wise?", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 7685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61743, 9507 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 782740, 784951, "-What?\r\n-Well, I eat a lot of meat, right?", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 7685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61744, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 785118, 788037, "-Yeah.\r\n-But suppose, until the baby\'s born...", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61745, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61745, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61745, 4590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61745, 4630 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61745, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61745, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61745, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61745, 1576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 788204, 789414, "...I laid off it.", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61746, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61746, 8153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61746, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61746, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 789580, 792959, "No extra animals would die.\r\nYou\'d just be eating my animals.", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 2393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 11587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 2353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 6791 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61747, 11587 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 794627, 796504, "Joey, I can\'t believe\r\nyou\'d do that for me.", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61748, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 796671, 800675, "Absolutely. I could be a vegetarian.", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61749, 1484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61749, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61749, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61749, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61749, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61749, 3279 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 800842, 803344, "There\'s no meat in beer, right?", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61750, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61750, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61750, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61750, 7685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61750, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61750, 397 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61750, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 803886, 806431, "Okay, we can still make dinner\r\nif we skip the appetizers...", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 200 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "skip" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 29832 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "appetizers" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61751, 29833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 806597, 808766, "...and ask for our check right away.", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61752, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61752, 400 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61752, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61752, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61752, 3080 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61752, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61752, 323 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 812687, 815982, "We can\'t go now. Looks like Rachel\r\nis going to put on a skit.", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61753, 14203 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 818109, 820778, "Oh, my God. Have you lost your mind?", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61754, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61754, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61754, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61754, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61754, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61754, 1596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61754, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61754, 3105 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 821571, 824282, "No, no, no. I am finally thinking clearly.", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61755, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61755, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61755, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61755, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61755, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61755, 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61755, 3930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61755, 16135 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 826075, 828202, "My lucky dress wasn\'t working out\r\ntoo well for me.", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 4009 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 3904 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 3971 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61756, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 828369, 831789, "But for four years,\r\nthis baby never missed.", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61757, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61757, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61757, 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61757, 476 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61757, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61757, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61757, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61757, 5736 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 831956, 835001, "Rachel, wait, I can\'t let—", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61758, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61758, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61758, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61758, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61758, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61758, 127 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 835251, 837462, "Actually, I kind of want to see\r\nwhat happens.", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61759, 5672 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 840006, 841132, "-Hey.\r\n-Hey.", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61760, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61760, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 841299, 843217, "Nice, uh, costume.", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61761, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61761, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61761, 16179 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 843384, 844677, "Oh, yeah.", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61762, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61762, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 844844, 849182, "Well, I wanted to give Emily\r\na big American goodbye cheer.", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 1754 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 18911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 20975 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 5327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61763, 9012 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 849640, 852602, "So okay. Whoo!", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61764, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61764, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61764, 7311 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 854771, 857065, "Ready. Okay.", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61765, 616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61765, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 857231, 858858, "-Give me an \"E.\"\r\n-E.", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61766, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61766, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61766, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61766, 18886 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61766, 18886 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 859025, 860651, "-Give me an \"M.\"\r\n-M.", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61767, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61767, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61767, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61767, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61767, 62 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 860818, 862445, "-Give me an \"I.\"\r\n-I.", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61768, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61768, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61768, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61768, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61768, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 862612, 863780, "-Give me an \"L\"\r\n-L.", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61769, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61769, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61769, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61769, 16478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61769, 16478 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 863946, 865615, "-Give me a \"Y.\"\r\n-Y.", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61770, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61770, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61770, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61770, 380 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61770, 380 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 865782, 867617, "What do you get?", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61771, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61771, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61771, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61771, 128 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 867784, 870161, "Oh! Emily.", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61772, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61772, 18911 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 870328, 872163, "Whoo! Emily.", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61773, 7311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61773, 18911 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 875333, 878377, "Okay. That\'s me as a cheerleader. Ta-da!", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 11555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 4604 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61774, 4605 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 880505, 882256, "-You all right?\r\n-I just loosened a tooth.", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61775, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61775, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61775, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61775, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61775, 7 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "loosened" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61775, 29834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61775, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61775, 6819 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 882423, 884300, "It\'s no big deal. I have a dentist, you know?", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 1671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61776, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 884467, 887220, "I\'m gonna go put some ice on it.\r\nExcuse me.", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 3060 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61777, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 889305, 891849, "-What do I do now? What do I do now?\r\n-I think you\'re done.", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61778, 393 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 893851, 895144, "Okay, time to take off the bra.", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61779, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61779, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61779, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61779, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61779, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61779, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61779, 3862 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 899148, 902318, "Uh, that was really great.", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61780, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61780, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61780, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61780, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61780, 505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 903027, 904946, "But I got to take off, actually.", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61781, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61781, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61781, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61781, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61781, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61781, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61781, 370 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 905154, 908074, "Take the bra off.", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61782, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61782, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61782, 3862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61782, 429 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 911661, 913329, "All right, come on,\r\nlet\'s go get your coat.", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61783, 8545 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 913496, 914622, "Okay.", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61784, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 916791, 918376, "Rachel\'s my girlfriend.", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61785, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61785, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61785, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61785, 3922 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 921754, 924382, "So this was really fun.", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61786, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61786, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61786, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61786, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61786, 2418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 924549, 926259, "Oh, yeah, it was real fun.", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61787, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61787, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61787, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61787, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61787, 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61787, 2418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 931431, 933307, "You know, this bra...", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61788, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61788, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61788, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61788, 3862 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 935935, 938271, "...is really bothering me.", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61789, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61789, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61789, 12956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61789, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 939897, 941482, "You know, this used to be my bedroom.", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61790, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61790, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61790, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61790, 3179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61790, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61790, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61790, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61790, 6808 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 942066, 943151, "Yeah.", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61791, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 943401, 947697, "A lot of memories in here. Lot of memories.", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 11564 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61792, 11564 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 948197, 950241, "If these walls could talk,\r\nyou know what they\'d say?", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 23763 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61793, 159 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 950616, 952743, "\"Want to hear some memories?\"", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61794, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61794, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61794, 564 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61794, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61794, 11564 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 959208, 960585, "Need a-? Need a little hand there?", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61795, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61795, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61795, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61795, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61795, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61795, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61795, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 960751, 962795, "Oh, no, no, no.\r\nNo, I got this all under control.", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 5641 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61796, 318 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 964922, 967300, "You really don\'t seem like you do.", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61797, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61797, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61797, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61797, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61797, 1516 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61797, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61797, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61797, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 972221, 976726, "God. Forget it! Oh, this is-", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61798, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61798, 1713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61798, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61798, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61798, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61798, 50 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 979061, 982106, "This is not how\r\nthis was supposed to happen.", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61799, 4006 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 982273, 983691, "What was supposed to happen?", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61800, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61800, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61800, 354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61800, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61800, 4006 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 985735, 987612, "Can you not look at me when I say this?", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61801, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 991908, 995369, "I thought that if I could get you here...", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61802, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 996746, 998748, "...I could seduce you.", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61803, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61803, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61803, 29814 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61803, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1003502, 1008299, "Uh, I don\'t wear suits to work...", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61804, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61804, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61804, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61804, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61804, 2555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61804, 29201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61804, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61804, 11 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1008466, 1011010, "...and I bought six of them from you.", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61805, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61805, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61805, 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61805, 4720 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61805, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61805, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61805, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61805, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1012011, 1014931, "Well, I\'m sorry.\r\nI thought you needed them.", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 4073 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61806, 447 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1016515, 1020519, "My point is, I kept coming back\r\nbecause I wanted to see you.", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 3200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 2398 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 2565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 1754 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61807, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1021437, 1023856, "-Why?\r\n-Because I like you.", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61808, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61808, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61808, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61808, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61808, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1024857, 1026567, "You like me?", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61809, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61809, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61809, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1026817, 1028110, "Yeah.", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61810, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1028277, 1033032, "I mean, you\'re beautiful\r\nand smart and sophisticated.", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 1580 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 3920 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61811, 554 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1033199, 1035326, "A lot of this isn\'t, you know,\r\nbased on tonight.", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 426 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 4567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61812, 353 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1038204, 1039664, "But you like me?", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61813, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61813, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61813, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61813, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1040039, 1044001, "Oh, my God. I can\'t believe this.\r\nAll this time, I liked you and you liked me.", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 2431 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 2431 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61814, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1044168, 1045211, "Oh!", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61815, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1046295, 1048047, "-But-\r\n-No don\'t say \"but.\"", 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61816, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61816, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61816, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61816, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61816, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61816, 175 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1048214, 1052677, "\"But\" is never good. Let\'s leave it\r\nat \"I like you and you like me.\"", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61817, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1053803, 1055137, "Okay, uh....", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61818, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61818, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1055304, 1058140, "-However—\r\n-No, that\'s a fancy \"but.\"", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61819, 10257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61819, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61819, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61819, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61819, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61819, 17717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61819, 175 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1060309, 1064480, "My marriage, like, just ended.", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61820, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61820, 8129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61820, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61820, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61820, 604 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1064647, 1068859, "And I\'m really not ready\r\nto get into anything yet.", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61821, 573 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1070027, 1071779, "But....", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61822, 175 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1073948, 1077118, "I\'m sorry. I just need a little time.", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61823, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1081163, 1082456, "Okay.", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61824, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1086544, 1089338, "Oh, there you are.\r\nI was looking for you before.", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61825, 211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1089505, 1092550, "Joshua\'s gone,\r\nso you and Emily are free to go.", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 29171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 3099 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 18911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 2446 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61826, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1092967, 1094468, "It\'s okay.", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61827, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61827, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61827, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1094927, 1097513, "She\'s still in there\r\nenjoying her fake party.", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 23196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 15406 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61828, 3167 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1097680, 1101809, "And, uh, it\'s too late to do\r\nany of the stuff I had planned, so....", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 1710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 6234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61829, 25 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1102643, 1104186, "Ross.", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61830, 161 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1104353, 1106606, "I\'m sorry.\r\nI completely ruined your evening.", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61831, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61831, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61831, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61831, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61831, 3206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61831, 1618 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61831, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61831, 4579 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1106772, 1108649, "Ahem. Yeah.", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61832, 1669 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61832, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1110443, 1114238, "Well, if it makes you feel any better,\r\nI made a fool of myself.", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 17142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61833, 108 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1116032, 1117074, "Helps a little.", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61834, 11194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61834, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61834, 386 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1119243, 1122038, "Is there a room on that step\r\nfor a pathetic loser?", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 2403 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 6673 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61835, 10365 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1122204, 1124206, "Yeah, have a seat.", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61836, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61836, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61836, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61836, 4614 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1130671, 1132715, "I\'m so sorry.", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61837, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61837, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61837, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61837, 268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1132882, 1133924, "It\'s okay.", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61838, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61838, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61838, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1134091, 1137094, "I mean, it was just a two-week thing\r\nanyway.", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 411 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61839, 243 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1137261, 1139722, "I just didn\'t want it to end this way,\r\nyou know.", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 1793 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61840, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1139889, 1142308, "Or maybe you didn\'t want it to end?", 302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61841, 1793 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1143476, 1145144, "What do you mean?", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61842, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61842, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61842, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61842, 239 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1145311, 1147730, "You seem to really like her.", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61843, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61843, 1516 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61843, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61843, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61843, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61843, 39 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1150107, 1151776, "Yeah, I really do.", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61844, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61844, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61844, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61844, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1153652, 1155029, "Yeah, but what am I gonna do?", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61845, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61845, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61845, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61845, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61845, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61845, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61845, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1155196, 1157782, "We both agreed it was gonna be\r\na two-week thing.", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 3067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 1736 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 411 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61846, 381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1157948, 1159784, "You know? No commitment.", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61847, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61847, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61847, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61847, 7729 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1161702, 1163079, "Ross?", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61848, 161 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1164246, 1168918, "That girl just spent the entire evening\r\ntalking to your friends...", 310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 1584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 1640 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 1588 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 4579 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61849, 1651 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 310   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1169085, 1171671, "...asking to hear stories about you...", 311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61850, 3928 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61850, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61850, 564 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61850, 1664 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61850, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61850, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 311   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1171837, 1174423, "...looking through\r\nMonica\'s photo albums.", 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61851, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61851, 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61851, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61851, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61851, 3960 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61851, 27073 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 312   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1174590, 1177676, "I mean, you don\'t do that\r\nif you\'re just in it for two weeks.", 313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61852, 3180 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 313   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1178469, 1179970, "You think?", 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61853, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61853, 298 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 314   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1181263, 1185893, "You\'ve got, like, 14 hours\r\nuntil she has to be at the airport.", 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 598 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 1605 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 4630 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 1517 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61854, 6780 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 315   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1186060, 1188521, "And you\'re sitting here in a hallway...", 316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61855, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61855, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61855, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61855, 1513 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61855, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61855, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61855, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61855, 19550 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 316   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1188688, 1191899, "...with a 28-year-old cheerleader\r\nwith a fat lip.", 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 14191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 1787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 11555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 3224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61856, 5303 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 317   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1195444, 1197780, "-Hey, you\'re right.\r\n-Yeah.", 318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61857, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61857, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61857, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61857, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61857, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 318   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1202660, 1204036, "Thanks.", 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61858, 130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 319   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1208457, 1210376, "What photo album was it?", 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61859, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61859, 3960 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61859, 1515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61859, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61859, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 320   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1211293, 1213921, "I don\'t know. It was you\r\nand a bunch of albino kids.", 321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 384 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 606 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61860, 512 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 321   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1217299, 1219760, "Oh, my God. Those weren\'t albino kids.", 322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61861, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61861, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61861, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61861, 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61861, 1758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61861, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61861, 606 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61861, 512 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 322   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1219927, 1222012, "That was computer camp. Rach.", 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61862, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61862, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61862, 15052 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61862, 10323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61862, 344 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 323   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1226684, 1227768, "-Hey.\r\n-Hey.", 324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61863, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61863, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 324   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1227935, 1231439, "-You\'re a pathetic loser, right?\r\n-Oh, yeah.", 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61864, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61864, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61864, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61864, 6673 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61864, 10365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61864, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61864, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61864, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 325   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1232356, 1233816, "Sit.", 326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61865, 349 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 326   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1239864, 1241198, "Hi.", 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61866, 103 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 327   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1241365, 1243826, "Oh, my God. Joshua.", 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61867, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61867, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61867, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61867, 29171 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 328   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1244452, 1245703, "Look.", 329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61868, 80 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 329   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1246162, 1249290, "All those things I said\r\nabout not being ready-", 330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61869, 616 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 330   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1249623, 1252042, "-They\'re not true?\r\n-No, they\'re all true.", 331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 2532 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61870, 2532 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 331   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1252209, 1253502, "Oh.", 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61871, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 332   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1254837, 1255880, "But....", 333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61872, 175 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 333   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1256046, 1257339, "Oh.", 334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61873, 45 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 334   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1259425, 1260760, "Oh, I love that \"but.\"", 335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61874, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61874, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61874, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61874, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61874, 175 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 335   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1271312, 1273647, "Um, do you want to go inside\r\nand get some coffee?", 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 6124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61875, 129 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 336   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1273814, 1275858, "-Yeah.\r\n-Okay.", 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61876, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61876, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 337   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1282823, 1285034, "Every time.", 338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61877, 1570 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61877, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 338   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1289705, 1292708, "-Ooh, what do you got there?\r\n-Pastrami.", 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61878, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61878, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61878, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61878, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61878, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61878, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61878, 29823 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 339   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1293334, 1294668, "Yeah.", 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61879, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 340   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1296253, 1298839, "-Hey, you know what goes good with that?\r\n-Mm-hm. Corned beef.", 341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 3229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 423 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 634 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "corned" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 29835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61880, 2429 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 341   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1300716, 1303719, "Ooh, I was going to say bologna.\r\nBut that\'s better.", 342 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 29798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61881, 312 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 342   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1304845, 1307389, "-How about a little of that smoked turkey?\r\n-Okay.", 343 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 9064 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 6125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61882, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 343   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1309558, 1311227, "Oh, Mama.", 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61883, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61883, 7188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 344   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1312478, 1315022, "-When is the baby due?\r\n-Six months.", 345 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61884, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61884, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61884, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61884, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61884, 24161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61884, 4720 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61884, 3087 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 345   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1316565, 1318776, "Hey, now, if a cow should die\r\nof natural causes...", 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 2522 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 2353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 13216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61885, 21998 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 346   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1318943, 1321028, "...I could have one of those, right?", 347 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61886, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61886, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61886, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61886, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61886, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61886, 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61886, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 347   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 177, 1321195, 1323280, "Not if I get there first.", 348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61887, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61887, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61887, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61887, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61887, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61887, 596 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/English.srt" ,  `subtitle`.`phrase_number` = 348   WHERE  `subtitle`.`id` = 177 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 89, 177 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 89, 2, "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 4500, 5290, "Что такое, а?", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61888, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61888, 715 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61888, 666 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 5540, 9510, "Не могу найти себе что-нибудь поесть.\r\nМеня от всего тошнит.", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 5532 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 22708 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61889, 2294 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 9760, 14090, "Говорю вам - быть беременной это\r\nвам не тортик кушать. O, тортик!", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 7606 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тортик" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 29836 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кушать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 29837 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "o" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 29838 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61890, 29836 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 16550, 19390, "Дорогая, я сожалею.", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61891, 11698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61891, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61891, 14879 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 19640, 23850, "О, а чем это пахнет?\r\nЭто из ванны.", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61892, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61892, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61892, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61892, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61892, 6587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61892, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61892, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61892, 9725 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 24100, 27900, "Беременность наделяет людей\r\nстранными пристрастиями.", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61893, 3566 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "наделяет" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61893, 29839 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61893, 4351 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "странными" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61893, 29840 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пристрастиями" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61893, 29841 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 28150, 33740, "Это я, это Фиби.\r\nСлушай, здесь есть что-то, что я хочу съесть.", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 2085 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61894, 16684 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 33990, 36660, "Что это так хорошо пахнет?", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61895, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61895, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61895, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61895, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61895, 6587 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 36910, 41080, "Это шампунь?\r\nЭто гуава.", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61896, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61896, 25261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61896, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гуава" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61896, 29842 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 41290, 43660, "О! Погоди-ка.", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61897, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61897, 6820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61897, 2653 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 43910, 47540, "Это мой бутерброд с докторской?", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61898, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61898, 891 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бутерброд" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61898, 29843 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61898, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "докторской" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61898, 29844 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 48090, 49630, "Да... да... да...", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61899, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61899, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61899, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 50250, 54550, "Я не могу поверить! Ребенок\r\nхочет колбасу. Я не могу есть мясо.", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 4993 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 2150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 1466 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "колбасу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 29845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61900, 670 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 54800, 57090, "Погоди-ка!", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61901, 6820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61901, 2653 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 57340, 59680, "Может огурчик?", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61902, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61902, 23296 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 59930, 63060, "Сезон 4, серия 16.\r\nТа, с фальшивой вечеринкой.", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61903, 1849 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61903, 1051 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61903, 1851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61903, 9560 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61903, 1957 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61903, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "фальшивой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61903, 29846 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61903, 14424 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 63270, 65890, "Русские субтитры:\r\nDais 2012.", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61904, 13306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61904, 13307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61904, 13308 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "2012" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61904, 29847 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 111310, 112390, "Что ты пишешь?", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61905, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61905, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61905, 15278 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 112640, 116980, "Джошуа должен завтра зайти, и раз\r\nу меня не хватает смелости...", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 29259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 2024 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 7566 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 7469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61906, 20392 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 117100, 121320, "пригласить его, я продам ему пальто\r\nи положу эту записку в карман.", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 1247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "продам" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 29848 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 5964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 11313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 3012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 1915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61907, 13377 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 121570, 125650, "\"Джошуа, позвони мне как-нибудь.\r\nТакие парни как ты...", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 29259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 5129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 1386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 7386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61908, 654 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 125910, 128990, "...никогда не выходят из моды.\"", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61909, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61909, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61909, 2103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61909, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61909, 23272 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 129240, 132790, "А что же ты выбросила?", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61910, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61910, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61910, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61910, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выбросила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61910, 29849 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 133500, 136370, "- Привет, ребята.\r\n- Чем вы занимались?", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61911, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61911, 843 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61911, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61911, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61911, 12574 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 136620, 140790, "Мы ходили на выставку\r\nвикторианских дверных ручек в музее.", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 13341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 18700 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "викторианских" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 29850 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дверных" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 29851 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ручек" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 29852 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61912, 9215 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 141050, 143960, "Без меня?..", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61913, 841 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61913, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 144130, 147760, "Мой дядя нас туда затащил,\r\nно это оказалось довольно интересно.", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 6356 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 4077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 8882 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 3437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 8894 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61914, 2117 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 148010, 150930, "Они такие витиеватые и красивые.\r\nПосмотрите.", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61915, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61915, 1386 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "витиеватые" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61915, 29853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61915, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61915, 5956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61915, 2303 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 151180, 156310, "Не знаю насчёт музеев в Англии, но здесь,\r\nиз музея нельзя ничего выносить.", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 2157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 25865 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 22088 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 12736 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61916, 24786 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 156560, 158350, "Я взяла это в сувенирной лавке.", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61917, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61917, 878 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61917, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61917, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сувенирной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61917, 29854 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "лавке" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61917, 29855 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 158600, 162070, "У них там чрезвычайно плохо с охраной.", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61918, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61918, 1388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61918, 713 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "чрезвычайно" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61918, 29856 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61918, 2263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61918, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "охраной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61918, 29857 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 162190, 165190, "Это шутка.", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61919, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61919, 9287 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 168820, 172830, "Мне уже пора идти.\r\nУвидимся ещё.", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61920, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61920, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61920, 1273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61920, 1955 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61920, 2581 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61920, 845 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 174620, 176290, "А вы хорошо проводите время.", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61921, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61921, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61921, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61921, 7543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61921, 1012 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 176540, 180670, "Она восхитительна,\r\nи к тому-же такая весёлая.", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61922, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61922, 15544 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61922, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61922, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61922, 4869 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61922, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61922, 2616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61922, 13201 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 180920, 186170, "И, знаете что?\r\nКогда я с ней, я тоже весёлый!", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 1157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 2016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61923, 7101 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 186340, 190050, "Я даже записался\r\nна вертолетные курсы.", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61924, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61924, 684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61924, 27458 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61924, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вертолетные" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61924, 29858 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61924, 9590 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 190300, 193430, "Она уезжает через 2 дня.\r\nМне не придется туда ходить.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 25850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 1833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 1850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 1320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 2631 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 4077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61925, 1130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 193680, 195680, "Через 2 дня?\r\nТы должно быть расстроен.", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61926, 1833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61926, 1850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61926, 1320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61926, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61926, 9257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61926, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61926, 11286 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 195930, 200060, "Ей нужно вернуться в Лондон,\r\nно я был к этому готов с самого начала.", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 840 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 2638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 29685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 2969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 2247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 2941 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61927, 7088 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 200310, 204020, "Мы знали что у нас только 2 недели\r\nи всё.", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 8069 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 1850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 5595 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61928, 681 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 204280, 207400, "Точно как мои все отношения.", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61929, 2128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61929, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61929, 1097 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61929, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61929, 2598 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 207650, 212490, "Но в случае Росса, - они оба\r\nзнали что у них только 2 недели, и всё.", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 999 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 858 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 3346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 8069 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 1388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 1850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 5595 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61930, 681 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 213030, 214990, "- Фибс!\r\n- Привет!", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61931, 2577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61931, 729 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 215160, 219330, "Да, я знаю. Хорошо, да!", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61932, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61932, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61932, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61932, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61932, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 219710, 222040, "Так вот, ребенок жаждет мяса.", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61933, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61933, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61933, 2150 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "жаждет" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61933, 29859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61933, 7864 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 222290, 226960, "Я сделала соевый бургер, чтобы\r\nон подумал что это мясо.", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 7627 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "соевый" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 29860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 12777 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 2950 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61934, 670 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 227210, 229130, "В результате у меня тошнота.", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61935, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "результате" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61935, 29861 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61935, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61935, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61935, 28780 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 229380, 233430, "Может, потому что\r\nсоевый бургер - гадость?", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61936, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61936, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61936, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61936, 29860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61936, 12777 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61936, 2723 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 233680, 235720, "Беременность тяжела для животика.", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61937, 3566 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тяжела" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61937, 29862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61937, 768 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "животика" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61937, 29863 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 235970, 238770, "Но зато у тебя появился сияющий\r\nореол беременной девушки.", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 1991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 20677 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сияющий" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 29864 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 18605 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 7606 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61938, 7505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 239020, 244230, "Если б тебя всё утро тошнило,\r\nты бы тоже так сиял.", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 10066 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 1260 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тошнило" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 29865 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 679 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сиял" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61939, 29866 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 251860, 256660, "- Вот пальто, какое ты хотел.\r\n- Отлично.", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61940, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61940, 5964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61940, 2008 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61940, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61940, 1016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61940, 1422 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 257080, 258660, "О!, слушай, оно удобное.", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61941, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61941, 2085 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61941, 1974 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "удобное" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61941, 29867 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 258910, 262330, "В нем можно в парке людям\r\nпричендалы демонстрировать.", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61942, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61942, 1321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61942, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61942, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61942, 1290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61942, 11784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61942, 29207 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "демонстрировать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61942, 29868 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 262580, 268170, "Не клади руки в карманы\r\nпока не выйдешь из магазина.", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 27571 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "карманы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 29869 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 5499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61943, 6851 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 268420, 269420, "Почему?", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61944, 661 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 269670, 271380, "Ну, потому что...", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61945, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61945, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61945, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 271630, 275430, "...у нас много...", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61946, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61946, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61946, 2200 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 276260, 277970, "Вот всё они портят.", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61947, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61947, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61947, 2022 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "портят" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61947, 29870 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 278270, 279600, "Да уж...", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61948, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61948, 1253 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 279600, 279640, "Я в этом свитере\r\nбыл на свидании вчера вечером.\r\nДа уж...", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 25391 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 1208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 1953 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 3826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61949, 1253 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 279640, 283650, "Я в этом свитере\r\nбыл на свидании вчера вечером.", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 25391 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 1208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 1953 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61950, 3826 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 283690, 286770, "Это было первое свидание\r\nсо времени развода.", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61951, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61951, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61951, 7981 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61951, 685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61951, 676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61951, 2959 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61951, 6632 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 287020, 290820, "Поздравляю.\r\nТак ты её любишь?", 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61952, 1252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61952, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61952, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61952, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61952, 2243 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 291070, 294700, "Нет, нет. Она милая,\r\nно там я вдруг понял...", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 2944 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 702 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61953, 903 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 294820, 298540, "...что я не готов пока к свиданиям.", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61954, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61954, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61954, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61954, 2247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61954, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61954, 831 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "свиданиям" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61954, 29871 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 298870, 300200, "А!\r\nЧтож...", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61955, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61955, 14090 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 302120, 305710, "Это интересно.", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61956, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61956, 2117 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 305960, 309550, "- Что это было?\r\n- Устройство против воров.", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61957, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61957, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61957, 742 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "устройство" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61957, 29872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61957, 4904 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61957, 26393 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 309800, 311260, "Тогда что это?", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61958, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61958, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61958, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 311510, 317140, "Те тоже нужны, ведь это\r\nвор может порвать.", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61959, 2934 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61959, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61959, 1382 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61959, 726 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61959, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вор" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61959, 29873 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61959, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61959, 22768 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 321060, 326690, "Так, у нас будет вечеринка через 5 минут,\r\nтак что, все меняйте свои планы.", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 5506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 1833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 1127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 2629 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 24147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 1359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61960, 1056 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 326940, 328190, "- О чём это ты?\r\n- Что случилось?", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61961, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61961, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61961, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61961, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61961, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61961, 868 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 328440, 331400, "У нас должна быть неожиданная\r\nпрощальная вечеринка для Эмили.", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 1058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 816 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "неожиданная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 29874 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прощальная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 29875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 5506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61962, 19107 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 331650, 334660, "Но на самом деле - для Джошуа.", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61963, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61963, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61963, 1979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61963, 1980 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61963, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61963, 29259 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 334910, 337280, "Он сказал, что не готов к свиданиям.", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61964, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61964, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61964, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61964, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61964, 2247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61964, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61964, 29871 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 337530, 340370, "Так что, мне пришлось пригласить\r\nего на вечеринку вне моей работы.", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 10605 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 1247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 28591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 11744 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 2056 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61965, 2102 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 340620, 346000, "И так я создала себе\r\nидеальную возможность соблазнить его.", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "создала" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 29876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 5785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 5068 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "соблазнить" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 29877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61966, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 346250, 351210, "Как бы сильно я ни хотел познакомиться с\r\nДжошем, и предупредить его...", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 5445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 3824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 1016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 8276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "джошем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 29878 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 21860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61967, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 351590, 355590, "...нас с Эмили здесь не будет.\r\nОна зайдет только попрощаться.", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 19107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 7962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61968, 17089 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 355840, 361470, "Я спланировал особый вечер.\r\nИзвини. Никаких вечеринок.", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61969, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "спланировал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61969, 29879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61969, 12781 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61969, 1057 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61969, 1447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61969, 1063 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61969, 11131 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 363810, 366690, "Сюрприз!", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61970, 5505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 366940, 370320, "Сюрприз!", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61971, 5505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 370440, 373030, "Мне никогда раньше\r\nне устраивали вечеринку-сюрприз.", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61972, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61972, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61972, 900 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61972, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "устраивали" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61972, 29880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61972, 28591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61972, 5505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 373280, 377620, "Ну, это всё идея Росса.", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61973, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61973, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61973, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61973, 4515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61973, 858 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 377700, 380080, "Ты такой милый.", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61974, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61974, 1310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61974, 4138 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 380330, 383830, "И я так удивлена.", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61975, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61975, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61975, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61975, 20565 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 386580, 389460, "Так ты не знала?", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61976, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61976, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61976, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61976, 792 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 398260, 401140, "Почему ты здесь\r\nесли Джошуа там?", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61977, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61977, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61977, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61977, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61977, 29259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61977, 713 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 401390, 404980, "Я пытаюсь выглядеть труднодоступной.", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61978, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61978, 12514 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61978, 5875 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "труднодоступной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61978, 29881 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 405230, 407940, "Быстро! Он смотрит сюда!\r\nСкажи что-нибудь смешное.", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61979, 4404 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61979, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61979, 6031 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61979, 1357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61979, 1164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61979, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61979, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61979, 16016 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 408190, 410980, "Например, что?", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61980, 2211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61980, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 411900, 415030, "Что у вас смешного?", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61981, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61981, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61981, 1044 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61981, 5585 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 415440, 418110, "Я сказал: \"Например, что?\"", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61982, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61982, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61982, 2211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61982, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 418530, 421240, "Ну и мыслитель.", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61983, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61983, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мыслитель" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61983, 29882 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 421490, 424290, "Эта труднодоступность\r\nне работает.", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61984, 2966 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "труднодоступность" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61984, 29883 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61984, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61984, 2145 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 424540, 428290, "Подай мне вишенки.", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61985, 13401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61985, 695 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вишенки" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61985, 29884 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 434960, 437260, "- Хочешь вишенку?\r\n- Нет, спасибо.", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61986, 1035 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вишенку" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61986, 29885 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61986, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61986, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 437510, 443100, "А я могу завязать их в узел\r\nодним только языком.", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 930 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "завязать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 29886 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 1935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "узел" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 29887 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 1412 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61987, 3835 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 454980, 456570, "Все нормально?", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61988, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61988, 2198 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 456820, 459200, "В порядке?", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61989, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61989, 766 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 463200, 465490, "Похоже, нам скоро пора идти.", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61990, 1899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61990, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61990, 2625 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61990, 1273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61990, 1955 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 465740, 468250, "Но вечеринка только начинается.", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61991, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61991, 5506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61991, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61991, 4747 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 468500, 472460, "А мы должны быть в  Четырёх Сезонах\r\nчерез 15 минут.", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 2305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 16124 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сезонах" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 29888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 1833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 9120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61992, 2629 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 472710, 474500, "А затем у нас ужин в Плазе.", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61993, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61993, 8033 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61993, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61993, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61993, 6308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61993, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "плазе" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61993, 29889 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 474750, 478920, "А чего-же запланировал вечеринку\r\nв то же время?", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 1000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 778 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "запланировал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 29890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 28591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61994, 1012 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 482220, 486310, "Вообще-то, американские вечеринки-сюрпризы\r\nочень короткие.", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61995, 897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61995, 835 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "американские" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61995, 29891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61995, 14080 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61995, 13048 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61995, 723 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "короткие" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61995, 29892 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 486560, 492520, "Обычно это, \"Сюрприз!\"\r\nЗатем, \"Боже, как неожиданно. Пока!\"", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61996, 12510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61996, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61996, 5505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61996, 8033 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61996, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61996, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61996, 21463 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61996, 1859 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 493110, 495320, "Но мне здесь очень нравится.", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61997, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61997, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61997, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61997, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61997, 3639 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 495570, 499530, "Твоя сестра рассказала что\r\nв детстве ты переодевался бабушкой...", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 1879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 5849 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 10561 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 2604 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "переодевался" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 29893 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61998, 6318 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 499780, 503780, "...и устраивал понарошковые\r\nчаепития.", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61999, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "устраивал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61999, 29894 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61999, 16735 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "чаепития" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 61999, 29895 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 505240, 508370, "Моника это рассказала, так ведь?", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62000, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62000, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62000, 10561 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62000, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62000, 726 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 508620, 511870, "О, точно, а что мы положим\r\nсверху на всё это?", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 2128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 18419 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 15689 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62001, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 512120, 515250, "Немного салями.", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62002, 1119 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "салями" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62002, 29896 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 516880, 519380, "А что на салями?", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62003, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62003, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62003, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62003, 29896 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 519630, 522010, "Пастрами.", 117 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пастрами" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62004, 29897 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 524430, 527970, "Да ты гений.", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62005, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62005, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62005, 22346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 529310, 532560, "Помоги мне с молнией.", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62006, 2052 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62006, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62006, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62006, 5864 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 533730, 536520, "Вверх!", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62007, 7660 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 537900, 541450, "- Ты переоделась?\r\n- Мне необходимо моё удачливое платье.", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62008, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62008, 23568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62008, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62008, 7954 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62008, 1948 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "удачливое" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62008, 29898 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62008, 5939 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 541700, 545870, "- Удача - это больше декольте?\r\n- Для меня это так.", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62009, 18684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62009, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62009, 888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62009, 20781 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62009, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62009, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62009, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62009, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 555790, 557550, "Посмотри на него!\r\nОн такой милый.", 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62010, 1992 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62010, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62010, 748 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62010, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62010, 1310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62010, 4138 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 557800, 560670, "Я хочу его схватить и поцеловать.", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62011, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62011, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62011, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62011, 9291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62011, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62011, 5437 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 560920, 563590, "Как я могу его поцеловать,\r\nчтобы он не догадался что он мне нравится?", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 5437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 7874 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62012, 3639 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 563840, 568430, "Я знаю как его привлечь.\r\nСними лифчик.", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62013, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62013, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62013, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62013, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62013, 10160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62013, 9648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62013, 4081 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 569060, 572310, "- Есть эпизод в \"Ногодрыжке\"...\r\n- Танце-вспышке.", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62014, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62014, 5073 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62014, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ногодрыжке" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62014, 29899 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "танце" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62014, 29900 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вспышке" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62014, 29901 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 572560, 576360, "- С девушкой сантехницей.\r\n- Сварщицей.", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62015, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62015, 4216 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сантехницей" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62015, 29902 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сварщицей" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62015, 29903 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 577360, 580690, "Ты тоже снимался в нем, что-ли?", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62016, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62016, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62016, 13969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62016, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62016, 1321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62016, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62016, 1234 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 581530, 586240, "Она снимает под рубашкой лифчик\r\nи вытаскивает его через рукав.", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 16800 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 5782 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рубашкой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 29904 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 4081 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вытаскивает" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 29905 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 1833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62017, 25746 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 586490, 590740, "Очень сексуально. И классично.", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62018, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62018, 14024 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62018, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "классично" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62018, 29906 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 592910, 596750, "- Или для поцелуя можешь использовать омелу.\r\n- Сейчас не рождество.", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 768 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поцелуя" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 29907 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 11342 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "омелу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 29908 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62019, 6640 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 597000, 600380, "- Раскрути бутылочку.\r\n- Ему не 11.", 133 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "раскрути" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62020, 29909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62020, 27607 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62020, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62020, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62020, 7364 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 602720, 605680, "Спасибо.\r\nЭто было так мило.", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62021, 756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62021, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62021, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62021, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62021, 2916 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 605930, 607140, "Вы уходите?", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62022, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62022, 11458 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 607390, 611100, "- Да, нам еще нужно успеть в другие места.\r\n- Я думаю, что тоже пойду.", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 3781 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 2104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 5605 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62023, 2300 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 611350, 614140, "Ребята, вы ещё не можете уйти.\r\nВы должны остаться.", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 843 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 2804 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 7882 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 2305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62024, 963 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 614390, 617400, "У нас еще столько всего\r\nзапланировано.", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62025, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62025, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62025, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62025, 2284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62025, 1183 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "запланировано" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62025, 29910 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 617650, 618730, "Например?", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62026, 2211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 618980, 622610, "Раскрути-бутылочку работает так:\r\nя кручу.", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62027, 29909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62027, 27607 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62027, 2145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62027, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62027, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62027, 18659 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 622860, 627200, "Указывает на Гантера,\r\nи я целую Гантера.", 141 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "указывает" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62028, 29911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62028, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62028, 19358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62028, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62028, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62028, 12829 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62028, 19358 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 629870, 631450, "Кто начнет?", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62029, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62029, 25601 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 631700, 634460, "Я.", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62030, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 638210, 641300, "Добро пожаловать в Америку.", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62031, 1429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62031, 1430 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62031, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "америку" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62031, 29912 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 646970, 649430, "O, боже!", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62032, 29838 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62032, 860 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 649680, 653930, "Два подряд!\r\nТеперь с языками.", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62033, 1223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62033, 8405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62033, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62033, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "языками" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62033, 29913 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 658060, 661020, "Круто.", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62034, 2816 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 661360, 664650, "Какое совпадение?", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62035, 2008 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62035, 4185 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 665030, 668110, "Так, ладно! Достаточно!", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62036, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62036, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62036, 13695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 668200, 671830, "Пусть кто-нибудь ещё поиграет.", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62037, 4948 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62037, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62037, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62037, 845 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поиграет" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62037, 29914 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 672080, 676460, "Если не хочешь играть,\r\nчего тогда пришёл на вечеринку?", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 3572 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 1000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 2795 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62038, 28591 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 676910, 680000, "Хорошо, моя очередь!", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62039, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62039, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62039, 5436 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 682630, 683670, "Посмотрите-ка!", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62040, 2303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62040, 2653 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 683920, 686720, "O, боже.", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62041, 29838 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62041, 860 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 689300, 691550, "Боже! Ребёнок только что лягнулся!", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62042, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62042, 2150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62042, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62042, 643 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "лягнулся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62042, 29915 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 691800, 696480, "Если раз лягнулся, лягнется ещё раз!", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62043, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62043, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62043, 29915 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "лягнется" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62043, 29916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62043, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62043, 1038 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 698770, 704400, "Все запомните свои места.", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62044, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62044, 22441 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62044, 1359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62044, 5605 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 712490, 715580, "Жук!", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62045, 23618 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 716660, 718410, "Неважно, как сильно мне хочется.", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62046, 5173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62046, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62046, 5445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62046, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62046, 745 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 718660, 723670, "Я никогда не буду есть мясо.\r\nВедь это убийство. Хладнокровное убийство.", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 726 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "убийство" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 29917 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "хладнокровное" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 29918 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62047, 29917 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 729760, 733600, "На моем сендвиче Фиби!", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62048, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62048, 2695 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сендвиче" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62048, 29919 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62048, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 734010, 735390, "Фиби, что ты делаешь?", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62049, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62049, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62049, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62049, 2721 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 735640, 737810, "Я не могу сдержаться!", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62050, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62050, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62050, 930 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сдержаться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62050, 29920 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 738060, 740940, "Мне нужно мясо!\r\nРебёнку нужно мясо.", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62051, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62051, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62051, 670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62051, 6350 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62051, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62051, 670 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 741190, 744860, "Да! Это как если ты с кем-то встречаешься,\r\nты же не хочешь изменять ему,..", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 2144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 655 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 8503 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62052, 2171 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 744940, 748070, "разве-что с кем-то очень уж хорошеньким.", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62053, 2177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62053, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62053, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62053, 2144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62053, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62053, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62053, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62053, 29223 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 749450, 750240, "Точно.", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62054, 2128 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 750490, 753620, "Тут всё то же самое.", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62055, 644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62055, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62055, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62055, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62055, 2942 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 753870, 757580, "Раз уж делаешь что-то неправильное...", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62056, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62056, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62056, 2721 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62056, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62056, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62056, 26948 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 758330, 760790, "...делай это правильно.", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62057, 2000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62057, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62057, 5133 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 766050, 767460, "Тебе лучше?", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62058, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62058, 991 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 767710, 770550, "Да, но какой ценой?", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62059, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62059, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62059, 657 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ценой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62059, 29921 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 770800, 772800, "6 месяцев, 3 блюда в день?", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62060, 4771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62060, 1185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62060, 2622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62060, 3507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62060, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62060, 1032 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 773050, 778520, "В итоге я съем,\r\nгде-то, миллион коров.", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62061, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62061, 7096 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62061, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "съем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62061, 29922 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62061, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62061, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62061, 822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62061, 11821 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 779520, 783310, "Что, если я компенсирую\r\nтвоё потребление мяса?", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62062, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62062, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62062, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "компенсирую" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62062, 29923 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62062, 4249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62062, 9155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62062, 7864 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 783560, 785400, "Я ем много мяса, так?", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62063, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62063, 10189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62063, 2200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62063, 7864 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62063, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 785650, 789150, "Но допустим, пока ребенок не родится,\r\nя не буду есть мясо.", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 3621 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 2150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "родится" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 29924 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62064, 670 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 789400, 794410, "Никаких лишних животных не умрет.\r\nТы будешь есть моих животных.", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 1063 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 6860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 3451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 12795 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 747 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 1937 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62065, 3451 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 794490, 796330, "Не могу поверить,\r\nчто ты пойдешь на это ради меня.", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 4993 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 9732 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 762 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62066, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 796580, 800370, "Конечно!\r\nЯ могу быть вегетарианцем.", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62067, 775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62067, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62067, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62067, 816 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вегетарианцем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62067, 29925 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 800620, 803500, "В пиве ведь нет мяса, правда?", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62068, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пиве" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62068, 29926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62068, 726 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62068, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62068, 7864 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62068, 780 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 803750, 809460, "Мы все ещё можем успеть на ужин,\r\nесли пропустим закуски, и тут-же попросим счёт.", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 4091 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 3781 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 6308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 790 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пропустим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 29927 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 10451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 28996 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62069, 2783 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 812550, 817430, "Мы не можем сейчас уйти.\r\nРейчел собирается что-то показать.", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 4091 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 7882 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 844 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 8502 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62070, 5935 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 817970, 820520, "O, боже!\r\nТы с ума сошла?", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62071, 29838 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62071, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62071, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62071, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62071, 6035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62071, 11314 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 820640, 821980, "Нет-нет-нет-нет.", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62072, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62072, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62072, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62072, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 822230, 825690, "Я наконец-то ясно мыслю.", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62073, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62073, 3436 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62073, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62073, 818 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мыслю" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62073, 29928 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 825900, 827940, "Мое платье, приносящее удачу, не сработало.", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62074, 1948 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62074, 5939 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "приносящее" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62074, 29929 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62074, 20077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62074, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62074, 15928 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 828190, 831990, "Но уже 4 года,\r\nэта крошка не промахивалась.", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62075, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62075, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62075, 1051 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62075, 1224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62075, 2966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62075, 11730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62075, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "промахивалась" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62075, 29930 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 832240, 834860, "Рейч, Рейч, подожди, я не могу позволить...", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62076, 13310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62076, 13310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62076, 972 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62076, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62076, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62076, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62076, 8511 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 835110, 838910, "А вобще,.. я хочу посмотреть что получится.", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62077, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62077, 13329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62077, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62077, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62077, 2899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62077, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62077, 2182 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 841250, 844620, "Симпатичный... костюм.", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62078, 3807 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62078, 3711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 844750, 850630, "Ну, я хотела показать Эмили\r\nбольшое американское \"до свидания\".", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 1082 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 5935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 19107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 967 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 25353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62079, 693 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 854630, 856800, "Готовы.", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62080, 5067 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 857050, 858680, "Дайте мне \"E\"!", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62081, 3723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62081, 695 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "e" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62081, 29931 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 858930, 860510, "Дайте \"M\"!", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62082, 3723 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "m" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62082, 29932 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 860760, 862140, "Дайте \"I\"!", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62083, 3723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62083, 17093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 862390, 863560, "Дайте \"L\"!", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62084, 3723 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "l" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62084, 29933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 863810, 865270, "Дайте \"Y\"!", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62085, 3723 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "y" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62085, 29934 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 865520, 868520, "Что получилось?", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62086, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62086, 2118 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 868730, 870320, "Эмили!", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62087, 19107 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 875610, 879410, "Хорошо! Это я - чирлидер!", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62088, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62088, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62088, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "чирлидер" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62088, 29935 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 880740, 884040, "У меня стал шататься зуб.\r\nЭто ничего. У меня есть дантист.", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 4995 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шататься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 29936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 6395 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62089, 16360 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 884290, 888330, "Я положу лед.\r\nИзвините.", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62090, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62090, 11313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62090, 3407 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62090, 1042 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 889170, 890500, "Что мне теперь делать?", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62091, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62091, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62091, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62091, 2025 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 890750, 893300, "Я думаю ты уже все сделала.", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62092, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62092, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62092, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62092, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62092, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62092, 7627 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 893710, 897260, "Время снимать лифчик.", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62093, 1012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62093, 13182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62093, 4081 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 899350, 902640, "Это было очень здорово.", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62094, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62094, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62094, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62094, 1261 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 902890, 904810, "Но мне уже пора.", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62095, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62095, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62095, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62095, 1273 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 905060, 909610, "Сни-ма-й лиф-чи-к.", 209 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сни" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62096, 29937 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62096, 7951 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62096, 1848 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "лиф" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62096, 29938 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "чи" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62096, 29939 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62096, 831 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 911570, 914740, "Пойдем за твоим пальто.", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62097, 4188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62097, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62097, 7129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62097, 5964 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 916650, 919820, "Рейчел моя девушка.", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62098, 844 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62098, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62098, 1958 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 921620, 924160, "Чтож, было весело.", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62099, 14090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62099, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62099, 5885 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 924410, 927670, "Да, было очень весело.", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62100, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62100, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62100, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62100, 5885 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 931290, 934710, "Знаешь, этот лифчик...", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62101, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62101, 717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62101, 4081 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 935800, 939510, "...мне очень мешает.", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62102, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62102, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62102, 5480 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 939760, 942930, "Раньше, это была моя спальня.", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62103, 900 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62103, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62103, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62103, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62103, 21863 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 943260, 947810, "Много здесь воспоминаний.\r\nмного воспоминаний.", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62104, 2200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62104, 827 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "воспоминаний" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62104, 29940 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62104, 2200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62104, 29940 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 948060, 950100, "Если бы эти стены могли говорить,\r\nони бы сказали:", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 1094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 23909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 3379 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 1202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62105, 3700 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 950350, 954020, "\"Хочешь услышать эти воспоминания?\"", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62106, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62106, 5484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62106, 1094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62106, 11792 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 959030, 960410, "Помощь нужна?", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62107, 2155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62107, 955 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 960660, 964240, "Нет-нет!\r\nУ меня все под контролем.", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62108, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62108, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62108, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62108, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62108, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62108, 5782 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "контролем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62108, 29941 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 964790, 968830, "Что-то совсем не похоже.", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62109, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62109, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62109, 1997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62109, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62109, 1899 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 972040, 976090, "Боже! Забудь!", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62110, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62110, 10230 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 978920, 981930, "Все идёт совсем не так\r\nкак должно было произойти.", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 3737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 1997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 9257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62111, 16954 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 982180, 985100, "А как всё должно было произойти?", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62112, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62112, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62112, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62112, 9257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62112, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62112, 16954 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 985600, 989060, "Можешь на меня не смотреть,\r\nкогда я это скажу?", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 5508 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62113, 2072 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 991770, 996110, "Я думала,\r\nчто если ты ко мне придёшь,..", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62114, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62114, 1453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62114, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62114, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62114, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62114, 1070 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62114, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62114, 11871 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 996360, 1000650, "...я смогу соблазнить тебя.", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62115, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62115, 3411 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62115, 29877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62115, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1004780, 1008120, "Я не одеваю костюмы на работу.", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62116, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62116, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "одеваю" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62116, 29942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62116, 3392 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62116, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62116, 1318 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1008370, 1011620, "А я их купил у тебя 6 штук.", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62117, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62117, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62117, 1935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62117, 11259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62117, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62117, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62117, 4771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62117, 8046 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1011870, 1016340, "Извини.\r\nЯ думала они тебе нужны.", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62118, 1447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62118, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62118, 1453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62118, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62118, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62118, 1382 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1016380, 1020630, "Дело в том, что я возвращаюсь\r\nпотому что хочу тебя увидеть.", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 1154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 1823 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "возвращаюсь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 29943 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62119, 2306 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1020930, 1021970, "Зачем?", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62120, 909 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1022130, 1024470, "Потому что ты мне нравишься.", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62121, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62121, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62121, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62121, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62121, 12770 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1024720, 1027970, "Я тебе нравлюсь?", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62122, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62122, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62122, 2846 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1028140, 1032900, "Я имею ввиду, что ты красивая\r\nи умная и утонченная.", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 1101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 13441 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 4320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 5136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "утонченная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62123, 29944 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1033150, 1036770, "Многое из этого основано\r\nне на сегодняшнем дне.", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62124, 21651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62124, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62124, 1220 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "основано" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62124, 29945 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62124, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62124, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62124, 21283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62124, 3770 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1038070, 1039650, "Но я тебе нравлюсь?", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62125, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62125, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62125, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62125, 2846 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1039900, 1045990, "Боже, не могу поверить. Всё это время,\r\nты мне нравился, а я нравилась тебе!", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 4993 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 1012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 12120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 5812 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62126, 753 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1046620, 1047830, "Не надо \"но\".", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62127, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62127, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62127, 803 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1048080, 1054080, "\"Но\" это всегда плохо. Давай остановимся\r\nна \"Ты мне нравишься, а я - тебе.\"", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 706 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 2263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 5920 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 12770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62128, 753 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1055170, 1059340, "- Однако...\r\n- Нет, это более изощрённое \"но\".", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62129, 6969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62129, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62129, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62129, 1346 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "изощрённое" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62129, 29946 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62129, 803 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1060170, 1064260, "Мой брак только недавно закончился.", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62130, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62130, 8320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62130, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62130, 2161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62130, 19046 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1064510, 1069600, "И я, на самом деле, не готов\r\nчто-то начинать сейчас.", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 1979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 1980 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 2247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 1401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62131, 752 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1070020, 1072100, "Нн-оо,..", 245 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нн" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62132, 29947 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "оо" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62132, 29948 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1073810, 1078570, "Мне жаль.\r\nМне нужно ещё немного времени.", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62133, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62133, 2965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62133, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62133, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62133, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62133, 1119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62133, 2959 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1086410, 1089160, "Вот ты где.\r\nЯ искала тебя.", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62134, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62134, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62134, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62134, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62134, 25422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62134, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1089410, 1092580, "Джошуа ушёл,\r\nтак что вы с Эмили можете идти.", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 29259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 9746 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 19107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 2804 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62135, 1955 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1092830, 1094540, "Все нормально.", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62136, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62136, 2198 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1094790, 1098670, "Она всё ещё там, и наслаждается\r\nэтой фальшивой вечеринкой.", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "наслаждается" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 29949 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 5026 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 29846 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62137, 14424 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1098920, 1102590, "Мы уже опоздали на всё,\r\nчто я запланировал.", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62138, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62138, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62138, 17576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62138, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62138, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62138, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62138, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62138, 29890 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1102920, 1103970, "Росс,..", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62139, 798 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1104050, 1107390, "... мне жаль,\r\nя испортила тебе вечер.", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62140, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62140, 2965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62140, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62140, 23892 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62140, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62140, 1057 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1107930, 1109180, "Да!", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62141, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1110310, 1115690, "Если это тебе поможет,\r\nя выставила себя дурой.", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62142, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62142, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62142, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62142, 12828 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62142, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выставила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62142, 29950 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62142, 947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62142, 11418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1116060, 1118480, "Немного помогает.", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62143, 1119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62143, 1140 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1119110, 1121860, "У тебя найдётся место\r\nдля жалкой неудачницы?", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62144, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62144, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62144, 3329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62144, 1270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62144, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62144, 6865 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "неудачницы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62144, 29951 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1122110, 1125450, "Да, садись.", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62145, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62145, 1047 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1130530, 1132700, "Мне очень жаль.", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62146, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62146, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62146, 2965 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1132950, 1133790, "Ничего.", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62147, 1214 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1134040, 1136960, "Это всё равно было только на 2 недели.", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62148, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62148, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62148, 1818 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62148, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62148, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62148, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62148, 1850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62148, 5595 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1137210, 1139540, "Я просто не хотел\r\nчтобы это так закончилось.", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62149, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62149, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62149, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62149, 1016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62149, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62149, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62149, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62149, 19122 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1139790, 1143090, "Или может ты не хотел,\r\nчтобы это вообще закончилось?", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 1016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62150, 19122 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1143340, 1144970, "О чём ты?", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62151, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62151, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62151, 654 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1145220, 1148970, "Похоже, она тебе очень нравится.", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62152, 1899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62152, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62152, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62152, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62152, 3639 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1149970, 1153180, "Да, очень.", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62153, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62153, 723 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1153520, 1157600, "Что же мне делать?\r\nМы же договорились, что это на 2 недели.", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 2025 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 686 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 1850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62154, 5595 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1157850, 1161230, "Без обязательств.", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62155, 841 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обязательств" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62155, 29952 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1164110, 1168780, "Эта девушка провела весь вечер\r\nобщаясь с твоими друзьями...", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 2966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 1958 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 12047 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 2984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 1057 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "общаясь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 29953 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 16031 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62156, 4211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1169030, 1171410, "...просила рассказать о тебе что-нибудь...", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62157, 1464 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62157, 2113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62157, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62157, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62157, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62157, 658 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1171660, 1174290, "...смотрела\r\nфотоальбомы Моники.", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62158, 16950 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "фотоальбомы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62158, 29954 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62158, 964 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1174540, 1178080, "Никто такого не делает,\r\nесли планирует только на 2 недели.", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 1279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 731 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 994 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 790 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "планирует" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 29955 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 1850 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62159, 5595 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1178330, 1180880, "Думаешь?", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62160, 1209 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1181130, 1185760, "У тебя 14 часов,\r\nдо того, как везти её в аэропорт.", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 6014 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 2201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 1151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 787 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "везти" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 29956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62161, 10699 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1186010, 1188260, "А ты сидишь здесь\r\nв коридоре...", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62162, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62162, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62162, 4121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62162, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62162, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62162, 10951 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1188510, 1193220, "...с 28-летней чирлидершей\r\nс распухшей губой.", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62163, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62163, 14355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62163, 27678 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "чирлидершей" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62163, 29957 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62163, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "распухшей" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62163, 29958 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "губой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62163, 29959 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1195310, 1198440, "Да, ты права.", 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62164, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62164, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62164, 2254 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1202520, 1203770, "Спасибо.", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62165, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1208400, 1210910, "Что это был за фотоальбом?", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62166, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62166, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62166, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62166, 870 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "фотоальбом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62166, 29960 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1211160, 1215370, "Там была фотка тебя\r\nи кучки детей альбиносов.", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62167, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62167, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62167, 8822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62167, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62167, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62167, 18822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62167, 1928 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "альбиносов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62167, 29961 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1217160, 1219620, "O, боже!\r\nЭто не альбиносы.", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62168, 29838 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62168, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62168, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62168, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "альбиносы" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62168, 29962 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1219870, 1223460, "Это компьютерный лагерь! Рейч!", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62169, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "компьютерный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62169, 29963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62169, 7948 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62169, 13310 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1227880, 1230010, "Ты, ведь, жалкий неудачник, так?", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62170, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62170, 726 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62170, 11351 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62170, 21117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62170, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1230130, 1231510, "О, да!", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62171, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62171, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1232220, 1235010, "Садись!", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62172, 1047 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1241310, 1245150, "O, боже. Джошуа.", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62173, 29838 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62173, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62173, 29259 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1246320, 1249150, "Всё, что я сказал\r\nо том что я не готов...", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 796 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 1823 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62174, 2247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1249400, 1253450, "- Это не правда?\r\n- Нет, всё правда.", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62175, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62175, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62175, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62175, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62175, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62175, 780 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1254740, 1257290, "Но...", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62176, 803 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1259290, 1262210, "Мне нравится это \"но\".", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62177, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62177, 3639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62177, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62177, 803 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1271180, 1275680, "Хочешь зайти выпить кофе?", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62178, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62178, 7566 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62178, 7474 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62178, 838 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1282690, 1286440, "Всегда.", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62179, 706 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1290150, 1294370, "- Что у тебя здесь?\r\n- Пастрами.", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62180, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62180, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62180, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62180, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62180, 29897 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1296370, 1300500, "- Ты знаешь что подойдет к ней?\r\n- Солонина.", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62181, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62181, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62181, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62181, 8812 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62181, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62181, 2016 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "солонина" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62181, 29964 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1300830, 1304750, "Я собирался сказать докторская.\r\nНо это на много лучше.", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 4807 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 1212 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "докторская" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 29965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 2200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62182, 991 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1305000, 1308550, "Как насчёт копчёной индейки?", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62183, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62183, 2157 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "копчёной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62183, 29966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62183, 6463 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1309670, 1312470, "O, мама!", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62184, 29838 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62184, 2042 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1312590, 1316390, "- Сколько ещё до рождения?\r\n- 6 месяцев.", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62185, 1132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62185, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62185, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62185, 3373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62185, 4771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62185, 1185 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1316640, 1321140, "Если корова умрёт по естественным причинам,\r\nя ведь смогу её поесть, так ведь?", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 4505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 12795 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 20292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 13449 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 726 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 3411 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 22708 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62186, 726 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 178, 1321390, 1325020, "Нет, если я доберусь до нее первая.", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62187, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62187, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62187, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "доберусь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62187, 29967 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62187, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62187, 1399 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 62187, 6954 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 89 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 04/s04e16 - The One with the Fake Party/Russian.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 178 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 89, 178 );