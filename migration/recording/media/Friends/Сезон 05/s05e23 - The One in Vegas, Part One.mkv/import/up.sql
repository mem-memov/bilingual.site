INSERT INTO `source` ( `source`.`file` ) VALUES ( "Friends/Сезон 05/s05e23 - The One in Vegas, Part One.mkv" );
# a query
INSERT INTO `video` ( `video`.`source_id`, `video`.`language_id`, `video`.`file`, `video`.`width`, `video`.`height`, `video`.`duration` ) VALUES ( 120, 1, "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/movie.mp4", 768, 432, 1332791 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 120, 2, "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.mp3", 1332810 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 120, 239 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 120, 1, "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.mp3", 1332810 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 120, 240 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 120, 1, "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 5172, 6215, "Hello?", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82900, 105 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 6381, 8383, "-Hey, Joey!\r\n-<i>Hey, Pheebs.</i>", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82901, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82901, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82901, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82901, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82901, 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82901, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 8592, 9843, "Can you do me a favor?", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82902, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82902, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82902, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82902, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82902, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82902, 3929 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 10052, 13430, "I forgot the PIN to my ATM card.\r\nCan you get it for me?", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 7291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 8640 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 5203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 588 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82903, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 13639, 14681, "<i>Sure. Where is it?</i>", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82904, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82904, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82904, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82904, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82904, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82904, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 14848, 17935, "Uh, I scratched it on the ATM machine\r\non the corner.", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 11205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 5203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82905, 17771 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 18852, 20896, "Oh, so you\'re 5-6-3-9?", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82906, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82906, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82906, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82906, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82906, 7740 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82906, 12942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82906, 7742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82906, 4549 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 21104, 22773, "That\'s it! Thanks.", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82907, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82907, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82907, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82907, 130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 24316, 26818, "-Wanna talk to Chandler?\r\n-Let me talk to him.", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82908, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 27027, 28820, "<i>He didn\'t believe in my movie...</i>", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 4647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82909, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 29029, 33242, "...which is a big mistake,\r\nbecause it is real. Real.", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 3278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82910, 336 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 33951, 35619, "I\'ve tried to apologize.", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82911, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82911, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82911, 4617 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82911, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82911, 18518 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 35869, 39414, "If I can\'t do it on the phone,\r\nI\'ll go and do it in person.", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82912, 252 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 39623, 41667, "I heard that. Uh, can you put him on?", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 2450 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82913, 14 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 41833, 42876, "Yeah.", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82914, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 44920, 46380, "-Hey.\r\n-Don\'t come out here.", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82915, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82915, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82915, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82915, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82915, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82915, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 48507, 51927, "I support you 100 percent\r\nand I wanna prove that to you in person.", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 17745 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 3077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 583 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 4729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82916, 252 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 52135, 55389, "I got that. I forgive you.\r\nDon\'t come out here.", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 15101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82917, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 55597, 57766, "You haven\'t taken\r\nmy calls for a week.", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 471 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 1749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82918, 411 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 57975, 61812, "I\'m totally over it. Friends forever.\r\nDon\'t come out here.", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 1651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 3058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82919, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 63188, 65482, "Would you mind doing\r\na picture with us?", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82920, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82920, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82920, 3105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82920, 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82920, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82920, 4526 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82920, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82920, 198 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 66316, 67359, "Uh, what was that?", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82921, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82921, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82921, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82921, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 67567, 69361, "Uh, <i>Entertainment Tonight.</i>", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82922, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82922, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82922, 16528 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82922, 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82922, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 71530, 74491, "Good talking to you\r\nand don\'t come out here.", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82923, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 114990, 116033, "Monica?", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82924, 85 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 116283, 118744, "I\'m sorry I\'m late.", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82925, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82925, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82925, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82925, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82925, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82925, 1710 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 121913, 123206, "Monica?", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82926, 85 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 124291, 125334, "Phoebe?", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82927, 88 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 126960, 129379, "I\'m so sorry.\r\nHave you been here long?", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82928, 374 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 129588, 131256, "That\'s okay.", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82929, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82929, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82929, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 132299, 134051, "What the hell took you so long?", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82930, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82930, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82930, 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82930, 2404 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82930, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82930, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82930, 374 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 136219, 138889, "You can\'t tell Chandler.\r\nI ran into Richard.", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 1638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82931, 4602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 139097, 140557, "-Which Richard?\r\n-The Richard.", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82932, 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82932, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82932, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82932, 4602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 140766, 142934, "Richard Simmons? Oh, my God!", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82933, 4602 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "simmons" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82933, 34851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82933, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82933, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82933, 181 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 144227, 148523, "No, my ex-boyfriend Richard.\r\nYou know, the tall guy, mustache.", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 1533 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 3066 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 3947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82934, 12013 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 148732, 152110, "Okay, that actually makes more sense.\r\nHow was it?", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 1591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 4530 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82935, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 152319, 156114, "It was really nice. We talked\r\nand I had lunch with him.", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 1764 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 4689 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82936, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 156323, 159451, "That\'s weird. I dreamed\r\nyou had lunch with Richard.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 17529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 4689 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82937, 4602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 159660, 161578, "-Really?\r\n-Again, Richard Simmons.", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82938, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82938, 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82938, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82938, 34851 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 162329, 163372, "Go on.", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82939, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82939, 14 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 163622, 166083, "The strange part was\r\nhe was really nice...", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82940, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82940, 9539 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82940, 1480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82940, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82940, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82940, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82940, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82940, 299 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 166291, 169211, "...and he looks great,\r\nbut I didn\'t feel anything.", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82941, 319 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 169419, 171254, "So why can\'t we tell Chandler?", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82942, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82942, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82942, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82942, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82942, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82942, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82942, 193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 171463, 176051, "It\'ll freak him out and tomorrow\'s our\r\nanniversary. I don\'t wanna spoil it.", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 3294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 1583 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 8193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 9087 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82943, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 176259, 179346, "I can\'t believe you guys\r\nhave lasted a whole year!", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 27087 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 592 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82944, 362 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 179554, 181848, "Wow, I owe Rachel 20 bucks.", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82945, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82945, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82945, 5283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82945, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82945, 4666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82945, 6129 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 182891, 183934, "What?", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82946, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 184142, 185936, "On a totally different bet.", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82947, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82947, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82947, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82947, 4051 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82947, 7191 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 188271, 189314, "Hey!", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82948, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 189481, 191942, "-It\'s almost our anniversary!\r\n-I know.", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82949, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82949, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82949, 5671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82949, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82949, 8193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82949, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82949, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 192150, 195237, "One year ago today, I was just\r\nyour annoying friend Chandler.", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 1756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 2510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 1691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82950, 193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 195404, 198907, "Aw, now you\'re just\r\nmy annoying friend Chandler.", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 5684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 2510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 1691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82951, 193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 199491, 201576, "-Huh.\r\n-I got you a gift.", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82952, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82952, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82952, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82952, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82952, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82952, 2447 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 201785, 205580, "-It\'s not till tomorrow.\r\n-I know, but you have to open it today.", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 4578 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 1583 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 2389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82953, 125 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 218260, 220429, "Okay! There you go!", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82954, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82954, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82954, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82954, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 220679, 223348, "-It\'s two tickets to Vegas!\r\n-Wow.", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82955, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82955, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82955, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82955, 3082 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82955, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82955, 639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82955, 477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 223515, 226476, "Yeah! For this weekend!\r\nIt\'ll be perfect.", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82956, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82956, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82956, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82956, 7204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82956, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82956, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82956, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82956, 3923 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 226685, 230814, "You\'ll see Joey, plus, we\'ll start\r\nour celebration on the plane.", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 10359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 2453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 22521 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82957, 6221 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 231064, 234109, "We can call it our \"Plane-a-versary.\"", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82958, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82958, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82958, 1622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82958, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82958, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82958, 6221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82958, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "versary" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82958, 34852 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 235193, 236236, "We have to?", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82959, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82959, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82959, 4 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 238363, 241658, "This is great, but Joey didn\'t want\r\nany of us there.", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82960, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 241867, 246580, "He doesn\'t want us to go to any trouble.\r\nHe\'ll be excited when we surprise him.", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 369 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 5258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82961, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 246830, 250667, "Plus, we get to have\r\nour own \"Anna-Vegas-ry\"!", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 10359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 595 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "anna" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 34853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 639 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "ry" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82962, 34854 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 252711, 255297, "\"Anna-vada-versary!\"", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82963, 34853 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "vada" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82963, 34855 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82963, 34852 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 256131, 258758, "I think we should see other people.", 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82964, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82964, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82964, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82964, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82964, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82964, 1520 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82964, 55 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 258967, 260302, "We can go, right?", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82965, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82965, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82965, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82965, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 260594, 261803, "Great idea.", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82966, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82966, 392 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 262012, 263305, "I\'m gonna go too!", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82967, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82967, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82967, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82967, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82967, 576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 266475, 268602, "Pheebs, it\'s kind of\r\nour anniversary.", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82968, 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82968, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82968, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82968, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82968, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82968, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82968, 8193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 268810, 271563, "You won\'t ditch me\r\nlike you did with London.", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 18859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82969, 7218 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 271772, 273607, "Ditch you?\r\nYou were pregnant-", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82970, 18859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82970, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82970, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82970, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82970, 1543 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 273774, 275358, "Uh-huh. Great story. I\'m going.", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82971, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82971, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82971, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82971, 4046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82971, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82971, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82971, 17 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 278320, 280197, "-Hi.\r\n-Hey.", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82972, 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82972, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 280405, 284659, "Listen, this weekend we\'re all gonna go\r\nto Las Vegas to surprise Joey!", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 7204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 5258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82973, 126 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 286620, 288163, "Including me!", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82974, 12589 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82974, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 288830, 289873, "Do you wanna go?", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82975, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82975, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82975, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82975, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 290081, 292000, "I can take a couple days off work.", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82976, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82976, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82976, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82976, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82976, 3284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82976, 1597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82976, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82976, 11 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 292209, 296630, "Of course you can take a couple days\r\noff, because this trip includes me!", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 551 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 3284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 1597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 18575 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "includes" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 34856 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82977, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 298465, 301718, "I\'ve got a presentation tomorrow.\r\nI can\'t miss that.", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 19254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 1583 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 1614 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82978, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 301927, 305972, "I\'ve got tickets to the Van Gogh\r\nexhibit. I waited a year for this.", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 3082 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 16833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 27066 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "exhibit" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 34857 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 5242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82979, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 306806, 310101, "-Rach, maybe we can fly out Saturday?\r\n-Sounds great.", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 10345 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 3872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82980, 505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 310310, 311770, "I\'ll call the airlines.", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82981, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82981, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82981, 1622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82981, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "airlines" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82981, 34858 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 312687, 315607, "That\'ll be nice to have\r\nthe apartment to myself.", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 5195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82982, 108 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 316274, 317776, "To walk around naked.", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82983, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82983, 6775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82983, 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82983, 73 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 320278, 323740, "No, so I can be by myself.\r\nHave a little alone time.", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82984, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 324324, 325617, "Naked alone time.", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82985, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82985, 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82985, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 326493, 330080, "Just because I\'m alone doesn\'t mean\r\nI wanna walk around naked.", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 6775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82986, 73 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 330330, 332707, "You live alone,\r\nyou don\'t walk around naked.", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 6775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82987, 73 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 332916, 335835, "Uh-huh. Why does it takes me so long\r\nto answer the door?", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 8086 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82988, 4673 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 342467, 347138, "So far, is this trip to Las Vegas\r\nbetter or worse than the trip to London?", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 1571 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 18575 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 1681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 18575 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82989, 7218 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 347389, 350016, "So far, it\'s pretty much\r\nthe same, Pheebs.", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 1571 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 1485 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 2372 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82990, 375 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 350767, 353979, "What about after I give you\r\nthese candies?", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82991, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82991, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82991, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82991, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82991, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82991, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82991, 385 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "candies" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82991, 34859 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 355313, 357691, "-I guess it\'s a little better now.\r\n-Ha, ha.", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82992, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 358441, 361903, "Las Vegas, 1. London, 0.", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82993, 638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82993, 639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82993, 8620 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82993, 7218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82993, 18548 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 362529, 363572, "I\'ll be back.", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82994, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82994, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82994, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82994, 63 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 367367, 368952, "Happy Plane-a-versary.", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82995, 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82995, 6221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82995, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82995, 34852 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 369494, 372163, "Aw. I love you.", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82996, 5684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82996, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82996, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82996, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 372414, 375250, "-Can I give you your present now?\r\n-Okay!", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82997, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82997, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82997, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82997, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82997, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82997, 5186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82997, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82997, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 379879, 381840, "Oh, don\'t tell me I did this.", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82998, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82998, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82998, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82998, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82998, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82998, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82998, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82998, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 382090, 385468, "I love \"I forgot the present\" fake-out!", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82999, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82999, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82999, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82999, 7291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82999, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82999, 5186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82999, 15406 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 82999, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 386177, 389889, "How about \"I did forget the present,\r\nforgive me\" not fake-out?", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 1713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 5186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 15101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 15406 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83000, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 390473, 392893, "You can give it to me\r\nwhen we get back.", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83001, 63 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 393143, 395520, "This is the worst thing\r\nthat could happen.", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83002, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83002, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83002, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83002, 4054 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83002, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83002, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83002, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83002, 4006 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 395729, 398648, "You decided to tell him\r\nabout the Richard thing.", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 2559 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83003, 381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 402861, 404195, "What \"Richard thing\"?", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83004, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83004, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83004, 381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 404404, 406281, "Oh, no.", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83005, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83005, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 408074, 409117, "What \"Richard thing\"?", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83006, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83006, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83006, 381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 409326, 411119, "Simmons. Go with Simmons.", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83007, 34851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83007, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83007, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83007, 34851 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 418293, 420253, "I ran into Richard yesterday.", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83008, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83008, 1638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83008, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83008, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83008, 5696 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 420462, 424174, "He asked me if I wanted to go\r\nfor a bite and I did.", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 1754 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 5302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83009, 158 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 424382, 426968, "I didn\'t tell you\r\nbecause you\'d get mad...", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83010, 1600 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 427177, 429679, "...and I didn\'t wanna spoil\r\nour anniversary.", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83011, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83011, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83011, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83011, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83011, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83011, 9087 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83011, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83011, 8193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 430263, 431765, "I\'m not mad.", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83012, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83012, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83012, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83012, 1600 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 433767, 434809, "Really?", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83013, 140 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 436186, 439272, "So you bumped into Richard.\r\nYou grabbed a \"bite.\"", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 21584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83014, 5302 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 441858, 443443, "No big deal.", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83015, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83015, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83015, 1671 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 446613, 447948, "Great.", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83016, 505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 451576, 454079, "Okay, London, 1.", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83017, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83017, 7218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83017, 8620 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 460752, 462253, "Ooh.", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83018, 340 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 474516, 476017, "Hmm.", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83019, 623 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 482941, 484567, "Oh, look what happened.", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83020, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83020, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83020, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83020, 452 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 486945, 488822, "<i>Huh, check me out.</i>", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83021, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83021, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83021, 3080 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83021, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83021, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83021, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 489030, 491366, "<i>I\'m in my kitchen...</i>", 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83022, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83022, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83022, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83022, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83022, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83022, 1598 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83022, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 491616, 493118, "<i>...naked.</i>", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83023, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83023, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83023, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 493952, 496454, "<i>I\'m picking up an orange.</i>", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83024, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83024, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83024, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83024, 2425 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83024, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83024, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83024, 20710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83024, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 496955, 497997, "<i>I\'m naked.</i>", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83025, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83025, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83025, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83025, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83025, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 500792, 503336, "<i>I\'m lighting the candles.</i>", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83026, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83026, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83026, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83026, 13573 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83026, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83026, 5211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83026, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 503545, 506339, "<i>Naked. And carefully.</i>", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83027, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83027, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83027, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83027, 19584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83027, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 517308, 520228, "<i>Oh, my God! That\'s Rachel, naked!</i>", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83028, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 522397, 525025, "<i>I can\'t look at that.\r\nI am looking at this.</i>", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83029, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 525233, 529195, "<i>Okay, vivid colors,\r\nexpressive brush strokes.</i>", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83030, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83030, 46 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "vivid" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83030, 34860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83030, 3993 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "expressive" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83030, 34861 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83030, 4678 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "strokes" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83030, 34862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83030, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 530321, 534200, "<i>Unless she wants me\r\nto be looking at that.</i>", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 1627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83031, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 534909, 536619, "<i>She knows I\'m home.</i>", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83032, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83032, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83032, 1771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83032, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83032, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83032, 620 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83032, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 536870, 539748, "<i>She knows I can see her.</i>", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83033, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83033, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83033, 1771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83033, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83033, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83033, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83033, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83033, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 539998, 543585, "<i>What kind of game is she playing?</i>", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 2469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 1639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83034, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 543835, 547797, "<i>I think maybe someone\'s lonely tonight.</i>", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 13557 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83035, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 548465, 550258, "<i>Oh, Dr. Geller.</i>", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83036, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83036, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83036, 1715 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83036, 1762 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83036, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 550508, 555180, "<i>Stop it. You\'re being silly. Ha, ha.</i>", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 10361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83037, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 555346, 557140, "<i>Or am I?</i>", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83038, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83038, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83038, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83038, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83038, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 560101, 561895, "<i>Love to love you baby</i>", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83039, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83039, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83039, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83039, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83039, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83039, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83039, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 562062, 563605, "<i>Ow</i>", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83040, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83040, 4042 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83040, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 564397, 567025, "<i>Love to love you baby\r\nOw</i>", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83041, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83041, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83041, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83041, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83041, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83041, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83041, 4042 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83041, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 574324, 577118, "<i>Love to love you baby\r\nOw</i>", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83042, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83042, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83042, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83042, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83042, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83042, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83042, 4042 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83042, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 578495, 579829, "Darn it!", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83043, 13888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83043, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 589422, 590465, "Hey.", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83044, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 595845, 597096, "May I come in?", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83045, 1697 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83045, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83045, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83045, 64 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 598139, 599808, "Uh, yeah, if you want to.", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83046, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83046, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83046, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83046, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83046, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83046, 4 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 600809, 602727, "Do you want me to?", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83047, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83047, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83047, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83047, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83047, 4 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 604521, 605647, "Yeah, sure?", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83048, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83048, 196 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 605855, 607690, "So do I.", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83049, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83049, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83049, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 612362, 615824, "Okay, Rach, before anything happens...", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83050, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83050, 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83050, 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83050, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83050, 5672 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 616074, 618785, "...I wanna lay down a couple\r\nof ground rules.", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 6149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 3284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 12602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83051, 4064 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 618993, 621663, "This is just about tonight.", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83052, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83052, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83052, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83052, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83052, 353 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 621955, 626084, "I won\'t go through with this\r\nif it\'ll raise the question of \"us.\"", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 1673 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83053, 198 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 628211, 629629, "I just want this to be...", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83054, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83054, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83054, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83054, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83054, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83054, 21 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 632757, 634759, "...about what it is.", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83055, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83055, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83055, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83055, 50 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 639013, 640139, "And, um...", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83056, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83056, 590 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 640390, 642183, "...what is that, Ross?", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83057, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83057, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83057, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83057, 161 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 642392, 645019, "The physical act of love.", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83058, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83058, 3101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83058, 1487 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83058, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83058, 269 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 646563, 647981, "What, are you crazy?", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83059, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83059, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83059, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83059, 5758 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 648189, 653111, "So you weren\'t trying to entice me\r\njust now with your nakedness?", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 1758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 1506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 32552 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 183 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "nakedness" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83060, 34863 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 655947, 658408, "Oh, God. You saw me?", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83061, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83061, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83061, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83061, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83061, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 662120, 666291, "You weren\'t trying to entice me\r\nwith your nakedness?", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 1758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 1506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 32552 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83062, 34863 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 666499, 667542, "No!", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83063, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 667792, 671921, "You actually thought\r\nI wanted to have sex with you?", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 1754 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83064, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 675758, 677051, "No!", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83065, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 678887, 680346, "No.", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83066, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 680638, 681806, "No.", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83067, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 684350, 686185, "No, no, no, no.", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83068, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83068, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83068, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83068, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 709083, 711711, "Hey, you guys, wait! You guys.", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83069, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83069, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83069, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83069, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83069, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83069, 388 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 711961, 714214, "This place is much better than London.", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83070, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83070, 3063 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83070, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83070, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83070, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83070, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83070, 7218 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 714464, 717091, "A lady dressed like Cleopatra\r\ngave me this.", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83071, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83071, 3893 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83071, 3145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83071, 60 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "cleopatra" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83071, 34864 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83071, 1601 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83071, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83071, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 717342, 719928, "Ninety-nine cent\r\nsteak and lobster dinner. Huh.", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83072, 3073 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83072, 5317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83072, 10363 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83072, 8980 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83072, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83072, 14469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83072, 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83072, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 720094, 721471, "You don\'t eat animals.", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83073, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83073, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83073, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83073, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83073, 11587 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 721721, 723973, "For ninety-nine cents I\'d eat you.", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83074, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83074, 3073 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83074, 5317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83074, 10362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83074, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83074, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83074, 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83074, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 725433, 727602, "I can totally settle down here.", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83075, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83075, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83075, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83075, 10288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83075, 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83075, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 727810, 731981, "It\'s got everything I could ever want.\r\nIncluding Joey! Look!", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 1482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 12589 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83076, 80 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 733107, 734234, "Look! Hey!", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83077, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83077, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 734692, 735777, "Oh, my God.", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83078, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83078, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83078, 181 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 736194, 737820, "Hey, Joey!", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83079, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83079, 126 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 741366, 742617, "-Wow!\r\n-Hi.", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83080, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83080, 103 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 742825, 744369, "Love your condoms, my man.", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83081, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83081, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83081, 7233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83081, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83081, 1645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 745787, 748164, "Why are you here?\r\nI told you not to come.", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83082, 13 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 748373, 750375, "Why are you dressed as a gladiator?", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83083, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83083, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83083, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83083, 3145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83083, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83083, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "gladiator" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83083, 34865 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 750541, 755213, "Uh, because I\'m shooting a scene right now.\r\nI play a gladiator.", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 21803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 9892 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 3127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83084, 34865 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 756923, 759342, "Hold on a second. Can we cut?", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83085, 2413 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83085, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83085, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83085, 1728 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83085, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83085, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83085, 428 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 759592, 762095, "My friends are here.\r\nI\'m gonna take a break.", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 1651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83086, 593 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 762512, 764222, "Who are you talking to?", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83087, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83087, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83087, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83087, 434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83087, 4 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 765515, 766599, "The director.", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83088, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83088, 4679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 766766, 768476, "Uh, her.", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83089, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83089, 39 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 771688, 774732, "All right, it\'s not a gladiator movie.\r\nI work here.", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 34865 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 4647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83090, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 774983, 776401, "Why? What happened?", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83091, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83091, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83091, 452 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 776609, 778736, "The movie shut down.\r\nRan out of money.", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83092, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83092, 4647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83092, 509 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83092, 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83092, 1638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83092, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83092, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83092, 295 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 778945, 782240, "I\'m working here till it starts up,\r\nif it ever does.", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 3971 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 4578 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83093, 26 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 782448, 784158, "-I\'m so sorry.\r\n-Such a drag.", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83094, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83094, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83094, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83094, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83094, 405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83094, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83094, 24811 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 784409, 786619, "Sorry I didn\'t tell you. I\'m sorry.", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83095, 268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 786828, 789998, "Apparently there\'s\r\na new policy of not sharing.", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 1574 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 585 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 14234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83096, 1779 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 790206, 793710, "-I knew you weren\'t okay with that.\r\n-So you\'re a gladiator!", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 1758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83097, 34865 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 794627, 796087, "What\'s going on?", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83098, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83098, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83098, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83098, 14 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 796295, 799173, "-Monica had lunch with Richard.\r\n-Dawson?", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83099, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83099, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83099, 4689 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83099, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83099, 4602 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "dawson" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83099, 34866 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 800550, 801592, "No....", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83100, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 801801, 804262, "But that would\'ve been so cool!", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83101, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83101, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83101, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83101, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83101, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83101, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83101, 2531 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 804721, 806097, "Her boyfriend Richard.", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83102, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83102, 3066 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83102, 4602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 806306, 810601, "It meant nothing. After all this time,\r\nhow can you not trust me?", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 4694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 5239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83103, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 810810, 814272, "When you go lunching with hunky men\r\nand don\'t tell me!", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 40 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "lunching" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 34867 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 12 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hunky" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 34868 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 560 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83104, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 816441, 819277, "You\'re right. I\'m sorry.\r\nI should\'ve told you.", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83105, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 819861, 820903, "Thanks.", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83106, 130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 821821, 824615, "-Aw, there we go.\r\n-Now I love Vegas.", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83107, 5684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83107, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83107, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83107, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83107, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83107, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83107, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83107, 639 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 825700, 828786, "I promise you next time\r\nI absolutely will tell you.", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 2435 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 4022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 1484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83108, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 832206, 833249, "\"Next time\"?", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83109, 4022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83109, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 833708, 835209, "Ooh, so close.", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83110, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83110, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83110, 1531 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 836127, 839255, "There won\'t be a next time.\r\nYou cannot see him again.", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 4022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 390 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83111, 177 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 839464, 843092, "I cannot see him?\r\nYou can\'t tell me what to do.", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 390 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83112, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 843301, 845303, "That\'s funny, I think I just did.", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83113, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83113, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83113, 1737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83113, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83113, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83113, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83113, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83113, 158 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 846346, 849057, "If you act like this,\r\nI won\'t be around you.", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 1487 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83114, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 849265, 850933, "-Fine.\r\n-Happy anniversary!", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83115, 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83115, 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83115, 8193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 851142, 853561, "Guys, please! Come on, come on.", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83116, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83116, 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83116, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83116, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83116, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83116, 14 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 853770, 855897, "This is obviously\r\na misunderstanding.", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83117, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83117, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83117, 3899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83117, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83117, 1548 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 856147, 858107, "-No, it\'s not.\r\n-What\'re you talking about?", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83118, 208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 858316, 860109, "Don\'t look at me. I just work here.", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83119, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 864322, 868993, "Okay, um, Ross? I\'m a little warm,\r\nso I\'m gonna be taking off my sweater.", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 3944 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83120, 5191 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 869202, 871788, "Now, I\'m just letting you know...", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83121, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83121, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83121, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83121, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83121, 5325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83121, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83121, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 872038, 876125, "...this is not an invitation\r\nto the physical act of love.", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 30596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 3101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 1487 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83122, 269 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 877919, 880088, "Yep, that\'s hilarious.", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83123, 2478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83123, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83123, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83123, 27058 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 880922, 882924, "I\'m sorry. I\'m done. I\'m done.", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83124, 393 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 883132, 886260, "You know, last night was\r\nembarrassing for you too.", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 7222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83125, 576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 886469, 888388, "No, not really.", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83126, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83126, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83126, 140 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 888596, 891391, "But you know,\r\nI just don\'t embarrass that easily.", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 30852 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83127, 34316 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 891599, 893726, "What? You totally get embarrassed.", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83128, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83128, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83128, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83128, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83128, 6774 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 893935, 898523, "No, I don\'t. Ross, I think I\'m just\r\na more secure person than you are.", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 226 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "secure" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 34869 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83129, 109 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 898731, 900400, "-Is that so?\r\n-Yeah.", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83130, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83130, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83130, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83130, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 902235, 903444, "Hey, lady!", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83131, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83131, 3893 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 903653, 906030, "I don\'t care how much\r\nyou want it, okay?", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 2561 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83132, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 906280, 909242, "I am not gonna have sex\r\nwith you in the bathroom!", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83133, 4662 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 914539, 917834, "You know, in Roman times,\r\nthis was more than just a hat.", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 64 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "roman" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 34870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 410 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83134, 283 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 918626, 919669, "Really?", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83135, 140 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 919877, 921170, "Yeah, sure, sure.", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83136, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83136, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83136, 196 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 921420, 923423, "They would scrub the floors.", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83137, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83137, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83137, 4675 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83137, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83137, 4609 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 923673, 926217, "They\'d use it to get the mud\r\noff their shoe.", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 2378 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 32532 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 1676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83138, 279 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 926425, 929262, "Sometimes underneath\r\nthe horse would get dirty.", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83139, 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83139, 15493 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83139, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83139, 33231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83139, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83139, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83139, 3939 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 929470, 931264, "So they would stick it right-", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83140, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83140, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83140, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83140, 2541 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83140, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83140, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 932140, 935184, "I can\'t believe this is how\r\nI\'m spending my anniversary.", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 7284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83141, 8193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 935393, 938771, "I\'ll take you someplace nice.\r\nA guy tipped me $100.", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 14157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 18862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83142, 3077 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 938980, 940022, "-Whoa!\r\n-Yeah.", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83143, 2366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83143, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 940231, 943484, "He was playing blackjack\r\nfor an hour and he won $5000.", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 1639 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "blackjack" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 34871 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83144, 24197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 943734, 945403, "Can you believe that?", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83145, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83145, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83145, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83145, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 945611, 947822, "If I won $5000, I\'d join a gym.", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 24197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 3911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83146, 9437 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 948030, 949490, "Build up my upper body...", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83147, 19901 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83147, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83147, 95 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "upper" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83147, 34872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83147, 2527 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 949699, 952201, "...and hit Richard from behind\r\nwith a stick.", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83148, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83148, 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83148, 4602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83148, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83148, 5228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83148, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83148, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83148, 2541 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 953536, 956289, "Wait! Why don\'t I just\r\ndo what that guy did?", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83149, 158 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 956539, 958708, "I\'ll take this $100,\r\nturn it into $5000...", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 3077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 3214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83150, 24197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 958958, 962211, "...and I\'ll turn that into enough\r\nto get my movie going!", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 3214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 495 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 4647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83151, 17 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 963212, 964505, "Good luck.", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83152, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83152, 542 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 964714, 968217, "-I don\'t need luck. I\'ve thought this through.\r\n-I see.", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 542 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83153, 174 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 973347, 975099, "-Thank you.\r\n-Thanks.", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83154, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83154, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83154, 130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 975683, 979103, "I can\'t believe this.\r\nThis is like the worst night ever.", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 4054 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83155, 451 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 979312, 982732, "You had a minor setback\r\nin your relationship.", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83156, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83156, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83156, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83156, 14530 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "setback" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83156, 34873 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83156, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83156, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83156, 3102 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 982982, 984650, "Big deal. It\'s only Chandler.", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83157, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83157, 1671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83157, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83157, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83157, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83157, 193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 986527, 989197, "I am so sorry.", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83158, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83158, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83158, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83158, 268 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 991073, 994202, "This is crazy having\r\nsuch a stupid argument.", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83159, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83159, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83159, 5758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83159, 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83159, 405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83159, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83159, 2548 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83159, 9500 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 994452, 996162, "I don\'t wanna see Richard.", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83160, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83160, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83160, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83160, 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83160, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83160, 4602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 996370, 998331, "So go fix it.", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83161, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83161, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83161, 9459 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83161, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 998581, 1001250, "Find Chandler.\r\nHe\'s probably in your room.", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83162, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83162, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83162, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83162, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83162, 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83162, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83162, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83162, 213 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1001501, 1003377, "Tell him sorry and you love him.", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83163, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83163, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83163, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83163, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83163, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83163, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83163, 24 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1003920, 1005671, "You\'re right, Phoebe.", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83164, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83164, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83164, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83164, 88 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1005880, 1007340, "-Thank you.\r\n-Sure.", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83165, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83165, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83165, 196 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1008591, 1011886, "Yeah! Las Vegas! Number one!", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83166, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83166, 638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83166, 639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83166, 3205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83166, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1021646, 1024065, "-Anybody lose this?\r\n-Coming out.", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83167, 553 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83167, 4063 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83167, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83167, 2565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83167, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1024273, 1025525, "Place your bet.", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83168, 3063 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83168, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83168, 7191 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1026526, 1028069, "Dice her out.", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83169, 19915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83169, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83169, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1030321, 1032406, "Yellow 11. Pay the front line!", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83170, 5682 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83170, 7209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83170, 587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83170, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83170, 1587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83170, 552 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1036744, 1038371, "Hmm.", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83171, 623 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1041165, 1044669, "So, uh, I\'m on my way\r\nback to the bathroom.", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83172, 4662 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1047630, 1050925, "All right, all right.\r\nJust keep walking, all right?", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 3254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83173, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1060643, 1062478, "What are you- I am sorry, sir.", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83174, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83174, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83174, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83174, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83174, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83174, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83174, 6082 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1062687, 1065690, "I think he just really likes you.", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83175, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83175, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83175, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83175, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83175, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83175, 3274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83175, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1070152, 1071195, "Hey....", 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83176, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1071404, 1075074, "You know that teacher who had a baby\r\nwith her student?", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 9471 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83177, 20935 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1086586, 1088087, "-Can I help you?\r\n-Yes.", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83178, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83178, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83178, 376 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83178, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83178, 339 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1088296, 1093134, "Do you have any extra pants? Uh, my friend\r\nseems to have had a little accident.", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 2393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 1691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 3903 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83179, 5740 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1098806, 1101475, "-Can I change 100?\r\n-Changing 100.", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83180, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83180, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83180, 1677 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83180, 3077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83180, 6814 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83180, 3077 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1102560, 1104812, "-Good luck, sir.\r\n-Thanks.", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83181, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83181, 542 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83181, 6082 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83181, 130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1106689, 1108024, "That\'s right.", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83182, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83182, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83182, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1112987, 1115072, "-Thirteen.\r\n-Hit me!", 284 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "thirteen" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83183, 34874 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83183, 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83183, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1116532, 1118534, "Oh, man!", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83184, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83184, 1645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1121329, 1122496, "Wait!", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83185, 33 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1124707, 1126792, "You are not gonna believe this!", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83186, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83186, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83186, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83186, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83186, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83186, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1127001, 1129754, "I have found my identical hand twin.", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83187, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83187, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83187, 577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83187, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83187, 20982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83187, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83187, 1683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1133799, 1135051, "What?", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83188, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1135926, 1137762, "My identical hand twin!", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83189, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83189, 20982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83189, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83189, 1683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1139639, 1141307, "What\'s an identical hand twin?", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83190, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83190, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83190, 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83190, 20982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83190, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83190, 1683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1141515, 1145186, "It was incredible. The dealer\'s hands\r\nwere exactly like mine.", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 3269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 10280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 6751 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 614 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83191, 1555 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1145436, 1147855, "It was like looking\r\nat my hands in a mirror!", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 6751 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83192, 15115 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1149440, 1152401, "You sure you weren\'t\r\nlooking at your hands in a mirror?", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 1758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 6751 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83193, 15115 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1153402, 1157281, "Don\'t you see what this means?\r\nI can forget about that stupid movie.", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 2332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 1713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 2548 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83194, 4647 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1157490, 1159075, "I\'m gonna be a millionaire!", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83195, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83195, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83195, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83195, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83195, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83195, 23956 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1161327, 1162370, "How?", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83196, 154 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1162578, 1164914, "I don\'t have it all worked out yet.", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 3111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83197, 573 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1165164, 1168793, "But it\'s gotta mean big money.\r\nCome on, identical hands!", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 20982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83198, 6751 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1170419, 1172421, "Again, I must go back to...", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83199, 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83199, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83199, 1679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83199, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83199, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83199, 4 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1172672, 1174090, "...how?", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83200, 154 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1175424, 1179178, "This is Vegas, man.\r\nPeople will pay to see freaky stuff!", 302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 1645 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 19183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83201, 124 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1181097, 1183683, "How much would you pay\r\nto see this hand...", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83202, 2364 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1183933, 1186185, "...twice? Huh?", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83203, 5674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83203, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1189397, 1191982, "I can\'t really put a price on that.", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 26852 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83204, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1192817, 1194860, "Are you un-supporting me again?", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83205, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83205, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83205, 5301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83205, 9478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83205, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83205, 177 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1195361, 1196404, "No. No.", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83206, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83206, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1196654, 1200282, "I support you 100 percent.\r\nI just didn\'t get it right away.", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 17745 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 3077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 583 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83207, 323 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1200533, 1201951, "Now I\'m caught up.", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83208, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83208, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83208, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83208, 407 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83208, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1202201, 1204036, "Identical hand twins.", 310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83209, 20982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83209, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83209, 1682 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 310   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1204286, 1205704, "It\'s a million-dollar idea!", 311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83210, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83210, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83210, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83210, 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83210, 18863 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83210, 392 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 311   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1211419, 1212503, "-Hey!\r\n-Pheebs!", 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83211, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83211, 375 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 312   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1212712, 1214588, "I found my identical hand twin!", 313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83212, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83212, 577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83212, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83212, 20982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83212, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83212, 1683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 313   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1214797, 1217007, "Oh, you\'re so lucky!", 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83213, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83213, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83213, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83213, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83213, 4009 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 314   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1221637, 1224473, "So where\'s Monica?\r\nDid you guys make up?", 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83214, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 315   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1224682, 1227601, "-No.\r\n-But she just came up here.", 316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83215, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83215, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83215, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83215, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83215, 597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83215, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83215, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 316   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1227935, 1229520, "That was Joey.", 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83216, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83216, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83216, 126 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 317   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1230146, 1232273, "I wonder where she is? It\'s so weird.", 318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 4634 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83217, 99 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 318   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1232481, 1234358, "She\'s probably talking to Richard.", 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83218, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83218, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83218, 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83218, 434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83218, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83218, 4602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 319   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1234608, 1236235, "Would you stop that?", 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83219, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83219, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83219, 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83219, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 320   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1236902, 1240156, "You know what she said\r\nafter her lunch with Richard?", 321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 4689 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83220, 4602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 321   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1240406, 1243242, "She didn\'t feel anything for him.\r\nShe loves you.", 322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 14458 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83221, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 322   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1244618, 1246287, "-Really?\r\n-Yes.", 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83222, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83222, 339 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 323   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1246495, 1250875, "She feels terrible. She really wants\r\nto make up. You\'ve gotta find her.", 324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 4547 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 2430 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 1627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 1511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83223, 39 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 324   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1252793, 1253836, "Okay.", 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83224, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 325   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1254378, 1255588, "Good.", 326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83225, 346 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 326   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1262428, 1264972, "I should really start wearing hats.", 327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83226, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83226, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83226, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83226, 2453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83226, 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83226, 11556 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 327   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1310434, 1312478, "Welcome to Las Vegas.", 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83227, 455 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83227, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83227, 638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83227, 639 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 328   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1313604, 1315773, "-Did you enjoy your flight?\r\n-I did.", 329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83228, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83228, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83228, 2406 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83228, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83228, 6748 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83228, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83228, 158 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 329   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1315981, 1318526, "Thank you very much.\r\nIt was excellent.", 330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83229, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83229, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83229, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83229, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83229, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83229, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83229, 2385 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 330   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 239, 1319735, 1323739, "-I hope you had a nice flight.\r\n-Oh, it was the best.", 331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 6748 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83230, 315 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/English.srt" ,  `subtitle`.`phrase_number` = 331   WHERE  `subtitle`.`id` = 239 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 120, 239 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 120, 2, "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 5171, 6213, "Алло.", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83231, 5815 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 6381, 8382, "- Привет, Джо!\r\n- Привет, Фиби.", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83232, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83232, 1003 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83232, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83232, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 8591, 9842, "Сделай одолжение.", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83233, 4228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83233, 4229 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 10051, 13429, "Я забыл пин-код своего автоответчика.\r\nМожешь его посмотреть?", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 1239 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пин" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 34875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 20822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 2771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 30638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83234, 2899 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 13638, 14680, "Конечно. А где?", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83235, 775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83235, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83235, 875 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 14848, 17933, "Я нацарапал его\r\nна автоответчике сбоку.", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83236, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нацарапал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83236, 34876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83236, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83236, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83236, 9567 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сбоку" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83236, 34877 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 18852, 20894, "Значит 5-6-3-9 это твой?", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83237, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83237, 1127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83237, 4771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83237, 2622 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83237, 4778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83237, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83237, 1330 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 21104, 22771, "Точно! Спасибо, Фиби.", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83238, 2128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83238, 756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83238, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 24315, 26817, "- Привет\r\n- Хочешь поговорить с Чендлером?\r\n- Это Джо? Дай мне трубку.", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 2957 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 12186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 1003 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 1358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83239, 721 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 27027, 28819, "Нет. Он не поверил в мой фильм...", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83240, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83240, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83240, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83240, 30291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83240, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83240, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83240, 6927 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 29029, 33240, "...и был не прав,\r\nпотому что он стоящий.", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83241, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83241, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83241, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83241, 3769 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83241, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83241, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83241, 667 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стоящий" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83241, 34878 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 33950, 35217, "Знаешь что.", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83242, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83242, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 35569, 39413, "Я всю неделю пытался перед ним извиниться\r\nЕсли он не хочет это делать по телефону,\r\nя к нему приеду.", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 1128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 10227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 1962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 21389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 1466 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 2025 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 7441 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 6966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83243, 31066 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 39622, 41665, "Фиби, я это слышал.\r\nДай ему трубку.", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83244, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83244, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83244, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83244, 6302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83244, 1358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83244, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83244, 721 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 41833, 42875, "Сейчас.", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83245, 752 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 44919, 46378, "- Привет.\r\n- Не приезжай.", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83246, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83246, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83246, 30458 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 48506, 51925, "Нет, нет, нет. Я поддерживаю тебя на все 100%.\r\nЯ докажу тебе это.", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 9269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 6247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 22698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83247, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 52135, 55387, "Я все понял. Я тебя прощая.\r\nНе приезжай сюда.", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 903 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 668 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прощая" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 34879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 30458 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83248, 1357 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 55597, 57765, "Прощаешь? Ты всю неделю\r\nне отвечал на мои звонки.", 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прощаешь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 34880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 1128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 11422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 1097 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83249, 14242 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 57974, 61810, "Я уже успокоился, Чендлер.\r\nДрузья на веки. Только не приезжай.", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 990 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "успокоился" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 34881 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 1383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 33271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83250, 30458 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 63188, 65481, "Вы не могли бы с нами сфотографироваться?", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83251, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83251, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83251, 3379 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83251, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83251, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83251, 1174 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сфотографироваться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83251, 34882 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 66316, 67357, "Кто это?", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83252, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83252, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 67567, 69359, "Из журнала о кино.", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83253, 1108 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "журнала" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83253, 34883 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83253, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83253, 5013 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 71529, 74490, "Рад был тебя слышать.\r\nИ не приезжай. Пока.", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83254, 9239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83254, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83254, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83254, 5166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83254, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83254, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83254, 30458 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83254, 1859 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 114989, 116031, "Моника?", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83255, 826 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 116282, 118742, "Извини, что я опоздала.", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83256, 1447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83256, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83256, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83256, 5951 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 121913, 123205, "Моника?", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83257, 826 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 124290, 125332, "Фиби?", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83258, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 126960, 129378, "Фиби, прости.\r\nДавно ждешь?", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83259, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83259, 932 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83259, 1222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83259, 4967 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 129587, 131255, "Не очень.", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83260, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83260, 723 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 132298, 134049, "Почему ты так долго?", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83261, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83261, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83261, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83261, 3679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 136219, 138887, "Только не говори Чендлеру.\r\nЯ встретила Ричарда.", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83262, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83262, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83262, 3041 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83262, 7524 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83262, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83262, 7608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83262, 17877 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 139097, 140556, "- Какого Ричарда?\r\n- Того самого.", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83263, 3829 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83263, 17877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83263, 1151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83263, 2941 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 140765, 142933, "Симмонса? Боже мой!", 34 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "симмонса" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83264, 34884 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83264, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83264, 891 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 144227, 148522, "Нет, своего бывшего друга, Ричарда.\r\nТакой высокий с усами.", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 2771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 7005 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 2773 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 17877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 1310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 4263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83265, 12254 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 148731, 152109, "Тогда все понятно.\r\nНу и как он? Как он тебе?", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 4409 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83266, 753 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 152318, 156113, "Все было чудесно. Мы разговарились\r\nи я даже с ним пообедала.", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 6000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разговарились" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 34885 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83267, 13369 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 156322, 159449, "Поразительно. Мне приснилось,\r\nчто ты обедаешь с Ричардом.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83268, 13732 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83268, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83268, 34834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83268, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83268, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обедаешь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83268, 34886 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83268, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83268, 17907 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 159659, 161577, "- Правда?\r\n- Только с Симмонсом.", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83269, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83269, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83269, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "симмонсом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83269, 34887 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 163621, 166081, "Самое странное, что он\r\nбыл очень милым...", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83270, 2942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83270, 4184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83270, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83270, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83270, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83270, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83270, 8525 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 166291, 169209, "...он отлично выглядет,\r\nи я ничего к нему не почувствовала.", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 1422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 34702 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 6966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83271, 6552 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 169419, 171253, "Почему нельзя\r\nоб этом говорить Чендлеру?", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83272, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83272, 1393 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83272, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83272, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83272, 1202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83272, 7524 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 171462, 176049, "Он начнет ревновать. А завтра\r\nу нас годовщина. Я не хочу все испортить.", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 25601 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 23683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 2024 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 3394 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83273, 10030 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 176259, 180244, "- Не верится, что вы продержались целый год!\r\n- Мне тоже.", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 3022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 34646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 7123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 6057 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83274, 1071 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 180354, 181847, "Я должна Рэйчел 20 баксов.", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83275, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83275, 1058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83275, 824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83275, 5044 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83275, 6385 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 182891, 183932, "Что?", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83276, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 184142, 185934, "Мы с ней поспорили совсем о другом.", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83277, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83277, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83277, 2016 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "поспорили" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83277, 34888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83277, 1997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83277, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83277, 9219 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 188271, 189313, "Привет!", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83278, 729 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 189480, 191940, "- Завтра у нас годовщина!\r\n- Я знаю.", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83279, 2024 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83279, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83279, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83279, 3394 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83279, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83279, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 192150, 195235, "Подумать только, год назад\r\nя был просто твоим нудным другом Чендлером.", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 2749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 6057 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 2249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 7129 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нудным" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 34889 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 9219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83280, 12186 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 195403, 198906, "А теперь ты только мой\r\nнудный друг Чендлер.", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83281, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83281, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83281, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83281, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83281, 891 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нудный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83281, 34890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83281, 2152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83281, 2148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 199490, 201575, "Я приготовила тебе подарок.", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83282, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83282, 12096 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83282, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83282, 2792 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 201784, 205579, "- Но годовщина завтра.\r\n- Открыть ты должен сегодня.", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83283, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83283, 3394 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83283, 2024 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83283, 8749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83283, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83283, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83283, 749 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 206039, 207456, "Ладно.", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83284, 1037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 218259, 220427, "Дай, я сама.", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83285, 1358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83285, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83285, 791 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 220678, 223347, "Два билета до Вегаса!", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83286, 1223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83286, 12832 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83286, 872 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вегаса" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83286, 34891 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 223514, 226475, "На все выходные!", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83287, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83287, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83287, 3023 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 226684, 230812, "Все будет прекрасно. Мы повидаем Джо.\r\nА годовщину начнем отмечать в самолете.", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 2686 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "повидаем" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 34892 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 1003 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 24304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 7601 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 6317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83288, 11907 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 231064, 234107, "Назовем это \"Самолето-годовщиной\".\r\nГодится?", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83289, 20649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83289, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "самолето" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83289, 34893 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83289, 24262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83289, 13390 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 235193, 236235, "Это обязательно?", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83290, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83290, 2051 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 237736, 238336, "Нет", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83291, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 239063, 242257, "Это конечно здорово.\r\nНо Джо не хотел, чтобы мы к нему приезжали.", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 1261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 1003 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 1016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 6966 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "приезжали" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83292, 34894 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 241866, 244878, "Просто он не хотел доставлять нам хлопоты.", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83293, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83293, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83293, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83293, 1016 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "доставлять" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83293, 34895 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83293, 1083 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "хлопоты" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83293, 34896 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 244929, 250666, "Представь, он обрадуется, когда мы устроим ему сюрприз.\r\nМы отметим романтическую \'Вегавщину\'.", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 941 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 16608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 3531 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 5505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 647 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отметим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 34897 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "романтическую" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 34898 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вегавщину" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83294, 34899 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 252710, 255295, "\"Невадовщину\"", 65 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "невадовщину" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83295, 34900 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 256130, 258757, "Не говори при мне таких слов.", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83296, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83296, 3041 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83296, 4153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83296, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83296, 1019 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83296, 10568 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 258967, 260300, "Мы поедем?", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83297, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83297, 8372 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 260593, 261802, "-Да.\r\n-Ура!\r\n-Отлично", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83298, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83298, 3645 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83298, 1422 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 262011, 264003, "Отличная мысль, я тоже поеду!", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83299, 7445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83299, 1146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83299, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83299, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83299, 15660 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 266474, 268600, "Фиби, вообще-то это наша годовщина.", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83300, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83300, 897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83300, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83300, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83300, 1004 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83300, 3394 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 268810, 271561, "Опять хотите меня\r\nпродинамить как с Лондоном?", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83301, 924 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83301, 2898 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83301, 711 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "продинамить" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83301, 34901 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83301, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83301, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83301, 31670 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 271771, 274605, "Продинамить?\r\nФиби, ты носила троих детей...", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83302, 34901 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83302, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83302, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83302, 16929 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83302, 21185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83302, 1928 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 274773, 276457, "Не важно. Я еду.", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83303, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83303, 935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83303, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83303, 5889 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 278319, 280195, "- Привет.\r\n- Привет.", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83304, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83304, 729 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 280405, 284658, "В эти выходные мы все вместе\r\nедем в Вегас, чтобы сделать сюрприз Джо!", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 1094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 3023 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 650 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 7036 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 18803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 2250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 5505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83305, 1003 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 285719, 286661, "Включая меня!", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83306, 12686 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83306, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 287130, 289871, "Поедете с нами? Я вас приглашаю.", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83307, 8374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83307, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83307, 1174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83307, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83307, 1044 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83307, 25019 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 290081, 291999, "Я могла бы отпроситься\r\nс работы на пару дней.", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 3016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 936 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отпроситься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 34902 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 2102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 3680 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83308, 3390 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 292208, 296628, "Конечно, могла бы. И отпросишься,\r\nпотому что на этот раз я еду с вами!", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 3016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отпросишься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 34903 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 5889 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83309, 6594 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 298464, 301717, "Нет, подождите, завтра у меня презентация.\r\nЯ не могу ее пропустить.", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 2270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 2024 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 19467 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83310, 8757 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 301926, 305971, "А я взял билеты на выставку Ван Гога.\r\nЦелый год ее ждал.", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 6823 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 3366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 18700 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 16920 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гога" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 34904 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 7123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 6057 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83311, 12149 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 306806, 310100, "- Рэйч, может нам с тобой поехать вместе в субботу?\r\n- Я не против.", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 3523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 735 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 5818 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 650 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 5790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83312, 4904 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 310310, 311768, "Да? Я закажу билеты.", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83313, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83313, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83313, 7122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83313, 3366 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 312687, 315605, "Приятно будет весь вечер\r\nпровести в квартире одной.", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83314, 2956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83314, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83314, 2984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83314, 1057 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83314, 6249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83314, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83314, 3738 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83314, 1068 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 316274, 317774, "Чтобы походить голышом.", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83315, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83315, 22632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83315, 26816 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 320278, 323739, "Нет. Чтобы побыть одной,\r\nкогда никто не мешает.", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83316, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83316, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83316, 8066 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83316, 1068 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83316, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83316, 1279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83316, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83316, 5480 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 324324, 325615, "Не мешает ходить голышом.", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83317, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83317, 5480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83317, 1130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83317, 26816 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 326492, 330078, "Нет, Фиби. То, что я остаюсь одна,\r\nеще не значит, что я стану ходить голой.", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 6593 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 846 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 7938 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 1130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83318, 4985 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 330330, 332706, "Ты вообще одна живешь.\r\nТы же не ходишь голой.", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 846 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 3617 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 4206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83319, 4985 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 332915, 335834, "А почему, ты думаешь,\r\nя так долго не открываю дверь?", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 1209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 3679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 2691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83320, 5058 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 342467, 347137, "На данный момент поездка в Вегас\r\nлучше поездки в Лондон или хуже?", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 24154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 3633 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 5886 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 18803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 18977 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 29685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83321, 2137 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 347388, 350015, "Пока они мало чем отличаются.", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83322, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83322, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83322, 1893 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83322, 890 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отличаются" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83322, 34905 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 350767, 353977, "А если я вас угощу конфетами?", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83323, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83323, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83323, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83323, 1044 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "угощу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83323, 34906 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "конфетами" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83323, 34907 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 355313, 357689, "- Теперь она стала гораздо лучше.\r\n- Хорошо.", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83324, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83324, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83324, 1129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83324, 4817 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83324, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83324, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 358441, 361902, "Лас-Вегас : Лондон\r\n1:0", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83325, 1471 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83325, 18803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83325, 29685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83325, 1847 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "0" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83325, 34908 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 362528, 363570, "Я сейчас приду.", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83326, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83326, 752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83326, 4478 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 367367, 368950, "С первой \'самолетовщиной\' тебя.", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83327, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83327, 3442 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "самолетовщиной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83327, 34909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83327, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 369494, 372162, "Я люблю тебя.", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83328, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83328, 934 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83328, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 372413, 375248, "- Я могу сделать тебе подарок?\r\n- Давай!", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83329, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83329, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83329, 2250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83329, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83329, 2792 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83329, 1427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 379879, 381838, "О, нет. Этого не может быть.", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83330, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83330, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83330, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83330, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83330, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83330, 816 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 382090, 385467, "Обожаю, когда притворяются,\r\nчто забыли подарок!", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83331, 3734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83331, 1126 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "притворяются" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83331, 34910 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83331, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83331, 10175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83331, 2792 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 386177, 389888, "А если не притворяются,\r\nа правда забывают и просят прощения?", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 34910 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 780 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "забывают" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 34911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 8360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83332, 2745 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 390473, 393491, "Ничего. Успокойся.\r\nПодаришь, когда вернемся.", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83333, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83333, 1933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83333, 27765 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83333, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83333, 7421 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 393542, 396119, "Ничего хуже в день годовщины\r\nслучиться не могло.", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83334, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83334, 2137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83334, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83334, 1032 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83334, 24733 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83334, 10028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83334, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83334, 7581 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 396228, 399047, "Хорошо. Ты все-таки\r\nрешилась рассказать ему о Ричарде?", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 1029 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "решилась" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 34912 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 2113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 825 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ричарде" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83335, 34913 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 402860, 404194, "О каком Ричарде?", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83336, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83336, 2003 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83336, 34913 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 404404, 406279, "О, нет.", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83337, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83337, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 408074, 409116, "О каком Ричарде?", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83338, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83338, 2003 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83338, 34913 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 409325, 411118, "Симмонсе. Скажи про Симмонса.", 109 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "симмонсе" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83339, 34914 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83339, 1164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83339, 946 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83339, 34884 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 418292, 420252, "Вчера я случайно\r\nвстретила Ричарда.", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83340, 1953 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83340, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83340, 9568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83340, 7608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83340, 17877 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 420461, 424172, "Он предложил мне пообедать.\r\nЯ согласилась.", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83341, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83341, 2260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83341, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83341, 9719 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83341, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83341, 4819 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 424382, 426967, "Я не стала тебе говорить,\r\nпотому что боялась, что ты разозлишься...", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 1129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 1202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 1954 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разозлишься" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83342, 34915 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 427176, 429678, "...а мне не хотелось\r\nпортить нам годовщину.", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83343, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83343, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83343, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83343, 5636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83343, 9366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83343, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83343, 24304 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 430263, 431763, "Я не злюсь.", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83344, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83344, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83344, 15989 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 433766, 434808, "Честно?", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83345, 1193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 436185, 439271, "Да. Ты столкнулись с Ричардом.\r\nВы перекусили. Подумаешь.", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83346, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83346, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83346, 19336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83346, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83346, 17907 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83346, 783 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "перекусили" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83346, 34916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83346, 11637 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 441858, 443442, "Здорово.", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83347, 1261 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 446612, 447946, "Отлично.", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83348, 1422 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 451576, 454077, "Так.\r\n1 : 1.", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83349, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83349, 1847 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83349, 1847 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 482940, 484566, "Какая неприятность.", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83350, 1353 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "неприятность" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83350, 34917 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 486944, 488820, "Вы только посмотрите на меня.", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83351, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83351, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83351, 2303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83351, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83351, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 489030, 491364, "Я у себя на кухне...", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83352, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83352, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83352, 947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83352, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83352, 1983 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 491616, 493116, "...обнаженная.", 123 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обнаженная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83353, 34918 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 493951, 496453, "Я беру апельсин.", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83354, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83354, 1230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83354, 27769 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 496954, 497996, "Я обнажена.", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83355, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обнажена" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83355, 34919 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 500791, 503335, "Зажигаю свечи обнаженной.", 126 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "зажигаю" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83356, 34920 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83356, 5381 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обнаженной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83356, 34921 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 503544, 506338, "Только осторожно.", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83357, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83357, 2239 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 517308, 520227, "Боже мой, это же Рэйчел обнаженная.", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83358, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83358, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83358, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83358, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83358, 824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83358, 34918 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 522396, 525023, "Я не могу на нее смотреть.", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83359, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83359, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83359, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83359, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83359, 1399 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83359, 5508 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 525233, 529194, "Я смотрю в альбом.\r\nЯркие цвета, выразительные мазки кисти.", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 1440 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 13837 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "яркие" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 34922 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 26791 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выразительные" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 34923 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мазки" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 34924 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кисти" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83360, 34925 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 530321, 534199, "Может, она сама хочет,\r\nчтобы я смотрел на нее.", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 791 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 1466 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 7467 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83361, 1399 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 534909, 536618, "Она же знает, что я дома.", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83362, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83362, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83362, 2192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83362, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83362, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83362, 1010 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 536869, 539746, "Она знает, что я ее вижу.", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83363, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83363, 2192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83363, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83363, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83363, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83363, 703 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 539997, 543583, "Что за игру она затеяла?", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83364, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83364, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83364, 3765 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83364, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83364, 6256 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 543834, 547796, "Возможно сегодня\r\nкому-то одиноко.", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83365, 1026 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83365, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83365, 2251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83365, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83365, 34063 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 548464, 550257, "Доктор Геллер.", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83366, 2190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83366, 2265 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 550508, 555178, "Перестаньте. Не говорите глупостей.", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83367, 14103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83367, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83367, 2678 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83367, 10667 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 555346, 557138, "А глупости ли это?", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83368, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83368, 9842 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83368, 1234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83368, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 578494, 579828, "Черт!", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83369, 776 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 589422, 590463, "Привет.", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83370, 729 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 595845, 597095, "Можно войти?", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83371, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83371, 7071 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 598139, 599806, "Да, если хочешь.", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83372, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83372, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83372, 1035 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 600808, 602726, "А ты этого хочешь?", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83373, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83373, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83373, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83373, 1035 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 604520, 605645, "Ну, да.", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83374, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83374, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 605855, 607689, "Я тоже.", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83375, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83375, 1071 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 612361, 615822, "Рэйч, прежде,\r\nчем это случится...", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83376, 3523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83376, 9659 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83376, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83376, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83376, 2765 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 616073, 618783, "...я хочу установить пару\r\nважных правил.", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83377, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83377, 815 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "установить" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83377, 34926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83377, 3680 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "важных" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83377, 34927 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83377, 7888 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 618993, 621661, "Речь идет только о сегодняшнем дне.", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83378, 3333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83378, 3737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83378, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83378, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83378, 21283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83378, 3770 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 621954, 626082, "Я даже начинать ничего не буду,\r\nесли ты подымешь вопрос \'о нас\'.", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 1401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подымешь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 34928 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 1112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83379, 1210 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 628210, 629628, "Идет? Пусть останется все как есть.", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83380, 3737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83380, 4948 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83380, 9793 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83380, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83380, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83380, 671 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 632757, 634758, "Ты согласна?", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83381, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83381, 19612 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 639013, 640138, "Ты...", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83382, 654 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 640389, 642182, "...о чем, Росс?", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83383, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83383, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83383, 798 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 642391, 645018, "О физическом акте любви.", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83384, 825 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "физическом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83384, 34929 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "акте" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83384, 34930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83384, 5049 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 646562, 647979, "Что? Ты с ума сошел?", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83385, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83385, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83385, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83385, 6035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83385, 20059 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 648189, 653109, "Ты что не пыталась меня\r\nсоблазнить своей наготой в окно?", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 19276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 29877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 2094 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "наготой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 34931 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83386, 3762 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 655946, 658406, "Боже! Ты меня видел?", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83387, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83387, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83387, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83387, 4265 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 662119, 666289, "Ты не пыталась\r\nсоблазнить меня своей наготой.", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83388, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83388, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83388, 19276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83388, 29877 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83388, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83388, 2094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83388, 34931 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 666499, 667540, "Нет. Нет!", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83389, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83389, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 667792, 671920, "Ты в самом деле решил,\r\nчто я хочу заняться с тобой сексом?", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 1979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 1980 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 1916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 4897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 735 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83390, 4898 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 675758, 677050, "Нет!", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83391, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 678886, 681805, "Нет.", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83392, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 684350, 686184, "Нет.Нет.Нет.", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83393, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83393, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83393, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 709083, 713210, "Ребята, подождите!\r\nЭтот город в сто раз лучше Лондона.", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 843 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 2270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 1409 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 3415 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83394, 29433 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 713463, 715690, "Девушка, одетая Клеопатрой, дала мне купон.", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83395, 1958 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "одетая" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83395, 34932 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "клеопатрой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83395, 34933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83395, 1988 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83395, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83395, 27882 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 715841, 719926, "Ужин за 99 центов.\r\nСтейк и лобстер.", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83396, 6308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83396, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83396, 27642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83396, 10673 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стейк" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83396, 34934 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83396, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83396, 17256 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 720094, 721469, "Фиби, ты не ешь животных.", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83397, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83397, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83397, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83397, 1448 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83397, 3451 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 721721, 723972, "За 99 центов\r\nя тебя готова сьесть.", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83398, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83398, 27642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83398, 10673 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83398, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83398, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83398, 1426 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сьесть" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83398, 34935 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 725433, 727000, "Я остаюсь.", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83399, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83399, 6593 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 727010, 730780, "Здесь есть все, что мне нужно.\r\nВ том числе Джо.", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 1823 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "числе" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 34936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83400, 1003 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 730807, 732232, "Смотрите!", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83401, 3417 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 733833, 734633, "Смотрите!", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83402, 3417 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 734692, 735775, "Боже мой.", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83403, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83403, 891 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 736193, 737819, "Джо!", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83404, 1003 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 740765, 742615, "- Привет!\r\n- Привет.", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83405, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83405, 729 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 742825, 744367, "Мне нравится твое белье.", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83406, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83406, 3639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83406, 4249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83406, 4247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 745786, 748163, "Что вы здесь делаете?\r\nЯ же просил не приезжать.", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 3452 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 10968 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83407, 19371 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 748372, 750373, "Почему ты одет гладиатором?", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83408, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83408, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83408, 20836 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гладиатором" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83408, 34937 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 750541, 754411, "Мы как раз снимаем сцену.\r\nЯ играю гладиатора.", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83409, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83409, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83409, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83409, 33184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83409, 17088 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83409, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83409, 5006 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гладиатора" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83409, 34938 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 754412, 755412, "Подождите минутку.", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83410, 2270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83410, 2206 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 756922, 758140, "Давайте прервемся.", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83411, 2808 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83411, 34035 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 758292, 762093, "Приехали мои друзья. Я хочу сделать перерыв.", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83412, 26700 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83412, 1097 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83412, 1383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83412, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83412, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83412, 2250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83412, 3475 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 762511, 764220, "С кем ты разговариваешь?", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83413, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83413, 2144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83413, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83413, 6353 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 765514, 766598, "С режиссером.", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83414, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83414, 25318 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 766766, 768475, "С ней. Вот.", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83415, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83415, 2016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83415, 694 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 771687, 774731, "Ладно. Это не фильм о гладиаторах.\r\nЯ здесь работаю.", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 6927 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 825 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гладиаторах" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 34939 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83416, 2049 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 774982, 776399, "Что случилось?", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83417, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83417, 868 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 776609, 779335, "Сьемки свернули, потому\r\nчто кончились деньги.", 188 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сьемки" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83418, 34940 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "свернули" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83418, 34941 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83418, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83418, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83418, 18044 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83418, 2077 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 779444, 783438, "Я устроился сюда, пока их\r\nне возобновят. Если это вообще случится.", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "устроился" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 34942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 1357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 1935 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "возобновят" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 34943 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83419, 2765 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 783548, 784157, "Сожалею.", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83420, 14879 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 784408, 786618, "Я должен был вам рассказать.\r\nИзвини, друг.", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83421, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83421, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83421, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83421, 952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83421, 2113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83421, 1447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83421, 2152 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 786827, 790396, "Все в порядке. У нас новая традиция,\r\nсогласно которой не обязательно всем делиться с друзьями.", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 766 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 9996 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 6461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 12729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 10589 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 2051 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 1444 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "делиться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 34944 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83422, 4211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 790406, 793708, "- Я так и знала, что ты злишься.\r\n- Так ты гладиатор?", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 792 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 7615 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гладиатор" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83423, 34945 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 794627, 796085, "Что все это значит?", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83424, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83424, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83424, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83424, 692 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 796295, 799172, "- Моника обедала с Ричардом.\r\n- Даусоном?", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83425, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83425, 13373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83425, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83425, 17907 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "даусоном" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83425, 34946 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 800549, 801591, "Нет.", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83426, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 801801, 804260, "Вот было бы здорово!", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83427, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83427, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83427, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83427, 1261 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 804720, 806995, "Нет. Со своим другом Ричардом.", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83428, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83428, 676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83428, 677 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83428, 9219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83428, 17907 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 807005, 811200, "Это ничего не значит,\r\nКак ты можешь мне не доверять после стольких месяцев?", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 26755 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 1824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 5018 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83429, 1185 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 811310, 814770, "Ты обедаешь с импозантным усатым\r\nмужчиной и ничего мне не говоришь!", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 34886 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "импозантным" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 34947 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "усатым" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 34948 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 4130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83430, 950 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 816440, 819275, "Ты прав. Извини.\r\nЯ должна была сказать.", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83431, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83431, 3769 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83431, 1447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83431, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83431, 1058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83431, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83431, 1212 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 819860, 820902, "Спасибо.", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83432, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 821821, 824614, "- Вот это другое дело.\r\n- Обожаю Вегас.", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83433, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83433, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83433, 5938 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83433, 1154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83433, 3734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83433, 18803 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 825699, 828785, "Обещаю, в следующий раз\r\nвсе тебе рассказывать.", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83434, 7650 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83434, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83434, 1839 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83434, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83434, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83434, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83434, 645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 832206, 833248, "В следующий раз?", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83435, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83435, 1839 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83435, 1038 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 833707, 835208, "Опять плохо.", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83436, 924 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83436, 2263 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 836126, 839654, "Следующего раза не будет.\r\nТы не будешь с ним больше встречаться.", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 33770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 1245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 747 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83437, 2592 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 839763, 843091, "Не буду встречаться?\r\nТы не можешь указывать мне что делать.", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 2592 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 1201 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "указывать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 34949 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83438, 2025 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 843300, 845301, "Забавно. Я тебе уже указал.", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83439, 3376 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83439, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83439, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83439, 990 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "указал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83439, 34950 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 846345, 849755, "Если ты собираешься весь вечер\r\nтак себя вести, я с тобой не разговариваю.", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 2060 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 2984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 1057 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 2768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 735 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83440, 8901 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 849865, 851632, "- Не возражаю.\r\n- Отлично, с годовщиной!", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83441, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83441, 3853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83441, 1422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83441, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83441, 24262 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 851741, 853560, "Ребята, перестаньте. Прошу вас.", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83442, 843 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83442, 14103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83442, 2744 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83442, 1044 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 853769, 855895, "Произошло чудовищное недоразумение.", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83443, 1181 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "чудовищное" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83443, 34951 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83443, 10987 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 856146, 858106, "- Ничего подобного.\r\n- Что ты хочешь сказать?", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83444, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83444, 2084 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83444, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83444, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83444, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83444, 1212 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 858315, 860108, "Не набрасывайтесь на меня.\r\nЯ просто здесь работаю.", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83445, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "набрасывайтесь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83445, 34952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83445, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83445, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83445, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83445, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83445, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83445, 2049 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 864321, 868992, "Росс, мне немного жарко,\r\nпоэтому я хочу снять кофту.", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 1119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 15283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 1373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 5072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83446, 13798 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 869201, 871786, "Только сразу предупреждаю...", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83447, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83447, 1124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83447, 28401 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 872037, 876124, "...что это не является приглашением\r\nк физическому акту любви.", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 14961 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "приглашением" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 34953 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 831 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "физическому" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 34954 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "акту" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 34955 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83448, 5049 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 877918, 880086, "Да, очень смешно.", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83449, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83449, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83449, 4466 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 880921, 882922, "Прости. Больше не буду.", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83450, 932 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83450, 888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83450, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83450, 2591 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 883132, 886259, "Ты вчера тоже смутилась,\r\nсогласись.", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83451, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83451, 1953 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83451, 1071 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "смутилась" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83451, 34956 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83451, 13611 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 886468, 889586, "Да нет. Меня не так-то легко смутить.", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83452, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83452, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83452, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83452, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83452, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83452, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83452, 7550 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83452, 27372 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 889696, 891389, "Не, не легко.", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83453, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83453, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83453, 7550 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 891599, 893725, "Что? Ты очень стеснительная.", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83454, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83454, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83454, 723 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стеснительная" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83454, 34957 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 893934, 898521, "Не правда! Росс, думаю,\r\nя менее ранимый человек, чем ты.", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 3743 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ранимый" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 34958 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 2823 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83455, 654 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 898731, 900398, "- Ты думаешь?\r\n- Да.", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83456, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83456, 1209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83456, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 902234, 903443, "Леди!", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83457, 16002 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 903652, 907529, "Меня не волнует насколько сильно вы этого хотите.", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83458, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83458, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83458, 14894 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83458, 923 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83458, 5445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83458, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83458, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83458, 2898 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 907580, 910140, "Я не буду заниматься\r\nс вами сексом в туалете!", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 9148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 6594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 4898 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83459, 8891 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 914538, 917832, "В Древнем Риме эта штука\r\nслужила не только шлемом.", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 25350 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 6864 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 2966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 8428 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 4841 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 757 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шлемом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83460, 34959 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 918626, 919667, "Неужели?", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83461, 1363 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 919877, 921169, "Да, конечно.", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83462, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83462, 775 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 921420, 924921, "Ею мыли полы, стряхивали грязь с обуви.", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83463, 3855 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мыли" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83463, 34960 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83463, 4927 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стряхивали" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83463, 34961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83463, 34714 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83463, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83463, 18460 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 925072, 927215, "А если их кони были перепачканны снизу.", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83464, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83464, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83464, 1935 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кони" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83464, 34962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83464, 876 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "перепачканны" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83464, 34963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83464, 18602 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 926425, 929260, "Так просовывали ее...", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83465, 679 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "просовывали" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83465, 34964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83465, 926 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 929470, 931262, "... вот так.", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83466, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83466, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 932139, 935183, "Джо, просто не верится,\r\nчто я так провожу свою годовщину.", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 1003 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 3022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 12513 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 3422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83467, 24304 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 935392, 938370, "Давай, я свожу тебе в какое-нибудь\r\nсимпатичное местечко.", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "свожу" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 34965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 2008 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 658 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "симпатичное" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 34966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83468, 9193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 938471, 940871, "Смотри, один парень\r\nдал мне на чай $100.", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83469, 1439 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83469, 1110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83469, 836 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83469, 4479 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83469, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83469, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83469, 2683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83469, 6247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 940230, 945383, "Да, да. Он около часа играл в Блэк джек\r\nи выиграл $5000.", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 10140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 2001 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 1291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "блэк" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 34967 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 3601 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 10494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83470, 24317 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 945434, 946001, "Ты представляешь? 5000.", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83471, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83471, 2938 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83471, 24317 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 946211, 949820, "Если бы я выиграл $5000, я бы пошел в тренажерный зал.", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 10494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 24317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 5497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 17284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83472, 17285 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 949930, 952189, "Накачал мускулатуру\r\nи ударил бы Ричарда палкой в спину.", 243 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "накачал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 34968 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мускулатуру" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 34969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 23602 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 17877 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "палкой" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 34970 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83473, 9678 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 953535, 956287, "Подожди. А почему бы мне\r\nне сделать то же самое?", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 972 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 2250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83474, 2942 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 956538, 958706, "Я поставлю $100,\r\nвыиграю $5000...", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83475, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83475, 17925 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83475, 6247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83475, 26146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83475, 24317 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 958958, 962210, "...а на эти деньги продолжу\r\nсьемки своего фильма!", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83476, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83476, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83476, 1094 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83476, 2077 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83476, 13608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83476, 34940 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83476, 2771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83476, 5139 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 963212, 964504, "Удачи.", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83477, 1325 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 964713, 968216, "- Чендлер, мне не нужна удача. Я все продумал.\r\n- Ясно.", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 955 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 18684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 4236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83478, 818 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 973347, 975098, "Спасибо.", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83479, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 975683, 979102, "Невероятно.\r\nКакой отвратительный вечер.", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83480, 5134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83480, 657 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83480, 8663 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83480, 1057 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 979311, 982730, "Моника, ты немного\r\nосложнила отношения с Чендлером.", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83481, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83481, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83481, 1119 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "осложнила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83481, 34971 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83481, 2598 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83481, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83481, 12186 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 982982, 984649, "Подумаешь.\r\nЭто всего лишь Чендлер.", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83482, 11637 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83482, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83482, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83482, 5526 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83482, 2148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 986527, 989195, "Я тебе очень сочувствую.", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83483, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83483, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83483, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83483, 16391 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 991073, 994200, "Это безумие. Такая глупая ссора.", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83484, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83484, 10050 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83484, 2616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83484, 9587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83484, 6955 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 994451, 996160, "Я даже не хочу больше\r\nс Ричардом встречаться.", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83485, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83485, 684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83485, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83485, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83485, 888 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83485, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83485, 17907 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83485, 2592 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 996370, 998329, "Помирись с ним.", 256 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "помирись" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83486, 34972 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83486, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83486, 649 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 998580, 1001249, "Найди Чендлера.\r\nНаверняка он в вашем номере.", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83487, 16784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83487, 5379 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83487, 656 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83487, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83487, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83487, 8416 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83487, 30258 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1001500, 1003376, "Извинись перед ним.\r\nСкажи, что любишь его.", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83488, 17060 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83488, 1962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83488, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83488, 1164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83488, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83488, 2243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83488, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1003919, 1005670, "А знаешь, ты права, Фиби.", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83489, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83489, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83489, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83489, 2254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83489, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1005879, 1007338, "- Ты права, спасибо.\r\n- Не за что.", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83490, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83490, 2254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83490, 756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83490, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83490, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83490, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1008590, 1011884, "Да! Лас-Вегас номер один!", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83491, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83491, 1471 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83491, 18803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83491, 2841 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83491, 1110 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1021645, 1024063, "Это не вы уронили?", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83492, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83492, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83492, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83492, 25371 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1024273, 1025523, "Вы играете? \r\nДелайте ставку.", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83493, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83493, 10425 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83493, 14396 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83493, 10678 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1026525, 1028067, "Бросаем кости.", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83494, 29015 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83494, 12754 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1030320, 1032405, "Ваша ставка выиграла. \r\nВыигрыш ваш!", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83495, 4315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83495, 10644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83495, 10636 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выигрыш" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83495, 34973 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83495, 2782 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1041165, 1044667, "Я как раз собирался \r\nзаглянуть в туалет.", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83496, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83496, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83496, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83496, 4807 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83496, 33283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83496, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83496, 5405 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1047629, 1050923, "Поздравляю.\r\nИдите дальше. Проходите.", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83497, 1252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83497, 19697 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83497, 3632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83497, 10110 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1060642, 1062477, "Росс, ты что... \r\nИзвините, сэр.", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83498, 798 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83498, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83498, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83498, 1042 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83498, 6267 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1062686, 1065688, "Я думаю, вы ему очень понравились.", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83499, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83499, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83499, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83499, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83499, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83499, 7978 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1071403, 1075073, "Вы читале об учительнице, \r\nкоторая родила от своего студента?", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 783 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "читале" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 34974 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 2963 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "учительнице" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 34975 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 1959 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 31596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 2771 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "студента" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83500, 34976 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1085374, 1086374, "Что ты делаешь?", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83501, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83501, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83501, 2721 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1086585, 1088086, "Здравствуйте. Прошу прощения.", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83502, 5545 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83502, 2744 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83502, 2745 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1088295, 1093132, "У вас не найдется запасной пары брюк? \r\nУ моего друга небольшая неприятность.", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 1044 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 3329 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 11819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 4301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 13179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 856 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 2773 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 27288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83503, 34917 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1098806, 1101474, "- Разменяете $100?\r\n- Меняю $100.", 274 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разменяете" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83504, 34977 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83504, 6247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83504, 7175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83504, 6247 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1102559, 1104811, "Удачи, сэр.", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83505, 1325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83505, 6267 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1106688, 1108022, "Поехали.", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83506, 1069 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1112986, 1115071, "- Тринадцать.\r\n- А у меня?", 277 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "тринадцать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83507, 34978 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83507, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83507, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83507, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1116532, 1118533, "О, нет!", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83508, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83508, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1121328, 1122495, "Подождите!", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83509, 2270 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1124706, 1126791, "Чендлер, ты не поверишь.", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83510, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83510, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83510, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83510, 23895 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1127000, 1129752, "Я нашел своего ручного близнеца!", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83511, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83511, 1011 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83511, 2771 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ручного" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83511, 34979 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "близнеца" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83511, 34980 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1133799, 1135049, "Что?", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83512, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1135926, 1137760, "Ручного близнеца!", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83513, 34979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83513, 34980 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1139638, 1141305, "А что такое ручной близнец?", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83514, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83514, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83514, 715 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ручной" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83514, 34981 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83514, 2139 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1141515, 1145184, "Неужели, не понятно.\r\nЧеловек, с точно такими же руками.", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 1363 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 4409 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 2823 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 2128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 2109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83515, 1844 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1145435, 1147854, "Это потрясающе, Чендлер.\r\nРуки крупье ничем не отличаются от моих!", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 1248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 879 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "крупье" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 34982 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 4085 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 34905 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83516, 1937 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1149439, 1152400, "А ты уверен, что они \r\nничем не отличаются?", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83517, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83517, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83517, 6588 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83517, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83517, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83517, 4085 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83517, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83517, 34905 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1153402, 1157280, "Ты что, не понимаешь что это значит?\r\nМожно забыть об этом дурацком фильме.", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 1213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 2187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 31419 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83518, 4990 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1157489, 1159073, "Я стану миллионером!", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83519, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83519, 7938 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83519, 24966 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1161326, 1162368, "Какм образом?", 290 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "какм" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83520, 34983 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83520, 14098 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1162578, 1164912, "Я не до конца еще все продумал.", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83521, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83521, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83521, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83521, 1319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83521, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83521, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83521, 4236 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1165164, 1168791, "Но денег будет море.\r\nСам посуди: одинаковые руки!", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83522, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83522, 971 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83522, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83522, 5883 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83522, 1039 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "посуди" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83522, 34984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83522, 24478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83522, 879 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1170419, 1172420, "И все-таки...", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83523, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83523, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83523, 1029 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1172671, 1174088, "...каким образом?", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83524, 4200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83524, 14098 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1175424, 1179177, "Это же Вегас.\r\nЛюди готовы готовы платить за чудеса!", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 18803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 1871 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 5067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 5067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 11621 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83525, 31824 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1181096, 1183681, "Сколько бы ты отдал, \r\nчтобы увидеть эту руку... ", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83526, 1132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83526, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83526, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83526, 3702 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83526, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83526, 2306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83526, 3012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83526, 2646 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1183932, 1186184, "...дважды?", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83527, 5796 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1189396, 1191981, "Я боюсь назвать цену, Джо.", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83528, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83528, 4281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83528, 14936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83528, 30401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83528, 1003 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1192816, 1194859, "Ты опять не хочешь меня поддержать?", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83529, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83529, 924 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83529, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83529, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83529, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83529, 25155 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1195360, 1196402, "Нет. Нет.", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83530, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83530, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1196653, 1200281, "Я на 100% тебя поддерживаю.\r\nКак раз собирался об этом тебе сказать.", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 6247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 9269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 4807 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83531, 1212 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1200532, 1201949, "Ручные близнецы.", 302 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ручные" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83532, 34985 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83532, 2138 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1202201, 1204035, "Идея на миллион долларов.", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83533, 4515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83533, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83533, 822 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83533, 823 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1204286, 1205703, "Это здорово!", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83534, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83534, 1261 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1211418, 1212501, "- Привет!\r\n- Фиби!", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83535, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83535, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1212711, 1214587, "Я нашел своего ручного близнеца!", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83536, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83536, 1011 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83536, 2771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83536, 34979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83536, 34980 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1214796, 1217006, "Как тебе повезло!", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83537, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83537, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83537, 4380 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1221637, 1224472, "А где Моника?\r\nВы с ней помирились?", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83538, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83538, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83538, 826 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83538, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83538, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83538, 2016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83538, 29463 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1224681, 1227600, "- Нет.\r\n- Она же поднялась сюда.", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83539, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83539, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83539, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83539, 19310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83539, 1357 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1227935, 1229518, "Это был Джо.", 310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83540, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83540, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83540, 1003 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 310   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1230145, 1232271, "Интересно, где она?.", 311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83541, 2117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83541, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83541, 771 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 311   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1232481, 1234357, "Наверно болтается с Ричардом.", 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83542, 1452 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83542, 23899 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83542, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83542, 17907 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 312   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1234608, 1236234, "Хватит тебе.", 313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83543, 951 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83543, 753 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 313   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1236902, 1240154, "Знаешь, что она сказала, \r\nкогда вернулась после обеда с Ричардом?", 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 973 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 1328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 1824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 5087 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83544, 17907 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 314   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1240405, 1243241, "Что она ничего к нему не почувствовала.\r\nОна любит тебя.", 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 6966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 6552 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 1236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83545, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 315   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1244618, 1246285, "- Правда?\r\n- Да.", 316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83546, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83546, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 316   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1246495, 1250873, "Она ужасно страдает и хочет помириться.\r\nТы должен ее найти.", 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 7666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 16265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 1466 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 24481 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83547, 5532 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 317   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1252793, 1253834, "Хорошо.", 318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83548, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 318   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1254378, 1255586, "Молодец.", 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83549, 1225 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 319   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1262427, 1264971, "Мне надо носить шляпы.", 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83550, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83550, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83550, 4298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83550, 5426 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 320   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1310434, 1312476, "Добро пожаловать в Вегас.", 321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83551, 1429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83551, 1430 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83551, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83551, 18803 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 321   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1313603, 1315771, "Спасибо. Полет был приятным?", 322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83552, 756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83552, 30104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83552, 834 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83552, 25146 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 322   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1315981, 1318524, "Да, очень. Большое спасибо.", 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83553, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83553, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83553, 967 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83553, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 323   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 240, 1319735, 1323738, "- Надеюсь, вы хорошо долетели?\r\n- Лучше всех.", 324 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83554, 855 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83554, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83554, 833 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "долетели" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83554, 34986 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83554, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 83554, 2833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 120 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 05/s05e23 - The One in Vegas, Part One/Russian.srt" ,  `subtitle`.`phrase_number` = 324   WHERE  `subtitle`.`id` = 240 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 120, 240 );