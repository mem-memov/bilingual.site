INSERT INTO `source` ( `source`.`file` ) VALUES ( "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel.mkv" );
# a query
INSERT INTO `video` ( `video`.`source_id`, `video`.`language_id`, `video`.`file`, `video`.`width`, `video`.`height`, `video`.`duration` ) VALUES ( 182, 1, "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/movie.mp4", 768, 432, 1319361 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 182, 2, "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.mp3", 1319370 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 182, 363 );
# a query
INSERT INTO `audio` ( `audio`.`source_id`, `audio`.`language_id`, `audio`.`file`, `audio`.`duration` ) VALUES ( 182, 1, "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.mp3", 1319370 );
# a query
INSERT INTO `video_audio` ( `video_audio`.`video_id`, `video_audio`.`audio_id` ) VALUES ( 182, 364 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 182, 1, "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 3587, 6173, "Hey, I got you a present!", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123474, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123474, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123474, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123474, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123474, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123474, 5186 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 6340, 8800, "Oh, my goodness, where did you hide it?", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123475, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123475, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123475, 19515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123475, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123475, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123475, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123475, 13537 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123475, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 8967, 12679, "I got it for your wedding, I ordered it\r\nweeks ago and it finally got here!", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 2520 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 3180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 1756 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123476, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 12846, 15474, "Pheebs, you didn\'t have to get us\r\nanything for our-", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123477, 1662 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 15641, 18602, "I love it! It\'s huge! Let\'s open it! Open it!", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 3932 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 2389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 2389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123478, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 21188, 25275, "-It\'s a Ms. Pac-Man machine!\r\n-Oh, my God!", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 2438 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "pac" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 44285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 1645 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123479, 181 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 25442, 27527, "I didn\'t know where to put it so I left.", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123480, 144 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 27694, 30489, "-We can put it in the guest bedroom.\r\n-Yeah, okay.", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 23181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 6808 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123481, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 35577, 38497, "-I kind of like it here.\r\n-Do you really like it?", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123482, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 38664, 42292, "Are you kidding? I practically spent my\r\nentire childhood at the arcade.", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 12651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 1640 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 1588 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 13916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123483, 35413 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 42459, 46171, "-This was, like, my second favorite game.\r\n-What was your first?", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 1728 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 396 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 2469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123484, 596 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 46338, 50384, "-I don\'t really remember the name of it.\r\n-Well, what did it do?", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123485, 328 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 50550, 55305, "Well, you\'d put a quarter in,\r\npull some handles and win a candy bar.", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 23742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 4652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 22874 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 8552 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 3207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123486, 3208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 55472, 57474, "A vending machine?", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123487, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "vending" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123487, 44286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123487, 427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 57641, 60602, "Hey, don\'t feel bad for me.\r\nI won every time!", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 1674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 1570 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123488, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 100600, 102811, "Hey! I\'m so glad you guys are here.", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 478 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123489, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 102978, 106565, "I\'ve been dying to tell someone what\r\nhappened in the paleontology department.", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 10747 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 452 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 6209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123490, 6090 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 106732, 109776, "Do you think he saw us,\r\nor can we still slip out?", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 25304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123491, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 110736, 114865, "Professor Newman, the head\r\nof the department, is retiring, so....", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 23965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 30010 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 6090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 33514 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123492, 25 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 115115, 120203, "-They made you head of the department!\r\n-No, I get to teach his advanced class!", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 6090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 19872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 549 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "advanced" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 44287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123493, 6115 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 120996, 124374, "Why didn\'t I get head\r\nof the department?", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123494, 6090 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 125375, 127336, "-Oh, hey, Rach, listen, um....\r\n-Yeah.", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123495, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123495, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123495, 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123495, 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123495, 590 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123495, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 127502, 130005, "I got a big date.\r\nDo you know a good restaurant?", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123496, 1630 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 130589, 135260, "Uh, Paul\'s Cafe. It\'s got great food\r\nand it\'s really romantic.", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 19191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 1629 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123497, 3293 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 135427, 136845, "Ooh. Great, thanks.", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123498, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123498, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123498, 130 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 137012, 140557, "And then after, you could take her\r\nto the Four Seasons for drinks.", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 29819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123499, 9547 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 141350, 144644, "Or go downtown\r\nand listen to some jazz.", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123500, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123500, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123500, 43440 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123500, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123500, 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123500, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123500, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123500, 23466 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 144811, 147272, "Or dancing. Oh, take her dancing!", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123501, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123501, 6227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123501, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123501, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123501, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123501, 6227 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 147439, 151068, "You sure are naming a lot of ways\r\nto postpone sex, I\'ll tell you.", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 21256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 1775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 30855 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123502, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 151568, 156281, "Agh, I miss dating. Getting dressed up,\r\ngoing to a fancy restaurant.", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 18258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 1614 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 2333 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 3145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 17717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123503, 1630 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 157157, 161411, "I\'m not gonna be able to do that\r\nfor so long. And it\'s so much fun.", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 472 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123504, 2418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 162120, 166249, "Not that sitting at home worrying about\r\ngiving birth to a 16-pound baby is not fun.", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 1513 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 620 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 8946 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 1760 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 7289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 12951 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 14836 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123505, 2418 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 167959, 171505, "Hey, you know what?\r\nWhy don\'t I take you out?", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123506, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 172047, 175008, "What? You don\'t want to go on\r\na date with a pregnant lady.", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 1543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123507, 3893 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 175175, 178053, "Yes, I do. We\'re gonna go out,\r\nwe\'re gonna have a good time.", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123508, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 178220, 182432, "I\'ll take your mind off of childbirth,\r\nand C-sections...", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 3105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 69 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "childbirth" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 44288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 9443 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "sections" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123509, 44289 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 182599, 185102, "...and giant baby heads stretching out-", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123510, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123510, 2407 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123510, 1636 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123510, 4043 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123510, 30166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123510, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 185268, 186770, "Okay, I\'ll go with you. I\'ll go.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123511, 40 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 186937, 189439, "-It will be fun.\r\n-All right. Okay.", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123512, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123512, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123512, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123512, 2418 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123512, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123512, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123512, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 189606, 191566, "No, no! Yes!", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123513, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123513, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123513, 339 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 193068, 194903, "Aw, would you look at that, Monica?", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123514, 5684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123514, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123514, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123514, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123514, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123514, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123514, 85 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 195070, 197114, "I knocked off all your top scores.\r\nHow sad.", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 28938 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 3976 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "scores" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 44290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123515, 5311 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 198448, 199908, "Okay, I\'m next.", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123516, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123516, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123516, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123516, 4022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 200075, 203412, "No, don\'t start another game.\r\nI\'m next! Phoebe?", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 2453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 2469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 4022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123517, 88 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 203578, 206832, "I\'m sorry, I couldn\'t hear you\r\nover all the winning.", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 3088 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 564 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123518, 8536 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 207624, 211211, "-Chandler! Phoebe\'s hogging the game.\r\n-Who cares? It\'s a stupid game.", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 11237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 2469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 7771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 2548 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123519, 2469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 211378, 214339, "Oh, you only think it\'s stupid\r\nbecause you suck at it.", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 2548 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 7773 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123520, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 215215, 217801, "I don\'t suck. It sucks.", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123521, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123521, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123521, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123521, 7773 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123521, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123521, 617 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 217968, 219761, "You suck.", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123522, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123522, 7773 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 222264, 225142, "If this game is gonna\r\ncause problems between you...", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 2469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 5256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 563 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 1670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123523, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 225308, 229146, "-...maybe I should keep it.\r\n-No, I love it. It is a great present.", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123524, 5186 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 229312, 233650, "In fact, why don\'t you go home\r\nand wait for the \"Thank You\" card?", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 3140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 620 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123525, 588 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 233817, 237154, "-Why do you want to play so badly?\r\n-Yeah, it\'s not like it spits out...", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 3127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 9520 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 42644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123526, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 237320, 239156, "...a Clark Bar after every game.", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123527, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123527, 6676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123527, 3208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123527, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123527, 1570 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123527, 2469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 239489, 243910, "Okay. Phoebe, that\'s it.\r\nOut of the chair. Come on. Out of the chair.", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 11192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123528, 11192 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 244911, 246830, "Phoebe!", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123529, 88 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 250459, 253003, "Joey, could you get that?", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123530, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123530, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123530, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123530, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123530, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 256089, 257632, "Ugh, Joey....", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123531, 3985 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123531, 126 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 266183, 268018, "What? I thought you were in your room.", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123532, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123532, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123532, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123532, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123532, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123532, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123532, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123532, 213 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 268185, 271688, "No, I\'m picking you up for our date.\r\nThese are for you.", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 2425 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123533, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 272189, 276902, "Oh, lilies. They\'re my favorite.\r\nThank you.", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123534, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123534, 38972 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123534, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123534, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123534, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123534, 396 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123534, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123534, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 277068, 278862, "And a brownie.", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123535, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123535, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "brownie" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123535, 44291 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 281198, 283617, "Well, half a brownie.", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123536, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123536, 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123536, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123536, 44291 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 283784, 285911, "Actually, it\'s just a bag.", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123537, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123537, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123537, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123537, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123537, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123537, 6137 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 286077, 289706, "It\'s a long walk from the flower shop\r\nand I was starting to feel faint, so....", 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 6775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 9484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 8182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 8166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 13468 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123538, 25 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 290290, 293668, "Oh, man, this is so great!", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123539, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123539, 1645 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123539, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123539, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123539, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123539, 505 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 293835, 296296, "I actually feel like\r\nI\'m going on a real date.", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123540, 53 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 296463, 301760, "With a hint of morning sickness and I\'m\r\nwearing underwear that goes up to about...", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 20191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 504 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 22000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 2556 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 3229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123541, 208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 301927, 303136, "...there.", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123542, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 303303, 306515, "Hey, come on. This is a real date. Uh....", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123543, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 306681, 308808, "So, nice place you got here.", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123544, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123544, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123544, 3063 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123544, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123544, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123544, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 310018, 313480, "Foosball, huh? Pizza box.", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123545, 17763 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123545, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123545, 3218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123545, 8092 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 313647, 317400, "Oh, a subscription to <i>Playboy.\r\n</i>My kind of woman.", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "subscription" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 44292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 29179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123546, 431 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 318068, 319569, "Actually, that\'s my roommate\'s.", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123547, 370 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123547, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123547, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123547, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123547, 5209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123547, 2 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 319736, 323532, "I\'d like to meet him.\r\nHe sounds like a standup guy.", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 1707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 8169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123548, 9 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 323698, 325825, "But he\'s very protective of me...", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123549, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123549, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123549, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123549, 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123549, 29973 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123549, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123549, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 325992, 327911, "-...so you\'d better watch yourself.\r\n-Ah.", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123550, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123550, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123550, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123550, 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123550, 421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123550, 1512 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123550, 3190 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 328078, 330956, "Hey, so, this roommate of yours,\r\nis he good-looking?", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 5209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 1678 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123551, 215 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 331122, 333250, "-Mm-hm.\r\n-Oh, yeah.", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123552, 423 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123552, 634 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123552, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123552, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 333416, 336253, "-Must be tough to keep your hands off him.\r\n-Ha, ha.", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 1679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 3983 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 6751 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123553, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 336753, 338630, "Yeah, but I\'m pretty sure he\'s gay.", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 1485 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123554, 5686 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 339548, 342384, "No! No, he\'s not! Why are you trying\r\nto ruin the game?", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 1506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 19538 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123555, 2469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 345554, 348932, "Which brings us back, of course,\r\nto Greely\'s Theory of Dominance.", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 13503 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 551 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 4 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "greely" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 44293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 4600 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 69 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "dominance" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123556, 44294 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 350475, 351518, "That\'s it for today.", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123557, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123557, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123557, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123557, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123557, 125 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 351685, 355188, "Oh, uh, does anyone know\r\nwhere the Freeman Building is?", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 1537 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "freeman" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 44295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123558, 50 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 355355, 357691, "Yeah, it\'s the new building on Avenue A.", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 585 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 10313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123559, 19 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 357857, 359317, "What? That\'s across town.", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123560, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123560, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123560, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123560, 321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123560, 9958 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 359484, 361945, "I\'m supposed to teach a seminar there\r\nin 10 minutes.", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 19872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 19 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "seminar" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 44296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 4545 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123561, 2355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 362112, 364322, "Ooh, dude, that\'s not gonna happen.", 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123562, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123562, 16820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123562, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123562, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123562, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123562, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123562, 4006 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 367492, 369536, "Move it. Move it. Move it!", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123563, 3890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123563, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123563, 3890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123563, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123563, 3890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123563, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 369703, 371955, "Hey! I\'m the teacher!", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123564, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123564, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123564, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123564, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123564, 9471 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 385677, 387220, "Hello.", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123565, 105 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 391391, 393059, "Sorry I\'m a little late. Uh-", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123566, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123566, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123566, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123566, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123566, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123566, 1710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123566, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 393226, 394644, "Whoa, a lot late.", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123567, 2366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123567, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123567, 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123567, 1710 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 395645, 399858, "Uh, well, let me start by,\r\nuh, introducing myself.", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 2453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 26204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123568, 108 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 400025, 401651, "I\'m, uh, Professor Geller.", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123569, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123569, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123569, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123569, 23965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123569, 1762 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 403445, 405989, "So to sum up...", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123570, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123570, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123570, 43688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123570, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 406156, 408992, "...I\'m Professor Geller. Good job today.", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123571, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123571, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123571, 23965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123571, 1762 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123571, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123571, 543 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123571, 125 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 412621, 414623, "Now, the filet mignon,\r\nwhat comes with that?", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123572, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123572, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123572, 30162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123572, 30163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123572, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123572, 1492 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123572, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123572, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 414789, 416416, "A side of steamed vegetables.", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123573, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123573, 382 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123573, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123573, 3118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123573, 3961 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 416583, 418835, "Mm.", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123574, 423 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 419544, 421630, "Instead of the vegetables...", 102 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123575, 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123575, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123575, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123575, 3961 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 421796, 426343, "...is there any way that I could\r\nsubstitute the, um...", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 15030 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123576, 590 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 426509, 428428, "...three-pound lobster?", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123577, 1546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123577, 14836 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123577, 14469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 430347, 432349, "You know what? Bring her both.", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123578, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123578, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123578, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123578, 1509 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123578, 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123578, 3067 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 432515, 434184, "And I\'ll have the same.", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123579, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123579, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123579, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123579, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123579, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123579, 2372 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 435101, 438855, "Wow, this is shaping up\r\nto be a pretty good date.", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 50 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "shaping" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 44297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 1485 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123580, 53 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 439272, 441399, "Oh, I almost forgot.\r\nI didn\'t pay you the rent.", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 5671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 7291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123581, 3952 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 441566, 444778, "Whoa, whoa, whoa,\r\nno roommate stuff. We\'re on a date.", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 2366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 2366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 2366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 5209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123582, 53 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 444944, 449866, "Okay. Wow, so I get to see what\r\nJoey Tribbiani is like on a date.", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 4721 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123583, 53 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 450033, 451117, "Do you have any moves?", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123584, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123584, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123584, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123584, 632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123584, 18022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 451284, 455538, "No, I\'m just myself,\r\nand if they don\'t like me for-", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123585, 202 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 458124, 460126, "I\'m sorry, I couldn\'t even\r\nget through that.", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 3088 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123586, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 462504, 464881, "I knew it. Come on, tell me your moves.", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123587, 18022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 465048, 466549, "Uh, all right. Urn....", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123588, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123588, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123588, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123588, 470 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 466716, 468343, "Well....", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123589, 206 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 468510, 473014, "I start by having a bottle of wine\r\nsent to my table from a fan.", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 2453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 8183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 2357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 511 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123590, 4618 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 473181, 476726, "Oh, my God. And that works?", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123591, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123591, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123591, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123591, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123591, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123591, 1792 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 476893, 479854, "It does when you combine it with,\r\n\"This is embarrassing.", 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 30006 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123592, 7222 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 480021, 482232, "I just want to have a normal life.\"", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123593, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123593, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123593, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123593, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123593, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123593, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123593, 9920 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123593, 277 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 482399, 484943, "-Aw, you poor little famous man.\r\n-Mm-hm.", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123594, 5684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123594, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123594, 5271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123594, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123594, 14176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123594, 1645 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123594, 423 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123594, 634 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 485110, 487946, "Oh, okay, how about this one?", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123595, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123595, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123595, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123595, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123595, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123595, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 488989, 492033, "I was gonna wait until the end\r\nof the night to kiss you...", 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 4630 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 1793 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 1483 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123596, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 492200, 495495, "...but you\'re just so beautiful,\r\nI don\'t think I can.", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 1580 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123597, 168 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 497580, 499666, "Oh, my God! Ha.", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123598, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123598, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123598, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123598, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 500875, 505130, "Wow, that was fantastic!\r\nI almost leaned in. I really almost did.", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 2470 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 5671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 13458 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 5671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123599, 158 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 505296, 507507, "All right, so tell me one of your moves.", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123600, 18022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 507716, 509718, "All right. Ahem.", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123601, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123601, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123601, 1669 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 512846, 514889, "So where\'d you grow up?", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123602, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123602, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123602, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123602, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123602, 4001 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123602, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 516015, 517517, "That\'s your move?", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123603, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123603, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123603, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123603, 3890 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 518226, 520603, "Boy, Rach, you\'re lucky you\'re hot.", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123604, 528 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123604, 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123604, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123604, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123604, 4009 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123604, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123604, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123604, 4637 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 520937, 524774, "-Come on, just answer the question.\r\n-Ugh. Queens.", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123605, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123605, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123605, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123605, 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123605, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123605, 401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123605, 3985 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123605, 5190 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 525567, 527360, "And so were you close to your parents?", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123606, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123606, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123606, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123606, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123606, 1531 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123606, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123606, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123606, 586 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 527527, 531072, "-With my mom. Not so much my dad.\r\n-Why not?", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 599 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 3129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123607, 51 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 531239, 534075, "I don\'t know. I guess there\'s just\r\nalways been this distance.", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123608, 2373 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 534242, 537871, "I mean, we both try to pretend\r\nit\'s not there, but it is.", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 3067 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 13247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123609, 50 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 538246, 539664, "Mm.", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123610, 423 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 539831, 541332, "It\'s gotta be rough.", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123611, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123611, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123611, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123611, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123611, 2575 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 541499, 545336, "It is. Yeah, it\'s really tough.\r\nYou know, sometimes I think....", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 3983 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123612, 298 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 545503, 547547, "Wow!", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123613, 477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 549674, 552427, "-Nice move!\r\n-Uh-huh.", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123614, 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123614, 3890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123614, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123614, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 552594, 556347, "-\"Where\'d you grow up?\" So simple.\r\n-Thank you.", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 4001 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 13254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123615, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 556514, 559267, "Ha, ha. Now, excuse me.\r\nI\'m gonna go to the restroom.", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 3060 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123616, 34995 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 562812, 567066, "-And now you\'re watching me walk away.\r\n-Yes, I am! Again, so simple.", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 5725 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 6775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123617, 13254 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 570195, 571237, "Hey.", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123618, 197 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 571404, 575950, "-You\'re not gonna believe what I did today.\r\n-Clearly wasn\'t showering or shaving.", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 16135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 4687 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123619, 25668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 576117, 579871, "I got good. I played this game all day,\r\nand now I rule at it.", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 3314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 2469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123620, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 580038, 582832, "They should change the name\r\nof it to \"Ms. Chandler.\"", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 1677 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 2438 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123621, 193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 585210, 586503, "Although, I hope they don\'t.", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123622, 359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123622, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123622, 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123622, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123622, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123622, 37 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 587462, 589839, "You stayed home all day\r\nand played Ms. Pac-Man...", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 13863 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 620 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 3314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 2438 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 44285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123623, 1645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 590006, 592008, "...while I went off to work\r\nlike some chump?", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 8 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "chump" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123624, 44298 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 592717, 597889, "Uh-huh. I got the top ten scores.\r\nI erased Phoebe off the board. High-five!", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 1608 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 3976 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 5316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 44290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 20752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 3154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 65 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123625, 409 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 599516, 601559, "What is the matter with your hand?", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123626, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123626, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123626, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123626, 1499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123626, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123626, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123626, 2364 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 605355, 607190, "I been playing for eight hours.", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123627, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123627, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123627, 1639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123627, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123627, 3110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123627, 1605 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 607357, 610276, "It\'ll loosen up. Come on,\r\ncheck out the scores.", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 17530 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 3080 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123628, 44290 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 610443, 613905, "And also, look at the initials.\r\nThey\'re dirty words.", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 2494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 19173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 3939 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123629, 491 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 614072, 618368, "-Chandler, why would you do that?\r\n-Because it\'s awesome.", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123630, 14213 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 618535, 620120, "You think this is clever?", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123631, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123631, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123631, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123631, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123631, 12315 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 620286, 624541, "They only give you three letters,\r\nso after A-S-S, it is a challenge.", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 1546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 8599 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123632, 1767 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 626209, 629879, "-This one\'s not dirty.\r\n-It is when you put it together with that one.", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 3939 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 494 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123633, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 630046, 631714, "Oh. Heh.", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123634, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123634, 360 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 631881, 635301, "If you don\'t clear this off,\r\nyou won\'t get one of those from me.", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 1699 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123635, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 637053, 639848, "Ben\'s coming over here tomorrow.\r\nThis can\'t be there.", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 2565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 1583 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123636, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 640014, 643935, "-He won\'t know what they mean.\r\n-He\'s 7, not stupid.", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 1594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 3917 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123637, 2548 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 644102, 648690, "-Have you talked to him lately?\r\n-Look, I\'m gonna unplug it.", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 1764 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 11208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 15000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123638, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 648857, 650942, "No, no! I\'ll have nothing\r\nto show for my day.", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 1500 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123639, 325 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 651109, 652402, "It\'ll be like I was at work.", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123640, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123640, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123640, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123640, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123640, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123640, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123640, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123640, 11 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 653403, 654779, "No!", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123641, 89 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 656322, 657949, "Hey, look! It\'s still there!", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123642, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123642, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123642, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123642, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123642, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123642, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 658116, 661494, "This thing must have some kind\r\nof primitive ROM chip in it or something!", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 1679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 69 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "primitive" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 44299 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "rom" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 44300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 15062 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123643, 22 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 661661, 664747, "-You gotta beat your scores.\r\n-With the claw?", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123644, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123644, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123644, 10369 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123644, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123644, 44290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123644, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123644, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123644, 32923 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 664914, 668626, "Fine, I\'ll do it.\r\nWe gotta get this off the screen.", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 429 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123645, 8575 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 668793, 671838, "Carol\'s still upset that\r\nyou taught him \"Pull my finger.\"", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 4026 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 6755 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 4652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123646, 1752 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 672839, 676509, "Pull my finger. My hand is messed up!", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123647, 4652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123647, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123647, 1752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123647, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123647, 2364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123647, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123647, 22247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123647, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 679929, 684767, "-I am not gonna answer that.\r\n-Oh, come on. Just pick one.", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 2492 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123648, 86 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 684934, 687437, "Between Monica, Phoebe,\r\nChandler and Ross...", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123649, 1670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123649, 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123649, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123649, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123649, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123649, 161 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 687604, 691149, "...if you had to- If you had to,\r\nwho would you punch?", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123650, 3164 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 691941, 694986, "No one. They\'re my friends.\r\nI wouldn\'t punch any of them.", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 1651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 3164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123651, 447 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 695737, 697280, "Chandler?", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123652, 193 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 697739, 699157, "Yeah, but I don\'t know why.", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123653, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123653, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123653, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123653, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123653, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123653, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123653, 148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 701618, 704329, "-I\'m having a wonderful time!\r\n-Me too.", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123654, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123654, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123654, 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123654, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123654, 3882 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123654, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123654, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123654, 576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 704495, 705788, "Hey, Rach, can I just say...", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123655, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123655, 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123655, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123655, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123655, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123655, 159 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 705955, 708541, "-...I think this is the best date I ever had.\r\n-I know!", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123656, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 708708, 712420, "I\'ve never laughed so hard. Did you\r\nsee the wine come out my nose?", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 570 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123657, 1795 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 712587, 716049, "Yes. Joey, I think everybody saw\r\nthe wine come out of your nose.", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123658, 1795 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 716215, 721054, "I gotta say, I never knew I could enjoy\r\nthe non-sex part of a date so much.", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 2406 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 24577 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 1480 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123659, 234 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 721221, 725642, "That\'s because you have never\r\nbeen on a date with me before.", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123660, 211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 726267, 727310, "Huh.", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123661, 1608 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 727477, 729812, "All right. Now don\'t judge me.", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123662, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123662, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123662, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123662, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123662, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123662, 3295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123662, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 729979, 733066, "I normally wait till my date leaves,\r\nbut you live here.", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 6081 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 4578 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 10332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123663, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 733233, 734442, "I\'m ripping into the swan.", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123664, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123664, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123664, 25269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123664, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123664, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "swan" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123664, 44301 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 734609, 737362, "Okay, well then, you don\'t judge me.", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123665, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123665, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123665, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123665, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123665, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123665, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123665, 3295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123665, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 737528, 741199, "I\'m gonna suck on the cellophane\r\nfrom the brownie I had before.", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 7773 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "cellophane" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 44302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 44291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123666, 211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 746496, 749249, "Mm. Mm.", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123667, 423 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123667, 423 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 749999, 754712, "So tell me, what are Joey Tribbiani\'s\r\nend-of-the-night moves?", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 4721 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 1793 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123668, 18022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 756130, 760969, "Uh, well, if I want the girl to kiss me...", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 1584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 1483 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123669, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 761135, 764430, "...first thing I do is make my lips\r\nlook irresistible.", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 596 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 381 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 4710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123670, 33679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 764597, 766349, "How do you do it?", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123671, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123671, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123671, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123671, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123671, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 766516, 770019, "Now you can\'t tell anyone, but, uh...", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123672, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123672, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123672, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123672, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123672, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123672, 1537 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123672, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123672, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 770311, 772480, "...I put on shiny lip balm.", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123673, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123673, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123673, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123673, 11975 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123673, 5303 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "balm" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123673, 44303 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 774524, 778820, "-Oh, my God.\r\n-Like a moth to a flame, I\'m telling you.", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 36113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 11583 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 1660 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123674, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 779988, 783449, "-Okay, all right, so now you go.\r\n-No, I don\'t want to tell you.", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123675, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 783992, 785451, "-Why not?\r\n-It\'s embarrassing.", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123676, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123676, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123676, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123676, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123676, 7222 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 786035, 788287, "More embarrassing than\r\nshiny raspberry lip balm?", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123677, 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123677, 7222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123677, 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123677, 11975 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "raspberry" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123677, 44304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123677, 5303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123677, 44303 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 790623, 793042, "I didn\'t say raspberry before, did I?", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123678, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123678, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123678, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123678, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123678, 44304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123678, 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123678, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123678, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 793710, 797213, "-Just tell me, Rach. Just tell me.\r\n-Ha, ha.", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 344 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123679, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 797380, 799257, "Okay. All right, stand up.", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123680, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123680, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123680, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123680, 1488 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123680, 352 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 803177, 805179, "Well, when we\'re at the door...", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123681, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123681, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123681, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123681, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123681, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123681, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123681, 4673 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 805555, 810435, "...I lightly press my lips against his.", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123682, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123682, 23179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123682, 10302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123682, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123682, 4710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123682, 2461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123682, 549 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 810893, 814814, "Then move into his body,\r\njust for a second.", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 3890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 555 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 2527 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123683, 1728 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 815315, 817567, "And then I make this sound:", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123684, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123684, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123684, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123684, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123684, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123684, 330 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 818151, 820528, "Mmm.", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123685, 13508 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 823531, 826367, "I know it doesn\'t sound like anything,\r\nbut it works.", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 515 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123686, 1792 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 826534, 829579, "Oh, yeah. That would work, yeah.", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123687, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123687, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123687, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123687, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123687, 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123687, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 830747, 834208, "All right, I gotta go to bed.\r\nI had such a wonderful time.", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 556 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 3882 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123688, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 834375, 836085, "-Mwah.\r\n-Oh, yeah.", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123689, 12932 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123689, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123689, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 841716, 843217, "Me too.", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123690, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123690, 576 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 854020, 857106, "You were 50 minutes late to the class?\r\nDid you crawl there?", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 582 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 2355 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 1710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 6115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 40469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123691, 1 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 857273, 860401, "No, I ran, okay? It\'s really far.", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123692, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123692, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123692, 1638 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123692, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123692, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123692, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123692, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123692, 1571 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 860568, 864614, "When did people stop understanding\r\n\"Get the hell out of my way\"?", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 21581 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123693, 420 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 865531, 867200, "Why didn\'t you take a cab?", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123694, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123694, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123694, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123694, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123694, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123694, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123694, 6781 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 867367, 872497, "Ugh. Between the traffic and one-way\r\nstreets, it\'d take me twice as long.", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 3985 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 1670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 1504 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 420 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 16527 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 5674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123695, 374 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 872663, 877168, "Besides, I teach the class three times\r\na week. Who am I, Rockefeller?", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 8642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 19872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 6115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 1546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 410 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 411 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123696, 16182 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 878294, 880546, "You\'re not gonna be able\r\nto keep doing this.", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 472 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123697, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 880713, 884717, "I have to. If I don\'t,\r\nthey\'ll take the class away from me.", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 6115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123698, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 884884, 887678, "And I already put it\r\nin my family newsletter.", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 1549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 1540 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123699, 10716 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 888930, 892266, "-Your what?\r\n-You\'ve seen it, <i>The Geller Yeller.</i>", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 519 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 1762 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 18827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123700, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 893935, 896771, "Right. Wow.", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123701, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123701, 477 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 897230, 902360, "Besides, I figured out a faster route.\r\nI\'m sure I can make it this time.", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 8642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 6206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 12661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123702, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 902527, 906489, "I just can\'t be afraid\r\nto get a little bit hit by cars.", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 11216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 4667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123703, 28490 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 909283, 911619, "-Hey.\r\n-Hi!", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123704, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123704, 103 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 911786, 914747, "Hey, remember, urn, last night\r\nwhen we were talking about <i>Cujo?</i>", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 470 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 10 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "cujo" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 44305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123705, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 914914, 918251, "Oh, yeah, I can\'t believe you haven\'t\r\nseen <i>Cujo. </i>What is wrong with you?", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 471 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 519 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 44305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123706, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 918417, 920420, "Relax, it\'s not like it\'s <i>Citizen Kane.</i>", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 23712 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 23713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123707, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 920586, 922964, "Have you ever tried to sit through\r\n<i>Citizen Kane?</i>", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 4617 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 349 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 23712 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 23713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123708, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 924340, 925967, "Yeah, I know. It\'s really boring.", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123709, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123709, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123709, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123709, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123709, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123709, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123709, 11572 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 926134, 928386, "-But it\'s a big deal.\r\n-Yeah.", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123710, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123710, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123710, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123710, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123710, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123710, 1671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123710, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 928845, 931722, "Anyway, I was thinking\r\nabout renting <i>Cujo </i>sometime.", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 3930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 208 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "renting" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 44306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 44305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123711, 621 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 931889, 934350, "Well, yeah, all right.\r\nLet\'s do it tonight.", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123712, 353 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 934517, 938104, "-Don\'t you have that big date tonight?\r\n-Oh, right.", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123713, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 938271, 941941, "-Hey, Joey, can I ask you something?\r\n-Yeah.", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123714, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123714, 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123714, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123714, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123714, 400 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123714, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123714, 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123714, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 942108, 944861, "After our date last night,\r\ndid you feel a little weird?", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 1496 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 506 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123715, 99 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 945027, 949782, "Oh, my God. You did too?\r\nIt freaked me out. What was that?", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123716, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 949949, 951951, "I don\'t know.", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123717, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123717, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123717, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123717, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 952243, 955454, "I\'m kind of thinking it was the lobster.", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 3930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123718, 14469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 958749, 961544, "Oh, yeah. Yeah, the lobster.", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123719, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123719, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123719, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123719, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123719, 14469 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 961711, 965339, "-I was up sick all night.\r\n-Yeah, me too. All night.", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 3259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 507 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123720, 507 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 965506, 968050, "Really? How come\r\nwe didn\'t cross paths?", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123721, 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123721, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123721, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123721, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123721, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123721, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123721, 22476 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "paths" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123721, 44307 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 968217, 970678, "Yeah, well, that\'s because, uh...", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123722, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123722, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123722, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123722, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123722, 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123722, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 971679, 972847, "...I stayed in my room.", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123723, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123723, 13863 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123723, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123723, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123723, 213 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 975016, 977477, "Yeah, you don\'t want to look\r\nin my hamper.", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123724, 21521 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 982815, 983983, "Okay, I got that.", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123725, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123725, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123725, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123725, 77 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 984150, 986986, "I\'ll escape there.\r\nI\'ll come back here.", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 15083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123726, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 987153, 989572, "All right. Come on, Ms. Pac-Man-", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123727, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123727, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123727, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123727, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123727, 2438 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123727, 44285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123727, 1645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 991866, 994243, "Well, you\'re just a little bitch, aren\'t you?", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 3255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 3090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123728, 15 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 997580, 999749, "Hey, you guys?", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123729, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123729, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123729, 388 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 999916, 1003711, "I\'m sorry that I was\r\nhogging the game before.", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 11237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 2469 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123730, 211 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1003878, 1009008, "Oh, my God, your friends\r\nhave some unfortunate initials.", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 1651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 15402 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123731, 19173 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1010259, 1012345, "-They\'re all Chandler.\r\n-Chandler sucks.", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123732, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123732, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123732, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123732, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123732, 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123732, 617 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1012511, 1016015, "-He couldn\'t have gotten this good.\r\n-I did.", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 3088 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 4644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123733, 158 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1016182, 1018643, "-But it came at a price.\r\n-Ugh.", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123734, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123734, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123734, 597 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123734, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123734, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123734, 26852 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123734, 3985 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1019852, 1022230, "Ben\'s coming, and he can\'t see this.", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 2565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123735, 49 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1022396, 1025900, "Come on, by age 7,\r\nkids have already seen orgies.", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 1535 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 3917 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 512 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 1549 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 519 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "orgies" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123736, 44308 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1029820, 1031572, "Was it just me?", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123737, 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123737, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123737, 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123737, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1041249, 1043918, "Yes, I made it! I\'m on time!", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123738, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123738, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123738, 497 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123738, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123738, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123738, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123738, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123738, 361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1046546, 1048339, "Agh.", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123739, 18258 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1049757, 1052343, "Okay, why don\'t we all, uh...", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123740, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123740, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123740, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123740, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123740, 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123740, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123740, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1054887, 1057682, "...open our books to page 23...", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123741, 2389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123741, 1662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123741, 6116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123741, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123741, 7211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123741, 23739 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1057932, 1059350, "...where...", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123742, 248 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1060768, 1063938, "...where you will see a, uh...", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123743, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123743, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123743, 529 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123743, 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123743, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123743, 332 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1065606, 1069694, "...a bunch of, uh, red spots.", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123744, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123744, 384 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123744, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123744, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123744, 3313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123744, 3136 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1071070, 1075992, "Okay, why don\'t, uh,\r\nyou all start to read, while I....", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 2453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 1652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 499 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123745, 10 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1085334, 1088671, "Hey, are you all right?\r\nYou seem a little distracted.", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 1516 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123746, 43204 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1088838, 1092341, "No, no, I\'m fine. It\'s just....", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123747, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123747, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123747, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123747, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123747, 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123747, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123747, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123747, 7 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1093384, 1095845, "Hey, can I ask you something?", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123748, 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123748, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123748, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123748, 400 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123748, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123748, 22 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1096095, 1099056, "Have you ever looked at someone\r\nthat you\'ve known for a while...", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 399 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123749, 499 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1099223, 1102518, "...and suddenly seen them\r\nin a different way?", 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123750, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123750, 4007 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123750, 519 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123750, 447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123750, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123750, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123750, 4051 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123750, 420 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1102977, 1104770, "You mean like from behind?", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123751, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123751, 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123751, 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123751, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123751, 5228 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1109525, 1112695, "Yeah. Yeah. That\'s exactly it.", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123752, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123752, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123752, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123752, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123752, 614 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123752, 6 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1112862, 1113988, "You know what?", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123753, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123753, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123753, 42 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1114155, 1117491, "One time I saw this guy from behind,\r\nand he seemed totally normal.", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 1790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 2359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 5228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 11923 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123754, 9920 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1117658, 1120119, "And then he turned around\r\nand it was Stephen Baldwin!", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 146 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "stephen" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 44309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123755, 29154 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1121537, 1126292, "Ooh, yeah. So you know\r\nexactly what I\'m talking about.", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 614 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 434 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123756, 208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1126459, 1128919, "-Totally! Wow.\r\n-Ha-ha-ha.", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123757, 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123757, 477 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123757, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123757, 2338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123757, 2338 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1131380, 1133841, "-Would you excuse me for a sec?\r\n-Yeah, sure.", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 3060 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 7764 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123758, 196 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1142141, 1143893, "Eh.", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123759, 363 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1145936, 1147813, "-God, Phoebe! You\'re on fire!\r\n-I know!", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123760, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123760, 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123760, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123760, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123760, 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123760, 7216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123760, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123760, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1149190, 1150650, "-You can do it!\r\n-Don\'t touch me!", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123761, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123761, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123761, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123761, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123761, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123761, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123761, 1581 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123761, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1150816, 1152151, "Don\'t touch her!", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123762, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123762, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123762, 1581 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123762, 39 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1152318, 1154695, "All right, go left! Go right! Go right!", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123763, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123763, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123763, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123763, 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123763, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123763, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123763, 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123763, 84 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1154862, 1156030, "I can\'t! I can\'t!", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123764, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123764, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123764, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123764, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123764, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123764, 37 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1157406, 1159992, "No! You son of a-", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123765, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123765, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123765, 7229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123765, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123765, 19 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1173255, 1174590, "Phoebe!", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123766, 88 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1175549, 1178177, "Oh, hi, Ben.\r\nNo, don\'t look at the machine!", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 12337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123767, 427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1188270, 1190648, "Aah! God! Thank God you\'re home.", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123768, 6718 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123768, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123768, 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123768, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123768, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123768, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123768, 620 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1190815, 1193109, "-I\'m watching <i>Cujo.\r\n</i>-Alone?", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123769, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123769, 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123769, 5725 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123769, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123769, 44305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123769, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123769, 137 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1193275, 1197613, "-Yes! What is wrong with this dog?\r\n-Did you get to the part...", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 2341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123770, 1480 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1197780, 1201200, "...where they\'re in the car and Cujo\'s\r\nthrowing himself at the windshield?", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 1502 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 44305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 7787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 612 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123771, 35737 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1201367, 1204995, "No. No! Seriously,\r\nwhat\'s wrong with the dog?", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 89 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 4651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123772, 2341 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1205621, 1208040, "What are you doing home?\r\nWhat happened to your date?", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 620 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 452 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123773, 53 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1208207, 1210584, "Oh, uh, it didn\'t work out.", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123774, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123774, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123774, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123774, 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123774, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123774, 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123774, 18 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1210751, 1213170, "Want to watch\r\nthe rest of the movie with me?", 302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 2545 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 4647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123775, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1213337, 1215798, "Oh, uh, okay. Yeah.", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123776, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123776, 332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123776, 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123776, 74 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1215965, 1218300, "I never thought I\'d say this\r\nabout a movie...", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 567 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123777, 4647 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1218467, 1220594, "...but I hope this dog dies.", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123778, 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123778, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123778, 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123778, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123778, 2341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123778, 18888 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1223013, 1225766, "What are you doing over there?\r\nCome sit here. Protect me.", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 1498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 349 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 188 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "protect" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 44310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123779, 61 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1225933, 1229103, "Oh. Sure, yeah. Why not?", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123780, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123780, 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123780, 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123780, 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123780, 51 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1233607, 1235192, "Okay.", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123781, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1239822, 1243659, "-That\'s him, that\'s him! That\'s Cujo!\r\n-All right, I know.", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 44305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123782, 153 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1244785, 1247288, "It\'s gonna be okay.", 310 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123783, 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123783, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123783, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123783, 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123783, 46 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 310   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1247538, 1251584, "Oh, my God. What\'s he gonna do now?\r\nI can\'t watch.", 311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 328 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123784, 421 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 311   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1254587, 1257882, "Ugh, seriously, how can you watch this?\r\nAren\'t you scared?", 312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 3985 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 4651 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 421 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 3090 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123785, 5273 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 312   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1261969, 1264013, "Terrified.", 313 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123786, 18233 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 313   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1274482, 1279361, "So is everybody here?\r\nI got here a little early, myself.", 314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 4730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123787, 108 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 314   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1279528, 1280529, "Let us begin.", 315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123788, 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123788, 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123788, 2452 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 315   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1280696, 1286911, "Now, the hadrosaurids have been\r\nunearthed in two main locations.", 316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hadrosaurids" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 44311 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 373 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "unearthed" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 44312 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 20194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123789, 38956 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 316   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1289288, 1290873, "Here...", 317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123790, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 317   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1291540, 1294835, "...and here.", 318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123791, 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123791, 188 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 318   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 1, 363, 1295753, 1298714, "Now, as for the hadrosaurus....", 319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123792, 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123792, 484 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123792, 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123792, 67 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 1, "hadrosaurus" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123792, 44313 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 1 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/English.srt" ,  `subtitle`.`phrase_number` = 319   WHERE  `subtitle`.`id` = 363 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 182, 363 );
# a query
INSERT INTO `subtitle` ( `subtitle`.`source_id`, `subtitle`.`language_id`, `subtitle`.`file`, `subtitle`.`phrase_number` ) VALUES ( 182, 2, "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt", 0 );
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 3586, 6171, "Эй, у меня для вас подарок.", 1 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123793, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123793, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123793, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123793, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123793, 1044 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123793, 2792 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 1   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 6339, 8799, "О боги, где же ты его спрятала?", 2 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123794, 825 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "боги" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123794, 44314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123794, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123794, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123794, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123794, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123794, 36668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 2   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 8967, 12678, "Я заказала его к вашей свадьбе\r\nи наконец его доставили.", 3 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 8879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 7560 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 11475 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 3436 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123795, 22234 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 3   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 12846, 15472, "Тебе не нужно было нам ничего...", 4 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123796, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123796, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123796, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123796, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123796, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123796, 1214 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 4   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 15640, 18600, "Я обожаю его, он огромный. Давай откроем!", 5 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123797, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123797, 3734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123797, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123797, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123797, 2718 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123797, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123797, 27808 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 5   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 21187, 25274, "- Это игровой автомат с Мисс Пакман!\r\n- О мой бог.", 6 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 35534 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 11279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 2776 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пакман" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 44315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123798, 993 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 6   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 25442, 27526, "Я не знала куда его поставить,\r\nпоэтому оставила здесь.", 7 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 792 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 7949 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 1373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 1118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123799, 827 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 7   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 27694, 30487, "- Мы можем поставить его в гостевую спальню.\r\n- Да, давай.", 8 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 4091 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 7949 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 35462 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 19678 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123800, 1427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 8   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 35577, 38495, "- Знаешь, мне нравится, как он смотрится тут.\r\n- Вам нравится?", 9 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 3639 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 4945 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123801, 3639 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 9   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 38663, 42291, "Конечно! Я практически провела все свое\r\nдетство в галерее игровых автоматов.", 10 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 6503 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 12047 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 3015 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 11800 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "галерее" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 44316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 15323 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "автоматов" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123802, 44317 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 10   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 42459, 46170, "- Это моя вторая по счёту любимая игра.\r\n- А какая первая?", 11 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 13303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 2696 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 12525 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 2848 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 1353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123803, 6954 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 11   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 46337, 50382, "- Я не помню названия.\r\n- Ну, а что в ней нужно было делать?", 12 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 675 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 27481 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 2016 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123804, 2025 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 12   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 50550, 55304, "Ты кладешь четвертак, дергаешь за несколько\r\nручек и выигрываешь шоколадный батончик.", 13 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 18149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 35111 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дергаешь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 44318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 5148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 29852 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 35030 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 42278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123805, 38879 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 13   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 55472, 57473, "Автомат с конфетами?", 14 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123806, 11279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123806, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123806, 34907 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 14   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 57640, 60601, "Не надо меня жалеть. Я всегда выигрывала!", 15 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123807, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123807, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123807, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123807, 21886 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123807, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123807, 706 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выигрывала" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123807, 44319 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 15   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 100600, 102810, "Я так рад, что вы здесь ребята.", 16 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123808, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123808, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123808, 9239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123808, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123808, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123808, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123808, 843 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 16   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 102977, 106563, "Я просто умирал от нетерпения рассказать,\r\nчто сегодня произошло на нашей кафедре.", 17 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 646 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "умирал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 44320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 10872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 2113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 1181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 848 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123809, 40368 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 17   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 106731, 109775, "Как ты думаешь, он нас видел\r\nили мы еще можем ускользнуть?", 18 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 1209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 4265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 4091 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123810, 39519 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 18   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 110735, 114863, "Профессор Ньюман, заведующий кафедры,\r\nуходит на пенсию, поэтому...", 19 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123811, 24114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123811, 30137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123811, 35610 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кафедры" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123811, 44321 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123811, 5096 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123811, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123811, 33562 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123811, 1373 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 19   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 115115, 120202, "- Они сделали тебя заведующим?\r\n- Нет, я буду учить его старший курс.", 20 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 11084 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 668 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заведующим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 44322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 22231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 1454 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123812, 5397 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 20   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 120995, 124373, "Почему меня не сделали заведующим?", 21 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123813, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123813, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123813, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123813, 11084 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123813, 44322 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 21   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 125375, 127334, "Эй, Рейч!", 22 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123814, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123814, 13310 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 22   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 127502, 130003, "У меня на носу важное свидание.\r\nТы знаешь какой-нибудь хороший ресторан?", 23 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 27170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 8913 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 657 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 4752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123815, 2047 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 23   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 130588, 135259, "Кафе Пола. Там отличная еда\r\nи он очень романтичный.", 24 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 5332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 1334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 7445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 3686 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123816, 21074 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 24   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 135426, 136844, "Отлично, спасибо!", 25 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123817, 1422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123817, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 25   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 137011, 140556, "Потом отведи ее в Four Seasons\r\nчто-нибудь выпить.", 26 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 707 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отведи" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 44323 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "four" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 44324 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "seasons" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 44325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123818, 7474 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 26   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 141349, 144643, "Или езжайте в центр и послушайте джаз.", 27 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123819, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123819, 14677 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123819, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123819, 16368 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123819, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123819, 3665 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123819, 39022 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 27   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 144811, 147271, "Или на танцы. Боже, ты должен\r\nотвести ее потанцевать.", 28 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 6612 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 28103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123820, 11676 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 28   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 147438, 151066, "Ты точно знаешь кучу способов,\r\nкак отстрочить секс.", 29 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123821, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123821, 2128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123821, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123821, 5384 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123821, 42936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123821, 787 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отстрочить" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123821, 44326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123821, 1845 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 29   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 151568, 156280, "Я скучаю по свиданиям. Красиво одеться,\r\nпойти в хороший ресторан.", 30 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 2017 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 819 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 29871 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 1106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 20773 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 2023 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 4752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123822, 2047 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 30   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 157157, 161410, "У меня еще не скоро появится шанс\r\nсделать это. А это так весело.", 31 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 2625 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 8402 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 5152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 2250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123823, 5885 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 31   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 162120, 166248, "Не то, чтобы сидеть и волноваться\r\nо родах, это не весело.", 32 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 1394 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 16337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 825 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "родах" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 44327 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123824, 5885 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 32   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 167959, 171503, "Эй, знаешь что? Почему бы мне\r\nне отвести тебя на свидание?", 33 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 28103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123825, 685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 33   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 172046, 175007, "Что? Ты не хочешь идти на свидание\r\nс беременной женщиной.", 34 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 1955 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 7606 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123826, 2119 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 34   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 175175, 178051, "Еще как хочу. Мы отлично проведем время.", 35 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123827, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123827, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123827, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123827, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123827, 1422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123827, 31863 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123827, 1012 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 35   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 178219, 182431, "Ты отвлечешься от мыслей о ребенке,\r\nо кесаревом сечении...", 36 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 23266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 11683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 14627 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 825 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кесаревом" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 44328 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сечении" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123828, 44329 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 36   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 182599, 185100, "...и голове ребенка вылезающей прямо...", 37 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123829, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123829, 906 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123829, 2213 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "вылезающей" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123829, 44330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123829, 3021 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 37   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 185268, 186768, "Ладно, ладно! Я пойду с тобой на свидание.", 38 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123830, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123830, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123830, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123830, 2300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123830, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123830, 735 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123830, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123830, 685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 38   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 186936, 189438, "- Это будет весело.\r\n- Ладно.", 39 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123831, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123831, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123831, 5885 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123831, 1037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 39   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 189606, 191565, "Нет, нет! Да!", 40 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123832, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123832, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123832, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 40   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 193067, 194902, "О, посмотри на это, Моника.", 41 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123833, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123833, 1992 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123833, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123833, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123833, 826 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 41   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 195069, 197112, "Я только что побила твои рекорды. Как жаль.", 42 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123834, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123834, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123834, 643 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "побила" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123834, 44331 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123834, 1961 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рекорды" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123834, 44332 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123834, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123834, 2965 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 42   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 198448, 199907, "Ладно, я следующая.", 43 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123835, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123835, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123835, 8530 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 43   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 200074, 203410, "Не начинай новую игру!\r\nЯ следующая, Фиби!", 44 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123836, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123836, 6505 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123836, 1076 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123836, 3765 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123836, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123836, 8530 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123836, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 44   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 203578, 206830, "Извини, я тебя не расслышала\r\nиз-за всех этих побед.", 45 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 1447 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 43129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 2833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 804 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "побед" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123837, 44333 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 45   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 207624, 211210, "- Фиби оккупировала игру.\r\n- Это дурацкая игра.", 46 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123838, 853 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "оккупировала" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123838, 44334 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123838, 3765 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123838, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123838, 8903 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123838, 2848 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 46   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 211377, 214338, "Ты считаешь, что она дурацкая,\r\nпотому то ты в нее отстойно играешь.", 47 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 12860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 8903 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 1399 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отстойно" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 44335 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123839, 4811 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 47   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 215215, 217799, "Я не остойно играю. Это игра отстой.", 48 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123840, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123840, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "остойно" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123840, 44336 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123840, 5006 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123840, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123840, 2848 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123840, 36038 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 48   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 217967, 219760, "Ты отстой.", 49 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123841, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123841, 36038 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 49   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 222263, 225140, "Если эта игра доставляет столько проблем...", 50 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123842, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123842, 2966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123842, 2848 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123842, 44274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123842, 2284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123842, 8774 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 50   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 225308, 229144, "...мне лучше оставить ее себе.\r\n- Нет, я обожаю ее. Это отличный подарок.", 51 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 991 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 5110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 997 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 3734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 1115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123843, 2792 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 51   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 229312, 233649, "Почему бы тебе не пойти домой и не дождаться\r\nнашей открытки с благодарностями?", 52 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 2023 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 832 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 19682 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 848 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 8862 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "благодарностями" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123844, 44337 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 52   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 233816, 237152, "- Почему ты так хочешь поиграть?\r\n- Да, она же не выдает шоколадные батончики...", 53 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 1035 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 10565 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выдает" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 44338 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 3518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123845, 15350 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 53   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 237320, 239154, "...после каждой игры.", 54 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123846, 1824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123846, 6037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123846, 3045 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 54   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 239489, 243909, "Все, Фиби, хватит. Слезай со стула.", 55 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123847, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123847, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123847, 951 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123847, 12036 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123847, 676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123847, 20922 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 55   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 244911, 246828, "Фиби!", 56 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123848, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 56   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 250458, 253001, "Джоуи, ты не мог бы подойти?", 57 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123849, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123849, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123849, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123849, 788 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123849, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123849, 14070 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 57   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 256089, 257631, "Джоуи.", 58 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123850, 854 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 58   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 266182, 268016, "Я думала, ты у себя в комнате.", 59 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123851, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123851, 1453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123851, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123851, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123851, 947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123851, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123851, 2095 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 59   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 268184, 271687, "У нас же сегодня свидание с тобой. Это тебе.", 60 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 735 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123852, 753 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 60   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 272188, 276900, "Лилии. Мои любимые. Спасибо.", 61 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123853, 39033 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123853, 1097 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123853, 18359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123853, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 61   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 277068, 278860, "И батончик.", 62 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123854, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123854, 38879 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 62   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 281197, 283615, "Ну, половинка батончика.", 63 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123855, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123855, 11078 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123855, 3663 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 63   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 283783, 285909, "Вообще-то, только пакетик.", 64 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123856, 897 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123856, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123856, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123856, 43074 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 64   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 286077, 289705, "Долгий путь от цветочного магазина,\r\nя почувствовал себя нехорошо...", 65 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "долгий" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 44339 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 15266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 730 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "цветочного" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 44340 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 6851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 5140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123857, 8667 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 65   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 290290, 293667, "Так здорово!", 66 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123858, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123858, 1261 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 66   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 293835, 296295, "Я чувствую себя как на настоящем свидании.", 67 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123859, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123859, 4412 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123859, 947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123859, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123859, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123859, 22969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123859, 1208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 67   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 296462, 301758, "Если не считать, что у меня утренняя тошнота\r\nи нижнее бельё доходит вот...", 68 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 3785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 28779 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 28780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 15890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 4247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 15624 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123860, 694 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 68   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 301926, 303135, "...сюда.", 69 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123861, 1357 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 69   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 303303, 306513, "Эй, ну хватит. Это настоящее свидание.", 70 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123862, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123862, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123862, 951 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123862, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123862, 5012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123862, 685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 70   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 306681, 308807, "У тебя тут так мило.", 71 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123863, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123863, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123863, 644 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123863, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123863, 2916 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 71   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 310018, 313478, "Настольный футбол. Коробка из-под пиццы.", 72 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123864, 36014 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123864, 5970 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123864, 23314 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123864, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123864, 5782 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123864, 3685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 72   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 313646, 317399, "О, и подписка на Playboy.\r\nЛюблю таких женщин.", 73 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123865, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123865, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подписка" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123865, 44341 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123865, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123865, 36916 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123865, 934 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123865, 1019 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123865, 3773 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 73   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 318067, 319568, "Это моего соседа.", 74 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123866, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123866, 856 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123866, 6834 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 74   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 319736, 323530, "Я бы с ним познакомился.\r\nКажется, он классный парень.", 75 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 13587 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 1046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 8004 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123867, 836 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 75   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 323698, 325824, "Он очень меня защищает...", 76 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123868, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123868, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123868, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123868, 21414 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 76   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 325992, 327909, "...так что тебе нужно быть осторожнее.", 77 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123869, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123869, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123869, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123869, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123869, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123869, 19847 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 77   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 328077, 330954, "А твой сосед, он симпатичный?", 78 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123870, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123870, 1330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123870, 5378 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123870, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123870, 3807 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 78   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 333416, 336251, "Наверное, сложновато тебе\r\nдержать руки при себе.", 79 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123871, 1940 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "сложновато" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123871, 44342 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123871, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123871, 1241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123871, 879 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123871, 4153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123871, 997 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 79   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 336753, 338628, "Я почти уверена, что он гей.", 80 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123872, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123872, 7147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123872, 5160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123872, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123872, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123872, 13944 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 80   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 339547, 342382, "Нет, нет, он не гей!\r\nЗачем ты пытаешься испортить игру?", 81 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 13944 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 25159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 10030 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123873, 3765 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 81   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 345553, 348930, "Что возвращает нас к теории\r\nдоминирования Грили.", 82 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123874, 643 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "возвращает" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123874, 44343 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123874, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123874, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123874, 13774 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "доминирования" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123874, 44344 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "грили" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123874, 44345 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 82   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 350475, 351516, "На сегодня всё.", 83 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123875, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123875, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123875, 681 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 83   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 351684, 355187, "Кто-нибудь знает, где корпус Фримана?", 84 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123876, 915 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123876, 658 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123876, 2192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123876, 875 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "корпус" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123876, 44346 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "фримана" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123876, 44347 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 84   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 355355, 357689, "Новое здание на Авеню А.", 85 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123877, 4921 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123877, 9765 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123877, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123877, 10528 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123877, 666 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 85   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 357857, 359316, "Но это же на другом конце города!", 86 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123878, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123878, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123878, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123878, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123878, 9219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123878, 9128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123878, 10217 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 86   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 359484, 361943, "У меня там занятие через 10 минут.", 87 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123879, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123879, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123879, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123879, 9791 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123879, 1833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123879, 4779 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123879, 2629 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 87   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 362111, 364321, "Чувак, этого не случится.", 88 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123880, 3361 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123880, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123880, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123880, 2765 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 88   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 367492, 369534, "Шустрее, шустрее!", 89 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "шустрее" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123881, 44348 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123881, 44348 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 89   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 369702, 371953, "Эй, я учитель!", 90 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123882, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123882, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123882, 22201 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 90   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 385676, 387219, "Здравствуйте.", 91 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123883, 5545 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 91   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 391391, 393058, "Простите, что немного опоздал.", 92 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123884, 3582 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123884, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123884, 1119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123884, 9668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 92   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 393226, 394643, "Ого! Сильно опоздал.", 93 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123885, 4169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123885, 5445 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123885, 9668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 93   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 395645, 399856, "Что ж, позвольте мне для начала\r\nпредставиться.", 94 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123886, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123886, 2641 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123886, 27134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123886, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123886, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123886, 7088 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "представиться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123886, 44349 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 94   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 400024, 401650, "Я профессор Геллер.", 95 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123887, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123887, 24114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123887, 2265 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 95   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 403444, 405987, "Что ж, подытожим...", 96 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123888, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123888, 2641 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "подытожим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123888, 44350 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 96   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 406155, 408990, "...я профессор Геллер.\r\nОтлично поработали сегодня.", 97 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123889, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123889, 24114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123889, 2265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123889, 1422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123889, 26459 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123889, 749 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 97   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 412620, 414621, "А что на гарнир к филе миньон?", 98 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123890, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123890, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123890, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123890, 6390 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123890, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123890, 30242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123890, 30243 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 98   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 414789, 416415, "Овощи на пару.", 99 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123891, 18692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123891, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123891, 3680 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 99   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 419544, 421628, "А вместо овощей...", 100 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123892, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123892, 714 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "овощей" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123892, 44351 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 100   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 421796, 426341, "...не могу ли я заказать...", 101 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123893, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123893, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123893, 1234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123893, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123893, 22119 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 101   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 426509, 428427, "...трёхфунтового лобстера?", 102 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "трёхфунтового" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123894, 44352 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123894, 22123 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 102   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 430346, 432347, "Знаете что? Принесите ей оба блюда.", 103 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123895, 1157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123895, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123895, 9977 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123895, 840 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123895, 3346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123895, 3507 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 103   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 432515, 434182, "И мне то же самое.", 104 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123896, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123896, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123896, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123896, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123896, 2942 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 104   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 435101, 438854, "Вау, кажется, намечается\r\nочень неплохое свидание.", 105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123897, 35495 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123897, 1046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123897, 40326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123897, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123897, 30315 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123897, 685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 105   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 439272, 441398, "Я чуть не забыла.\r\nЯ забыла заплатить за квартиру.", 106 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 664 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 1977 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 1977 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 17270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123898, 10488 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 106   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 441566, 444776, "Никаких соседских разговоров.\r\nУ нас свидание.", 107 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123899, 1063 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "соседских" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123899, 44353 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123899, 9408 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123899, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123899, 1210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123899, 685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 107   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 444944, 449865, "Вау, я увижу, каким бывает\r\nДжоуи Триббиани на свидании.", 108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 35495 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 7120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 4200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 1009 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 5165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123900, 1208 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 108   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 450032, 451116, "Какие у тебя есть приёмчики?", 109 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123901, 1055 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123901, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123901, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123901, 671 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "приёмчики" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123901, 44354 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 109   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 451284, 455537, "Я просто стараюсь быть собой.\r\nИ если девушке не нравлюсь я такой...", 110 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 6262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 1406 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 12724 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 2846 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123902, 1310 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 110   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 458124, 460125, "Прости, я даже договорить это не могу.", 111 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123903, 932 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123903, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123903, 684 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123903, 5128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123903, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123903, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123903, 930 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 111   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 462503, 464880, "Так и знала. Рассказывай про приёмчики.", 112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123904, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123904, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123904, 792 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123904, 5033 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123904, 946 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123904, 44354 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 112   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 465047, 466548, "Ладно.", 113 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123905, 1037 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 113   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 466716, 468341, "Что ж...", 114 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123906, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123906, 2641 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 114   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 468509, 473013, "Я начинаю с того, что мне за стол\r\nприносят бутылку вина \"от фаната\".", 115 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 14851 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 1151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 1269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 18373 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 8775 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 6044 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 730 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "фаната" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123907, 44355 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 115   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 473181, 476725, "Боже мой. И это работает?", 116 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123908, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123908, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123908, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123908, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123908, 2145 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 116   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 476893, 479853, "Особенно если добавить: \"Мне так неловко.", 117 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123909, 2843 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123909, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123909, 1978 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123909, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123909, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123909, 4126 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 117   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 480021, 482230, "Я просто хочу жить нормальной жизнью!\"", 118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123910, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123910, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123910, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123910, 744 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123910, 12058 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123910, 5981 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 118   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 482398, 484941, "Бедненький известный мальчик.", 119 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бедненький" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123911, 44356 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123911, 15594 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123911, 1296 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 119   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 485109, 487944, "ОК, а как тебе вот это?", 120 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123912, 13609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123912, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123912, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123912, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123912, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123912, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 120   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 488988, 492032, "Я собирался дождаться конца вечера,\r\nпрежде, чем поцеловать тебя...", 121 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 4807 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 19682 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 1319 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 7523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 9659 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 890 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 5437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123913, 668 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 121   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 492200, 495494, "...но ты такая красивая,\r\nчто не думаю, что я смогу.", 122 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 2616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 4320 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 1322 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123914, 3411 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 122   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 497580, 499664, "Боже мой.", 123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123915, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123915, 891 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 123   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 500875, 505128, "Вау, здорово!\r\nЯ почти потянулась к тебе, серьёзно!", 124 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123916, 35495 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123916, 1261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123916, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123916, 7147 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "потянулась" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123916, 44357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123916, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123916, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123916, 774 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 124   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 505296, 507506, "Давай ты расскажи о своих приёмчиках.", 125 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123917, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123917, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123917, 3568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123917, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123917, 2747 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "приёмчиках" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123917, 44358 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 125   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 507715, 509716, "Хорошо.", 126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123918, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 126   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 512845, 514888, "Где ты вырос?", 127 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123919, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123919, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123919, 13836 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 127   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 516015, 517516, "И это твой приём?", 128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123920, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123920, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123920, 1330 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123920, 17607 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 128   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 518226, 520602, "Да уж, Рэйч, тебе повезло,\r\nчто ты сексуальная.", 129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123921, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123921, 1253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123921, 3523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123921, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123921, 4380 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123921, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123921, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123921, 5135 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 129   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 520937, 524773, "- Ну давай, отвечай на вопрос.\r\n- Квинс.", 130 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123922, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123922, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123922, 20799 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123922, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123922, 1112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123922, 42143 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 130   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 525566, 527359, "У тебя были близкие отношения с родителями?", 131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123923, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123923, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123923, 876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123923, 21700 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123923, 2598 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123923, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123923, 3025 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 131   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 527527, 531071, "- С мамой. Но не с отцом.\r\n- Почему?", 132 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123924, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123924, 7623 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123924, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123924, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123924, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123924, 8501 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123924, 661 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 132   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 531239, 534074, "Не знаю. Просто между нами\r\nвсегда была такая пропасть.", 133 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 1173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 1174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 706 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 2616 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123925, 10619 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 133   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 534242, 537869, "Мы оба делали вид, что её не было,\r\nно она есть.", 134 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 3346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 4272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 1375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 926 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 771 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123926, 671 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 134   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 539830, 541331, "Наверное, это нелегко.", 135 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123927, 1940 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123927, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123927, 3006 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 135   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 541499, 545335, "Да, очень нелегко. Иногда мне кажется...", 136 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123928, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123928, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123928, 3006 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123928, 793 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123928, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123928, 1046 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 136   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 545503, 547546, "Вау!", 137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123929, 35495 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 137   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 549674, 552425, "Отлично!", 138 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123930, 1422 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 138   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 552593, 556346, "- \"Где ты вырос?\" Это так просто!\r\n- Спасибо.", 139 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123931, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123931, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123931, 13836 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123931, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123931, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123931, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123931, 756 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 139   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 556514, 559266, "Так, прошу прощения.\r\nЯ пройду в дамскую комнату.", 140 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123932, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123932, 2744 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123932, 2745 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123932, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123932, 20426 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123932, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "дамскую" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123932, 44359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123932, 874 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 140   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 562812, 567065, "- А теперь ты смотришь, как я ухожу.\r\n- И правда! Снова, так просто!", 141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 5969 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 8078 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 697 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123933, 646 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 141   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 571404, 575949, "- Ты не поверишь, что я сделал сегодня.\r\n- Очевидно, не умылся и не побрился.", 142 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 23895 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 2854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 4187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "умылся" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 44360 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "побрился" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123934, 44361 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 142   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 576117, 579869, "Я играл весь день, и теперь я жгу.", 143 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123935, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123935, 1291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123935, 2984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123935, 1032 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123935, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123935, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123935, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123935, 16705 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 143   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 580037, 582831, "Им нужно сменить название на \"Ms Chandler\".", 144 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123936, 1921 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123936, 1966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123936, 2976 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123936, 13666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123936, 784 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ms" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123936, 44362 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "chandler" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123936, 44363 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 144   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 585209, 586501, "Хотя я надеюсь, что они не сделают этого.", 145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123937, 2078 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123937, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123937, 855 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123937, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123937, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123937, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123937, 14579 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123937, 1220 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 145   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 587461, 589838, "Ты весь день сидел дома\r\nи играл в Ms. Pac-Man...", 146 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 2984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 1032 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 8247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 1010 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 1291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 44362 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "pac" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 44364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123938, 42715 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 146   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 590006, 592007, "...пока я, как неудачница, была на работа?", 147 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123939, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123939, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123939, 787 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "неудачница" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123939, 44365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123939, 772 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123939, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123939, 5834 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 147   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 592717, 597887, "Я занял весь топ-10.\r\nЯ удалил Фиби с доски. Дай пять!", 148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 28820 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 2984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 33746 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 4779 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 32451 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "доски" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 44366 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 1358 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123940, 2628 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 148   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 599515, 601558, "Что с твоей рукой?", 149 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123941, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123941, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123941, 1964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123941, 13176 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 149   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 605354, 607188, "Я играл восемь часов.", 150 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123942, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123942, 1291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123942, 3427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123942, 2201 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 150   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 607356, 610275, "Это пройдёт. Ты зацени результаты.", 151 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123943, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123943, 2630 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123943, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123943, 3365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123943, 7865 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 151   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 610443, 613903, "А ещё посмотри на инициалы.\r\nОни все неприличные.", 152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123944, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123944, 845 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123944, 1992 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123944, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123944, 28099 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123944, 2022 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123944, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123944, 40077 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 152   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 614071, 618366, "- Чендлер, зачем ты это сделал?\r\n- Потому что это круто.", 153 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 909 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 2854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123945, 2816 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 153   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 618534, 620118, "Ты думаешь, это остроумно?", 154 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123946, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123946, 1209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123946, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123946, 40922 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 154   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 620286, 624539, "Можно поставить только три буквы,\r\nтак что после A-S-S это нетривиально.", 155 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 7949 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 1244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 20604 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 1824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 37724 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 36670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 36670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 683 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "нетривиально" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123947, 44367 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 155   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 626208, 629878, "- Это не непристойно.\r\n- Соедини вот с этим.", 156 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123948, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123948, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123948, 12236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123948, 25386 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123948, 694 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123948, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123948, 1138 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 156   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 630046, 631713, "Ой.", 157 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123949, 1217 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 157   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 631881, 635300, "Но если ты это не уберёшь,\r\nто ты всего этого от меня не получишь.", 158 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "уберёшь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 44368 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 730 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123950, 6555 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 158   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 637053, 639846, "Бен завтра приезжает,\r\nэтого здесь быть не должно.", 159 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123951, 12546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123951, 2024 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123951, 19974 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123951, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123951, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123951, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123951, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123951, 9257 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 159   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 640014, 643933, "- Он же не знает, что это значит.\r\n- Ему 7 лет, но он не тупой.", 160 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 2192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 692 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 2995 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 2968 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123952, 13427 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 160   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 644101, 648688, "- Ты с ним в последнее время общалась?\r\n- Я его выдерну из розетки.", 161 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 649 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 9790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 1012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 19364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 933 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "выдерну" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 44369 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123953, 29694 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 161   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 648856, 650940, "Но тогда мой сегодняшний день\r\nбудет бессмысленен!", 162 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123954, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123954, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123954, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123954, 8670 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123954, 1032 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123954, 966 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бессмысленен" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123954, 44370 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 162   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 651108, 652400, "Как на работе.", 163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123955, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123955, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123955, 2184 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 163   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 653402, 654778, "Нет...", 164 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123956, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 164   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 656322, 657947, "Эй, смотри, всё на месте.", 165 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123957, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123957, 1439 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123957, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123957, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123957, 2662 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 165   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 658115, 661493, "В этой штуке должен быть какой-то\r\nпримитивный чип ROM...", 166 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 5026 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 22075 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 816 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 657 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 835 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "примитивный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 44371 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 10453 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "rom" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123958, 44372 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 166   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 661661, 664746, "- Ты должен побить свои результаты!\r\n- Клешнёй?", 167 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123959, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123959, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123959, 7535 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123959, 1359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123959, 7865 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "клешнёй" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123959, 44373 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 167   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 664914, 668625, "Ну хорошо, я это сделаю.\r\nМы должны это удалить.", 168 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 4518 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 2305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123960, 12794 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 168   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 668793, 671836, "Кэрол до сих пор расстроена, что я рассказала\r\nему про \"Потяни меня за палец\".", 169 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 750 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 3498 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 1200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 8245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 10561 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 2171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 946 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 39317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123961, 2623 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 169   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 672838, 676508, "Потяни меня за палец.\r\nМоя рука в ужасном состоянии!", 170 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 39317 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 2623 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 961 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 2189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "ужасном" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 44374 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123962, 2002 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 170   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 679929, 684766, "- Я не буду отвечать на этот вопрос.\r\n- Ну давай. Просто выбери.", 171 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 6576 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 1112 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123963, 7960 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 171   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 684934, 687435, "Из Моники, Фиби, Чендлера и Росса...", 172 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123964, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123964, 964 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123964, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123964, 5379 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123964, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123964, 858 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 172   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 687603, 691147, "...если бы пришлось выбирать,\r\nкого бы ты стукнула?", 173 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123965, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123965, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123965, 10605 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123965, 12367 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123965, 918 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123965, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123965, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123965, 22415 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 173   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 691941, 694984, "Мы друзья. Я не стала бы никого из них бить.", 174 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 1383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 1129 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 1410 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 1388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123966, 19372 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 174   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 695736, 697278, "Чендлер?", 175 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123967, 2148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 175   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 697738, 699155, "Да, но я не знаю, почему.", 176 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123968, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123968, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123968, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123968, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123968, 1093 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123968, 661 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 176   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 701617, 704327, "- Я отлично провожу время.\r\n- Я тоже.", 177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123969, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123969, 1422 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123969, 12513 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123969, 1012 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123969, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123969, 1071 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 177   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 704495, 705787, "Это лучшее свидание, которое у меня было.", 178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123970, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123970, 7984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123970, 685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123970, 4178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123970, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123970, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123970, 742 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 178   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 705955, 708540, "Понимаю.", 179 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123971, 799 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 179   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 708708, 712419, "Я так смеялся. Ты заметила,\r\nчто у меня из носа потекло вино?", 180 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 17350 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 894 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 22185 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "потекло" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 44375 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123972, 4245 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 180   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 712586, 716047, "Джоуи, мне кажется, это все заметили.", 181 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123973, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123973, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123973, 1046 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123973, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123973, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123973, 2815 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 181   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 716215, 721052, "Никогда не думал, что мне может так\r\nпонравиться свидание без секса.", 182 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 2255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 962 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 13446 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 841 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123974, 690 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 182   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 721220, 725640, "Это потому, что ты никогда раньше\r\nне ходил на свидание со мной.", 183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 900 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 1302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 676 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123975, 1368 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 183   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 727476, 729811, "Ну ладно. Только не осуждай меня.", 184 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123976, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123976, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123976, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123976, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "осуждай" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123976, 44376 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123976, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 184   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 729979, 733064, "Я обычно дожидаюсь, пока ухажёр уйдёт,\r\nно ты живёшь здесь.", 185 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 12510 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 9742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 26482 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 20808 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 3617 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123977, 827 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 185   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 733232, 734441, "Я хочу уткнуться в подушку.", 186 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123978, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123978, 815 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "уткнуться" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123978, 44377 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123978, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123978, 21059 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 186   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 734608, 737360, "ОК, тогда ты тоже меня не осуждай.", 187 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123979, 13609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123979, 1028 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123979, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123979, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123979, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123979, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123979, 44376 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 187   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 737528, 741197, "Я буду облизывать целлофан\r\nиз-под батончика.", 188 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123980, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123980, 2591 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "облизывать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123980, 44378 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "целлофан" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123980, 44379 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123980, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123980, 5782 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123980, 3663 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 188   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 749999, 754711, "Ну рассказывай, какие у тебя приёмчики\r\nприпасены на конец?", 189 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 5033 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 1055 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 44354 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 24752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123981, 3044 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 189   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 756130, 760967, "Ну, если я хочу, чтобы девушка\r\nменя поцеловала...", 190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123982, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123982, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123982, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123982, 815 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123982, 770 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123982, 1958 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123982, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123982, 33902 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 190   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 761135, 764429, "...то сначала я делаю\r\nсвои губы неотразимыми.", 191 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123983, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123983, 8904 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123983, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123983, 910 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123983, 1359 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123983, 9290 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "неотразимыми" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123983, 44380 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 191   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 764597, 766347, "И как ты это делаешь?", 192 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123984, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123984, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123984, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123984, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123984, 2721 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 192   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 766515, 770018, "Так, только никому не говори, но...", 193 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123985, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123985, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123985, 5178 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123985, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123985, 3041 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123985, 803 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 193   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 770311, 772479, "...я использую бальзам для губ с блеском.", 194 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123986, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123986, 26365 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123986, 15979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123986, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123986, 6861 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123986, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "блеском" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123986, 44381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 194   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 774523, 778818, "- Боже мой.\r\n- Как мотыльки к огню, я тебе говорю.", 195 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 787 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "мотыльки" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 44382 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 36190 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123987, 956 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 195   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 779987, 783448, "- ОК, теперь ты давай.\r\n- Нет, я тебе не буду рассказывать.", 196 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 13609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123988, 645 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 196   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 783991, 785450, "- Почему?\r\n- Потому что мне неудобно.", 197 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123989, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123989, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123989, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123989, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123989, 7428 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 197   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 786035, 788286, "Более неудобно, чем земляничный бальзам\r\nдля губ с блеском?", 198 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 1346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 7428 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 890 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "земляничный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 44383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 15979 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 6861 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123990, 44381 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 198   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 790623, 793041, "Я до этого не говорил \"земляничный\", да?", 199 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123991, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123991, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123991, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123991, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123991, 4806 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123991, 44383 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123991, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 199   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 793709, 797212, "Ну расскажи, Рэйч. Просто расскажи мне.", 200 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123992, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123992, 3568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123992, 3523 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123992, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123992, 3568 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123992, 695 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 200   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 797379, 799255, "ОК. Ладно, вставай.", 201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123993, 13609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123993, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123993, 8849 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 201   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 803177, 805178, "Ну, когда мы стоим у двери...", 202 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123994, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123994, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123994, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123994, 6524 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123994, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123994, 7653 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 202   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 805554, 810433, "...я легонько прикасаюсь\r\nсвоими губами к его.", 203 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123995, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123995, 25613 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прикасаюсь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123995, 44384 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123995, 8297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123995, 5131 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123995, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123995, 933 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 203   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 810893, 814813, "А потом прижимаюсь к нему,\r\nвсего на секунду.", 204 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123996, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123996, 707 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "прижимаюсь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123996, 44385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123996, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123996, 6966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123996, 1183 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123996, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123996, 3499 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 204   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 815314, 817565, "А потом я делаю так.", 205 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123997, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123997, 707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123997, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123997, 910 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123997, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 205   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 823531, 826366, "Казалось бы, ничего особенного, но работает.", 206 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123998, 9599 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123998, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123998, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123998, 2227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123998, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123998, 2145 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 206   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 826534, 829577, "О да. Это работает, да.", 207 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123999, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123999, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123999, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123999, 2145 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 123999, 652 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 207   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 830746, 834207, "Ну ладно, я спать. Я чудесно провела время.", 208 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124000, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124000, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124000, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124000, 1435 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124000, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124000, 6000 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124000, 12047 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124000, 1012 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 208   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 841715, 843216, "Я тоже.", 209 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124001, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124001, 1071 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 209   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 854019, 857105, "Ты опоздал на 50 минут? Ты что туда, полз?", 210 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 9668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 6273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 2629 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 4077 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "полз" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124002, 44386 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 210   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 857273, 860400, "Нет, я бежал, понятно? Это очень далеко!", 211 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124003, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124003, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124003, 32437 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124003, 4409 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124003, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124003, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124003, 15656 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 211   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 860568, 864612, "И когда люди разучились понимать\r\nпростую фразу: \"Прочь с моей дороги!\"", 212 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 1126 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 1871 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разучились" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 44387 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 2081 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 27852 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 17859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 11325 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 2056 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124004, 4501 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 212   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 865531, 867198, "Почему ты не взял такси?", 213 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124005, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124005, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124005, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124005, 6823 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124005, 7080 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 213   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 867366, 872495, "С пробками и односторонним движением\r\nя бы так вдвое дольше добирался.", 214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 648 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пробками" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 44388 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "односторонним" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 44389 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 24465 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 43357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 6982 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "добирался" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124006, 44390 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 214   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 872663, 877166, "И потом, у меня там занятия трижды в неделю.\r\nЯ что, Рокфеллер?", 215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 713 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 11137 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 35326 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 1128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124007, 44183 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 215   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 878294, 880545, "Ты не можешь и дальше так делать.", 216 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124008, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124008, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124008, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124008, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124008, 3632 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124008, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124008, 2025 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 216   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 880713, 884716, "Но если я не буду,\r\nто у меня отберут этот класс.", 217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 2591 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 835 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 711 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "отберут" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 44391 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124009, 5587 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 217   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 884884, 887677, "А я уже написал об этом в семейную газету.", 218 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 1914 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 2963 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 785 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 698 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "семейную" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 44392 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124010, 13934 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 218   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 888929, 892265, "- Куда?\r\n- Ну, вы же видели: The Geller Yeller.", 219 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124011, 911 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124011, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124011, 783 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124011, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124011, 1947 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124011, 16436 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "geller" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124011, 44393 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "yeller" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124011, 44394 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 219   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 893934, 896769, "Точно. Вау.", 220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124012, 2128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124012, 35495 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 220   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 897229, 902358, "Я разработал более быстрый маршрут.\r\nУверен, что на этот раз успею вовремя.", 221 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 674 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "разработал" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 44395 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 1346 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 10984 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 12743 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 6588 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 717 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 1038 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 10015 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124013, 1275 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 221   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 902526, 906487, "Просто не надо бояться,\r\nесли меня немного собьёт машина.", 222 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 758 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 13812 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 790 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 1119 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "собьёт" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 44396 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124014, 4314 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 222   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 909283, 911618, "- Привет.\r\n- Привет.", 223 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124015, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124015, 729 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 223   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 911785, 914746, "Помнишь, мы говорили про \"Куджо\"?", 224 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124016, 859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124016, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124016, 4805 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124016, 946 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "куджо" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124016, 44397 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 224   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 914914, 918249, "Поверить не могу, что ты не видела \"Куджо\"!", 225 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124017, 4993 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124017, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124017, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124017, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124017, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124017, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124017, 901 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124017, 44397 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 225   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 918417, 920418, "Да успокойся, это же не \"Гражданин Кейн\".", 226 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124018, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124018, 1933 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124018, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124018, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124018, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гражданин" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124018, 44398 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кейн" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124018, 44399 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 226   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 920586, 922962, "А ты досмотрела \"Гражданина Кейна\" до конца?", 227 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124019, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124019, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124019, 19121 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гражданина" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124019, 44400 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "кейна" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124019, 44401 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124019, 872 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124019, 1319 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 227   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 924340, 925965, "Ага, он очень скучный.", 228 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124020, 2880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124020, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124020, 723 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124020, 6852 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 228   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 926133, 928384, "Но считается, что он, типа, крутой.", 229 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124021, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124021, 11453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124021, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124021, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124021, 1809 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124021, 4108 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 229   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 928844, 931721, "В общем, я хотела взять в прокате \"Куджо\".", 230 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124022, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124022, 902 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124022, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124022, 1082 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124022, 1141 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124022, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124022, 40500 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124022, 44397 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 230   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 931889, 934349, "Ну давай сегодня.", 231 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124023, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124023, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124023, 749 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 231   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 934516, 938102, "- А разве у тебя сегодня не свидание?\r\n- О, точно.", 232 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 2177 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 749 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 685 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124024, 2128 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 232   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 938270, 941940, "- Эй, Джоуи, можно у тебя кое-что спросить?\r\n- Ага.", 233 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 854 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 2876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 1457 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124025, 2880 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 233   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 942107, 944859, "После нашего свидания ты ничего\r\nстранного не почувствовал?", 234 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124026, 1824 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124026, 3662 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124026, 693 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124026, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124026, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124026, 15988 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124026, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124026, 5140 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 234   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 945027, 949781, "Боже мой! Ты тоже!\r\nЭто меня так напугало. Что это было?", 235 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 30092 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124027, 742 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 235   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 949949, 951950, "Не знаю.", 236 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124028, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124028, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 236   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 952242, 955453, "У меня есть ощущение, что это лобстер.", 237 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124029, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124029, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124029, 671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124029, 21946 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124029, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124029, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124029, 17256 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 237   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 958749, 961542, "О да. Да, это лобстер.", 238 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124030, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124030, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124030, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124030, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124030, 17256 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 238   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 961710, 965338, "- Мне всю ночь было плохо.\r\n- Да, мне тоже. Всю ночь.", 239 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 3584 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 742 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 2263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 1071 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 942 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124031, 3584 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 239   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 965506, 968049, "Правда? Мы не пересекались.", 240 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124032, 780 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124032, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124032, 672 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "пересекались" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124032, 44402 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 240   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 968217, 970677, "Ну да, потому что...", 241 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124033, 642 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124033, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124033, 1305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124033, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 241   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 971679, 972845, "...я не выходил из комнаты.", 242 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124034, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124034, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124034, 11461 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124034, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124034, 22974 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 242   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 975015, 977475, "Да, не стоит заглядывать\r\nв мою корзину для белья.", 243 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 4096 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "заглядывать" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 44403 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 760 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 4318 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 768 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124035, 13415 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 243   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 982815, 983982, "ОК, всё под контролем.", 244 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124036, 13609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124036, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124036, 5782 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124036, 29941 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 244   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 984149, 986985, "Здесь я ухожу. Возвращаюсь сюда.", 245 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124037, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124037, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124037, 8078 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124037, 29943 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124037, 1357 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 245   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 987152, 989570, "Ладно. Вперёд, Ms. Pac-Man.", 246 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124038, 1037 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124038, 3741 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124038, 44362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124038, 44364 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124038, 42715 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 246   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 991865, 994242, "А ты просто маленькая сучка, не так ли?", 247 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124039, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124039, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124039, 646 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124039, 4385 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124039, 3740 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124039, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124039, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124039, 1234 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 247   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 997579, 999747, "Привет ребята?", 248 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124040, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124040, 843 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 248   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 999915, 1003710, "Мне жаль, что я так на вас\r\nкричала из-за игры.", 249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 695 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 2965 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 1044 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 15267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 1108 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124041, 3045 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 249   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1003877, 1009007, "О Боже мой. Как вашим друзьям\r\nне повезло с инициалами...", 250 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 19732 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 5163 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 4380 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124042, 19264 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 250   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1010259, 1012343, "Это Чендлер.", 251 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124043, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124043, 2148 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 251   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1012511, 1016014, "- Чендлер не смог бы играть так хорошо.\r\n- Я смог.", 252 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 2148 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 4906 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 3572 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 833 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124044, 4906 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 252   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1016181, 1018641, "Но вот цена!", 253 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124045, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124045, 694 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "цена" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124045, 44404 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 253   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1019852, 1022228, "Бен скоро придёт, он не должен этого увидеть.", 254 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124046, 12546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124046, 2625 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124046, 5881 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124046, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124046, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124046, 1123 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124046, 1220 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124046, 2306 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 254   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1022396, 1025898, "Да к семи годам дети уже оргии видели.", 255 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124047, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124047, 831 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124047, 7941 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124047, 40870 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124047, 6882 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124047, 990 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "оргии" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124047, 44405 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124047, 1947 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 255   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1029820, 1031571, "Это только я?", 256 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124048, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124048, 757 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124048, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 256   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1041248, 1043916, "Да, я вовремя!", 257 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124049, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124049, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124049, 1275 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 257   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1049757, 1052341, "ОК, почему бы нам всем...", 258 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124050, 13609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124050, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124050, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124050, 1083 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124050, 1444 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 258   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1054887, 1057680, "...не открыть учебники на странице 23...", 259 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124051, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124051, 8749 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "учебники" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124051, 44406 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124051, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124051, 7671 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124051, 12383 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 259   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1057931, 1059348, "...где...", 260 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124052, 875 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 260   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1060768, 1063936, "...мы видим...", 261 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124053, 647 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124053, 4796 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 261   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1065606, 1069692, "...множество красных точек.", 262 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124054, 32612 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "красных" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124054, 44407 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "точек" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124054, 44408 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 262   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1071070, 1075990, "ОК, почему бы вам не начать читать,\r\nпока я...", 263 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 13609 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 936 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 4791 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 12152 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 1859 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124055, 674 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 263   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1085334, 1088669, "Ты в порядке? Ты какой-то рассеянный.", 264 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124056, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124056, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124056, 766 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124056, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124056, 657 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124056, 835 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "рассеянный" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124056, 44409 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 264   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1088837, 1092340, "Нет, я в порядке. Просто...", 265 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124057, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124057, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124057, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124057, 766 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124057, 646 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 265   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1093383, 1095843, "Эй, можно у тебя кое-что спросить?", 266 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124058, 2275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124058, 734 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124058, 710 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124058, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124058, 2876 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124058, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124058, 1457 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 266   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1096095, 1099055, "Ты никогда не смотрела на кого-то знакомого...", 267 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124059, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124059, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124059, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124059, 16950 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124059, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124059, 918 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124059, 835 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "знакомого" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124059, 44410 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 267   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1099223, 1102517, "...с абсолютно новой стороны?", 268 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124060, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124060, 6362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124060, 4215 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124060, 2057 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 268   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1102976, 1104769, "Типа сзади?", 269 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124061, 1809 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124061, 11013 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 269   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1109525, 1112693, "Ага. Именно так.", 270 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124062, 2880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124062, 1805 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124062, 679 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 270   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1112861, 1113986, "Знаешь что?", 271 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124063, 737 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124063, 643 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 271   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1114154, 1117490, "Я однажды увидела одного парня сзади,\r\nи он казался нормальным.", 272 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 948 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 7105 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 6953 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 2752 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 11013 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 16895 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124064, 14079 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 272   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1117658, 1120118, "А потом он повернулся\r\nи оказался Стивеном Болдуином.", 273 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124065, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124065, 707 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124065, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124065, 24941 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124065, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124065, 1360 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "стивеном" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124065, 44411 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "болдуином" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124065, 44412 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 273   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1121537, 1126290, "Ага. Так что я тебя так понимаю.", 274 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124066, 2880 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124066, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124066, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124066, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124066, 668 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124066, 679 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124066, 799 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 274   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1126458, 1128918, "Абсолютно. Вау.", 275 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124067, 6362 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124067, 35495 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 275   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1131380, 1133840, "- Ты меня извинишь?\r\n- Конечно.", 276 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124068, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124068, 711 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124068, 33917 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124068, 775 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 276   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1145936, 1147812, "- Фиби, ты жжёшь!\r\n- Знаю.", 277 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124069, 853 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124069, 654 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "жжёшь" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124069, 44413 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124069, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 277   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1149189, 1150648, "- Ты сможешь!\r\n- Не трогай меня!", 278 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124070, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124070, 4297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124070, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124070, 1952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124070, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 278   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1150816, 1152150, "Не трогай её!", 279 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124071, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124071, 1952 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124071, 926 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 279   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1152317, 1154694, "Налево, направо, направо...", 280 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124072, 18631 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124072, 18630 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124072, 18630 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 280   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1154862, 1156028, "Я не могу, я не могу...", 281 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124073, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124073, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124073, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124073, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124073, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124073, 930 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 281   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1157406, 1159991, "Нет! Ах ты су...", 282 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124074, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124074, 4118 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124074, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124074, 43309 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 282   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1173255, 1174589, "Фиби?", 283 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124075, 853 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 283   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1175549, 1178176, "Ой, привет, Бен. Нет, не смотри на автомат!", 284 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124076, 1217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124076, 729 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124076, 12546 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124076, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124076, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124076, 1439 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124076, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124076, 11279 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 284   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1188270, 1190646, "Слава Богу, ты дома.", 285 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124077, 828 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124077, 829 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124077, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124077, 1010 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 285   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1190814, 1193107, "- Я смотрю \"Куджо\".\r\n- Одна?", 286 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124078, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124078, 1440 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124078, 44397 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124078, 846 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 286   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1193275, 1197612, "- Да. Что такое с этой собакой?\r\n- Ты уже видела эпизод...", 287 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 652 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 715 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 5026 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 20316 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 990 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 901 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124079, 5073 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 287   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1197779, 1201199, "...где Куджо бросается на машину?", 288 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124080, 875 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124080, 44397 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "бросается" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124080, 44414 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124080, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124080, 4274 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 288   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1201366, 1204994, "Нет, серьёзно, что с этой собакой?", 289 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124081, 691 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124081, 774 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124081, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124081, 648 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124081, 5026 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124081, 20316 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 289   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1205621, 1208039, "Стоп, а как же твоё свидание?", 290 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124082, 3003 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124082, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124082, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124082, 778 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124082, 4249 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124082, 685 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 290   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1208207, 1210583, "Ой, ничего не получилось.", 291 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124083, 1217 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124083, 1214 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124083, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124083, 2118 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 291   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1210751, 1213169, "Хочешь досмотрим?", 292 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124084, 1035 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "досмотрим" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124084, 44415 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 292   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1213337, 1215796, "Давай, ага.", 293 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124085, 1427 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124085, 2880 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 293   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1215964, 1218299, "Никогда не думала, что скажу это...", 294 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124086, 727 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124086, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124086, 1453 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124086, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124086, 2072 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124086, 683 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 294   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1218467, 1220593, "...но надеюсь, эта собака умрёт.", 295 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124087, 803 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124087, 855 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124087, 2966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124087, 8793 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124087, 12795 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 295   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1223013, 1225765, "Что ты делаешь? Иди сюда и защищай меня!", 296 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124088, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124088, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124088, 2721 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124088, 1034 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124088, 1357 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124088, 688 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "защищай" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124088, 44416 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124088, 711 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 296   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1225933, 1229101, "О, точно. Действительно. Почему нет?", 297 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124089, 825 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124089, 2128 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124089, 1881 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124089, 661 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124089, 691 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 297   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1233607, 1235191, "ОК.", 298 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124090, 13609 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 298   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1239821, 1243658, "- Это он, это он! Это Куджо.\r\n- Знаю.", 299 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124091, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124091, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124091, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124091, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124091, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124091, 44397 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124091, 1093 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 299   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1244785, 1247286, "Всё будет хорошо.", 300 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124092, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124092, 966 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124092, 833 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 300   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1247537, 1251582, "Боже мой! Что он теперь делает?\r\nЯ не могу на это смотреть.", 301 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 860 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 891 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 667 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 810 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 994 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 930 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 784 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124093, 5508 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 301   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1254586, 1257880, "Как ты можешь это смотреть?\r\nТебе не страшно?", 302 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124094, 787 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124094, 654 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124094, 1201 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124094, 683 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124094, 5508 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124094, 753 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124094, 672 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124094, 2273 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 302   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1261969, 1264011, "Я в ужасе.", 303 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124095, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124095, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124095, 40744 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 303   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1274481, 1279360, "Итак, все здесь?\r\nЛично я приехал немного раньше.", 304 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124096, 4438 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124096, 681 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124096, 827 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124096, 3337 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124096, 674 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124096, 13586 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124096, 1119 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124096, 900 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 304   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1279528, 1280528, "Давайте начнём.", 305 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124097, 2808 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124097, 7601 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 305   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1280696, 1286909, "Итак, гадрозавры были обнаружены\r\nв двух основных регионах.", 306 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124098, 4438 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гадрозавры" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124098, 44417 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124098, 876 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "обнаружены" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124098, 44418 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124098, 698 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124098, 5123 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "основных" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124098, 44419 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "регионах" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124098, 44420 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 306   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1289288, 1290871, "Здесь...", 307 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124099, 827 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 307   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1291540, 1294834, "...и здесь.", 308 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124100, 688 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124100, 827 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 308   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `phrase` ( `phrase`.`language_id`, `phrase`.`subtitle_id`, `phrase`.`start`, `phrase`.`stop`, `phrase`.`phrase`, `phrase`.`order_number` ) VALUES ( 2, 364, 1295752, 1298713, "А что касается гидрозавров...", 309 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124101, 666 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124101, 643 );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124101, 24473 );
# a query
INSERT INTO `word` ( `word`.`language_id`, `word`.`word` ) VALUES ( 2, "гидрозавров" );
# a query
INSERT INTO `phrase_word` ( `phrase_word`.`phrase_id`, `phrase_word`.`word_id` ) VALUES ( 124101, 44421 );
# a query
UPDATE `subtitle` SET   `subtitle`.`source_id` = 182 ,  `subtitle`.`language_id` = 2 ,  `subtitle`.`file` = "Friends/Сезон 08/s08e12 - The One Where Joey Dates Rachel/Russian.srt" ,  `subtitle`.`phrase_number` = 309   WHERE  `subtitle`.`id` = 364 ;
# a query
INSERT INTO `video_subtitle` ( `video_subtitle`.`video_id`, `video_subtitle`.`subtitle_id` ) VALUES ( 182, 364 );