CREATE TABLE IF NOT EXISTS `audio` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `source_id` INT,
    `language_id` INT,
    `file` VARCHAR(255),
    `duration` INT,
    INDEX `source_id_index` (`source_id`),
    INDEX `language_id_index` (`language_id`),
    INDEX `file_index` (`file`)
);

