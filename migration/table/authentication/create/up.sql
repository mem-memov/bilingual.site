CREATE TABLE IF NOT EXISTS `authentication` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `email` VARCHAR(255) UNIQUE,
    `password` CHAR(60),
    `restore_date` DATETIME,
    `restore_hash` CHAR(60),
    `confirm_hash` CHAR(60),
    `registration_date` DATETIME
);

