CREATE TABLE IF NOT EXISTS `clip` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `language_id` INT,
    `phrase_id` INT,
    `phrase_delay` INT,
    `start` INT,
    `stop` INT,
    `duration` INT,
    `file` VARCHAR(255),
    INDEX `language_id_index` (`language_id`),
    INDEX `phrase_id_index` (`phrase_id`),
    INDEX `file_index` (`file`)
);

