CREATE TABLE IF NOT EXISTS `foreign_clip` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `foreign_id` INT,
    `clip_id` INT,
    INDEX `foreign_id_index` (`foreign_id`),
    INDEX `clip_id_index` (`clip_id`)
);

