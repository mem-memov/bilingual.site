CREATE TABLE IF NOT EXISTS `language` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `language` VARCHAR(255),
    INDEX `language_index` (`language`)
);

