CREATE TABLE IF NOT EXISTS `original` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `language_id` INT,
    `video_id` INT,
    `audio_id` INT,
    `subtitle_id` INT,
    `clip_number` INT,
    INDEX `language_id_index` (`language_id`),
    INDEX `video_id_index` (`video_id`),
    INDEX `audio_id_index` (`audio_id`),
    INDEX `subtitle_id_index` (`subtitle_id`)
);

