CREATE TABLE IF NOT EXISTS `original_clip` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `original_id` INT,
    `clip_id` INT,
    INDEX `original_id_index` (`original_id`),
    INDEX `clip_id_index` (`clip_id`)
);

