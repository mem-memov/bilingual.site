CREATE TABLE IF NOT EXISTS `phrase` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `language_id` INT,
    `subtitle_id` INT,
    `start` INT,
    `stop` INT,
    `phrase` VARCHAR(511),
    `order_number` INT,
    INDEX `language_id_index` (`language_id`),
    INDEX `subtitle_id_index` (`subtitle_id`)
);

