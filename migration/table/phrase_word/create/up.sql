CREATE TABLE IF NOT EXISTS `phrase_word` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `phrase_id` INT,
    `word_id` INT,
    INDEX `phrase_id_index` (`phrase_id`),
    INDEX `word_id_index` (`word_id`)
);

