CREATE TABLE IF NOT EXISTS `source` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `file` VARCHAR(255),
    INDEX `file_index` (`file`)
);

