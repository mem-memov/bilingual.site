CREATE TABLE IF NOT EXISTS `subtitle` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `source_id` INT,
    `language_id` INT,
    `file` VARCHAR(255),
    `phrase_number` INT,
    INDEX `source_id_index` (`source_id`),
    INDEX `language_id_index` (`language_id`),
    INDEX `file_index` (`file`)
);

