CREATE TABLE IF NOT EXISTS `video_audio` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `video_id` INT,
    `audio_id` INT,
    INDEX `video_id_index` (`video_id`),
    INDEX `audio_id_index` (`audio_id`)
);

