CREATE TABLE IF NOT EXISTS `video_subtitle` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `video_id` INT,
    `subtitle_id` INT,
    INDEX `video_id_index` (`video_id`),
    INDEX `subtitle_id_index` (`subtitle_id`)
);

