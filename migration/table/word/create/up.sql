CREATE TABLE IF NOT EXISTS `word` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `language_id` INT,
    `word` VARCHAR(255),
    INDEX `language_id_index` (`language_id`),
    INDEX `word_index` (`word`)
);

