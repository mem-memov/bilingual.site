<?php
class Factory implements IFactory
{
    private $configuration;

    private $core;
    private $controller;
    private $mailer;
    
    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
        $this->core = null;
        $this->controller = null;
        $this->mailer = null;
    }

    public function core(): core\IFactory
    {
        if (is_null($this->core)) {
            $this->core = new core\Factory($this->configuration);
        }

        return $this->core;
    }
    
    public function controller(): controller\IFactory
    {
        if (is_null($this->controller)) {

            $page = new controller\PageFactory(
                $this->core()->role()
            );

            $result = new controller\ResultFactory(
                $page
            );

            $this->controller = new controller\Factory(
                $this->core()->role(),
                $result
            );
        }

        return $this->controller;
    }
    
    public function mailer(): mailer\IFacade
    {
        if (is_null($this->mailer)) {

            $extractor = new mailer\configuration\Extractor();
            $configuration = new mailer\configuration\Factory($extractor, $this->configuration);
            $mailer = new mailer\Mailer($configuration->mailer());
            $templates = new mailer\TemplateFactory();
            $mails = new mailer\MailFactory($mailer, $templates);
            $services = new mailer\service\Factory($configuration);
            $task = $services->task();

            $this->mailer = new mailer\Facade($mails, $task);
        }

        return $this->mailer;
    }
}

