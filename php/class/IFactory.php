<?php
interface IFactory
{
    public function core(): core\IFactory;
    public function controller(): controller\IFactory;
    public function mailer(): mailer\IFacade;
}

