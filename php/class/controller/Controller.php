<?php
namespace controller;
use core\role\IFactory as IRoleFactory;
abstract class Controller implements IController
{
    protected $roleFactory;
    protected $result;
    
    public function __construct(
        IRoleFactory $roleFactory,
        IResultFactory $result
    ) {
        $this->roleFactory = $roleFactory;
        $this->result = $result;
    }
}