<?php
namespace controller;
use core\role\IFactory as IRoleFactory;
class Factory implements IFactory
{
    private $role;
    private $result;
    private $user;
    private $developer;
    private $cutter;
    private $visitor;
    
    public function __construct(
        IRoleFactory $role,
        IResultFactory $result
    ) {
        $this->role = $role;
        $this->result = $result;
        $this->user = null;
        $this->developer = null;
        $this->cutter = null;
        $this->visitor = null;
    }

    public function index(): IController
    {
        return new Index(
            $this->role, 
            $this->result
        );
    }
    
    public function developer(): developer\IFactory
    {
        if (is_null($this->developer)) {
            $this->developer = new developer\Factory(
                $this->role,
                $this->result
            );
        }
        
        session_start();
        return $this->developer;
    }
    
    public function user(): user\IFactory
    {
        if (is_null($this->user)) {
            $this->user = new user\Factory(
                $this->role,
                $this->result
            );
        }

        return $this->user;
    }
    
    public function cutter(): cutter\IFactory
    {
        if (is_null($this->cutter)) {
            $this->cutter = new cutter\Factory(
                $this->role,
                $this->result
            );
        }
        
        session_start();
        return $this->cutter;
    }
    
    public function visitor(): visitor\IFactory
    {
        if (is_null($this->visitor)) {
            $this->visitor = new visitor\Factory(
                $this->role,
                $this->result
            );
        }
        
        return $this->visitor;
    }
}

