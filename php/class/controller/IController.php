<?php
namespace controller;
interface IController
{
    public function run(): IResult;
}

