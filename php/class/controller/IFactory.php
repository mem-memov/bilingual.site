<?php
namespace controller;
interface IFactory
{
    public function index(): IController;
    public function developer(): developer\IFactory;
    public function user(): user\IFactory;
    public function cutter(): cutter\IFactory;
    public function visitor(): visitor\IFactory;
}

