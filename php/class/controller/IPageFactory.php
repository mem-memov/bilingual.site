<?php
namespace controller;
interface IPageFactory
{
    public function setResult(IResultFactory $result): IPageFactory;
    public function user(IResult $result): IPage;
    public function visitor(IResult $result): IPage;
}
