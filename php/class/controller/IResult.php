<?php
namespace controller;
interface IResult
{
    public function headers(): array;
    public function content(): string;
    public function send();
}
