<?php
namespace controller;
interface IResultFactory
{
    public function html(string $html): IResult;
    public function ajax(array $data): IResult;
    public function template(string $template, array $data): IResult;
    public function redirect(string $url): IResult;
    public function page(): IPageFactory;
    public function console(array $lines): IResult;
}

