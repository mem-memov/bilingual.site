<?php
namespace controller;
class Index extends Controller
{
    public function run(): IResult
    {
        $data = [
            [
                'title' => 'developer',
                'url' => 'developer/'
            ],
            [
                'title' => 'cutter',
                'url' => 'cutter/'
            ]
        ];
        
        return $this->result->template('index_list', $data);
    }
}

