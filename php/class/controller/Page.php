<?php
namespace controller;
use core\role\IFactory as IRoleFactory;
abstract class Page implements IPage
{
    protected $role;
    protected $result;
    protected $controllerResult;
    
    public function __construct(
        IRoleFactory $role,
        IResultFactory $result,
        IResult $controllerResult
    ) {
        $this->role = $role;
        $this->result = $result;
        $this->controllerResult = $controllerResult;
    }
}