<?php
namespace controller;
use core\role\IFactory as IRoleFactory;
class PageFactory implements IPageFactory
{
    private $role;
    private $result;
    
    public function __construct(
        IRoleFactory $role
    ) {
        $this->role = $role;
    }
    
    public function setResult(IResultFactory $result): IPageFactory
    {
        $this->result = $result;
        
        return $this;
    }
    
    public function user(IResult $result): IPage
    {
        return new user\Page(
            $this->role,
            $this->result,
            $result
        );
    }
        
    public function visitor(IResult $result): IPage
    {
        return new visitor\Page(
            $this->role,
            $this->result,
            $result
        );
    }    

}
