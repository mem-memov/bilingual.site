<?php
namespace controller;
class Result implements IResult
{
    private $headers;
    private $content;
    
    public function __construct(array $headers, string $content)
    {
        $this->headers = $headers;
        $this->content = $content;
    }
    
    public function headers(): array
    {
        return $this->headers;
    }
    
    public function content(): string
    {
        return $this->content;
    }
    
    public function send()
    {
        foreach ($this->headers as $header) {
            header($header, false);
        }
        
        echo $this->content;
    }
}

