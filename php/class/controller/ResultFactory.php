<?php
namespace controller;
class ResultFactory implements IResultFactory
{
    private $page;
    
    public function __construct(
        IPageFactory $page
    ) {
        $this->page = $page->setResult($this);
    }
    
    public function html(string $html): IResult
    {
        $headers = [];
        return new Result($headers, $html);
    }
    
    public function ajax(array $data): IResult
    {
        $headers = [];
        $json = json_encode($data);
        return new Result($headers, $json);
    }
    
    public function template(string $template, array $data): IResult
    {
        $path = __DIR__ . '/../../template/' . $template . '.php';
        
        if (!file_exists($path)) {
            throw new exception\TemplateNotFound($path);
        }
        
        ob_start();
        require $path;
        $content = ob_get_clean();
        
        // prevent browser from caching
        $headers = [
            'Expires: Tue, 03 Jul 2001 06:00:00 GMT',
            'Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT',
            'Cache-Control: no-store, no-cache, must-revalidate, max-age=0',
            'Cache-Control: post-check=0, pre-check=0',
            'Pragma: no-cache'
        ];
        
        return new Result($headers, $content);
    }

    public function redirect(string $url): IResult
    {
        $headers = [
            'Location: ' . $url
        ];
        
        $content = '';
        
        return new Result($headers, $content);
    }
    
    public function page(): IPageFactory
    {
        return $this->page;
    }

    public function console(array $lines): IResult
    {
        $content = implode("\n", $lines) . "\n";

        return new Result([], $content);
    }
}