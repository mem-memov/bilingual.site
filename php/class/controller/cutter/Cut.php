<?php
namespace controller\cutter;
use controller\{Controller, IResult};
class Cut extends Controller
{
    public function run(): IResult
    {

        $data = $this->roleFactory->cutter()->media()->cut()->perform();

        $lines = [
            'video sources are cut'
        ];

        return $this->result->console($lines);
    }
}
