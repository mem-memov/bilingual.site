<?php
namespace controller\cutter;
use core\role\IFactory as IRoleFactory;
use controller\{IResult, IResultFactory, IController};
class Factory implements IFactory
{
    private $roleFactory;
    private $result;
    
    public function __construct(
        IRoleFactory $roleFactory,
        IResultFactory $result  
    ) {
        $this->roleFactory = $roleFactory;
        $this->result = $result;
    }

    public function cut(): IController
    {
        return new Cut(
            $this->roleFactory,
            $this->result
        );
    }

    public function import(): IController
    {
        return new Import(
            $this->roleFactory,
            $this->result
        );
    }

    public function listSourceFiles(): IController
    {
        return new ListSourceFiles(
            $this->roleFactory,
            $this->result 
        );
    }
}
