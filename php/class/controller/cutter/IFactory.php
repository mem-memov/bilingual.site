<?php
namespace controller\cutter;
use controller\{IResult, IController};
interface IFactory
{
    public function cut(): IController;
    public function import(): IController;
    public function listSourceFiles(): IController;
}
