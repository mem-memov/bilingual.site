<?php
namespace controller\cutter;
use controller\{Controller, IResult};
class Import extends Controller
{
    public function run(): IResult
    {
        $data = $this->roleFactory->cutter()->media()->import()->perform();

        if (isset($data['error'])) {
            $lines = [
                'Error',
                'Type: ' . $data['error']['type'],
                'Message: ' . $data['error']['message']
            ];
        } else {
            $lines = [
                'video source is imported'
            ];
        }

        return $this->result->console($lines);
    }
}
