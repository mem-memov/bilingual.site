<?php
namespace controller\cutter;
use controller\{Controller, IResult};
class Index extends Controller
{
    public function run(): IResult
    {
        $data = [
            [
                'title' => 'cut',
                'url' => 'cut/'
            ],
            [
                'title' => 'list source files',
                'url' => 'list_source_files/'
            ]
        ];
        
        return $this->result->template('index_list', $data);
    }
}
