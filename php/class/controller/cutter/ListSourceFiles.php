<?php
namespace controller\cutter;
use controller\{Controller, IResult};
class ListSourceFiles extends Controller
{
    public function run(): IResult
    {
        $data = $this->roleFactory->cutter()->media()->listSourceFiles()
                ->perform();

        return $this->result->template('cutter/listSourceFiles', $data);
    }
}
