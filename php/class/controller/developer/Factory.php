<?php
namespace controller\developer;
use core\role\IFactory as IRoleFactory;
use controller\{IResult, IResultFactory};
class Factory implements IFactory
{
    private $roleFactory;
    private $result;
    private $databaseFactory;
    
    public function __construct(
        IRoleFactory $roleFactory,
        IResultFactory $result  
    ) {
        $this->roleFactory = $roleFactory;
        $this->result = $result;
        $this->databaseFactory = null;
    }

    public function database(): database\IFactory
    {
        if (is_null($this->databaseFactory)) {
            $this->databaseFactory = new database\Factory(
                $this->roleFactory,
                $this->result 
            );
        }
        
        return $this->databaseFactory;
    }
}
