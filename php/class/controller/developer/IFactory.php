<?php
namespace controller\developer;
use controller\IResult;
interface IFactory
{
    public function database(): database\IFactory;
}
