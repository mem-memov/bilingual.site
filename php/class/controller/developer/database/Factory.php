<?php
namespace controller\developer\database;
use core\role\IFactory as IRoleFactory;
use controller\{IResult, IResultFactory, IController};
class Factory implements IFactory
{
    private $roles;
    private $results;
    private $migrations;
    
    public function __construct(
        IRoleFactory $roles,
        IResultFactory $results
    ) {
        $this->roles = $roles;
        $this->results = $results;
        $this->migrations = null;
    }

    public function migration(): migration\IFactory
    {
        if (is_null($this->migrations)) {
            $this->migrations = new migration\Factory(
                $this->roles,
                $this->results
            );
        }

        return $this->migrations;
    }
    
    public function schema(): IController
    {
        return new Schema(
            $this->roles,
            $this->results
        );
    }
}
