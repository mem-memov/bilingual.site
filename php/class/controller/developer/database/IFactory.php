<?php
namespace controller\developer\database;
use controller\{IResult, IController};
interface IFactory
{
    public function migration(): migration\IFactory;
    public function schema(): IController;
}
