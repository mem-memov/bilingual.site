<?php
namespace controller\developer\database;
use controller\{Controller, IResult};
class Schema extends Controller
{
    public function run(): IResult
    {
        $data = $this->roleFactory->developer()->database()->schema()->view()->perform();

        $lines = [];

        foreach ($data as $table => $fields) {
            $lines[] = $table . ':';
            foreach ($fields as $field => $status) {
                $lines[] = '    ' . $status . ' ' . $field;
            }
        }

        return $this->result->console($lines);
    }
}
