<?php
namespace controller\developer\database\migration;
use controller\{Controller, IResult};
class Apply extends Controller
{
    public function run(): IResult
    {
        if (($_SERVER['argc'] != 2)) {
            $lines = [
                'Migration not specified.',
                'php listAll.php'
            ];
            return $this->result->console($lines);
        }

        $migration = $_SERVER['argv'][1];

        $data = $this->roleFactory->developer()->database()->migration()->apply()
                                  ->setTargetMigration($migration)
                                  ->perform();

        if (isset($data['error'])) {
            $lines = ['Error:'];
            if ($data['error']['type'] == 'MigrationsNotUnique') {
                $lines[] = 'Migration list contains repeating names: ' . $data['error']['message'];
            }
            return $this->result->console($lines);
        }

        $lines = ['Queries: '];
        $lines += $data['queries'];
        $lines[] = 'Applied migration: ' . $migration;

        return $this->result->console($lines);
    }
}
