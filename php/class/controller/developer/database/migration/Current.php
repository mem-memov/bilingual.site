<?php
namespace controller\developer\database\migration;
use controller\{Controller, IResult};
use core\database\migration\exception\MigrationsNotUnique;
class Current extends Controller
{
    public function run(): IResult
    {
        $data = $this->roleFactory->developer()->database()->migration()->current()->perform();

        $lines = [];
        $lines[] = 'Current:';
        $lines[] = $data['current'];

        return $this->result->console($lines);
    }
}
