<?php
namespace controller\developer\database\migration;
use core\role\IFactory as IRoleFactory;
use controller\{IResult, IResultFactory, IController};
class Factory implements IFactory
{
    private $roleFactory;
    private $result;
    
    public function __construct(
        IRoleFactory $roleFactory,
        IResultFactory $result  
    ) {
        $this->roleFactory = $roleFactory;
        $this->result = $result;
    }

    public function listAll(): IController
    {
        return new ListAll(
            $this->roleFactory,
            $this->result
        );
    }

    public function current(): IController
    {
        return new Current(
            $this->roleFactory,
            $this->result
        );
    }

    public function apply(): IController
    {
        return new Apply(
            $this->roleFactory,
            $this->result
        );
    }

    public function show(): IController
    {
        return new Show(
            $this->roleFactory,
            $this->result
        );
    }
}
