<?php
namespace controller\developer\database\migration;
use controller\{IResult, IController};
interface IFactory
{
    public function listAll(): IController;
    public function current(): IController;
    public function apply(): IController;
    public function show(): IController;
}
