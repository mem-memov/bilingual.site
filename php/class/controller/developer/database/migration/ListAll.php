<?php
namespace controller\developer\database\migration;
use controller\{Controller, IResult};
class ListAll extends Controller
{
    public function run(): IResult
    {
        $data = $this->roleFactory->developer()->database()->migration()->listAll()->perform();

        $lines = [];
        $lines[] = 'Migrations:';
        foreach ($data['migrations'] as $migration) {
            $lines[] = $migration;
        }

        return $this->result->console($lines);
    }
}
