<?php
namespace controller\developer\database\migration;
use controller\{Controller, IResult};
use core\database\migration\exception\MigrationsNotUnique;
class Show extends Controller
{
    public function run(): IResult
    {
        if (($_SERVER['argc'] != 2)) {
            $lines = [
                'Migration not specified.',
                'php listAll.php'
            ];
            return $this->result->console($lines);
        }

        $migrationKey = $_SERVER['argv'][1];
        
        $data = $this->roleFactory->developer()->database()->migration()
            ->show()
            ->setMigrationKey($migrationKey)
            ->perform();

        $lines = [];
        $lines[] = 'Migration: ' . $data['migration']['name'];
        $lines[] = 'up ' . $data['migration']['path']['up'];
        $lines[] = $data['migration']['query']['up'];
        $lines[] = 'down ' . $data['migration']['path']['down'];
        $lines[] = $data['migration']['query']['down'];

        return $this->result->console($lines);
    }
}
