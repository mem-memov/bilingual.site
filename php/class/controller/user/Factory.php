<?php
namespace controller\user;
use core\role\IFactory as IRoleFactory;
use controller\{IResult, IResultFactory};
class Factory implements IFactory
{
    private $roleFactory;
    private $result;
    private $authenticationFactory;
    
    public function __construct(
        IRoleFactory $roleFactory,
        IResultFactory $result  
    ) {
        $this->roleFactory = $roleFactory;
        $this->result = $result;
        $this->authenticationFactory = null;
    }
    
    public function run(): IResult
    {
        $controller = new Index(
            $this->roleFactory,
            $this->result
        );
        
        session_start();
        return $controller->run();
    }
    
    public function authentication(): authentication\IFactory
    {
        if (is_null($this->authenticationFactory)) {
            $this->authenticationFactory = new authentication\Factory(
                $this->roleFactory,
                $this->result 
            );
        }
        
        return $this->authenticationFactory;
    }
}
