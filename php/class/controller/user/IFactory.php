<?php
namespace controller\user;
use controller\IResult;
interface IFactory
{
    public function run(): IResult;
    public function authentication(): authentication\IFactory;
}
