<?php
namespace controller\user;
use controller\{Controller, IResult};
class Index extends Controller
{
    public function run(): IResult
    {
        if (!$_SESSION['user/id']) {
            return $this->result->redirect('/user/authentication/login/');
        }
        
        $data = [];
        
        $result = $this->result->template('user/menu', $data);
        
        return $this->result->page()->user($result)->run();
    }
}
