<?php
namespace controller\user;
use controller\{Page, IResult};
class Page extends Page
{
    public function run(): IResult
    {
        $data = [
            'content' => $this->controllerResult->content(),
            'email' => $_SESSION['user/email']
        ];
     
        return $this->result->template('user/page', $data);
    }
}
