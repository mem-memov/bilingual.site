<?php
namespace controller\user\authentication;
use core\role\IFactory as IRoleFactory;
use controller\{IResult, IResultFactory, IController};
class Factory implements IFactory
{
    private $roleFactory;
    private $result;
    
    public function __construct(
        IRoleFactory $roleFactory,
        IResultFactory $result  
    ) {
        $this->roleFactory = $roleFactory;
        $this->result = $result;
    }
    
    public function run(): IResult
    {
        $controller = new Index(
            $this->roleFactory,
            $this->result
        );
        return $controller->run();
    }

    public function login(): IController
    {
        return new Login(
            $this->roleFactory,
            $this->result
        );
    }

    public function logout(): IController
    {
        return new Logout(
            $this->roleFactory,
            $this->result
        );
    }

    public function password(): IController
    {
        return new Password(
            $this->roleFactory,
            $this->result
        );
    }
}
