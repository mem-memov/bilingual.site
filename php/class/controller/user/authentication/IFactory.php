<?php
namespace controller\user\authentication;
use controller\{IResult, IController};
interface IFactory
{
    public function run(): IResult;
    public function login(): IController;
    public function logout(): IController;
    public function password(): IController;
}
