<?php
namespace controller\user\authentication;
use controller\{Controller, IResult};
class Login extends Controller
{
    public function run(): IResult
    {
        $data = [
            'title' => 'Вход на сайт',
            'passwordHelp' => 'Не короче 6 и не длиннее 72 символов. Без повторяющихся подряд символов и пробелов.',
            'email' => isset($_REQUEST['email']) ? $_REQUEST['email'] : '',
            'errors' => [
                'email' => [],
                'password' => []
            ]
        ];

        if (isset($_REQUEST['submit'])) {
            
            if (!isset($_REQUEST['email']) || empty($_REQUEST['email'])) {
                $data['errors']['email'][] = 'Не указан адрес электронной почты.';
            }

            if (!isset($_REQUEST['password']) || empty($_REQUEST['password'])) {
                $data['errors']['password'][] = 'Не был введён пароль.';
            }
            
            if (empty($data['errors']['email']) && empty($data['errors']['password'])) {

                $action = $this->roleFactory->user()->authentication()->login()
                        ->eMail($_REQUEST['email'])
                        ->password($_REQUEST['password'])
                        ->perform();

                if (!isset($action['error'])) {

                    session_start();
                    $_SESSION['user/id'] = $action['id'];
                    $_SESSION['user/email'] = $action['email'];

                    return $this->result->redirect('/user/');

                } else {

                    $errorMessages = [
                        'EmailNotValid' => 'Неверный формат электронной почты.',
                        'PasswordTooShort' => 'Слишком короткий пароль.',
                        'PasswordTooLong' => 'Слишком длинный пароль',
                        'PasswordContainsRepeatingCharacters' => 'Больше двух повторяющихся символов подряд.',
                        'WrongPassword' => 'Введён неверный пароль. Ссылку для восстановления выслали Вам сейчас на почту.',
                        'PasswordWithWrongCharacters' => 'Пароль содержит недопустимые символы.'
                    ];

                    if ($action['error'] == 'EmailNotValid') {
                        $data['errors']['email'][] = $errorMessages[$action['error']];
                    } else {
                        $data['errors']['password'][] = $errorMessages[$action['error']];
                    }
                }
            }
            
        }

        $result = $this->result->template('user/authentication/login', $data);
        
        return $this->result->page()->visitor($result)->run();
    }
}
