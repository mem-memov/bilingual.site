<?php
namespace controller\user\authentication;
use controller\{Controller, IResult};
class Logout extends Controller
{
    public function run(): IResult
    {
        $_SESSION = [];
        
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        
        session_destroy();

        return $this->result->redirect('/');
    }
}
