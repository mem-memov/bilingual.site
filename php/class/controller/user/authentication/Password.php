<?php
namespace controller\user\authentication;
use controller\{Controller, IResult};
class Password extends Controller
{
    public function run(): IResult
    {
        $data = [
            'title' => 'Изменение пароля',
            'passwordHelp' => 'Не короче 6 и не длиннее 72 символов. Без повторяющихся подряд символов и пробелов.',
            'success' => '',
            'successButton' => 'В меню',
            'errors' => [],
            'loginErrorButton' => 'Повторите вход'
        ];
        
        if (isset($_REQUEST['key'])) {

            $action = $this->roleFactory->user()->authentication()->loginByHash()
                    ->hash($_REQUEST['key'])
                    ->perform();
            
            if (!isset($action['error'])) {

                session_start();
                $_SESSION['user/id'] = $action['id'];
                $_SESSION['user/email'] = $action['email'];

                return $this->result->redirect('/user/authentication/password/');

            }
            
            $messages = [
                'HashNotFound' => 'Ключ для входа уже был использован или неправильный.',
                'StaleHash' => 'Ключ для входа просрочен.'
            ];
            
            $data['loginError'] = $messages[$action['error']];
            
            $result = $this->result->template('user/authentication/password', $data);

            return $this->result->page()->visitor($result)->run();
            
        }
        
        session_start();

        if (isset($_REQUEST['password']) && isset($_REQUEST['password2'])) {
            
            if ($_REQUEST['password'] == $_REQUEST['password2']) {
                
                $action = $this->roleFactory->user()->authentication()->changePassword()
                        ->id($_SESSION['user/id'])
                        ->password($_REQUEST['password'])
                        ->perform();
                
                if (!isset($action['error'])) {
                
                    $data['success'] = 'Пароль был успешно изменён.';
                    
                } else {
                    
                    $errorMessages = [
                        'PasswordTooShort' => 'Слишком короткий пароль.',
                        'PasswordTooLong' => 'Слишком длинный пароль',
                        'PasswordContainsRepeatingCharacters' => 'Больше двух повторяющихся символов подряд.',
                        'WrongPassword' => 'Введён неверный пароль. Ссылку для восстановления выслали Вам сейчас на почту.',
                        'PasswordWithWrongCharacters' => 'Пароль содержит недопустимые символы.'
                    ];

                    $data['errors'][] = $errorMessages[$action['error']];
                    
                }
                
            } else {
                $data['errors'][] = 'Введённые значения не совпадают.';
            }

        }

        $result = $this->result->template('user/authentication/password', $data);
        
        return $this->result->page()->user($result)->run();
    }
}
