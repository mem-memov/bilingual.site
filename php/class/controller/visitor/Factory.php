<?php
namespace controller\visitor;
use core\role\IFactory as IRoleFactory;
use controller\{IResult, IResultFactory};
class Factory implements IFactory
{
    private $role;
    private $result;
    private $lesson;
    
    public function __construct(
        IRoleFactory $role,
        IResultFactory $result  
    ) {
        $this->role = $role;
        $this->result = $result;
        $this->lesson = null;
    }

    public function lesson(): lesson\IFactory
    {
        if (is_null($this->lesson)) {
            $this->lesson = new lesson\Factory(
                $this->role,
                $this->result 
            );
        }
        
        return $this->lesson;
    }
}
