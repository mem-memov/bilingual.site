<?php
namespace controller\visitor;
use controller\{Page, IResult};
class Page extends Page
{
    public function run(): IResult
    {
        $data = [
            'content' => $this->controllerResult->content()
        ];
        
        return $this->result->template('visitor/page', $data);
    }
}
