<?php
namespace controller\visitor\lesson;
use core\role\IFactory as IRoleFactory;
use controller\{IResult, IResultFactory, IController};
class Factory implements IFactory
{
    private $roleFactory;
    private $result;
    
    public function __construct(
        IRoleFactory $roleFactory,
        IResultFactory $result  
    ) {
        $this->roleFactory = $roleFactory;
        $this->result = $result;
    }

    public function teach(): IController
    {
        return new Teach(
            $this->roleFactory,
            $this->result
        );
    }
}
