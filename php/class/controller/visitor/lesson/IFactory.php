<?php
namespace controller\visitor\lesson;
use controller\{IResult, IController};
interface IFactory
{
    public function teach(): IController;
}
