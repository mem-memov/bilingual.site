<?php
namespace controller\visitor\lesson;
use controller\{Controller, IResult};
class Teach extends Controller
{
    public function run(): IResult
    {
        $data = $this->roleFactory->visitor()->lesson()->teach()->perform();

        return $this->result->ajax($data);
    }
}
