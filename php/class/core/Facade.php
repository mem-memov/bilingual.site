<?php
namespace core;
class Facade implements IFacade
{
    private $configuration;
    private $coreFactory;
    
    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
        $this->coreFactory = null;
    }
    
    public function role(): role\IFactory
    {
        if (is_null($this->coreFactory)) {
            $this->coreFactory = new Factory($this->configuration);
        }
        
        return $this->coreFactory->role();
    }
}

