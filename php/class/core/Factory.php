<?php
namespace core;
class Factory implements IFactory
{
    private $config;

    private $role;
    private $database;
    private $service;
    private $configuration;
    private $media;
    private $user;
    
    public function __construct(array $config)
    {
        $this->config = $config;

        $this->role = null;
        $this->database = null;
        $this->service = null;
        $this->configuration = null;
        $this->media = null;
        $this->user = null;
    }
    
    public function role(): role\IFactory
    {
        if (is_null($this->role)) {
            $this->role = new role\Factory($this);
        }

        return $this->role;
    }

    public function database(): database\IFactory
    {
        if (is_null($this->database)) {
            $this->database = new database\Factory(
                $this->configuration()->database()->migrations(),
                $this->service()->database()->query(),
                $this->service()->database()->transaction(),
                $this->service()->database()->renderer()
            );
        }

        return $this->database;
    }
    
    public function service(): service\IFactory
    {
        if (is_null($this->service)) {
            $this->service = new service\Factory(
                $this->configuration()
            );
        }

        return $this->service;
    }
    
    public function configuration(): configuration\IFactory
    {
        if (is_null($this->configuration)) {

            $extractor = new configuration\Extractor();

            $this->configuration = new configuration\Factory(
                $extractor,
                $this->config
            );
        }

        return $this->configuration;
    }
    
    public function media(): media\IFactory
    {
        if (is_null($this->media)) {
            $this->media = new media\Factory(
                $this
            );
        }

        return $this->media;
    }
    
    public function user(): user\IFactory
    {
        if (is_null($this->user)) {
            $this->user = new user\Factory(
                $this
            );
        }

        return $this->user;
    }
}

