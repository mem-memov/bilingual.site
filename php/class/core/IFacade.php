<?php
namespace core;
interface IFacade
{
    public function role(): role\IFactory;
}

