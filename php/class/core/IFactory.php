<?php
namespace core;
interface IFactory
{
    public function role(): role\IFactory;
    public function database(): database\IFactory;
    public function service(): service\IFactory;
    public function configuration(): configuration\IFactory;
    public function media(): media\IFactory;
    public function user(): user\IFactory;
}

