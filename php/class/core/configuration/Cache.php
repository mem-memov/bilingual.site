<?php
namespace core\configuration;
use core\configuration\IExtractor;
class Cache implements ICache
{
    private $extractor;
    private $configuration;
    
    public function __construct(
        IExtractor $extractor,
        array $configuration
    ) {
        $this->extractor = $extractor;
        $this->configuration = $configuration;
    }
    
    public function type(): string
    {
        return $this->extractor->extractStringByKey('type', $this->configuration);
    }
    
    public function servers(): array
    {
        return $this->extractor->extractArrayByKey('servers', $this->configuration);
    }
}
