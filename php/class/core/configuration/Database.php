<?php
namespace core\configuration;
use core\configuration\IExtractor;
class Database implements IDatabase
{
    private $extractor;
    private $configuration;
    
    public function __construct(
        IExtractor $extractor,
        array $configuration
    ) {
        $this->extractor = $extractor;
        $this->configuration = $configuration;
    }
    
    public function type(): string
    {
        return $this->extractor->extractStringByKey('type', $this->configuration);
    }
    
    public function server(): string
    {
        return $this->extractor->extractStringByKey('server', $this->configuration);
    }

    public function port(): string
    {
        return $this->extractor->extractStringByKey('port', $this->configuration);
    }

    public function user(): string
    {
        return $this->extractor->extractStringByKey('user', $this->configuration);
    }

    public function password(): string
    {
        return $this->extractor->extractStringByKey('password', $this->configuration);
    }

    public function database(): string
    {
        return $this->extractor->extractStringByKey('database', $this->configuration);
    }
    
    public function migrations(): string
    {
        return $this->extractor->extractStringByKey('migrations', $this->configuration);
    }
}
