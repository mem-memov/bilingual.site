<?php
namespace core\configuration;
class Directory implements IDirectory
{
    private $extractor;
    private $configuration;
    
    public function __construct(
        IExtractor $extractor,
        array $configuration
    ) {
        $this->extractor = $extractor;
        $this->configuration = $configuration;
    }
    
    public function root(): string
    {
        return $this->extractor->extractPathByKey('root', $this->configuration);
    }

    public function master(): string
    {
        return $this->extractor->extractPathByKey('master', $this->configuration);
    }

    public function copy(): string
    {
        return $this->extractor->extractPathByKey('copy', $this->configuration);
    }

    public function cut(): string
    {
        return $this->extractor->extractPathByKey('cut', $this->configuration);
    }
}
