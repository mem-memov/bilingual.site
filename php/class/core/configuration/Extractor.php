<?php
namespace core\configuration;
class Extractor implements IExtractor
{
    public function __construct(
    ) {
        
    }
    
    public function extractArrayByKey(string $key, array $configuration): array
    {
        if (!isset($configuration[$key])) {
            throw new exception\ConfigurationMissing($key);
        }
        
        if (!is_array($configuration[$key])) {
            throw new exception\WrongConfigurationType($key, 'array', $configuration[$key]);
        }
        
        return $configuration[$key];
    }
    
    public function extractStringByKey(string $key, array $configuration): string
    {
        if (!isset($configuration[$key])) {
            throw new exception\ConfigurationMissing($key);
        }
        
        if (!is_string($configuration[$key])) {
            throw new exception\WrongConfigurationType($key, 'string', $configuration[$key]);
        }
        
        return $configuration[$key];
    }
    
    public function extractPathByKey(string $key, array $configuration): string
    {
        $rawPath = $this->extractStringByKey($key, $configuration);
        
        if (!file_exists($rawPath) && !is_dir($rawPath)) {
            throw new exception\DirectoryMissing($rawPath);
        }
        
        return realpath($rawPath);
    }
}
