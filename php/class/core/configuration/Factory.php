<?php
namespace core\configuration;
class Factory implements IFactory
{
    private $extractor;
    private $configuration;

    private $server;
    private $database;
    private $cache;
    private $directory;
    private $queue;
    
    public function __construct(
        IExtractor $extractor, 
        array $configuration
    ) {
        $this->extractor = $extractor;
        $this->configuration = $configuration;

        $this->server = null;
        $this->database = null;
        $this->cache = null;
        $this->directory = null;
        $this->queue = null;
    }

    public function server(): IServer
    {
        if (is_null($this->server)) {

            $configuration = $this->extractor->extractArrayByKey('server', $this->configuration);

            $this->server = new Server(
                $this->extractor,
                $configuration
            );
        }

        return $this->server;
    }
    
    public function database(): IDatabase
    {
        if (is_null($this->database)) {

            $configuration = $this->extractor->extractArrayByKey('database', $this->configuration);

            $this->database = new Database(
                $this->extractor,
                $configuration
            );
        }

        return $this->database;
    }
    
    public function cache(): ICache
    {
        if (is_null($this->cache)) {

            $configuration = $this->extractor->extractArrayByKey('cache', $this->configuration);

            $this->cache = new Cache(
                $this->extractor,
                $configuration
            );

        }

        return $this->cache;
    }
    
    public function directory(): IDirectory
    {
        if (is_null($this->directory)) {

            $configuration = $this->extractor->extractArrayByKey('directory', $this->configuration);

            $this->directory = new Directory(
                $this->extractor,
                $configuration
            );
        }

        return $this->directory;
    }
    
    public function queue(): IQueue
    {
        if (is_null($this->queue)) {

            $configuration = $this->extractor->extractArrayByKey('queue', $this->configuration);

            $this->queue = new Queue(
                $this->extractor,
                $configuration
            );
        }

        return $this->queue;
    }
}

