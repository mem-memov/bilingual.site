<?php
namespace core\configuration;
interface ICache
{
    public function type(): string;
    public function servers(): array;
}
