<?php
namespace core\configuration;
interface IDatabase
{
    public function type(): string;
    public function server(): string;
    public function port(): string;
    public function user(): string;
    public function password(): string;
    public function database(): string;
    public function migrations(): string;
}
