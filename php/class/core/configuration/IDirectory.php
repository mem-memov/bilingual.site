<?php
namespace core\configuration;
interface IDirectory
{
    public function root(): string;
    public function master(): string;
    public function copy(): string;
    public function cut(): string;
}
