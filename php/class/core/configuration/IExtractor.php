<?php
namespace core\configuration;
interface IExtractor {
    public function extractArrayByKey(string $key, array $configuration): array;
    public function extractStringByKey(string $key, array $configuration): string;
    public function extractPathByKey(string $key, array $configuration): string;
}
