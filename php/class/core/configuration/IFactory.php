<?php
namespace core\configuration;
interface IFactory
{
    public function server(): IServer;
    public function database(): IDatabase;
    public function cache(): ICache;
    public function directory(): IDirectory;
    public function queue(): IQueue;
}

