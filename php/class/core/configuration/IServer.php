<?php
namespace core\configuration;
interface IServer
{
    public function name(): string;
}
