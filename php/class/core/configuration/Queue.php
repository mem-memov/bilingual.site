<?php
namespace core\configuration;
class Queue implements IQueue
{
    private $extractor;
    private $configuration;
    
    public function __construct(
        IExtractor $extractor,
        array $configuration
    ) {
        $this->extractor = $extractor;
        $this->configuration = $configuration;
    }
    
    public function server(): string
    {
        return $this->extractor->extractStringByKey('server', $this->configuration);
    }
    
    public function port(): int
    {
        return intval($this->extractor->extractStringByKey('port', $this->configuration));
    }
    
    public function user(): string
    {
        return $this->extractor->extractStringByKey('user', $this->configuration);
    }
    
    public function password(): string
    {
        return $this->extractor->extractStringByKey('password', $this->configuration);
    }
}
