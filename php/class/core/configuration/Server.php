<?php
namespace core\configuration;
class Server implements IServer
{
    private $extractor;
    private $configuration;
    
    public function __construct(
        IExtractor $extractor,
        array $configuration
    ) {
        $this->extractor = $extractor;
        $this->configuration = $configuration;
    }
    
    public function name(): string
    {
        return $this->extractor->extractStringByKey('name', $this->configuration);
    }
}
