<?php
namespace core\configuration\exception;
class DirectoryMissing extends Exception
{
    public function __construct($path)
    {
        $message = 'Directory ' . $path . ' is missing.';
        
        parent::__construct($message);
    }
}
