<?php
namespace core\configuration\exception;
class WrongConfigurationType extends Exception
{
    public function __construct($key, $properType, $value)
    {
        parent::__construct(
            'Configuration ' . $key . 
            ' must be of type ' . $properType . ', ' . 
            gettype($value) . ' was given.'
        );
    }
}
