<?php
namespace core\database;
use core\service\sql\IQuery;
use core\service\sql\ITransaction;
use core\service\sql\IRenderer;
class Factory implements IFactory
{
    private $migrationDirectory;
    private $query;
    private $transaction;
    private $renderer;
    
    private $tables;
    private $filters;
    private $migration;
    private $schema;
    
    public function __construct(
        $migrationDirectory,
        IQuery $query,
        ITransaction $transaction,
        IRenderer $renderer
    ) {
        $this->migrationDirectory = $migrationDirectory;
        $this->query = $query;
        $this->transaction = $transaction;
        $this->renderer = $renderer;
        
        $this->tables = null;
        $this->filters = null;
        $this->migration = null;
        $this->schema = null;
    }

    public function query(): IQuery
    {
        return $this->query;
    }
    
    public function transaction(): ITransaction
    {
        return $this->transaction;
    }
    
    public function tables(): table\IFactory
    {
        if (is_null($this->tables)) {
            
            $fields = new field\Factory($this->renderer);
            
            $this->tables = new table\Factory(
                $this->renderer,
                $fields
            );
        }

        return $this->tables;
    }
    
    public function filters(): filter\IFactory
    {
        if (is_null($this->filters)) {
            $this->filters = new filter\Factory();
        }

        return $this->filters;
    }
    
    public function migration(): migration\IFacade
    {
        if (is_null($this->migration)) {

            $differenceFactory = new migration\DifferenceFactory($this->transaction);
            $migrationFactory = new migration\MigrationFactory($this->query, $this->migrationDirectory);
            $migrationList = new migration\MigrationList($migrationFactory, $this->migrationDirectory, $differenceFactory);
            $currentMigration = new migration\CurrentMigration($migrationFactory, $this->migrationDirectory);
            $recording = new migration\Recording($migrationFactory, $migrationList, $currentMigration);
            
            $this->migration = new migration\Facade(
                $migrationList,
                $currentMigration,
                $migrationFactory,
                $recording
            );
        }

        return $this->migration;
    }

    public function schema(): ISchema
    {
        if (is_null($this->schema)) {

            $this->schema = new Schema(
                $this->tables(),
                $this->database
            );
            
        }

        return $this->schema;
    }
}
