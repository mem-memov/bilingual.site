<?php
namespace core\database;
use core\service\sql\IQuery;
use core\service\sql\ITransaction;
interface IFactory
{
    public function query(): IQuery;
    public function transaction(): ITransaction;
    public function tables(): table\IFactory;
    public function filters(): filter\IFactory;
    public function migration(): migration\IFacade;
    public function schema(): ISchema;
}

