<?php
namespace core\database;
interface ISchema
{
    public function view(): array;
}
