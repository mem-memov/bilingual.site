<?php
namespace core\database;
use core\database\table\IFactory as ITableFactory;
use core\service\sql\IDatabase;
use core\service\sql\exception\QueryFailed;
class Schema implements ISchema
{
    private $tableFactory;
    private $database;
    
    public function __construct(
        ITableFactory $tableFactory,
        IDatabase $database
    ) {
        $this->tableFactory = $tableFactory;
        $this->database = $database;
    }
    
    public function view(): array
    {
        $schema = [];
        
        
        $tableMethods = get_class_methods($this->tableFactory);
        
        foreach ($tableMethods as $tableMethod) {
            
            if (in_array($tableMethod, ['__construct'])) {
                continue;
            }

            $table = $this->tableFactory->$tableMethod();
            
            $sql = 'SHOW TABLES LIKE "' . $table->rawName() . '";';
            $rows = $this->database->select($sql);
            
            $tableMissing = empty($rows);
            
            $fields = $table->fields();

            foreach ($fields as $field) {

                if ($tableMissing) {
                    $schema[$table->rawName()][$field->rawName()] = 'missing';
                    continue;
                }
                
                $sql = 'SHOW COLUMNS IN ' . $table->name() . ' LIKE "' . $field->rawName() . '";';
                $rows = $this->database->select($sql);
                
                if (empty($rows)) {
                    $schema[$table->rawName()][$field->rawName()] = 'missing';
                    continue;
                }

                $schema[$table->rawName()][$field->rawName()] = 'ok';
            }

        }

        return $schema;
    }
}
