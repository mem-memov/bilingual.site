<?php
namespace core\database\field;
class Boolean extends Field implements IBoolean
{
    protected function valueSpecific(bool $value): string
    {
        return $this->renderer->asBoolean($value);
    }
}
