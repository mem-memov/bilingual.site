<?php
namespace core\database\field;
class Date extends Field implements IDate
{
    protected function valueSpecific(\DateTime $value): string
    {
        return $this->renderer->asDate($value);
    }
    
    public function before(\DateTime $value): string
    {
        return ' ' . $this->name() . ' < ' . $this->value($value) . ' ';
    }
    
    public function after(\DateTime $value): string
    {
        return ' ' . $this->name() . ' > ' . $this->value($value) . ' ';
    }
}