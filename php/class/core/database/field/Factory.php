<?php
namespace core\database\field;
use core\service\sql\IRenderer;
class Factory implements IFactory
{
    private $renderer;
    
    public function __construct(IRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    public function integer(string $table, string $name): IInteger
    {
        return new Integer($this->renderer, $table, $name);
    }
    
    public function text(string $table, string $name): IText
    {
        return new Text($this->renderer, $table, $name);
    }
   
    public function boolean(string $table, string $name): IBoolean
    {
        return new Boolean($this->renderer, $table, $name);
    }
   
    public function date(string $table, string $name): IDate
    {
        return new Date($this->renderer, $table, $name);
    }
}
