<?php
namespace core\database\field;
use core\service\sql\IRenderer;
abstract class Field implements IField
{
    protected $renderer;
    protected $table;
    protected $name;

    public function __construct(
        IRenderer $renderer, 
        string $table,
        string $name
    ) {
        $this->renderer = $renderer;
        $this->table = $table;
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->renderer->asField($this->table, $this->name);
    }
    
    public function value($value = null): string
    {
        if (is_null($value)) {
            return $this->renderer->asNull();
        }
        
        return $this->valueSpecific($value);
    }
    
    public function set($value = null): string
    {
        if (is_null($value)) {
            return ' ' . $this->name() . ' = ' . $this->renderer->asNull() . ' ';
        }
        
        return ' ' . $this->name() . ' = ' . $this->value($value) . ' ';
    }
    
    public function is($value = null): string
    {
        if (is_null($value)) {
            return ' ' . $this->name() . ' ' . $this->renderer->isNull() . ' ';
        }
        
        return ' ' . $this->name() . ' = ' . $this->value($value) . ' ';
    }

    public function among(array $values): string
    {
        $items = [];
        
        foreach ($values as $value) {
            $items[] = $this->value($value);
        }
        
        return ' ' . $this->name() . ' IN (' . implode(', ', $items) . ') ';
    }

    public function ascend(): string
    {
        return ' ' . $this->name() . ' ASC ';
    }
    
    public function descend(): string
    {
        return ' ' . $this->name() . ' DESC ';
    }
}
