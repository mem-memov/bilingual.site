<?php
namespace core\database\field;
interface IDate extends IField
{
    public function before(\DateTime $value): string;
    public function after(\DateTime $value): string;
}
