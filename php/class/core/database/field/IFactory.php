<?php
namespace core\database\field;
interface IFactory
{
    public function integer(string $table, string $name): IInteger;
    public function text(string $table, string $name): IText;
    public function boolean(string $table, string $name): IBoolean;
    public function date(string $table, string $name): IDate;
}

