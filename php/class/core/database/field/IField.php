<?php
namespace core\database\field;
use core\database\table\ITable;
interface IField
{
    public function name(): string;
    public function value($value = null): string;
    public function set($value = null): string;
    public function is($value = null): string;
    public function among(array $values): string;
    public function ascend(): string;
    public function descend(): string;
}

