<?php
namespace core\database\field;
interface IInteger extends IField
{
    public function above(int $value): string;
    public function below(int $value): string;
}
