<?php
namespace core\database\field;
interface IText extends IField
{
    public function beginsWith(string $value): string;
    public function contains(string $value): string;
}
