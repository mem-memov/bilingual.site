<?php
namespace core\database\field;
class Integer extends Field implements IInteger
{
    protected function valueSpecific(int $value): string
    {
        return $this->renderer->asInteger($value);
    }
    
    public function above(int $value): string
    {
        return ' ' . $this->name() . ' > ' . $this->value($value) . ' ';
    }
    
    public function below(int $value): string
    {
        return ' ' . $this->name() . ' < ' . $this->value($value) . ' ';
    }
}

