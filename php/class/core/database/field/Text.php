<?php
namespace core\database\field;
class Text extends Field implements IText
{
    protected function valueSpecific(string $value): string
    {
        return $this->renderer->asText($value);
    }

    public function beginsWith(string $value): string
    {
        return ' ' . $this->name() . ' LIKE ' . $this->value($value . '%') . ' ';
    }
    
    public function contains(string $value): string
    {
        return ' ' . $this->name() . ' LIKE ' . $this->value('%' . $value . '%') . ' ';
    }
}

