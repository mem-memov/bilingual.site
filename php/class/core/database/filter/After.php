<?php
namespace core\database\filter;
use core\database\field\IDate;
class After extends Filter
{
    private $value;
    
    public function __construct(\DateTime $value)
    {
        $this->value = $value;
    }

    protected function selectChecked(IDate $field): string
    {
        return $field->after($this->value);
    }
}
