<?php
namespace core\database\filter;
use core\database\field\filter\IAmongFilter;
class Among extends Filter
{
    private $values;
    
    public function __construct(array $values)
    {
        $this->values = $values;
    }

    protected function selectChecked(IAmongFilter $field): string
    {
        return $field->among($this->values);
    }
}
