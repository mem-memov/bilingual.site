<?php
namespace core\database\filter;
use core\database\field\IField;
class Any implements IFilter 
{
    private $filters;
    
    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    public function select(IField $field): string
    {
        $selections = [];
        
        foreach ($this->filters as $filter) {
            $selections[] = $filter->select();
        }
        
        return ' (' . implode(' OR ', $selections) . ') ';
    }
}
