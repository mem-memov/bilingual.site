<?php
namespace core\database\filter;
use core\database\field\IDate;
class Before extends Filter
{
    private $value;
    
    public function __construct(\DateTime $value)
    {
        $this->value = $value;
    }

    protected function selectChecked(IDate $field): string
    {
        return $field->before($this->value);
    }
}
