<?php
namespace core\database\filter;
use core\database\field\IText;
class BeginsWith extends Filter
{
    private $value;
    
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    protected function selectChecked(IText $field): string
    {
        return $field->beginsWith($this->value);
    }
}
