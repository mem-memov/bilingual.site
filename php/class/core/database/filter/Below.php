<?php
namespace core\database\filter;
use core\database\field\IInteger;
class Below extends Filter
{
    private $value;
    
    public function __construct(int $value)
    {
        $this->value = $value;
    }

    protected function selectChecked(IInteger $field): string
    {
        return $field->below($this->value);
    }
}
