<?php
namespace core\database\filter;
use core\database\field\IText;
class Contains extends Filter
{
    private $value;
    
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    protected function selectChecked(IText $field): string
    {
        return $field->contains($this->value);
    }
}
