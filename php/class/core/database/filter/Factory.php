<?php
namespace core\database\filter;
class Factory implements IFactory
{
    public function all(array $filters): IFilter
    {
        return new All($filters);
    }

    public function any(array $filters): IFilter
    {
        return new Any($filters);
    }

    public function not(IFilter $filter): IFilter
    {
        return new Not($filter);
    }
    
    public function among(array $values): IFilter
    {
        return new Among($values);
    }
    
    public function is($value): IFilter
    {
        return new Is($value);
    }
    
    public function above(int $value): IFilter
    {
        return new Above($value);
    }
    
    public function below(int $value): IFilter
    {
        return new Below($value);
    }

    public function after(\DateTime $value): IFilter
    {
        return new After($value);
    }
    
    public function before(\DateTime $value): IFilter
    {
        return new Before($value);
    }
    
    public function contains(string $value): IFilter
    {
        return new Contains($value);
    }
    
    public function beginsWith(string $value): IFilter
    {
        return new BeginsWith($value);
    }
}
