<?php
namespace core\database\filter;
use core\database\field\IField;
abstract class Filter implements IFilter
{
    public function select(IField $field): string
    {
        return $this->selectChecked($field);
    }
}
