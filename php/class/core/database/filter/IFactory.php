<?php
namespace core\database\filter;
use core\database\field\{IField, IId, IInteger, IText, IDate, IBoolean};
interface IFactory
{
    public function all(array $filters): IFilter;
    public function any(array $filters): IFilter;
    public function not(IFilter $filter): IFilter;
    public function among(array $values): IFilter;
    public function is($value): IFilter;
    public function above(int $value): IFilter;
    public function below(int $value): IFilter;
    public function after(\DateTime $value): IFilter;
    public function before(\DateTime $value): IFilter;
    public function contains(string $value): IFilter;
    public function beginsWith(string $value): IFilter;
}
