<?php
namespace core\database\filter;
use core\database\field\IField;
interface IFilter
{
    public function select(IField $field): string;
}
