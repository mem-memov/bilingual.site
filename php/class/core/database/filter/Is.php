<?php
namespace core\database\filter;
use core\database\field\IField;
class Is extends Filter
{
    private $value;
    
    public function __construct($value)
    {
        $this->value = $value;
    }

    protected function selectChecked(IField $field): string
    {
        return $field->is($this->value);
    }
}
