<?php
namespace core\database\migration;
class BackwardDifference extends Difference
{
    public function add(IMigration $migration)
    {
        array_unshift($this->migrations, $migration);
    }

    public function apply()
    {
        $this->transaction->start();
        
        foreach ($this->migrations as $migration) {
            try {
                $migration->applyDownSql();
            } catch (Exception $ex) {
                $this->transaction->rollback();
                throw new exception\MigrationsNotApplied($migration);
            }
        }
        
        $this->transaction->commit();
    }
}
