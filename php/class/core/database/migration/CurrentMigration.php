<?php
namespace core\database\migration;
class CurrentMigration implements ICurrentMigration
{
    private $migrationFactory;
    private $path;
    
    public function __construct(
        IMigrationFactory $migrationFactory,
        string $directory
    ) {
        $this->migrationFactory = $migrationFactory;
        $this->path = $directory . '/current';
    }

    public function get(): IMigration
    {
        $name = trim(file_get_contents($this->path));
        
        return $this->migrationFactory->make($name);
    }
    
    public function set(IMigration $migration)
    {
        file_put_contents($this->path, $migration->getName());
    }
}
