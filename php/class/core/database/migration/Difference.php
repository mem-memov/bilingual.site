<?php
namespace core\database\migration;
use core\service\sql\ITransaction;
abstract class Difference implements IDifference
{
    protected $transaction;
    protected $migrations;

    public function __construct(
        ITransaction $transaction
    ) {
        $this->transaction = $transaction;
        $this->migrations = [];
    }
}
