<?php
namespace core\database\migration;
use core\service\sql\ITransaction;
class DifferenceFactory implements IDifferenceFactory
{
    private $transaction;

    public function __construct(
        ITransaction $transaction
    ) {
        $this->transaction = $transaction;
    }
    
    public function forward(): IDifference
    {
        return new ForwardDifference($this->transaction);
    }
    
    public function backward(): IDifference
    {
        return new BackwardDifference($this->transaction);
    }
}
