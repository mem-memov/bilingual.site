<?php
namespace core\database\migration;
class Facade implements IFacade
{
    private $migrationList;
    private $currentMigration;
    private $migrationFactory;
    private $recording;
    
    public function __construct(
        IMigrationList $migrationList,
        ICurrentMigration $currentMigration,
        IMigrationFactory $migrationFactory,
        IRecording $recording
    ) {
        $this->migrationList = $migrationList;
        $this->currentMigration = $currentMigration;
        $this->migrationFactory = $migrationFactory;
        $this->recording = $recording;
    }
    
    public function getCurrent(): string
    {
        return $this->currentMigration->get()->getName();
    }
    
    public function viewMigration(string $name): array
    {
        $migration = $this->migrationFactory->make($name);
        
        return [
            'name' => $migration->getName(),
            'query' => [
                'up' => $migration->getUpSql(),
                'down' => $migration->getDownSql()
            ],
            'path' => [
                'up' => $migration->getUpPath(),
                'down' => $migration->getDownPath()
            ],
            'isCurrent' => ($migration->getName() == $this->currentMigration->get()->getName())
        ];
    }
    
    public function getAll(): array
    {
        $migrations = $this->migrationList->getAll();
        
        $names = [];
        
        foreach ($migrations as $migration) {
            $names[] = $migration->getName();
        }
        
        return $names;
    }
    
    public function migrate(string $name): array
    {
        $origin = $this->currentMigration->get();
        $target = $this->migrationFactory->make($name);

        try {
            $difference = $this->migrationList->getDifference($origin, $target);
        } catch (exception\NoDifference $e) {
            return [];
        }

        $difference->apply();
        $this->currentMigration->set($target);
        
        return [];
    }

    public function recording(): IRecording
    {
        return $this->recording;
    }
}
