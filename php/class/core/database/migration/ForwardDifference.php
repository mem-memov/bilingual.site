<?php
namespace core\database\migration;
class ForwardDifference extends Difference
{
    public function add(IMigration $migration)
    {
        $this->migrations[] = $migration;
    }
    
    public function apply()
    {
        $this->transaction->start();
        
        foreach ($this->migrations as $migration) {
            
            try {
                $migration->applyUpSql();
            } catch (Exception $ex) {
                $this->transaction->rollback();
                throw new exception\MigrationsNotApplied($migration);
            }
        }
        
        $this->transaction->commit();
    }
}
