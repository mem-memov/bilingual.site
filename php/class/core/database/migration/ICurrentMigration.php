<?php
namespace core\database\migration;
interface ICurrentMigration
{
    public function get(): IMigration;
    public function set(IMigration $migration);
}
