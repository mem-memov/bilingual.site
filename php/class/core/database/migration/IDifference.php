<?php
namespace core\database\migration;
interface IDifference
{
    public function add(IMigration $migration);
    public function apply();
}
