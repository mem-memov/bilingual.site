<?php
namespace core\database\migration;
interface IDifferenceFactory
{
    public function forward(): IDifference;
    public function backward(): IDifference;
}
