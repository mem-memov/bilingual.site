<?php
namespace core\database\migration;
interface IFacade
{
    public function getCurrent(): string;
    public function viewMigration(string $name): array;
    public function getAll(): array;
    public function migrate(string $name): array;
    public function recording(): IRecording;
}
