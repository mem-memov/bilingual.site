<?php
namespace core\database\migration;
interface IMigration
{
    public function getName(): string;
    public function getUpPath(): string;
    public function getDownPath(): string;
    public function applyUpSql();
    public function applyDownSql();
    public function save(array $upQueries, array $downQueries);
}
