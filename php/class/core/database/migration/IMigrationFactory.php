<?php
namespace core\database\migration;
interface IMigrationFactory
{
    public function make(string $name): IMigration;
}
