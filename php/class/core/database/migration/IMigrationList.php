<?php
namespace core\database\migration;
interface IMigrationList
{
    /**
     * @throws \core\database\migration\exception\MigrationsNotUnique
     */
    public function getAll(): array;
    
    /**
     * @throws \core\database\migration\exception\MigrationsNotUnique
     * @throws \core\database\migration\exception\MigrationNotFound
     * @throws \core\database\migration\exception\NoDifference
     */
    public function getDifference(IMigration $origin, IMigration $target): IDifference;

    /**
     * @throws \core\database\migration\exception\NoTargetForMigrationInList
     * @throws \core\database\migration\exception\RepeatedMigrationCreation
     */
    public function insertAfter(IMigration $target, IMigration $new);
}
