<?php
namespace core\database\migration;
interface IRecording
{
    public function append(string $upQuery, string $downQuery);
    public function save();
}
