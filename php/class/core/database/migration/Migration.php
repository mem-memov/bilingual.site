<?php
namespace core\database\migration;
use core\service\sql\IQuery as IDatabase;
class Migration implements IMigration
{
    private $database;
    private $directory;
    private $separator;
    private $queryCount;
    
    public function __construct(
        IDatabase $database,
        string $directory,
        string $name
    ) {
        $this->database = $database;
        $this->directory = $directory;
        $this->name = $name;
        $this->separator = '# a query';
        $this->queryCount = 0;
    }
    
    public function getName(): string
    {
        return $this->name;
    }
    
    public function getUpPath(): string
    {
        return $this->directory . '/' . $this->name . '/up.sql';
    }

    public function getDownPath(): string
    {
        return $this->directory . '/' . $this->name . '/down.sql';
    }

    public function applyUpSql()
    {
        $fileHandle = fopen($this->getUpPath(), 'r');

        $sql = '';
        while (!feof($fileHandle)) {
            $line = fgets($fileHandle);

            if (trim($line) !== $this->separator) {
                $sql .= $line;
            } else {
                $this->database->apply($sql);
                $sql = '';
            }
        }

        if (!empty($sql)) {
            $this->database->apply($sql);
        }

        fclose($fileHandle);
    }

    public function applyDownSql()
    {
        $fileHandle = fopen($this->getDownPath(), 'r');

        $sql = '';
        while (!feof($fileHandle)) {
            $line = fgets($fileHandle);

            if (trim($line) !== $this->separator) {
                $sql .= $line;
            } else {
                $this->database->apply($sql);
                $sql = '';
            }
        }

        if (!empty($sql)) {
            $this->database->apply($sql);
        }

        fclose($fileHandle);
    }

    public function save(array $upQueries, array $downQueries)
    {
        $upSql = implode("\n" . $this->separator . "\n", $upQueries);
        $downSql = implode("\n" . $this->separator . "\n", $downQueries);

        $upPath = $this->getUpPath();
        $downPath = $this->getDownPath();

        if (file_exists($upPath) || file_exists($downPath)) {
            throw new exception\RepeatedMigrationCreation($this);
        }

        $directory = dirname($upPath);
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        file_put_contents($upPath, $upSql);
        file_put_contents($downPath, $downSql);
    }
}
