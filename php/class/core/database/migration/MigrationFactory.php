<?php
namespace core\database\migration;
use core\service\sql\IQuery as IDatabase;
class MigrationFactory implements IMigrationFactory
{
    private $database;
    private $directory;
    
    public function __construct(
        IDatabase $database,
        string $directory
    ) {
        $this->database = $database;
        $this->directory = $directory;
    }
    
    public function make(string $name): IMigration
    {
        return new Migration($this->database, $this->directory, $name);
    }
}
