<?php
namespace core\database\migration;
class MigrationList implements IMigrationList
{
    private $migrationFactory;
    private $path;
    private $differenceFactory;
    
    public function __construct(
        IMigrationFactory $migrationFactory,
        string $directory,
        IDifferenceFactory $differenceFactory
    ) {
        $this->migrationFactory = $migrationFactory;
        $this->path = $directory . '/list';
        $this->differenceFactory = $differenceFactory;
    }
    
    public function getAll(): array
    {
        // read file
        $names = explode("\n", file_get_contents($this->path));
        
        // skip empty lines
        $names = array_filter($names, function($name) {
            return $name != '' && $name != "\r";
        });
        
        // check uniqueness
        $valueNumbers = array_count_values($names);
        $repeatingNames = [];
        foreach ($valueNumbers as $name => $count) {
            if ($count > 1) {
                $repeatingNames[] = $name;
            }
        }
        if (count($repeatingNames) > 0) {
            throw new exception\MigrationsNotUnique($repeatingNames);
        }
        
        // make objects
        $migrations = [];
        
        foreach ($names as $name) {
            $migrations[] = $this->migrationFactory->make($name);
        }
        
        return $migrations;
    }
    
    public function getDifference(IMigration $origin, IMigration $target): IDifference
    {
        $migrations = $this->getAll();
        
        $difference = null;
        $originFound = false;
        $targetFound = false;
        foreach ($migrations as $migration) {
            
            // forward
            if ($originFound && !$targetFound) {
                if (is_null($difference)) {
                    $difference = $this->differenceFactory->forward();
                }
                $difference->add($migration);
            }
            
            // backward
            if ($targetFound && !$originFound) {
                if (is_null($difference)) {
                    $difference = $this->differenceFactory->backward();
                }
                $difference->add($migration);
            }

            // find origin
            if ($origin->getName() == $migration->getName()) {
                $originFound = true;
            }
            
            // find target
            if ($target->getName() == $migration->getName()) {
                $targetFound = true;
            }

        }
        
        if (!$originFound) {
            throw new exception\MigrationNotFound($origin);
        }
        
        if (!$targetFound) {
            throw new exception\MigrationNotFound($target);
        }
        
        if (is_null($difference)) {
            throw new exception\NoDifference($origin, $target);
        }

        return $difference;
    }

    public function insertAfter(IMigration $target, IMigration $new)
    {
        $migrations = $this->getAll();

        $names = [];
        foreach ($migrations as $migration) {
            $names[] = $migration->getName();
        }

        if (in_array($new->getName(), $names)) {
            throw new exception\RepeatedMigrationCreation($new);
        }

        $index = array_search($target->getName(), $names);

        if ($index === false) {
            throw new exception\NoTargetForMigrationInList($target, $new);
        }

        array_splice($names, $index+1, 0, $new->getName());

        file_put_contents($this->path, implode("\n", $names));
    }
}
