<?php
namespace core\database\migration;
class Recording implements IRecording
{
    private $migrations;
    private $migrationList;
    private $currentMigration;
    private $querySeparator;
    private $migration;
    private $upQueries;
    private $downQueries;

    public function __construct(
        IMigrationFactory $migrations,
        IMigrationList $migrationList,
        ICurrentMigration $currentMigration
    ) {
        $this->migrations = $migrations;
        $this->migrationList = $migrationList;
        $this->currentMigration = $currentMigration;
        $this->migration = null;
        $this->upQueries = [];
        $this->downQueries = [];
    }
    
    public function start(string $name)
    {
        if (!is_null($this->migration)) {
            throw new exception\AlreadyRecording($name, $this->migration);
        }
        
        $fullName = 'recording/' . $name;

        $this->migration = $this->migrations->make($fullName);
    }

    public function append(string $upQuery, string $downQuery)
    {
        $this->upQueries[] = $upQuery;
        $this->downQueries[] = $downQuery;
    }

    public function save()
    {
        $this->migration->save($this->upQueries, array_reverse($this->downQueries));

        $this->upQueries = [];
        $this->downQueries = [];

        $current = $this->currentMigration->get();
        $this->migrationList->insertAfter($current, $this->migration);
        $this->currentMigration->set($this->migration);
        
        $this->migration = null;
    }
}
