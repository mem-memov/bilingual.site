<?php
namespace core\database\migration\exception;
use core\database\migration\IMigration;
class AlreadyRecording extends Exception
{
    public function __construct(string $name, IRecording $migration)
    {
        parent::__construct('New recording ' . $name . ' cannot start because a recording named ' . $migration->getName() . ' has not finished, yet.');
    }
}
