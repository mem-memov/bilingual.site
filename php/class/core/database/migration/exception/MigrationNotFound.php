<?php
namespace core\database\migration\exception;
use core\sql\migration\IMigration;
class MigrationNotFound extends Exception
{
    public function __construct(IMigration $migration)
    {
        parent::__construct($migration->getName());
    }
}
