<?php
namespace core\database\migration\exception;
use core\database\migration\IMigration;
class MigrationsNotApplied extends Exception
{
    public function __construct(IMigration $migration)
    {
        parent::__construct('This migration could not be applied ' . $migration->getName());
    }
}
