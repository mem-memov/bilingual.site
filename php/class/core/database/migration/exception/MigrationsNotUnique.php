<?php
namespace core\database\migration\exception;
class MigrationsNotUnique extends Exception
{
    public function __construct(array $repeatingNames)
    {
        parent::__construct(implode(', ', $repeatingNames));
    }
}
