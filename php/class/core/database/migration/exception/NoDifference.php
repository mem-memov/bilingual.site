<?php
namespace core\database\migration\exception;
use core\database\migration\IMigration;
class NoDifference extends Exception
{
    public function __construct(IMigration $origin, IMigration $target)
    {
        parent::__construct('No difference between migrations: ' . $origin->getName() . ' and ' . $target->getName());
    }
}
