<?php
namespace core\database\migration\exception;
use core\database\migration\IMigration;
class NoTargetForMigrationInList extends Exception
{
    public function __construct(IMigration $target, IMigration $new)
    {
        parent::__construct('Could not insert migration  ' . $new->getName() . ' after ' . $target->getName());
    }
}
