<?php
namespace core\database\migration\exception;
use core\database\migration\IMigration;
class RepeatedMigrationCreation extends Exception
{
    public function __construct(IMigration $migration)
    {
        parent::__construct('Migration already exists: ' . $migration->getName());
    }
}
