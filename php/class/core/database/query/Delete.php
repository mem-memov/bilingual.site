<?php
namespace core\database\query;
abstract class Delete extends Query implements IDelete
{
    public function run(): int
    {
        return $this->database->delete($this->query());
    }
}
