<?php
namespace core\database\query;
interface IDelete extends IQuery
{
    public function run(): int;
}
