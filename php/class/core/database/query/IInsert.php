<?php
namespace core\database\query;
interface IInsert extends IQuery
{
    public function run(): int;
}
