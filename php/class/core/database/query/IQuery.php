<?php
namespace core\database\query;
interface IQuery
{
    public function query(): string;
}
