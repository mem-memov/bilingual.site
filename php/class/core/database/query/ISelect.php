<?php
namespace core\database\query;
interface ISelect extends IQuery
{
    public function run(): array;
}
