<?php
namespace core\database\query;
interface IUpdate extends IQuery
{
    public function run(): int;
}
