<?php
namespace core\database\query;
abstract class Insert extends Query implements IInsert
{
    public function run(): int
    {
        return $this->database->insert($this->query());
    }
}
