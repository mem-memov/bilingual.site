<?php
namespace core\database\query;
use core\service\sql\IQuery as IDatabase;
abstract class Query implements IQuery
{
    protected $database;
    
    public function __construct(
        IDatabase $database
    ) {
        $this->database = $database;
    }
}
