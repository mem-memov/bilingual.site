<?php
namespace core\database\query;
abstract class Select extends Query implements ISelect
{
    public function run(): array
    {
        return $this->database->select($this->query());
    }
}
