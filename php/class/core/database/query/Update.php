<?php
namespace core\database\query;
abstract class Update extends Query implements IUpdate
{
    public function run(): int
    {
        return $this->database->update($this->query());
    }
}
