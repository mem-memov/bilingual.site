<?php
namespace core\database\table;
use core\service\sql\IRenderer;
use core\database\field\IFactory as IFieldFactory;
class Factory implements IFactory
{
    private $renderer;
    private $fields;

    private $source;
    private $language;
    private $video;
    private $audio;
    private $subtitle;
    private $phrase;
    private $word;
    private $videoAudio;
    private $videoSubtitle;
    private $phraseWord;
    private $original;
    private $foreign;
    private $clip;
    private $authentication;

    public function __construct(
        IRenderer $renderer,
        IFieldFactory $fields
    ) {
        $this->renderer = $renderer;
        $this->fields = $fields;

        $this->source = null;
        $this->language = null;
        $this->video = null;
        $this->audio = null;
        $this->subtitle = null;
        $this->phrase = null;
        $this->word = null;
        $this->videoAudio = null;
        $this->videoSubtitle = null;
        $this->phraseWord = null;
        $this->original = null;
        $this->foreign = null;
        $this->clip = null;
        $this->authentication = null;
    }

    public function source(): source\ITable
    {
        if (is_null($this->source)) {
            
            $name = 'source';
            
            $this->source = new source\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->text($name, 'file')
                ]
            );
        }

        return $this->source;
    }

    public function language(): language\ITable
    {
        if (is_null($this->language)) {
            
            $name = 'language';
            
            $this->language = new language\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->text($name, 'language')
                ]
            );
        }

        return $this->language;
    }

    public function video(): video\ITable
    {
        if (is_null($this->video)) {
            
            $name = 'video';
            
            $this->video = new video\Table(
                $this->renderer,
                'video',
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'source_id'),
                    $this->fields->integer($name, 'language_id'),
                    $this->fields->text($name, 'file'),
                    $this->fields->integer($name, 'width'),
                    $this->fields->integer($name, 'height'),
                    $this->fields->integer($name, 'duration')
                ]
            );
        }

        return $this->video;
    }
    
    public function audio(): audio\ITable
    {
        if (is_null($this->audio)) {

            $name = 'audio';
            
            $this->audio = new audio\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'source_id'),
                    $this->fields->integer($name, 'language_id'),
                    $this->fields->text($name, 'file'),
                    $this->fields->integer($name, 'duration')
                ]
            );
        }

        return $this->audio;
    }
    
    public function subtitle(): subtitle\ITable
    {
        if (is_null($this->subtitle)) {
            
            $name = 'subtitle';
            
            $this->subtitle = new subtitle\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'source_id'),
                    $this->fields->integer($name, 'language_id'),
                    $this->fields->text($name, 'file'),
                    $this->fields->integer($name, 'phrase_number')
                ]
            );
        }

        return $this->subtitle;
    }
    
    public function phrase(): phrase\ITable
    {
        if (is_null($this->phrase)) {
            
            $name = 'phrase';
            
            $this->phrase = new phrase\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'language_id'),
                    $this->fields->integer($name, 'subtitle_id'),
                    $this->fields->integer($name, 'start'),
                    $this->fields->integer($name, 'stop'),
                    $this->fields->text($name, 'phrase'),
                    $this->fields->integer($name, 'order_number')
                ]
            );
        }

        return $this->phrase;
    }

    public function word(): word\ITable
    {
        if (is_null($this->word)) {
            
            $name = 'word';
            
            $this->word = new word\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'language_id'),
                    $this->fields->text($name, 'word')
                ]
            );
        }

        return $this->word;
    }
    
    public function videoAudio(): videoAudio\ITable
    {
        if (is_null($this->videoAudio)) {
            
            $name = 'video_audio';
            
            $this->videoAudio = new videoAudio\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'video_id'),
                    $this->fields->integer($name, 'audio_id')
                ]
            );
        }

        return $this->videoAudio;
    }

    public function videoSubtitle(): videoSubtitle\ITable
    {
        if (is_null($this->videoSubtitle)) {
            
            $name = 'video_subtitle';
            
            $this->videoSubtitle = new videoSubtitle\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'video_id'),
                    $this->fields->integer($name, 'subtitle_id')
                ]
            );
        }

        return $this->videoSubtitle;
    }

    public function phraseWord(): phraseWord\ITable
    {
        if (is_null($this->phraseWord)) {
            
            $name = 'phrase_word';
            
            $this->phraseWord = new phraseWord\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'phrase_id'),
                    $this->fields->integer($name, 'word_id')
                ]
            );
        }

        return $this->phraseWord;
    }
    
    public function original(): original\ITable
    {
        if (is_null($this->original)) {
            
            $name = 'original';
            
            $this->original = new original\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'language_id'),
                    $this->fields->integer($name, 'video_id'),
                    $this->fields->integer($name, 'audio_id'),
                    $this->fields->integer($name, 'subtitle_id'),
                    $this->fields->integer($name, 'clip_number')
                ]
            );
        }

        return $this->original;
    }

    public function foreign(): foreign\ITable
    {
        if (is_null($this->foreign)) {
            
            $name = 'foreign';
            
            $this->foreign = new foreign\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'original_id'),
                    $this->fields->integer($name, 'language_id'),
                    $this->fields->integer($name, 'video_id'),
                    $this->fields->integer($name, 'audio_id'),
                    $this->fields->integer($name, 'subtitle_id'),
                    $this->fields->integer($name, 'clip_number')
                ]
            );
        }

        return $this->foreign;
    }

    public function clip(): clip\ITable
    {
        if (is_null($this->clip)) {
            
            $name = 'clip';
            
            $this->clip = new clip\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'language_id'),
                    $this->fields->integer($name, 'phrase_id'),
                    $this->fields->integer($name, 'phrase_delay'),
                    $this->fields->integer($name, 'start'),
                    $this->fields->integer($name, 'stop'),
                    $this->fields->integer($name, 'duration'),
                    $this->fields->text($name, 'file')
                ]
            );
        }

        return $this->clip;
    }
    
    public function originalClip(): originalClip\ITable
    {
        if (is_null($this->originalClip)) {
            
            $name = 'original_clip';
            
            $this->originalClip = new originalClip\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'original_id'),
                    $this->fields->integer($name, 'clip_id')
                ]
            );
        }

        return $this->originalClip;
    }
    
    public function foreignClip(): foreignClip\ITable
    {
        if (is_null($this->foreignClip)) {
            
            $name = 'foreign_clip';
            
            $this->foreignClip = new foreignClip\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->integer($name, 'foreign_id'),
                    $this->fields->integer($name, 'clip_id')
                ]
            );
        }

        return $this->foreignClip;
    }
    
    public function authentication(): authentication\ITable
    {
        if (is_null($this->authentication)) {
            
            $name = 'authentication';
            
            $this->authentication = new authentication\Table(
                $this->renderer,
                $name,
                $this->fields->integer($name, 'id'),
                [
                    $this->fields->text($name, 'email'),
                    $this->fields->text($name, 'password'),
                    $this->fields->date($name, 'restore_date'),
                    $this->fields->text($name, 'restore_hash'),
                    $this->fields->text($name, 'confirm_hash'),
                    $this->fields->date($name, 'registration_date')
                ]
            );
        }

        return $this->authentication;
    }
}

