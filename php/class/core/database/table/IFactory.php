<?php
namespace core\database\table;
interface IFactory
{
    public function source(): source\ITable;
    public function language(): language\ITable;
    public function video(): video\ITable;
    public function audio(): audio\ITable;
    public function subtitle(): subtitle\ITable;
    public function phrase(): phrase\ITable;
    public function word(): word\ITable;
    public function videoAudio(): videoAudio\ITable;
    public function videoSubtitle(): videoSubtitle\ITable;
    public function phraseWord(): phraseWord\ITable;
    public function original(): original\ITable;
    public function foreign(): foreign\ITable;
    public function clip(): clip\ITable;
    public function originalClip(): originalClip\ITable;
    public function foreignClip(): foreignClip\ITable;
    public function authentication(): authentication\ITable;
}

