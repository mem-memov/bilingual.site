<?php
namespace core\database\table;
interface ITable
{
    public function name(): string;
    public function id(): string;
    public function idIs(int $id): string;
    public function idIn(array $values): string;
    public function fields(): string;
    public function fieldsWithId(): string;
    public function specifications(): array;
    public function values(array $values): string;
    public function conditions(array $filters): string;
    public function sorters(array $isAscending): string;
    public function leftJoin(ITable $table): string;
    public function innerJoin(ITable $table): string;
    public function fullJoin(ITable $table): string;
}

