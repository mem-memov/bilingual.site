<?php
namespace core\database\table;
use core\service\sql\IRenderer;
use core\database\field\IField;
class Table implements ITable
{
    protected $name;
    protected $id;
    protected $fields;
    
    public function __construct(
        IRenderer $renderer,
        string $name,
        IField $id,
        array $fields
    )
    {
        $this->renderer = $renderer;
        $this->name = $name;
        $this->id = $id;
        $this->fields = $fields;
    }
    
    public function name(): string
    {
        return $this->renderer->asTable($this->name);
    }

    public function id(): string
    {
        return $this->id->name();
    }
    
    public function idIs(int $id): string
    {
        return $this->id->is($id);
    }
    
    public function idIn(array $values): string
    {
        return $this->id->among($values);
    }
    
    public function fields(): string
    {
        $names = [];
        
        foreach ($this->fields as $field) {
            $names[] = $field->name();
        }
        
        return ' ' . implode(', ', $names) . ' ';
    }
    
    public function fieldsWithId(): string
    {
        return $this->id() . ', ' . $this->fields();
    }

    public function specifications(): array
    {
        return array_fill(0, count($this->fields), null);
    }
    
    public function values(array $values): string
    {
        $preparedValues = [];
        
        foreach ($this->fields as $index => $field) {
            $preparedValues[] = $field->value($values[$index]);
        }  
        
        return ' ' . implode(', ', $preparedValues) . ' ';
    }
    
    public function conditions(array $filters): string
    {
        $conditions = [];
 
        foreach ($this->fields as $index => $field) {
            if (!is_null($filters[$index])) {
                $conditions[] = $filters[$index]->select($field);
            }
        }

        return ' ' . implode(' AND ', $conditions) . ' ';
    }
    
    public function assignments(array $values): string
    {
        $assignments = [];
        
        foreach ($this->fields as $index => $field) {
            $assignments[] = $field->set($values[$index]);
        }  
        
        return ' ' . implode(', ', $assignments) . ' ';
    }
    
    public function sorters(array $isAscending): string
    {
        $sorters = [];
        
        foreach ($this->fields as $index => $field) {
            switch ($isAscending[$index]) {
                case null:
                    break;
                case true:
                    $sorters[] = $field->name() . ' ASC';
                    break;
                case false:
                    $sorters[] = $field->name() . ' DESC';
                    break;
                default:
                    throw new \Exception('unknown sorter');
                    break;
            }
        }
        
        return ' ' . implode(', ', $sorters) . ' ';
    }
    
    public function leftJoin(ITable $table): string
    {
        return ' LEFT OUTER JOIN ' . $table->name() . ' ON(' . $table->id() . ' = ' . $this->id() . ') ';
    }

    public function innerJoin(ITable $table): string
    {
        return ' INNER JOIN ' . $table->name() . ' ON(' . $table->id() . ' = ' . $this->id() . ') ';
    }
    
    public function fullJoin(ITable $table): string
    {
        return ' FULL OUTER JOIN ' . $table->name() . ' ON(' . $table->id() . ' = ' . $this->id() . ') ';
    }
}

