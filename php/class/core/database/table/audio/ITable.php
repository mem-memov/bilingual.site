<?php
namespace core\database\table\audio;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function sourceId();
    public function languageId();
    public function file();
    public function duration();
}
