<?php
namespace core\database\table\audio;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function sourceId() { return 0; }
    public function languageId() { return 1; }
    public function file() { return 2; }
    public function duration() { return 3; }
}
