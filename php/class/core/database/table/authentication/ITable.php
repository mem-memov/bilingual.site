<?php
namespace core\database\table\authentication;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function email();
    public function password();
    public function restoreDate();
    public function restoreHash();
    public function confirmHash();
    public function registrationDate();
}
