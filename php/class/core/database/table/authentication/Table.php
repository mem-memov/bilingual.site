<?php
namespace core\database\table\authentication;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function email() { return 0; }
    public function password() { return 1; }
    public function restoreDate() { return 2; }
    public function restoreHash() { return 3; }
    public function confirmHash() { return 4; }
    public function registrationDate() { return 5; }
}
