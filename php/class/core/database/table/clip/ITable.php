<?php
namespace core\database\table\clip;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function languageId();
    public function phraseId();
    public function phraseDelay();
    public function start();
    public function stop();
    public function duration();
    public function file();
}
