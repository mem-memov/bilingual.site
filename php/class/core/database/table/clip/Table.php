<?php
namespace core\database\table\clip;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function languageId() { return 0; }
    public function phraseId() { return 1; }
    public function phraseDelay() { return 2; }
    public function start() { return 3; }
    public function stop() { return 4; }
    public function duration() { return 5; }
    public function file() { return 6; }
}
