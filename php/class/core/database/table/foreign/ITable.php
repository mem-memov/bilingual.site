<?php
namespace core\database\table\foreign;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function originalId();
    public function languageId();
    public function videoId();
    public function audioId();
    public function subtitleId();
    public function clipNumber();
}
