<?php
namespace core\database\table\foreign;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function originalId() { return 0; }
    public function languageId() { return 1; }
    public function videoId() { return 2; }
    public function audioId() { return 3; }
    public function subtitleId() { return 4; }
    public function clipNumber() { return 5; }
}
