<?php
namespace core\database\table\foreignClip;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function foreignId();
    public function clipId();
}
