<?php
namespace core\database\table\foreignClip;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function foreignId() { return 0; }
    public function clipId() { return 1; }
}
