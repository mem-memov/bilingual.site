<?php
namespace core\database\table\language;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function language();
}
