<?php
namespace core\database\table\original;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function languageId() { return 0; }
    public function videoId() { return 1; }
    public function audioId() { return 2; }
    public function subtitleId() { return 3; }
    public function clipNumber() { return 4; }
}
