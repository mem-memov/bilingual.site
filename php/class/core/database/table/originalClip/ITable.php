<?php
namespace core\database\table\originalClip;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function originalId();
    public function clipId();
}
