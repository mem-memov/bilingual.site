<?php
namespace core\database\table\originalClip;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function originalId() { return 0; }
    public function clipId() { return 1; }
}
