<?php
namespace core\database\table\phrase;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function subtitleId();
    public function languageId();
    public function start();
    public function stop();
    public function phrase();
    public function orderNumber();
}
