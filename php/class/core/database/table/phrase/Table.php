<?php
namespace core\database\table\phrase;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function languageId() { return 0; }
    public function subtitleId() { return 1; }
    public function start() { return 2; }
    public function stop() { return 3; }
    public function phrase() { return 4; }
    public function orderNumber() { return 5; }
}
