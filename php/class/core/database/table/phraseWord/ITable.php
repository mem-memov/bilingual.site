<?php
namespace core\database\table\phraseWord;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function phraseId();
    public function wordId();
}
