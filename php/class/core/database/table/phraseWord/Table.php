<?php
namespace core\database\table\phraseWord;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function phraseId() { return 0; }
    public function wordId() { return 1; }
}
