<?php
namespace core\database\table\source;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function file();
}
