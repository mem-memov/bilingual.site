<?php
namespace core\database\table\subtitle;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function sourceId();
    public function languageId();
    public function file();
    public function phraseNumber();
}
