<?php
namespace core\database\table\subtitle;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function sourceId() { return 0; }
    public function languageId() { return 1; }
    public function file() { return 2; }
    public function phraseNumber() { return 3; }
}
