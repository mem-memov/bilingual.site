<?php
namespace core\database\table\video;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function sourceId();
    public function languageId();
    public function file();
    public function width();
    public function height();
    public function duration();
}
