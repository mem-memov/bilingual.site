<?php
namespace core\database\table\video;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function sourceId() { return 0; }
    public function languageId() { return 1; }
    public function file() { return 2; }
    public function width() { return 3; }
    public function height() { return 4; }
    public function duration() { return 5; }
}
