<?php
namespace core\database\table\videoAudio;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function videoId();
    public function audioId();
}
