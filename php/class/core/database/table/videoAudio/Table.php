<?php
namespace core\database\table\videoAudio;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function videoId() { return 0; }
    public function audioId() { return 1; }
}
