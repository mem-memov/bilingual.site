<?php
namespace core\database\table\videoSubtitle;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function videoId() { return 0; }
    public function subtitleId() { return 1; }
}
