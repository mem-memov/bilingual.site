<?php
namespace core\database\table\word;
use core\database\table\ITable as IBaseTable;
interface ITable extends IBaseTable
{
    public function languageId();
    public function word();
}
