<?php
namespace core\database\table\word;
use core\database\table\Table as BaseTable;
class Table extends BaseTable implements ITable
{
    public function languageId() { return 0; }
    public function word() { return 1; }
}
