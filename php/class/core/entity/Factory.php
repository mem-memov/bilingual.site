<?php
namespace core\entity;
abstract class Factory implements IFactory
{
    public function many(array $rows): array
    {
        $entities = [];
        
        foreach ($rows as $row) {
            $entities[] = $this->one($row);
        }
        
        return $entities;
    }
}
