<?php
namespace core\entity;
interface ICommand 
{
    public function execute();
}
