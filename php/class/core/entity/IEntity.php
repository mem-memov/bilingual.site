<?php
namespace core\entity;
interface IEntity
{
    public function id(): int;
}
