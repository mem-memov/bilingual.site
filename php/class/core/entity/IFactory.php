<?php
namespace core\entity;
interface IFactory
{
    public function one(array $row): IEntity;
    public function many(array $rows): array;
}
