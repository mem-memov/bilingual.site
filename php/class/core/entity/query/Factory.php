<?php
namespace core\entity\query;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
abstract class Factory
{
    protected $database;
    protected $filters;
    
    private $crud;
    private $oneToMany;
    private $manyToMany;
    private $filter;

    public function __construct(
        IDatabase $database,
        IFilterFactory $filters
    ) {
        $this->database = $database;
        $this->filters = $filters;
        
        $this->crud = null;
        $this->oneToMany = null;
        $this->manyToMany = null;
        $this->filter = null;
    }
    
    protected function crud(): crud\IFactory
    {
        if (is_null($this->crud)) {
            $this->crud = new crud\Factory($this->database, $this->filters);
        }
        
        return $this->crud;
    }
    
    protected function oneToMany(): oneToMany\IFactory
    {
        if (is_null($this->oneToMany)) {
            $this->oneToMany = new oneToMany\Factory($this->database, $this->filters);
        }
        
        return $this->oneToMany;
    }
    
    protected function manyToMany(): manyToMany\IFactory
    {
        if (is_null($this->manyToMany)) {
            $this->manyToMany = new manyToMany\Factory($this->database, $this->filters);
        }
        
        return $this->manyToMany;
    }
    
    protected function filter(): filter\IFactory
    {
        if (is_null($this->filter)) {
            $this->filter = new filter\Factory($this->database, $this->filters);
        }
        
        return $this->filter;
    }
}
