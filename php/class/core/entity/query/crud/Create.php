<?php
namespace core\entity\query\crud;
use core\database\query\Insert;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
class Create extends Insert
{
    private $table;
    private $values;
    
    public function __construct(
        IDatabase $database,
        ITable $table,
        array $values
    ) {
        parent::__construct($database);
        $this->table = $table;
        $this->values = $values;
    }

    public function query(): string
    {
        $table = $this->table->name();
        $fields = $this->table->fields();
        $values = $this->table->values($this->values);
        
        return 'INSERT INTO ' . $table . ' (' . $fields . ') VALUES (' . $values . ');';
    }
}
