<?php
namespace core\entity\query\crud;
use core\database\query\Delete as BaseDelete;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
class Delete extends BaseDelete
{
    private $table;
    private $id;
    
    public function __construct(
        IDatabase $database,
        ITable $table,
        int $id
    ) {
        parent::__construct($database);
        $this->table = $table;
        $this->id = $id;
    }
    
    public function query(): string
    {
        $table = $this->table->name();
        $condition = $this->table->idIs($this->id);
        
        return 'DELETE FROM ' . $table . ' WHERE ' . $condition . ';';
    }
}
