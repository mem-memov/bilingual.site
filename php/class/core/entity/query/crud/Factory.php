<?php
namespace core\entity\query\crud;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\ITable;
use core\database\query\IInsert;
use core\database\query\IDelete;
use core\database\query\ISelect;
use core\database\query\IUpdate;
class Factory implements IFactory
{
    protected $database;
    protected $filters;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters
    ) {
        $this->database = $database;
        $this->filters = $filters;
    }

    public function create(ITable $table, array $values): IInsert
    {
        return new Create(
            $this->database,
            $table,
            $values
        );
    }
    
    public function read(ITable $table, int $id): ISelect
    {
        return new Read(
            $this->database,
            $table,
            $id
        );
    }

    public function update(ITable $table, int $id, array $values): IUpdate
    {
        return new Update(
            $this->database,
            $table,
            $id,
            $values
        );
    }
    
    public function delete(ITable $table, int $id): IDelete
    {
        return new Delete(
            $this->database,
            $table,
            $id
        );
    }
}
