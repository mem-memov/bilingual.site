<?php
namespace core\entity\query\crud;
use core\database\table\ITable;
use core\database\query\IInsert;
use core\database\query\IDelete;
use core\database\query\ISelect;
use core\database\query\IUpdate;
interface IFactory
{
    public function create(ITable $table, array $values): IInsert;
    public function read(ITable $table, int $id): ISelect;
    public function update(ITable $table, int $id, array $values): IUpdate;
    public function delete(ITable $table, int $id): IDelete;
}
