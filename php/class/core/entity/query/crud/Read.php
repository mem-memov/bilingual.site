<?php
namespace core\entity\query\crud;
use core\database\query\Select;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
class Read extends Select
{
    private $table;
    private $id;
    
    public function __construct(
        IDatabase $database,
        ITable $table,
        int $id
    ) {
        parent::__construct($database);
        $this->table = $table;
        $this->id = $id;
    }

    public function query(): string
    {
        $table = $this->table->name();
        $fields = $this->table->fieldsWithId();
        $condition = $this->table->idIs($this->id);
        
        return 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE ' . $condition . ' LIMIT 1;';
    }
}
