<?php
namespace core\entity\query\crud;
use core\database\query\Update as BaseUpdate;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
class Update extends BaseUpdate
{
    private $table;
    private $id;
    private $values;
    
    public function __construct(
        IDatabase $database,
        ITable $table,
        int $id,
        array $values
    ) {
        parent::__construct($database);
        $this->table = $table;
        $this->id = $id;
        $this->values = $values;
    }

    public function query(): string
    {
        $table = $this->table->name();
        $assignments = $this->table->assignments($this->values);
        $condition = $this->table->idIs($this->id);
        
        return 'UPDATE ' . $table . ' SET ' . $assignments . ' WHERE ' . $condition . ';';
    }
}
