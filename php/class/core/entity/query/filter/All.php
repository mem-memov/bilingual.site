<?php
namespace core\entity\query\filter;
use core\database\query\Select;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
class All extends Select
{
    private $records;
    
    public function __construct(
        IDatabase $database,
        ITable $records
    ) {
        parent::__construct($database);
        $this->records = $records;
    }

    public function query(): string
    {
        $table = $this->records->name();
        $fields = $this->records->fieldsWithId();
        
        return 'SELECT ' . $fields . ' FROM ' . $table . ';';
    }
}
