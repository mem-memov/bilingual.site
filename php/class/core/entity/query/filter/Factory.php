<?php
namespace core\entity\query\filter;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\ITable;
use core\database\query\ISelect;
class Factory implements IFactory
{
    protected $database;
    protected $filters;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters
    ) {
        $this->database = $database;
        $this->filters = $filters;
    }
    
    public function all(ITable $records): ISelect
    {
        return new All(
            $this->database,
            $records
        );
    }
    
    public function records(ITable $records, array $filters): ISelect
    {
        return new Records(
            $this->database,
            $records,
            $filters
        );
    }
    
    public function orderedRecords(ITable $records, array $filters, array $isAscending): ISelect
    {
        return new OrderedRecords(
            $this->database,
            $records,
            $filters,
            $isAscending
        );
    }
}
