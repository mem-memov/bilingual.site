<?php
namespace core\entity\query\filter;
use core\database\table\ITable;
use core\database\query\ISelect;
interface IFactory
{
    public function all(ITable $records): ISelect;
    public function records(ITable $records, array $filters): ISelect;
    public function orderedRecords(ITable $records, array $filters, array $isAscending): ISelect;
}
