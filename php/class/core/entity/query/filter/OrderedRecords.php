<?php
namespace core\entity\query\filter;
use core\database\query\Select;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
class OrderedRecords extends Select
{
    private $records;
    private $filters;
    private $isAscendings;
    
    public function __construct(
        IDatabase $database,
        ITable $records,
        array $filters,
        array $isAscending
    ) {
        parent::__construct($database);
        $this->records = $records;
        $this->filters = $filters;
        $this->isAscending = $isAscending;
    }

    public function query(): string
    {
        $table = $this->records->name();
        $fields = $this->records->fieldsWithId();
        $condition = $this->records->conditions($this->filters);
        $sorter = $this->records->sorters($this->isAscending);
        
        return 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE ' . $condition . ' ORDER BY ' . $sorter . ';';
    }
}
