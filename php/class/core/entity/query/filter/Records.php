<?php
namespace core\entity\query\filter;
use core\database\query\Select;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
class Records extends Select
{
    private $records;
    private $filters;
    
    public function __construct(
        IDatabase $database,
        ITable $records,
        array $filters
    ) {
        parent::__construct($database);
        $this->records = $records;
        $this->filters = $filters;
    }

    public function query(): string
    {
        $table = $this->records->name();
        $fields = $this->records->fieldsWithId();
        $condition = $this->records->conditions($this->filters);
        
        return 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE ' . $condition . ';';
    }
}
