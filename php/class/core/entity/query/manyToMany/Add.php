<?php
namespace core\entity\query\manyToMany;
use core\database\query\Insert;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
class Add extends Insert
{
    private $relations;
    private $containerFieldIndex;
    private $containerId;
    private $itemFieldIndex;
    private $itemId;
    
    public function __construct(
        IDatabase $database,
        ITable $relations,
        int $containerFieldIndex,
        int $containerId,
        int $itemFieldIndex,
        int $itemId
    ) {
        parent::__construct($database);
        
        $this->relations = $relations;
        $this->containerFieldIndex = $containerFieldIndex;
        $this->containerId = $containerId;
        $this->itemFieldIndex = $itemFieldIndex;
        $this->itemId = $itemId;
    }

    public function query(): string
    {
        $table = $this->relations->name();
        $fields = $this->relations->fields();
        
        $ids = $this->relations->specifications();
        $ids[$this->containerFieldIndex] = $this->containerId;
        $ids[$this->itemFieldIndex] = $this->itemId;
        $values = $this->relations->values($ids);
        
        return 'INSERT INTO ' . $table . ' (' . $fields . ') VALUES (' . $values . ');';
    }
}
