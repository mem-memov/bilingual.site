<?php
namespace core\entity\query\manyToMany;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\ITable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
class Factory implements IFactory
{
    protected $database;
    protected $filters;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters
    ) {
        $this->database = $database;
        $this->filters = $filters;
    }
    
    public function add(ITable $relations, int $containerFieldIndex, int $containerId, int $itemFieldIndex, int $itemId): IInsert
    {
        return new Add(
            $this->database,
            $relations,
            $containerFieldIndex,
            $containerId,
            $itemFieldIndex,
            $itemId
        );
    }

    public function remove(ITable $relations, int $containerFieldIndex, int $containerId, int $itemFieldIndex, int $itemId): IDelete
    {
        return new Remove(
            $this->database,
            $this->filters,
            $relations,
            $containerFieldIndex,
            $containerId,
            $itemFieldIndex,
            $itemId
        );
    }

    public function items(ITable $items, ITable $relations, int $containerFieldIndex, int $containerId): ISelect
    {
        return new Items(
            $this->database,
            $this->filters,
            $items,
            $relations,
            $containerFieldIndex,
            $containerId
        );
    }
    
    public function filteredOrderedItems(
        ITable $items, 
        ITable $relations, 
        int $containerFieldIndex, 
        int $containerId, 
        int $sortedFieldIndex, 
        bool $isAscending,
        string $condition
    ): ISelect
    {
        return new FilteredOrderedItems(
            $this->database,
            $this->filters,
            $items,
            $relations,
            $containerFieldIndex,
            $containerId,
            $sortedFieldIndex,
            $isAscending,
            $condition
        );
    }
}
