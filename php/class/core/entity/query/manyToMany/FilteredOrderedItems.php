<?php
namespace core\entity\query\manyToMany;
use core\database\query\Select;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\ITable;
class FilteredOrderedItems extends Select
{
    private $filters;
    private $items;
    private $relations;
    private $containerFieldIndex;
    private $containerId;
    private $sortedFieldIndex;
    private $isAscending;
    private $condition;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        ITable $items,
        ITable $relations,
        int $containerFieldIndex,
        int $containerId,
        int $sortedFieldIndex,
        bool $isAscending,
        string $condition
    ) {
        parent::__construct($database);
        
        $this->filters = $filters;
        $this->items = $items;
        $this->relations = $relations;
        $this->containerFieldIndex = $containerFieldIndex;
        $this->containerId = $containerId;
        $this->sortedFieldIndex = $sortedFieldIndex;
        $this->isAscending = $isAscending;
        $this->condition = $condition;
    }
    
    public function query(): string
    {
        $table = $this->items->name();
        $fields = $this->items->fieldsWithId();
        $join = $this->items->leftJoin($this->relations);
        
        $filters = $this->items->specifications();
        $filters[$this->containerFieldIndex] = $this->filters->is($this->containerId);
        $condition = $this->relations->conditions($filters) . ' AND (' . $this->condition . ') ';
        
        $isAscending = $this->items->specifications();
        $isAscending[$this->sortedFieldIndex] = $this->isAscending;
        $sorter = $this->items->sorters($isAscending);

        return 'SELECT ' . $fields . ' FROM ' . $table . ' ' . $join . ' WHERE ' . $condition . ' ORDER BY ' . $sorter . ';';
    }
}
