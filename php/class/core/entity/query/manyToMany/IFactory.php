<?php
namespace core\entity\query\manyToMany;
use core\database\table\ITable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
interface IFactory
{
    public function add(ITable $relations, int $containerFieldIndex, int $containerId, int $itemFieldIndex, int $itemId): IInsert;
    
    public function remove(ITable $relations, int $containerFieldIndex, int $containerId, int $itemFieldIndex, int $itemId): IDelete;
    
    public function items(ITable $items, ITable $relations, int $containerFieldIndex, int $containerId): ISelect;
    
    public function filteredOrderedItems(
        ITable $items, 
        ITable $relations, 
        int $containerFieldIndex, 
        int $containerId, 
        int $sortedFieldIndex, 
        bool $isAscending,
        string $condition
    ): ISelect;
}
