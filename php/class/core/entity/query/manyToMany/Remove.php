<?php
namespace core\entity\query\manyToMany;
use core\database\query\Delete;
use core\service\sql\IQuery as IDatabase;
use core\database\table\ITable;
use core\database\filter\IFactory as IFilterFactory;
class Remove extends Delete
{
    private $filters;
    private $relations;
    private $containerFieldIndex;
    private $containerId;
    private $itemFieldIndex;
    private $itemId;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        ITable $relations,
        int $containerFieldIndex,
        int $containerId,
        int $itemFieldIndex,
        int $itemId
    ) {
        parent::__construct($database);
        
        $this->filters = $filters;
        $this->relations = $relations;
        $this->containerFieldIndex = $containerFieldIndex;
        $this->containerId = $containerId;
        $this->itemFieldIndex = $itemFieldIndex;
        $this->itemId = $itemId;
    }
    
    public function query(): string
    {
        $table = $this->relations->name();
        
        $filters = $this->relations->specifications();
        $filters[$this->containerFieldIndex] = $this->filters->is($this->containerId);
        $filters[$this->itemFieldIndex] = $this->filters->is($this->itemId);
        $condition = $this->relations->conditions($filters);

        return 'DELETE FROM ' . $table . ' WHERE ' . $condition . ';';
    }
}
