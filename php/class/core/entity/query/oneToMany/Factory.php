<?php
namespace core\entity\query\oneToMany;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\ITable;
use core\database\query\ISelect;
class Factory implements IFactory
{
    protected $database;
    protected $filters;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters
    ) {
        $this->database = $database;
        $this->filters = $filters;
    }
    
    public function items(ITable $items, int $containerFieldIndex, int $containerId): ISelect
    {
        return new Items(
            $this->database,
            $this->filters,
            $items,
            $containerFieldIndex,
            $containerId
        );
    }

    public function orderedItems(ITable $items, int $containerFieldIndex, int $containerId, int $sortedFieldIndex, bool $isAscending): ISelect
    {
        return new OrderedItems(
            $this->database,
            $this->filters,
            $items,
            $containerFieldIndex,
            $containerId,
            $sortedFieldIndex,
            $isAscending
        );
    }

    public function itemAtPosition(ITable $items, int $containerFieldIndex, int $containerId, int $positionFieldIndex, int $position): ISelect
    {
        return new ItemAtPosition(
            $this->database,
            $this->filters,
            $items,
            $containerFieldIndex,
            $containerId,
            $positionFieldIndex,
            $position
        );
    }
}
