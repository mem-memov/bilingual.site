<?php
namespace core\entity\query\oneToMany;
use core\database\table\ITable;
use core\database\query\ISelect;
interface IFactory
{
    public function items(ITable $items, int $containerFieldIndex, int $containerId): ISelect;
    public function orderedItems(ITable $items, int $containerFieldIndex, int $containerId, int $sortedFieldIndex, bool $isAscending): ISelect;
    public function itemAtPosition(ITable $items, int $containerFieldIndex, int $containerId, int $positionFieldIndex, int $position): ISelect;
}
