<?php
namespace core\entity\query\oneToMany;
use core\database\query\Select;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\ITable;
class ItemAtPosition extends Select
{
    private $filters;
    private $items;
    private $containerId;
    private $containerFieldIndex;
    private $positionFieldIndex;
    private $position;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        ITable $items,
        int $containerFieldIndex,
        int $containerId,
        int $positionFieldIndex,
        int $position
    ) {
        parent::__construct($database);
        
        $this->filters = $filters;
        $this->items = $items;
        $this->containerFieldIndex = $containerFieldIndex;
        $this->containerId = $containerId;
        $this->positionFieldIndex = $positionFieldIndex;
        $this->position  = $position;
    }
    
    public function query(): string
    {
        $table = $this->items->name();
        $fields = $this->items->fieldsWithId();
        
        $filters = $this->items->specifications();
        $filters[$this->containerFieldIndex] = $this->filters->is($this->containerId);
        $filters[$this->positionFieldIndex] = $this->filters->is($this->position);
        $condition = $this->items->conditions($filters);

        return 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE ' . $condition . ';';
    }
}
