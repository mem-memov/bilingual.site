<?php
namespace core\entity\query\oneToMany;
use core\database\query\Select;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\ITable;
class Items extends Select
{
    private $filters;
    private $items;
    private $containerFieldIndex;
    private $containerId;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        ITable $items,
        int $containerFieldIndex,
        int $containerId
    ) {
        parent::__construct($database);
        
        $this->filters = $filters;
        $this->items = $items;
        $this->containerFieldIndex = $containerFieldIndex;
        $this->containerId = $containerId;
    }
    
    public function query(): string
    {
        $table = $this->items->name();
        $fields = $this->items->fieldsWithId();
         
        $filters = $this->items->specifications();
        $filters[$this->containerFieldIndex] = $this->filters->is($this->containerId);
        $condition = $this->items->conditions($filters);

        return 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE ' . $condition . ';';
    }
}
