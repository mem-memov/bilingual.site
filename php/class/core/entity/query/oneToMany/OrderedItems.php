<?php
namespace core\entity\query\oneToMany;
use core\database\query\Select;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\ITable;
class OrderedItems extends Select
{
    private $filters;
    private $items;
    private $containerFieldIndex;
    private $containerId;
    private $sortedFieldIndex;
    private $isAscending;
    
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        ITable $items,
        int $containerFieldIndex,
        int $containerId,
        int $sortedFieldIndex,
        bool $isAscending
    ) {
        parent::__construct($database);
        
        $this->filters = $filters;
        $this->items = $items;
        $this->containerFieldIndex = $containerFieldIndex;
        $this->containerId = $containerId;
        $this->sortedFieldIndex = $sortedFieldIndex;
        $this->isAscending = $isAscending;
    }
    
    public function query(): string
    {
        $table = $this->items->name();
        $fields = $this->items->fieldsWithId();
        
        $filters = $this->items->specifications();
        $filters[$this->containerFieldIndex] = $this->filters->is($this->containerId);
        $condition = $this->items->conditions($filters);
        
        $isAscending = $this->items->specifications();
        $isAscending[$this->sortedFieldIndex] = $this->isAscending;
        $sorter = $this->items->sorters($isAscending);

        return 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE ' . $condition . ' ORDER BY ' . $sorter . ';';
    }
}
