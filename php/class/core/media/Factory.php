<?php
namespace core\media;
use core\IFactory as ICore;
class Factory implements IFactory
{
    private $core;

    private $audio;
    private $clip;
    private $original;
    private $foreign;
    private $language;
    private $phrase;
    private $source;
    private $subtitle;
    private $video;
    private $word;

    public function __construct(
        ICore $core
    ) {
        $this->core = $core;

        $this->audio = null;
        $this->clip = null;
        $this->original = null;
        $this->foreign = null;
        $this->language = null;
        $this->phrase = null;
        $this->source = null;
        $this->subtitle = null;
        $this->video = null;
        $this->word = null;
    }
    
    public function audio(): audio\IRepository
    {
        if (is_null($this->audio)) {

            $query = new audio\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->audio(),
                $this->core->database()->tables()->videoAudio()
            );

            $command = new audio\command\Factory(
                $this->core
            );

            $factory = new audio\Factory(
                $this,
                $command
            );
            
            $this->audio = new audio\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->audio;
    }
    
    public function clip(): clip\IRepository
    {
        if (is_null($this->clip)) {

            $query = new clip\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->clip(),
                $this->core->database()->tables()->originalClip(),
                $this->core->database()->tables()->foreignClip()
            );

            $command = new clip\command\Factory(
                $this->core
            );

            $factory = new clip\Factory(
                $this,
                $command
            );

            $this->clip = new clip\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->clip;
    }
    
    public function original(): original\IRepository
    {
        if (is_null($this->original)) {

            $query = new original\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->original()
            );

            $command = new original\command\Factory();

            $factory = new original\Factory(
                $this,
                $command
            );

            $this->original = new original\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->original;
    }
    
    public function foreign(): foreign\IRepository
    {
        if (is_null($this->foreign)) {

            $query = new foreign\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->foreign()
            );

            $command = new foreign\command\Factory();

            $factory = new foreign\Factory(
                $this,
                $command
            );

            $this->foreign = new foreign\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->foreign;
    }
    
    public function language(): language\IRepository
    {
        if (is_null($this->language)) {

            $factory = new language\Factory();

            $query = new language\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->language()
            );

            $this->language = new language\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->language;
    }
    
    public function phrase(): phrase\IRepository
    {
        if (is_null($this->phrase)) {

            $query = new phrase\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->phrase(),
                $this->core->database()->tables()->phraseWord()
            );

            $command = new phrase\command\Factory(
                $this->core
            );

            $factory = new phrase\Factory(
                $this,
                $command
            );

            $this->phrase = new phrase\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->phrase;
    }

    public function source(): source\IRepository
    {
        if (is_null($this->source)) {

            $query = new source\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->source()
            );

            $command = new source\command\Factory(
                $this->core
            );

            $factory = new source\Factory(
                $this,
                $command
            );

            $this->source = new source\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->source;
    }
    
    public function subtitle(): subtitle\Repository
    {
        if (is_null($this->subtitle)) {

            $query = new subtitle\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->subtitle(),
                $this->core->database()->tables()->videoSubtitle()
            );

            $command = new subtitle\command\Factory(
                $this->core
            );

            $factory = new subtitle\Factory(
                $this,
                $command
            );

            $this->subtitle = new subtitle\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->subtitle;
    }
    
    public function video(): video\IRepository
    {
        if (is_null($this->video)) {

            $query = new video\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->video(),
                $this->core->database()->tables()->videoAudio(),
                $this->core->database()->tables()->videoSubtitle()
            );

            $command = new video\command\Factory(
                $this->core
            );

            $factory = new video\Factory(
                $this,
                $command
            );

            $this->video = new video\Repository(
                $factory,
                $query,
                $this->core->database()->migration()->recording()
            );
        }

        return $this->video;
    }

    public function word(): word\IRepository
    {
        if (is_null($this->word)) {

            $query = new word\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters(),
                $this->core->database()->tables()->word(),
                $this->core->database()->tables()->phraseWord()
            );

            $factory = new word\Factory(
                $this
            );

            $this->word = new word\Repository(
                $factory,
                $query,
                $this->core->service()->cache(),
                $this->core->database()->migration()->recording()
            );
        }

        return $this->word;
    }
}
