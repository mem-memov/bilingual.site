<?php
namespace core\media;
interface IFactory
{
    public function audio(): audio\IRepository;
    public function clip(): clip\IRepository;
    public function original(): original\IRepository;
    public function foreign(): foreign\IRepository;
    public function language(): language\IRepository;
    public function phrase(): phrase\IRepository;
    public function source(): source\IRepository;
    public function subtitle(): subtitle\Repository;
    public function video(): video\IRepository;
    public function word(): word\IRepository;
}
