<?php
namespace core\media\audio;
use core\media\source\IRepository as ISourceRepository;
use core\media\language\IRepository as ILanguageRepository;
use core\media\source\ISource;
use core\media\language\ILanguage;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IAudioStream;
class Audio implements IAudio
{
    private $commands;
    private $sources;
    private $languages;
    private $id;
    private $sourceId;
    private $languageId;
    private $file;
    private $duration;
    
    public function __construct(
        command\IFactory $commands,
        ISourceRepository $sources,
        ILanguageRepository $languages,
        int $id,
        int $sourceId,
        int $languageId,
        string $file,
        int $duration
    ) {
        $this->commands = $commands;
        $this->sources = $sources;
        $this->languages = $languages;
        $this->id = $id;
        $this->sourceId = $sourceId;
        $this->languageId = $languageId;
        $this->file = $file;
        $this->duration = $duration;        
    }
    
    public function id(): int
    {
        return $this->id;
    }

    public function source(): ISource
    {
        return $this->sources->read($this->sourceId);
    }
    
    public function language(): ILanguage
    {
        return $this->languages->read($this->languageId);
    }
    
    public function file(): string
    {
        return $this->file;
    }
    
    public function duration(): int
    {
        return $this->duration;
    }
    
    public function import(IMediaData $mediaData, IAudioStream $audioStream)
    {
        $this->commands->import($this, $mediaData, $audioStream);
    }
}
