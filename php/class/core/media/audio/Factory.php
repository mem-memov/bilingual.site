<?php
namespace core\media\audio;
use core\media\IFactory as IRepositoryFactory;
use core\entity\{IEntity, Factory as EntityFactory};
class Factory extends EntityFactory
{
    private $repositories;
    private $commands;
    
    public function __construct(
        IRepositoryFactory $repositories,
        command\IFactory $commands
    ) {
        $this->repositories = $repositories;
        $this->commands = $commands;
    }

    public function one(array $row): IEntity
    {
        return new Audio(
            $this->commands,
            $this->repositories->source(),
            $this->repositories->language(), 
            (int)$row['id'],
            (int)$row['source_id'],
            (int)$row['language_id'],
            (string)$row['file'],
            (int)$row['duration']
        );
    }
}
