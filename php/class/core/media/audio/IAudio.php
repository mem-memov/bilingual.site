<?php
namespace core\media\audio;
use core\entity\IEntity;
use core\media\source\ISource;
use core\media\language\ILanguage;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IAudioStream;
interface IAudio extends IEntity
{
    public function source(): ISource;
    public function language(): ILanguage;
    public function file(): string;
    public function duration(): int;
    public function import(IMediaData $mediaData, IAudioStream $audioStream);
}
