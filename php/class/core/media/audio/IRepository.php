<?php
namespace core\media\audio;
use core\media\source\ISource;
use core\media\language\ILanguage;
interface IRepository
{
    public function create(
        ISource $source,
        ILanguage $language,
        string $file,
        int $duration
    ): IAudio;
    
    public function read(int $id): IAudio;
    public function ofVideo(int $videoId): array;
    public function ofSource(int $sourceId): array;
}
