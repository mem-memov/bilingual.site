<?php
namespace core\media\audio;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\source\ISource;
use core\media\language\ILanguage;
class Repository implements IRepository
{
    private $factory;
    private $queries;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $queries,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->queries = $queries;
        $this->migration = $migration;
    }
    
    public function create(
        ISource $source,
        ILanguage $language,
        string $file,
        int $duration
    ): IAudio
    {
        $audios = $this->queries->byFile($file)->run();
        
        if (!empty($audios)) {
            throw new exception\RepeatedAudioCreation($file);
        }

        $create = $this->queries->create(
            $source->id(),
            $language->id(),
            $file,
            $duration
        );

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->queries->delete($id)->query()
        );
        
        return $this->read($id);
    }
    
    public function read(int $id): IAudio
    {
        $rows = $this->queries->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\AudioNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }

    public function ofVideo(int $videoId): array
    {
        return $this->factory->many(
            $this->queries->ofVideo($videoId)->run()
        );
    }

    public function ofSource(int $sourceId): array
    {
        return $this->factory->many(
            $this->queries->ofSource($sourceId)->run()
        );
    }
}
