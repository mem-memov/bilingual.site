<?php
namespace core\media\audio\command;
use core\IFactory as ICore;
use core\media\audio\IAudio;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IAudioStream;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICore $core
    ) {
        $this->core = $core;
    }
    
    public function import(IAudio $audio, IMediaData $mediaData, IAudioStream $audioStream)
    {
        $command = new Import(
            $audio,
            $mediaData,
            $audioStream,
            $this->core->configuration()->directory()->copy(),
            $this->core->service()->mediaEditor()
        );
        
        $command->execute();
    }
}
