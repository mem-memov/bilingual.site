<?php
namespace core\media\audio\command;
use core\media\audio\IAudio;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IAudioStream;
interface IFactory
{
    public function import(IAudio $audio, IMediaData $mediaData, IAudioStream $audioStream);
}
