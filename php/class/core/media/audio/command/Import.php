<?php
namespace core\media\audio\command;
use core\entity\ICommand;
use core\media\audio\IAudio;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IAudioStream;
use core\service\media\IEditor as IEditorService;
class Import implements ICommand
{
    private $audio;
    private $mediaData;
    private $audioStream;
    private $copyDirectory;
    private $editor;
    
    public function __construct(
        IAudio $audio,
        IMediaData $mediaData,
        IAudioStream $audioStream,
        string $copyDirectory,
        IEditorService $editor
    ) {
        $this->audio = $audio;
        $this->mediaData = $mediaData;
        $this->audioStream = $audioStream;
        $this->copyDirectory = $copyDirectory;
        $this->editor = $editor;
    }

    public function execute()
    {
        $target = $this->copyDirectory . '/' . $this->audio->file();

        if (!file_exists($target)) {

            $this->editor->extractAudio(
                $this->mediaData->file(),
                $target,
                $this->audioStream->index()
            );
        }
    }
}
