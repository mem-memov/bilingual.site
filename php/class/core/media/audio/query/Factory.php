<?php
namespace core\media\audio\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\audio\ITable as IAudioTable;
use core\database\table\videoAudio\ITable as IVideoAudioTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
class Factory extends EntityQueryFactory implements IFactory
{
    private $audio;
    private $videoAudio;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        IAudioTable $audio,
        IVideoAudioTable $videoAudio
    ) {
        parent::__construct($database, $filters);
        $this->audio = $audio;
        $this->videoAudio = $videoAudio;
    }
    
    public function create(
        int $sourceId, 
        int $languageId, 
        string $file, 
        int $duration
    ): IInsert
    {
        $values = $this->audio->specifications();
        
        $values[$this->audio->sourceId()] = $sourceId;
        $values[$this->audio->languageId()] = $languageId;
        $values[$this->audio->file()] = $file;
        $values[$this->audio->duration()] = $duration;

        return $this->crud()->create($this->audio, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->audio, $id);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->audio, $id);
    }
    
    public function byFile(string $file): ISelect
    {
        $filters = $this->audio->specifications();
        $filters[$this->audio->file()] = $this->filters->is($file);
        
        return $this->filter()->records($this->audio, $filters);
    }

    public function ofVideo(int $videoId): ISelect
    {
        return $this->manyToMany()->items(
            $this->audio, 
            $this->videoAudio,
            $this->videoAudio->videoId(),
            $videoId
        );
    }

    public function ofSource(int $sourceId): ISelect
    {
        return $this->oneToMany()->items(
            $this->audio,
            $this->audio->sourceId(),
            $sourceId
        );
    }
}
