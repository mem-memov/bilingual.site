<?php
namespace core\media\audio\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
interface IFactory
{
    public function create(
        int $sourceId, 
        int $languageId, 
        string $file, 
        int $duration
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function delete(int $id): IDelete;
    
    public function byFile(string $file): ISelect;
    
    public function ofVideo(int $videoId): ISelect;
    
    public function ofSource(int $sourceId): ISelect;
}
