<?php
namespace core\media\clip;
use core\media\language\IRepository as ILanguageRepository;
use core\media\phrase\IRepository as IPhraseRepository;
use core\media\language\ILanguage;
use core\media\phrase\IPhrase;
use core\media\video\IVideo;
use core\media\audio\IAudio;
class Clip implements IClip
{
    private $command;
    private $language;
    private $word;
    private $id;
    private $languageId;
    private $phraseId;
    private $phraseDelay;
    private $start;
    private $stop;
    private $duration;
    private $file;

    public function __construct(
        command\IFactory $command,
        ILanguageRepository $language,
        IPhraseRepository $phrase,
        int $id,
        int $languageId,
        int $phraseId,
        int $phraseDelay,
        int $start,
        int $stop,
        int $duration,
        string $file
    ) {
        $this->command = $command;
        $this->language = $language;
        $this->phrase = $phrase;
        $this->id = $id;
        $this->languageId = $languageId;
        $this->phraseId = $phraseId;
        $this->phraseDelay = $phraseDelay;
        $this->start = $start;
        $this->stop = $stop;
        $this->duration = $duration;
        $this->file = $file;
    }
    
    public function id(): int
    {
        return $this->id;
    }

    public function language(): ILanguage
    {
        return $this->language->read($this->languageId);
    }

    public function phrase(): IPhrase
    {
        return $this->phrase->read($this->phraseId);
    }

    public function phraseDelay(): int
    {
        return $this->phraseDelay;
    }

    public function start(): int
    {
        return $this->start;
    }
    
    public function stop(): int
    {
        return $this->stop;
    }
    
    public function duration(): int
    {
        return $this->duration;
    }
    
    public function file(): string
    {
        return $this->file;
    }
    
    public function cut(IVideo $video, IAudio $audio)
    {
        $this->command->cut($this, $video, $audio);
    }
}
