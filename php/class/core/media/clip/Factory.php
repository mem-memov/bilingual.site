<?php
namespace core\media\clip;
use core\media\IFactory as IRepositoryFactory;
use core\entity\{IEntity, Factory as EntityFactory};
class Factory extends EntityFactory
{
    private $repositories;
    private $commands;
    
    public function __construct(
        IRepositoryFactory $repositories,
        command\IFactory $commands
    ) {
        $this->repositories = $repositories;
        $this->commands = $commands;
    }

    public function one(array $row): IEntity
    {
        return new Clip(
            $this->commands,
            $this->repositories->language(),
            $this->repositories->phrase(),
            (int)$row['id'],
            (int)$row['language_id'],
            (int)$row['phrase_id'],
            (int)$row['phrase_delay'],
            (int)$row['start'],
            (int)$row['stop'],
            (int)$row['duration'],
            (string)$row['file']
        );
    }
}
