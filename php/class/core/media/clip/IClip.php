<?php
namespace core\media\clip;
use core\entity\IEntity;
use core\media\language\ILanguage;
use core\media\phrase\IPhrase;
use core\media\video\IVideo;
use core\media\audio\IAudio;
interface IClip extends IEntity
{
    public function language(): ILanguage;
    public function phrase(): IPhrase;
    public function phraseDelay(): int;
    public function start(): int;
    public function stop(): int;
    public function duration(): int;
    public function file(): string;
    public function cut(IVideo $video, IAudio $audio);
}
