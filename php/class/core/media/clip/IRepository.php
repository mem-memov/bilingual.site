<?php
namespace core\media\clip;
use core\media\language\ILanguage;
use core\media\phrase\IPhrase;
use core\media\original\IOriginal;
use core\media\foreign\IForeign;
interface IRepository
{
    public function create(
        ILanguage $language, 
        IPhrase $phrase, 
        int $phraseDelay, 
        int $start, 
        int $stop, 
        int $duration, 
        string $file
    ): IClip;
    
    public function read(int $id): IClip;
    
    public function ofOriginal(IOriginal $original, bool $isAscending, int $start, int $stop): array;
    
    public function ofForeign(IForeign $foreign, bool $isAscending, int $start, int $stop): array;   
}
