<?php
namespace core\media\clip;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\language\ILanguage;
use core\media\phrase\IPhrase;
use core\media\original\IOriginal;
use core\media\foreign\IForeign;
class Repository implements IRepository
{
    private $factory;
    private $queries;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $queries,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->queries = $queries;
        $this->migration = $migration;
    }
    
    public function create(
        ILanguage $language, 
        IPhrase $phrase, 
        int $phraseDelay, 
        int $start, 
        int $stop, 
        int $duration, 
        string $file
    ): IClip
    {
        $create = $this->queries->create(
            $language->id(),
            $phrase->id(),
            $phraseDelay,
            $start,
            $stop,
            $duration,
            $file
        );

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->queries->delete($id)->query()
        );
        
        return $this->read($id);
    }
    
    public function read(int $id): IClip
    {
        $rows = $this->queries->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\ClipNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function ofOriginal(IOriginal $original, bool $isAscending, int $start, int $stop): array
    {
        return $this->factory->many(
            $this->queries->ofOriginal(
                $original->id(),
                $isAscending,
                $start,
                $stop
            )->run()
        );
    }
    
    public function ofForeign(IForeign $foreign, bool $isAscending, int $start, int $stop): array
    {
        return $this->factory->many(
            $this->queries->ofForeign(
                $foreign->id(),
                $isAscending,
                $start,
                $stop
            )->run()
        );
    }
}
