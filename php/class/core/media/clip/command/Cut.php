<?php
namespace core\media\clip\command;
use core\entity\ICommand;
use core\media\clip\IClip;
use core\media\video\IVideo;
use core\media\audio\IAudio;
use core\service\media\IEditor as IEditorService;
class Cut implements ICommand
{
    private $clip;
    private $editor;
    private $copyDirectory;
    private $cutDirectory;
    private $video;
    private $audio;
    
    public function __construct(
        IClip $clip,
        IEditorService $editor,
        string $copyDirectory,
        string $cutDirectory,
        IVideo $video,
        IAudio $audio
    ) {
        $this->clip = $clip;
        $this->editor = $editor;
        $this->copyDirectory = $copyDirectory;
        $this->cutDirectory = $cutDirectory;
        $this->video = $video;
        $this->audio = $audio;
    }
    
    public function execute()
    {
        $height = 420;

        $targetDirectory = dirname($this->clip->file());
        if (!file_exists($this->cutDirectory . '/' . $targetDirectory)) {
            mkdir($this->cutDirectory . '/' . $targetDirectory, 0777, true);
        }
        
        $targetVideo = $targetDirectory . '/_' . $this->clip->id() . '_video.mp4';
        $targetAudio = $targetDirectory . '/_' . $this->clip->id() . '_audio.mp3';

        if (!file_exists($this->cutDirectory . '/' . $this->clip->file())) {

            $this->editor->cutVideo(
                $this->copyDirectory . '/' . $this->video->file(), 
                $this->cutDirectory . '/' . $targetVideo, 
                $this->clip->start(), 
                $this->clip->duration(),
                $height
            );

            $sourceAudio = $this->audio->file();

            $this->editor->cutAudio(
                $this->copyDirectory . '/' . $sourceAudio,
                $this->cutDirectory . '/' . $targetAudio, 
                $this->clip->start(), 
                $this->clip->duration()
            );

            $this->editor->mix(
                $this->cutDirectory . '/' . $targetVideo, 
                $this->cutDirectory . '/' . $targetAudio, 
                $this->cutDirectory . '/' . $this->clip->file()
            );

        }
        
        if (file_exists($this->cutDirectory . '/' . $targetVideo)) {
            unlink($this->cutDirectory . '/' . $targetVideo);
        }
        
        if (file_exists($this->cutDirectory . '/' . $targetAudio)) {
            unlink($this->cutDirectory . '/' . $targetAudio);
        }
    }
}
