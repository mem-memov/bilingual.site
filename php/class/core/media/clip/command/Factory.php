<?php
namespace core\media\clip\command;
use core\IFactory as ICore;
use core\media\clip\IClip;
use core\media\video\IVideo;
use core\media\audio\IAudio;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICore $core
    ) {
        $this->core = $core;
    }

    public function cut(IClip $clip, IVideo $video, IAudio $audio)
    {
        $action = new Cut(
            $clip, 
            $this->core->service()->mediaEditor(),
            $this->core->configuration()->directory()->copy(),
            $this->core->configuration()->directory()->cut(),
            $video,
            $audio
        );
        
        $action->execute();
    }
}
