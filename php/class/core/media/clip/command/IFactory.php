<?php
namespace core\media\clip\command;
use core\media\clip\IClip;
use core\media\video\IVideo;
use core\media\audio\IAudio;
interface IFactory
{
    public function cut(IClip $clip, IVideo $video, IAudio $audio);
}
