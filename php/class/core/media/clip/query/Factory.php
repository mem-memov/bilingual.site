<?php
namespace core\media\clip\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\clip\ITable as IClipTable;
use core\database\table\originalClip\ITable as IOriginalClipTable;
use core\database\table\foreignClip\ITable as IForeignClipTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
class Factory extends EntityQueryFactory implements IFactory
{
    private $clip;
    private $originalClip;
    private $foreignClip;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        IClipTable $clip,
        IOriginalClipTable $originalClip,
        IForeignClipTable $foreignClip
    ) {
        parent::__construct($database, $filters);
        $this->clip = $clip;
        $this->originalClip = $originalClip;
        $this->foreignClip = $foreignClip;
    }
    
    public function create(
        int $languageId, 
        int $phraseId, 
        int $phraseDelay, 
        int $start, 
        int $stop, 
        int $duration, 
        string $file
    ): IInsert 
    {
        $values = $this->clip->specifications();
        
        $values[$this->clip->languageId()] = $languageId;
        $values[$this->clip->phraseId()] = $phraseId;
        $values[$this->clip->start()] = $start;
        $values[$this->clip->stop()] = $stop;
        $values[$this->clip->stop()] = $stop;
        $values[$this->clip->duration()] = $duration;
        $values[$this->clip->file()] = $file;

        return $this->crud()->create($this->clip, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->clip, $id);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->clip, $id);
    }
    
    public function ofOriginal(
        int $originalId, 
        bool $isAscending,
        int $start, 
        int $stop
    ): ISelect
    {
        $filters = $this->clip->specifications();
        
        $filters[$this->clip->start()] = $this->filters->all([
            $this->filters->above($start), 
            $this->filters->below($stop)
        ]);
        
        $condition = $this->clip->conditions($filters);

        return $this->manyToMany()->filteredOrderedItems(
            $this->clip, 
            $this->originalClip, 
            $this->originalClip->originalId(), 
            $originalId, 
            $this->clip->start(),
            $isAscending,
            $condition
        );
    }
    
    public function ofForeign(
        int $foreignId, 
        bool $isAscending,
        int $start, 
        int $stop
    ): ISelect
    {
        $filters = $this->clip->specifications();
        
        $filters[$this->clip->start()] = $this->filters->all([
            $this->filters->above($start), 
            $this->filters->below($stop)
        ]);
        
        $condition = $this->clip->conditions($filters);

        return $this->manyToMany()->filteredOrderedItems(
            $this->clip, 
            $this->foreignClip, 
            $this->foreignClip->foreignId(), 
            $foreignId, 
            $this->clip->start(),
            $isAscending,
            $condition
        );
    }
}
