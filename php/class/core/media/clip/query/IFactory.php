<?php
namespace core\media\clip\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
interface IFactory
{
    public function create(
        int $languageId, 
        int $phraseId, 
        int $phraseDelay, 
        int $start, 
        int $stop, 
        int $duration, 
        string $file
    ): IInsert;
     
    public function read(int $id): ISelect;
    
    public function delete(int $id): IDelete;
    
    public function ofOriginal(
        int $originalId, 
        bool $isAscending,
        int $start, 
        int $stop
    ): ISelect;
    
    public function ofForeign(
        int $foreignId, 
        bool $isAscending,
        int $start, 
        int $stop
    ): ISelect;
}
