<?php
namespace core\media\foreign;
use core\media\original\IRepository as IOriginalRepository;
use core\media\language\IRepository as ILanguageRepository;
use core\media\video\IRepository as IVideoRepository;
use core\media\audio\IRepository as IAudioRepository;
use core\media\subtitle\IRepository as ISubtitleRepository;
use core\media\foreign\IRepository as IForeignRepository;
use core\media\clip\IRepository as IClipRepository;
use core\media\original\IOriginal;
use core\media\language\ILanguage;
use core\media\video\IVideo;
use core\media\audio\IAudio;
use core\media\subtitle\ISubtitle;
use core\media\clip\IClip;
class Foreign implements IForeign
{
    private $commands;
    private $originals;
    private $languages;
    private $videos;
    private $audios;
    private $subtitles;
    private $foreigns;
    private $clips;
    private $id;
    private $originalId;
    private $languageId;
    private $videoId;
    private $audioId;
    private $subtitleId;
    private $clipNumber;

    public function __construct(
        command\IFactory $commands,
        IOriginalRepository $originals,
        ILanguageRepository $languages,
        IVideoRepository $videos,
        IAudioRepository $audios,
        ISubtitleRepository $subtitles,
        IForeignRepository $foreigns,
        IClipRepository $clips,
        int $id,
        int $originalId, 
        int $languageId,
        int $videoId,
        int $audioId,
        int $subtitleId,
        int $clipNumber
    ) {
        $this->commands = $commands;
        $this->originals = $originals;
        $this->languages = $languages;
        $this->videos = $videos;
        $this->audios = $audios;
        $this->subtitles = $subtitles;
        $this->foreigns = $foreigns;
        $this->clips = $clips;
        $this->id = $id;
        $this->originalId = $originalId;
        $this->languageId = $languageId;
        $this->videoId = $videoId;
        $this->audioId = $audioId;
        $this->subtitleId = $subtitleId;
        $this->clipNumber = $clipNumber;
    }
    
    public function id(): int
    {
        return $this->id;
    }
    
    public function original(): IOriginal
    {
        return $this->originals->read($this->originalId);
    }
    
    public function language(): ILanguage
    {
        return $this->languages->read($this->languageId);
    }

    public function video(): IVideo
    {
        return $this->videos->read($this->videoId);
    }

    public function audio(): IAudio
    {
        return $this->audios->read($this->audioId);
    }

    public function subtitle(): ISubtitle
    {
        return $this->subtitles->read($this->subtitleId);
    }

    public function clipNumber(): int
    {
        return $this->clipNumber;
    }

    public function clips(int $start, int $stop): array
    {
        return $this->clips->ofForeign($this, true, $start, $stop);
    }

    public function addClip(IClip $clip)
    {
        $this->clipNumber++;
    }
    
    public function cut()
    {
        $this->commands->cut($this);
    }
}
