<?php
namespace core\media\foreign;
use core\entity\IEntity;
use core\media\original\IOriginal;
use core\media\language\ILanguage;
use core\media\video\IVideo;
use core\media\audio\IAudio;
use core\media\subtitle\ISubtitle;
use core\media\clip\IClip;
interface IForeign extends IEntity
{
    public function original(): IOriginal;
    public function language(): ILanguage;
    public function video(): IVideo;
    public function audio(): IAudio;
    public function subtitle(): ISubtitle;
    public function clipNumber(): int;
    public function clips(int $start, int $stop): array;
    public function addClip(IClip $clip);
    public function cut();
}
