<?php
namespace core\media\foreign;
use core\media\original\IOriginal;
use core\media\language\ILanguage;
use core\media\video\IVideo;
use core\media\audio\IAudio;
use core\media\subtitle\ISubtitle;
use core\media\clip\IClip;
interface IRepository
{
    public function create(
        IOriginal $original,
        ILanguage $language,
        IVideo $video, 
        IAudio $audio, 
        ISubtitle $subtitle
    ): IForeign;
    
    public function read(int $id): IForeign;
    
    public function update(IForeign $foreign);
    
    public function all(): array;
    
    public function exists(
        IOriginal $original,
        ILanguage $language,
        IVideo $video, 
        IAudio $audio, 
        ISubtitle $subtitle
    ): bool;
    
    public function ofOriginal(IOriginal $original): array;
    
    public function addClip(IForeign $foreign, IClip $clip);
}
