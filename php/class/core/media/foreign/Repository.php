<?php
namespace core\media\foreign;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\original\IOriginal;
use core\media\language\ILanguage;
use core\media\video\IVideo;
use core\media\audio\IAudio;
use core\media\subtitle\ISubtitle;
use core\media\clip\IClip;
class Repository implements IRepository
{
    private $factory;
    private $queries;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $queries,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->queries = $queries;
        $this->migration = $migration;
    }
    
    public function create(
        IOriginal $original,
        ILanguage $language,
        IVideo $video, 
        IAudio $audio, 
        ISubtitle $subtitle
    ): IForeign
    {
        $create = $this->queries->create(
            $original->id(),
            $language->id(),
            $video->id(),
            $audio->id(),
            $subtitle->id(),
            0
        );

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->queries->delete($id)->query()
        );

        return $this->read($id);
    }

    public function read(int $id): IForeign
    {
        $rows = $this->queries->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\ForeignNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function update(IForeign $foreign)
    {
        $update = $this->queries->update(
            $foreign->id(), 
            $foreign->original()->id(),
            $foreign->language()->id(),
            $foreign->video()->id(),
            $foreign->audio()->id(),
            $foreign->subtitle()->id(),
            $foreign->clipNumber()
        );
        
        $old = $this->read($foreign->id());
        
        $revert = $this->queries->update(
            $old->id(), 
            $old->original()->id(),
            $old->language()->id(),
            $old->video()->id(),
            $old->audio()->id(),
            $old->subtitle()->id(),
            $old->clipNumber()
        );
        
        $update->run();
        
        $this->migration->append(
            $update->query(),
            $revert->query()
        );
    }
    
    public function all(): array
    {
        return $this->factory->many(
            $this->queries->all()->run()
        );
    }
    
    public function exists(
        IOriginal $original,
        ILanguage $language,
        IVideo $video, 
        IAudio $audio, 
        ISubtitle $subtitle
    ): bool
    {
        $rows = $this->queries->exists(
            $original->id(),
            $language->id(),
            $video->id(),
            $audio->id(),
            $subtitle->id()
        )->run();
        
        return !empty($rows);
    }
    
    public function ofOriginal(IOriginal $original): array
    {
        return $this->factory->many(
            $this->queries->ofOriginal($original->id())->run()
        );
    }
    
    public function addClip(IForeign $foreign, IClip $clip)
    {
        $addClip = $this->queries->addClip($foreign->id(), $clip->id());
        $removeClip = $this->queries->removeClip($foreign->id(), $clip->id());

        $this->migration->append(
            $addClip->query(),
            $removeClip->query()
        );

        $addClip->run();
    }
}
