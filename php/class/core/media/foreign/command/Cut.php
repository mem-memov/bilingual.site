<?php
namespace core\media\foreign\command;
use core\entity\ICommand;
use core\media\foreign\IForeign;
class Cut implements ICommand
{
    private $foreign;
    
    public function __construct(
        IForeign $foreign
    ) {
        $this->foreign = $foreign;
    }
    
    public function execute()
    {
        foreach ($this->foreign->subtitle()->phrases() as $phrase) {

            $phrase->cutForeign($this->foreign);
        }
    }
}
