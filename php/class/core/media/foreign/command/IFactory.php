<?php
namespace core\media\foreign\command;
use core\media\foreign\IForeign;
interface IFactory
{
    public function cut(IForeign $foreign);
}
