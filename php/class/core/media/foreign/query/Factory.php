<?php
namespace core\media\foreign\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\foreign\ITable as IForeignTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
use core\database\query\IUpdate;
class Factory extends EntityQueryFactory implements IFactory
{
    private $foreign;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        IForeignTable $foreign
    ) {
        parent::__construct($database, $filters);
        $this->foreign = $foreign;
    }
    
    public function create(
        int $originalId,
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId, 
        int $clipNumber
    ): IInsert 
    {
        $values = $this->foreign->specifications();
        
        $values[$this->foreign->originalId()] = $originalId;
        $values[$this->foreign->languageId()] = $languageId;
        $values[$this->foreign->videoId()] = $videoId;
        $values[$this->foreign->audioId()] = $audioId;
        $values[$this->foreign->subtitleId()] = $subtitleId;
        $values[$this->foreign->clipNumber()] = $clipNumber;

        return $this->crud()->create($this->foreign, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->foreign, $id);
    }

    public function update(
        int $id, 
        int $originalId,
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId, 
        int $clipNumber
    ): IUpdate
    {
        $values = $this->foreign->specifications();
        
        $values[$this->foreign->originalId()] = $originalId;
        $values[$this->foreign->languageId()] = $languageId;
        $values[$this->foreign->videoId()] = $videoId;
        $values[$this->foreign->audioId()] = $audioId;
        $values[$this->foreign->subtitleId()] = $subtitleId;
        $values[$this->foreign->clipNumber()] = $clipNumber;
        
        return $this->crud()->update($this->foreign, $id, $values);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->foreign, $id);
    }
    
    public function all(): ISelect
    {
        return $this->filter()->all($this->foreign);
    }
    
    public function exists(
        int $originalId,
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId
    ): ISelect
    {
        $filters = $this->foreign->specifications();
        
        $filters[$this->foreign->originalId()] = $this->filters->is($originalId);
        $filters[$this->foreign->languageId()] = $this->filters->is($languageId);
        $filters[$this->foreign->videoId()] = $this->filters->is($videoId);
        $filters[$this->foreign->audioId()] = $this->filters->is($audioId);
        $filters[$this->foreign->subtitleId()] = $this->filters->is($subtitleId);
        
        return $this->filter()->records($this->foreign, $filters);
    }
    
    public function ofOriginal(int $originalId): ISelect
    {
        return $this->oneToMany()->items(
            $this->foreign, 
            $this->foreign->originalId(), 
            $originalId
        );
    }
    
    public function addClip(int $foreignId, int $clipId): IInsert
    {
        return $this->manyToMany()->add(
            $this->foreignClip,
            $this->foreignClip->foreignId(),
            $foreignId,
            $this->foreignClip->clipId(),
            $clipId
        );
    }
    
    public function removeClip(int $foreignId, int $clipId): IDelete
    {
        return $this->manyToMany()->remove(
            $this->foreignClip,
            $this->foreignClip->foreignId(),
            $foreignId,
            $this->foreignClip->clipId(),
            $clipId
        );
    }
}
