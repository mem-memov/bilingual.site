<?php
namespace core\media\foreign\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
use core\database\query\IUpdate;
interface IFactory
{
    public function create(
        int $originalId,
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId, 
        int $clipNumber
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function update(
        int $id, 
        int $originalId,
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId, 
        int $clipNumber
    ): IUpdate;
    
    public function delete(int $id): IDelete;
    
    public function all(): ISelect;
    
    public function exists(
        int $originalId,
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId
    ): ISelect;
    
    public function ofOriginal(int $originalId): ISelect;
    
    public function addClip(int $foreignId, int $clipId): IInsert;
    
    public function removeClip(int $foreignId, int $clipId): IDelete;
}
