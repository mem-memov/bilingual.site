<?php
namespace core\media\language;
use core\entity\{IEntity, Factory as EntityFactory};
class Factory extends EntityFactory
{
    public function __construct(
    ) {
        
    }

    public function one(array $row): IEntity
    {
        return new Language(
            (int)$row['id'],
            (string)$row['language']
        );
    }
}
