<?php
namespace core\media\language;
use core\entity\IEntity;
interface ILanguage extends IEntity
{
    public function language(): string;
}
