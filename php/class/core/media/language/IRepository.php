<?php
namespace core\media\language;
interface IRepository
{
    public function provide(string $language): ILanguage;
    public function create(string $language): ILanguage;
    public function read(int $id): ILanguage;
}
