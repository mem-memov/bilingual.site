<?php
namespace core\media\language;
class Language implements ILanguage
{
    private $id;
    private $language;
    
    public function __construct(
        int $id,
        string $language
    ) {
        $this->id = $id;
        $this->language = $language;
    }
    
    public function id(): int
    {
        return $this->id;
    }

    public function language(): string
    {
        return $this->language;
    }
}
