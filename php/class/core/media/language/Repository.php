<?php
namespace core\media\language;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
class Repository implements IRepository
{
    private $factory;
    private $query;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $query,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->query = $query;
        $this->migration = $migration;
    }
    
    public function provide(string $language): ILanguage
    {
        $languages = $this->query->byLanguage($language)->run();
        
        if (!empty($languages)) {
            return $this->factory->one($languages[0]);
        }
        
        return $this->create($language);
    }
    
    public function create(string $language): ILanguage
    {
        $languages = $this->query->byLanguage($language)->run();
        
        if (!empty($languages)) {
            throw new exception\RepeatedLanguageCreation($language);
        }

        $create = $this->query->create($language);

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->query->delete($id)->query()
        );

        return $this->read($id);
    }
    
    public function read(int $id): ILanguage
    {
        $rows = $this->query->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\LanguageNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
}
