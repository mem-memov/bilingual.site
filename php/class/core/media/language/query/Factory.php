<?php
namespace core\media\language\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\language\ITable as ILanguageTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
class Factory extends EntityQueryFactory implements IFactory
{
    private $language;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        ILanguageTable $language
    ) {
        parent::__construct($database, $filters);
        $this->language = $language;
    }
    
    public function create(
        string $language
    ): IInsert 
    {
        $values = $this->language->specifications();
        
        $values[$this->language->language()] = $language;

        return $this->crud()->create($this->language, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->language, $id);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->language, $id);
    }
    
    public function byLanguage(string $language): ISelect
    {
        $filters = $this->language->specifications();
        $filters[$this->language->language()] = $this->filters->is($language);
        
        return $this->filter()->records($this->language, $filters);
    }
}
