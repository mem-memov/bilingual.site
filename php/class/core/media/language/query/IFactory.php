<?php
namespace core\media\language\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
interface IFactory
{
    public function create(
        string $language
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function delete(int $id): IDelete;
    
    public function byLanguage(string $language): ISelect;
}
