<?php
namespace core\media\original;
use core\media\IFactory as IRepositoryFactory;
use core\entity\{IEntity, Factory as EntityFactory};
class Factory extends EntityFactory
{
    private $repositories;
    private $commands;
    
    public function __construct(
        IRepositoryFactory $repositories,
        command\IFactory $commands
    ) {
        $this->repositories = $repositories;
        $this->commands = $commands;
    }

    public function one(array $row): IEntity
    {
        return new Original(
            $this->commands,
            $this->repositories->language(),
            $this->repositories->video(),
            $this->repositories->audio(),
            $this->repositories->subtitle(),
            $this->repositories->foreign(),
            $this->repositories->clip(),
            (int)$row['id'],
            (int)$row['language_id'],
            (int)$row['video_id'],
            (int)$row['audio_id'],
            (int)$row['subtitle_id'],
            (int)$row['clip_number']
        );
    }
}
