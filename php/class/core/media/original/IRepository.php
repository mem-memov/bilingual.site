<?php
namespace core\media\original;
use core\media\language\ILanguage;
use core\media\video\IVideo;
use core\media\audio\IAudio;
use core\media\subtitle\ISubtitle;
use core\media\clip\IClip;
interface IRepository
{
    public function create(
        ILanguage $language,
        IVideo $video, 
        IAudio $audio, 
        ISubtitle $subtitle
    ): IOriginal;
    
    public function read(int $id): IOriginal;
    
    public function update(IOriginal $original);
    
    public function all(): array;
    
    public function exists(
        ILanguage $language,
        IVideo $video, 
        IAudio $audio, 
        ISubtitle $subtitle
    ): bool;
    
    public function addClip(IOriginal $original, IClip $clip);
}
