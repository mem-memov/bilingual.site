<?php
namespace core\media\original;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\language\ILanguage;
use core\media\video\IVideo;
use core\media\audio\IAudio;
use core\media\subtitle\ISubtitle;
use core\media\clip\IClip;
class Repository implements IRepository
{
    private $factory;
    private $queries;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $queries,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->queries = $queries;
        $this->migration = $migration;
    }
    
    public function create(
        ILanguage $language,
        IVideo $video, 
        IAudio $audio, 
        ISubtitle $subtitle
    ): IOriginal
    {
        $create = $this->queries->create(
            $language->id(),
            $video->id(),
            $audio->id(),
            $subtitle->id(),
            0
        );

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->queries->delete($id)->query()
        );

        return $this->read($id);
    }

    public function read(int $id): IOriginal
    {
        $rows = $this->queries->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\OriginalNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function update(IOriginal $original)
    {
        $update = $this->queries->update(
            $original->id(), 
            $original->language()->id(),
            $original->video()->id(),
            $original->audio()->id(),
            $original->subtitle()->id(),
            $original->clipNumber()
        );
        
        $old = $this->read($original->id());
        
        $revert = $this->queries->update(
            $old->id(), 
            $old->language()->id(),
            $old->video()->id(),
            $old->audio()->id(),
            $old->subtitle()->id(),
            $old->clipNumber()
        );
        
        $update->run();
        
        $this->migration->append(
            $update->query(),
            $revert->query()
        );
    }
    
    public function all(): array
    {
        return $this->factory->many(
            $this->queries->all()->run()
        );
    }
    
    public function exists(
        ILanguage $language,
        IVideo $video, 
        IAudio $audio, 
        ISubtitle $subtitle
    ): bool
    {
        $rows = $this->queries->exists(
            $language->id(),
            $video->id(),
            $audio->id(),
            $subtitle->id()
        )->run();
        
        return !empty($rows);
    }
    
    public function addClip(IOriginal $original, IClip $clip)
    {
        $addClip = $this->queries->addClip($original->id(), $clip->id());
        $removeClip = $this->queries->removeClip($original->id(), $clip->id());

        $this->migration->append(
            $addClip->query(),
            $removeClip->query()
        );

        $addClip->run();
    }
}
