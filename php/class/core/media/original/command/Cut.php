<?php
namespace core\media\original\command;
use core\entity\ICommand;
use core\media\original\IOriginal;
class Cut implements ICommand
{
    private $original;
    
    public function __construct(
        IOriginal $original
    ) {
        $this->original = $original;
    }
    
    public function execute()
    {
        foreach ($this->original->subtitle()->phrases() as $phrase) {

            $phrase->cutOriginal($this->original);
        }
    }
}
