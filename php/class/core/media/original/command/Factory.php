<?php
namespace core\media\original\command;
use core\media\original\IOriginal;
class Factory implements IFactory
{

    
    public function __construct(

    ) {

    }

    public function cut(IOriginal $original)
    {
        $command = new Cut(
            $original
        );
        
        $command->execute();
    }

}
