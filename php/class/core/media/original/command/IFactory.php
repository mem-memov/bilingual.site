<?php
namespace core\media\original\command;
use core\media\original\IOriginal;
interface IFactory
{
    public function cut(IOriginal $original);
}
