<?php
namespace core\media\original\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\original\ITable as IOriginalTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
use core\database\query\IUpdate;
class Factory extends EntityQueryFactory implements IFactory
{
    private $original;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        IOriginalTable $original
    ) {
        parent::__construct($database, $filters);
        $this->original = $original;
    }
    
    public function create(
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId, 
        int $clipNumber
    ): IInsert 
    {
        $values = $this->original->specifications();
        
        $values[$this->original->languageId()] = $languageId;
        $values[$this->original->videoId()] = $videoId;
        $values[$this->original->audioId()] = $audioId;
        $values[$this->original->subtitleId()] = $subtitleId;
        $values[$this->original->clipNumber()] = $clipNumber;

        return $this->crud()->create($this->original, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->original, $id);
    }

    public function update(
        int $id, 
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId, 
        int $clipNumber
    ): IUpdate
    {
        $values = $this->original->specifications();
        
        $values[$this->original->languageId()] = $languageId;
        $values[$this->original->videoId()] = $videoId;
        $values[$this->original->audioId()] = $audioId;
        $values[$this->original->subtitleId()] = $subtitleId;
        $values[$this->original->clipNumber()] = $clipNumber;
        
        return $this->crud()->update($this->original, $id, $values);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->original, $id);
    }
    
    public function all(): ISelect
    {
        return $this->filter()->all($this->original);
    }
    
    public function exists(
        int $languageId,
        int $videoId, 
        int $audioId, 
        int $subtitleId
    ): ISelect
    {
        $filters = $this->original->specifications();
        
        $filters[$this->original->languageId()] = $this->filters->is($languageId);
        $filters[$this->original->videoId()] = $this->filters->is($videoId);
        $filters[$this->original->audioId()] = $this->filters->is($audioId);
        $filters[$this->original->subtitleId()] = $this->filters->is($subtitleId);
        
        return $this->filter()->records($this->original, $filters);
    }
    
    public function addClip(int $originalId, int $clipId): IInsert
    {
        return $this->manyToMany()->add(
            $this->originalClip,
            $this->originalClip->originalId(),
            $originalId,
            $this->originalClip->clipId(),
            $clipId
        );
    }
    
    public function removeClip(int $originalId, int $clipId): IDelete
    {
        return $this->manyToMany()->remove(
            $this->originalClip,
            $this->originalClip->originalId(),
            $originalId,
            $this->originalClip->clipId(),
            $clipId
        );
    }
}
