<?php
namespace core\media\phrase;
use core\media\IFactory as IRepositoryFactory;
use core\entity\{IEntity, Factory as EntityFactory};
class Factory extends EntityFactory
{
    private $repositories;
    private $commands;
    
    public function __construct(
        IRepositoryFactory $repositories,
        command\IFactory $commands
    ) {
        $this->repositories = $repositories;
        $this->commands = $commands;
    }

    public function one(array $row): IEntity
    {
        return new Phrase(
            $this->commands,
            $this->repositories->language(),
            $this->repositories->subtitle(),
            $this->repositories->word(),
            (int)$row['id'],
            (int)$row['language_id'],
            (int)$row['subtitle_id'],
            (int)$row['start'],
            (int)$row['stop'],
            (string)$row['phrase'],
            (int)$row['order_number']
        );
    }
}
