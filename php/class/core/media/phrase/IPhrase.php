<?php
namespace core\media\phrase;
use core\entity\IEntity;
use core\media\language\ILanguage;
use core\media\subtitle\ISubtitle;
use core\media\original\IOriginal;
use core\media\foreign\IForeign;
interface IPhrase extends IEntity
{
    public function language(): ILanguage;
    public function subtitle(): ISubtitle;
    public function start(): int;
    public function stop(): int;
    public function phrase(): string;
    public function orderNumber(): string;
    public function words(): array;
    public function import();
    public function cutOriginal(IOriginal $original);
    public function cutForeign(IForeign $foreign);
}
