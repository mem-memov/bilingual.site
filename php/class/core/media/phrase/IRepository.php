<?php
namespace core\media\phrase;
use core\media\language\ILanguage;
use core\media\subtitle\ISubtitle;
use core\media\word\IWord;
interface IRepository
{
    public function create(
        ILanguage $language, 
        ISubtitle $subtitle,
        int $start, 
        int $stop, 
        string $phrase, 
        int $orderNumber
    ): IPhrase;
    public function read(int $id): IPhrase;
    public function addWord(IPhrase $phrase, IWord $word);
    public function ofSubtitle(int $subtitleId, bool $isOrderNumberAscending): array;
    public function ofSubtitleAt(int $subtitleId, int $orderNumber): array;
}
