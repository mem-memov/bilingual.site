<?php
namespace core\media\phrase;
use core\media\language\IRepository as ILanguageRepository;
use core\media\subtitle\IRepository as ISubtitleRepository;
use core\media\word\IRepository as IWordRepository;
use core\media\language\ILanguage;
use core\media\subtitle\ISubtitle;
use core\media\original\IOriginal;
use core\media\foreign\IForeign;
class Phrase implements IPhrase
{
    private $command;
    private $languages;
    private $subtitles;
    private $word;
    private $id;
    private $languageId;
    private $start;
    private $stop;
    private $phrase;
    private $orderNumber;

    public function __construct(
        command\IFactory $command,
        ILanguageRepository $languages,
        ISubtitleRepository $subtitles,
        IWordRepository $words,
        int $id,
        int $languageId,
        int $subtitleId,
        int $start,
        int $stop,
        string $phrase,
        int $orderNumber
    ) {
        $this->command = $command;
        $this->languages = $languages;
        $this->subtitles = $subtitles;
        $this->words = $words;
        $this->id = $id;
        $this->languageId = $languageId;
        $this->subtitleId = $subtitleId;
        $this->start = $start;
        $this->stop = $stop;
        $this->phrase = $phrase;
        $this->orderNumber =$orderNumber;
    }
    
    public function id(): int
    {
        return $this->id;
    }
    
    public function language(): ILanguage
    {
        return $this->languages->read($this->languageId);
    }

    public function subtitle(): ISubtitle
    {
        return $this->subtitles->read($this->subtitleId);
    }

    public function start(): int
    {
        return $this->start;
    }

    public function stop(): int
    {
        return $this->stop;
    }
    
    public function phrase(): string
    {
        return $this->phrase;
    }

    public function orderNumber(): string
    {
        return $this->orderNumber;
    }

    public function words(): array
    {
        return $this->words->ofPhrase($this->id);
    }
    
    public function import()
    {
        $this->command->import($this);
    }
    
    public function cutOriginal(IOriginal $original)
    {
        $this->command->cutOriginal($this, $original);
    }
    
    public function cutForeign(IForeign $foreign)
    {
        $this->command->cutForeign($this, $foreign);
    }
}
