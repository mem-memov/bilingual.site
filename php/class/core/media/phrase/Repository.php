<?php
namespace core\media\phrase;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\language\ILanguage;
use core\media\subtitle\ISubtitle;
use core\media\word\IWord;
class Repository implements IRepository
{
    private $factory;
    private $queries;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $queries,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->queries = $queries;
        $this->migration = $migration;
    }
    
    public function create(
        ILanguage $language, 
        ISubtitle $subtitle,
        int $start, 
        int $stop, 
        string $phrase, 
        int $orderNumber
    ): IPhrase
    {
        $create = $this->queries->create(
            $language->id(),
            $subtitle->id(),
            $start,
            $stop,
            $phrase,
            $orderNumber
        );

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->queries->delete($id)->query()
        );

        return $this->read($id);
    }
    
    public function read(int $id): IPhrase
    {
        $rows = $this->queries->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\PhraseNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function addWord(IPhrase $phrase, IWord $word)
    {
        $addWord = $this->queries->addWord($phrase->id(), $word->id());
        $removeWord = $this->queries->removeWord($phrase->id(), $word->id());

        $this->migration->append(
            $addWord->query(),
            $removeWord->query()
        );

        $addWord->run();
    }
    
    public function ofSubtitle(int $subtitleId, bool $isOrderNumberAscending): array
    {
        return $this->factory->many(
            $this->queries->ofSubtitle($subtitleId, $isOrderNumberAscending)->run()
        );
    }
    
    public function ofSubtitleAt(int $subtitleId, int $orderNumber): array
    {
        return $this->factory->many(
            $this->queries->ofSubtitleAt($subtitleId, $orderNumber)->run()
        );
    }
}
