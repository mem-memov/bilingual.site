<?php
namespace core\media\phrase\command;
use core\entity\ICommand;
use core\media\phrase\IPhrase;
use core\media\clip\IRepository as IClipRepository;
use core\media\foreign\IRepository as IForeignRepository;
use core\media\foreign\IForeign;
use core\service\media\IEditor as IEditorService;
class CutForeign implements ICommand
{
    private $phrase;
    private $clips;
    private $foreigns;
    private $foreign;
    private $editor;
    
    public function __construct(
        IPhrase $phrase,
        IClipRepository $clips,
        IForeignRepository $foreigns,
        IForeign $foreign,
        IEditorService $editor
    ) {
        $this->phrase = $phrase;
        $this->clips = $clips;
        $this->foreigns = $foreigns;
        $this->foreign = $foreign;
        $this->editor = $editor;
    }
    
    public function execute()
    {
        $start = $this->phrase->start();

        $stop = $this->phrase->stop();

        $nextPhrases = $this->foreign->subtitle()->nextPhrases($this->phrase);

        if (!empty($nextPhrases)) {
            $stop = $nextPhrases[0]->start();
        }

        $duration = $stop - $start;

        $target = $this->foreign->video()->urlSecret() . '/' . '_' . $this->phrase->orderNumber() . '_' . $this->phrase->language()->id() . '_clip.mp4';
        
        $clip = $this->clips->create(
            $this->phrase->language(), 
            $this->phrase, 
            0, 
            $start,
            $stop,
            $duration, 
            $target
        );
        
        $this->foreign->addClip($clip);
        $this->foreigns->update($this->foreign);

        $clip->cut($this->foreign->video(), $this->foreign->audio());
    }
}
