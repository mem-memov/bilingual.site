<?php
namespace core\media\phrase\command;
use core\entity\ICommand;
use core\media\phrase\IPhrase;
use core\media\clip\IRepository as IClipRepository;
use core\media\original\IRepository as IOriginalRepository;
use core\media\original\IOriginal;
use core\service\media\IEditor as IEditorService;
class CutOriginal implements ICommand
{
    private $phrase;
    private $clips;
    private $originals;
    private $original;
    private $editor;
    
    public function __construct(
        IPhrase $phrase,
        IClipRepository $clips,
        IOriginalRepository $originals,
        IOriginal $original,
        IEditorService $editor
    ) {
        $this->phrase = $phrase;
        $this->clips = $clips;
        $this->originals = $originals;
        $this->original = $original;
        $this->editor = $editor;
    }
    
    public function execute()
    {
        $start = $this->phrase->start();

        $stop = $this->phrase->stop();

        $nextPhrases = $this->original->subtitle()->nextPhrases($this->phrase);

        if (!empty($nextPhrases)) {
            $stop = $nextPhrases[0]->start();
        }

        $duration = $stop - $start;

        $target = $this->original->video()->urlSecret() . '/' . '_' . $this->phrase->orderNumber() . '_' . $this->phrase->language()->id() . '_clip.mp4';
        
        $clip = $this->clips->create(
            $this->phrase->language(), 
            $this->phrase, 
            0, 
            $start,
            $stop,
            $duration, 
            $target
        );
        
        $this->original->addClip($clip);
        $this->originals->update($this->original);

        $clip->cut($this->original->video(), $this->original->audio());
    }
}
