<?php
namespace core\media\phrase\command;
use core\IFactory as ICore;
use core\media\phrase\IPhrase;
use core\media\original\IOriginal;
use core\media\foreign\IForeign;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICore $core
    ) {
        $this->core = $core;
    }

    public function import(IPhrase $phrase)
    {
        $action = new Import(
            $phrase, 
            $this->core->media()->phrase(), 
            $this->core->media()->word()
        );
        
        $action->execute();
    }
    
    public function cutOriginal(IPhrase $phrase, IOriginal $original)
    {
        $action = new CutOriginal(
            $phrase, 
            $this->core->media()->clip(),
            $this->core->media()->original(),
            $original,
            $this->core->service()->mediaEditor()
        );
        
        $action->execute();
    }
    
    public function cutForeign(IPhrase $phrase, IForeign $foreign)
    {
        $action = new CutForeign(
            $phrase, 
            $this->core->media()->clip(),
            $this->core->media()->foreign(),
            $foreign,
            $this->core->service()->mediaEditor()
        );
        
        $action->execute();
    }
}
