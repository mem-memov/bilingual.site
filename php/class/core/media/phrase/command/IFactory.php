<?php
namespace core\media\phrase\command;
use core\media\phrase\IPhrase;
use core\media\original\IOriginal;
use core\media\foreign\IForeign;
interface IFactory
{
    public function import(IPhrase $phrase);
    public function cutOriginal(IPhrase $phrase, IOriginal $original);
    public function cutForeign(IPhrase $phrase, IForeign $foreign);
}
