<?php
namespace core\media\phrase\command;
use core\entity\ICommand;
use core\media\phrase\IPhrase;
use core\media\phrase\IRepository as IPhraseRepository;
use core\media\word\IRepository as IWordRepositiory;
class Import implements ICommand
{
    private $phrase;
    private $phrases;
    private $words;
    
    public function __construct(
        IPhrase $phrase,
        IPhraseRepository $phrases,
        IWordRepositiory $words
    ) {
        $this->phrase = $phrase;
        $this->phrases = $phrases;
        $this->words = $words;
    }
    
    public function execute()
    {
        $matches = [];
        preg_match_all('/\w+/u', $this->phrase->phrase(), $matches);

        foreach ($matches[0] as $match) {

            $text = mb_strtolower($match);
            
            $word = $this->words->provide(
                $this->phrase->language(), 
                $text
            );
            
            $this->phrases->addWord($this->phrase, $word);
        }
    }
}
