<?php
namespace core\media\phrase\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\phrase\ITable as IPhraseTable;
use core\database\table\phraseWord\ITable as IPhraseWordTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
class Factory extends EntityQueryFactory implements IFactory
{
    private $phrase;
    private $phraseWord;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        IPhraseTable $phrase,
        IPhraseWordTable $phraseWord
    ) {
        parent::__construct($database, $filters);
        $this->phrase = $phrase;
        $this->phraseWord = $phraseWord;
    }
    
    public function create(
        int $languageId, 
        int $subtitleId,
        int $start, 
        int $stop, 
        string $phrase, 
        int $orderNumber
    ): IInsert 
    {
        $values = $this->phrase->specifications();
        
        $values[$this->phrase->languageId()] = $languageId;
        $values[$this->phrase->subtitleId()] = $subtitleId;
        $values[$this->phrase->start()] = $start;
        $values[$this->phrase->stop()] = $stop;
        $values[$this->phrase->phrase()] = $phrase;
        $values[$this->phrase->orderNumber()] = $orderNumber;

        return $this->crud()->create($this->phrase, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->phrase, $id);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->phrase, $id);
    }
    
    public function ofSubtitle(int $subtitleId, bool $isOrderNumberAscending): ISelect
    {
        return $this->oneToMany()->orderedItems(
            $this->phrase,
            $this->phrase->subtitleId(),
            $subtitleId,
            $this->phrase->orderNumber(),
            $isOrderNumberAscending
        );
    }
    
    public function ofSubtitleAt(int $subtitleId, int $orderNumber): ISelect
    {
        return $this->oneToMany()->itemAtPosition(
            $this->phrase,
            $this->phrase->subtitleId(),
            $subtitleId,
            $this->phrase->orderNumber(),
            $orderNumber
        );
    }
    
    public function addWord(int $phraseId, int $wordId): IInsert
    {
        return $this->manyToMany()->add(
            $this->phraseWord,
            $this->phraseWord->phraseId(),
            $phraseId,
            $this->phraseWord->wordId(),
            $wordId
        );
    }
    
    public function removeWord(int $phraseId, int $wordId): IDelete
    {
        return $this->manyToMany()->remove(
            $this->phraseWord,
            $this->phraseWord->phraseId(),
            $phraseId,
            $this->phraseWord->wordId(),
            $wordId
        );
    }
}
