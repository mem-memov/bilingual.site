<?php
namespace core\media\phrase\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
interface IFactory
{
    public function create(
        int $languageId, 
        int $subtitleId,
        int $start, 
        int $stop, 
        string $phrase, 
        int $orderNumber
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function delete(int $id): IDelete;
    
    public function ofSubtitle(int $subtitleId, bool $isOrderNumberAscending): ISelect;
    
    public function ofSubtitleAt(int $subtitleId, int $orderNumber): ISelect;
    
    public function addWord(int $phraseId, int $wordId): IInsert;
    
    public function removeWord(int $phraseId, int $wordId): IDelete;
}
