<?php
namespace core\media\source;
use core\media\IFactory as IRepositoryFactory;
use core\entity\{IEntity, Factory as EntityFactory};
class Factory extends EntityFactory
{
    private $repositories;
    private $commands;
    
    public function __construct(
        IRepositoryFactory $repositories,
        command\IFactory $commands
    ) {
        $this->repositories = $repositories;
        $this->commands = $commands;
    }

    public function one(array $row): IEntity
    {
        return new Source(
            $this->commands,
            $this->repositories->video(),
            $this->repositories->audio(),
            $this->repositories->subtitle(),
            (int)$row['id'],
            (string)$row['file']
        );
    }
}
