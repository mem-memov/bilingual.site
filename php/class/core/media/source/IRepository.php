<?php
namespace core\media\source;
use core\media\source\ISource;
use core\media\language\ILanguage;
interface IRepository
{
    public function create(
        string $file
    ): ISource;
    public function read(int $id): ISource;
    public function all(): array;
}
