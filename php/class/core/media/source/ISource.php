<?php
namespace core\media\source;
use core\entity\IEntity;
interface ISource extends IEntity
{
    public function videos(): array; // IVideo[]
    public function audios(): array; // IAudio[]
    public function subtitles(): array; // ISubtitle[]
    public function file(): string;
    public function import();
}
