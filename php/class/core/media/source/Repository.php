<?php
namespace core\media\source;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
class Repository implements IRepository
{
    private $factory;
    private $query;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $query,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->query = $query;
        $this->migration = $migration;
    }
    
    public function create(
        string $file
    ): ISource
    {
        $sources = $this->query->byFile($file)->run();
        
        if (!empty($sources)) {
            throw new exception\RepeatedSourceCreation($file);
        }
        
        $create = $this->query->create($file);

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->query->delete($id)->query()
        );
        
        return $this->read($id);
    }
    
    public function read(int $id): ISource
    {
        $rows = $this->query->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\SourceNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function all(): array
    {
        $rows = $this->query->all()->run();
        return $this->factory->many($rows);
    }
}
