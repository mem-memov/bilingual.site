<?php
namespace core\media\source;
use core\media\video\IRepository as IVideoRepository;
use core\media\audio\IRepository as IAudioRepository;
use core\media\subtitle\IRepository as ISubtitleRepository;
class Source implements ISource
{
    private $commands;
    private $videos;
    private $audios;
    private $subtitles;
    private $id;
    private $file;
    
    public function __construct(
        command\IFactory $commands,
        IVideoRepository $videos,
        IAudioRepository $audios,
        ISubtitleRepository $subtitles,
        int $id,
        string $file
    ) {
        $this->commands = $commands;
        $this->videos = $videos;
        $this->audios = $audios;
        $this->subtitles = $subtitles;
        $this->id = $id;
        $this->file = $file;
    }
    
    public function id(): int
    {
        return $this->id;
    }

    public function videos(): array
    {
        return $this->videos->ofSource($this->id);
    }

    public function audios(): array
    {
        return $this->audios->ofSource($this->id);
    }

    public function subtitles(): array
    {
        return $this->subtitles->ofSource($this->id);
    }

    public function file(): string
    {
        return $this->file;
    }

    public function import()
    {
        $this->commands->import($this);
    }
}
