<?php
namespace core\media\source\command;
use core\IFactory as ICore;
use core\media\source\ISource;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICore $core
    ) {
        $this->core = $core;
    }
    
    public function import(ISource $source)
    {
        $selector = new Selector();

        $command = new Import(
            $source,
            $this->core->configuration()->directory()->master(),
            $this->core->configuration()->directory()->copy(),
            $this->core->service()->mediaEditor(),
            $selector,
            $this->core->media()->language(),
            $this->core->media()->video(),
            $this->core->media()->audio(),
            $this->core->media()->subtitle()
        );
        
        $command->execute();
    }
}
