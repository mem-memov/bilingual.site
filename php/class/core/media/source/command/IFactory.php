<?php
namespace core\media\source\command;
use core\media\source\ISource;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\ISourceStream;
interface IFactory
{
    public function import(ISource $source);
}
