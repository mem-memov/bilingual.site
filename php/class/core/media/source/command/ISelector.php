<?php
namespace core\media\source\command;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IVideoStream;
interface ISelector
{
    public function video(IMediaData $mediaData): IVideoStream;
}
