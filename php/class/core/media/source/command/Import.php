<?php
namespace core\media\source\command;
use core\entity\ICommand;
use core\media\source\ISource;
use core\service\media\IEditor as IMediaEditor;
use core\media\language\IRepository as ILanguageRepository;
use core\media\video\IRepository as IVideoRepository;
use core\media\audio\IRepository as IAudioRepository;
use core\media\subtitle\IRepository as ISubtitleRepository;
class Import implements ICommand
{
    private $source;
    private $masterDirectory;
    private $copyDirectory;
    private $editor;
    private $selector;
    private $languages;
    private $videos;
    private $audios;
    private $subtitles;
    
    public function __construct(
        ISource $source,
        string $masterDirectory,
        string $copyDirectory,
        IMediaEditor $editor,
        ISelector $selector,
        ILanguageRepository $languages,
        IVideoRepository $videos,
        IAudioRepository $audios,
        ISubtitleRepository $subtitles
    ) {
        $this->source = $source;
        $this->masterDirectory = $masterDirectory;
        $this->copyDirectory = $copyDirectory;
        $this->editor = $editor;
        $this->selector = $selector;
        $this->languages = $languages;
        $this->videos = $videos;
        $this->audios = $audios;
        $this->subtitles = $subtitles;
    }

    public function execute()
    {
        $directory = dirname($this->source->file()) . '/' . pathinfo($this->source->file(), PATHINFO_FILENAME);
        $copyDirectory = $this->copyDirectory . '/' . $directory;

        if (!file_exists($copyDirectory)) {
            mkdir($copyDirectory, 0777, true);
        }

        $sourcePath = $this->masterDirectory . '/' . $this->source->file();
        $mediaData = $this->editor->getMediaData($sourcePath);
        $videoStream = $this->selector->video($mediaData);
        $language = $this->languages->provide($videoStream->language());
        $videoFile = $directory . '/movie.mp4';
        $video = $this->videos->create(
            $this->source,
            $language,
            $videoFile,
            $videoStream->width(),
            $videoStream->height(),
            $videoStream->duration()
        );

        $video->import($mediaData, $videoStream);

        // import audio
        
        $audioStreams = $mediaData->audio();

        foreach ($audioStreams as $audioStream) {
            $language = $this->languages->provide($audioStream->language());
            $audioPath = $directory . '/' . $language->language() . '.mp3';
            $audio = $this->audios->create(
                $video->source(),
                $language,
                $audioPath,
                $audioStream->duration()
            );
            $audio->import($mediaData, $audioStream);
            $this->videos->addAudio($video, $audio);
        }

        // import subtitles from streams

        $subtitleStreams = $mediaData->subtitle();
        foreach ($subtitleStreams as $subtitleStream) {
            $language = $this->languages->provide($subtitleStream->language());
            $subtitlePath = $directory . '/' . $language->language() . '.srt';
            $subtitle = $this->subtitles->create(
                $video->source(),
                $language,
                $subtitlePath
            );
            $subtitle->importStream($mediaData, $subtitleStream);
            $this->videos->addSubtitle($video, $subtitle);
        }

        // import subtitles from files

        $subtitleFiles = glob($copyDirectory . '/*.srt');

        $subtitles = $video->subtitles();

        if (count($subtitles) < count($subtitleFiles)) {

            foreach ($subtitleFiles as $subtitleFile) {

                $isImported = false;

                foreach ($subtitles as $subtitle) {
                    if (strpos($subtitleFile, $subtitle->file()) !== false) {
                        $isImported = true;
                        break;
                    }
                }

                if (!$isImported) {

                    $language = $this->languages->provide(basename($subtitleFile, '.srt'));
                    $subtitlePath = $directory . '/' . $language->language() . '.srt';
                    $subtitle = $this->subtitles->create(
                        $video->source(),
                        $language,
                        $subtitlePath
                    );
                    $subtitle->importFile();
                    $this->videos->addSubtitle($video, $subtitle);
                }
            }
        }
    }
}
