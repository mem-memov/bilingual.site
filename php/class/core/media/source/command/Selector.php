<?php
namespace core\media\source\command;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IVideoStream;
class Selector implements ISelector
{
    public function video(IMediaData $mediaData): IVideoStream
    {
        $videoStreams = $mediaData->video();
        
        $maxDuration = 0;
        $durationCandidates = [];
        foreach ($videoStreams as $videoStream) {
            if ($videoStream->duration() > $maxDuration) {
                $maxDuration = $videoStream->duration();
                $durationCandidates = [$videoStream];
            } elseif ($videoStream->duration() == $maxDuration) {
                $durationCandidates[] = $selectedVideo;
            }
        }
        
        $durationCount = count($durationCandidates);
        
        if ($durationCount == 0) {
            throw new exception\VideoStreamNotSelected();
        }
        
        if ($durationCount == 1) {
            return $durationCandidates[0];
        }

        $maxWidth = 0;
        $widthCandidates = [];
        foreach ($durationCandidates as $candidate) {
            if ($candidate->width() > $maxWidth) {
                $maxWidth = $candidate->width();
                $widthCandidates = [$candidate];
            } elseif ($candidate->width() == $maxWidth) {
                $widthCandidates[] = $candidate;
            }
        }
        
        $widthCount = count($widthCandidates);
        
        if ($widthCount == 1) {
            return $widthCandidates[0];
        }
        
        $maxFrameRate = 0;
        $frameRateCandidates = [];
        foreach ($widthCandidates as $candidate) {
            if ($candidate->frameRate() > $maxFrameRate) {
                $maxFrameRate = $candidate->frameRate();
                $frameRateCandidates = [$candidate];
            } elseif ($candidate->frameRate() == $maxFrameRate) {
                $frameRateCandidates[] = $candidate;
            }
        }
        
        $frameRateCount = count($frameRateCandidates);
        
        if ($frameRateCount == 1) {
            return $frameRateCandidates[0];
        }
        
        throw new exception\VideoStreamNotSelected();
    }
}
