<?php
namespace core\media\source\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\source\ITable as ISourceTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
class Factory extends EntityQueryFactory implements IFactory
{
    private $source;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        ISourceTable $source
    ) {
        parent::__construct($database, $filters);
        $this->source = $source;
    }
    
    public function create(
        string $file
    ): IInsert 
    {
        $values = $this->source->specifications();
        
        $values[$this->source->file()] = $file;

        return $this->crud()->create($this->source, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->source, $id);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->source, $id);
    }
    
    public function byFile(string $file): ISelect
    {
        $filters = $this->source->specifications();
        $filters[$this->source->file()] = $this->filters->is($file);
        
        return $this->filter()->records($this->source, $filters);
    }
    
    public function all(): ISelect
    {
        return $this->filter()->all($this->source);
    }
}
