<?php
namespace core\media\source\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
interface IFactory
{
    public function create(
        string $file
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function delete(int $id): IDelete;
    
    public function byFile(string $file): ISelect;
    
    public function all(): ISelect;
}
