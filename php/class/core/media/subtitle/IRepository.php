<?php
namespace core\media\subtitle;
use core\media\source\ISource;
use core\media\language\ILanguage;
interface IRepository
{
    public function create(
        ISource $source,
        ILanguage $language,
        string $file
    ): ISubtitle;
    public function read(int $id): ISubtitle;
    public function update(ISubtitle $subtitle);
    public function ofVideo(int $videoId): array;
    public function ofSource(int $sourceId): array;
}
