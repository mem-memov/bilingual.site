<?php
namespace core\media\subtitle;
use core\entity\IEntity;
use core\media\source\ISource;
use core\media\language\ILanguage;
use core\media\phrase\IPhrase;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\ISubtitleStream;
interface ISubtitle extends IEntity
{
    public function source(): ISource;
    public function language(): ILanguage;
    public function file(): string;
    public function phraseNumber(): int;
    public function phrases(): array;
    public function nextPhrases(IPhrase $phrase): array;
    public function addPhrase(IPhrase $phrase);
    public function importStream(IMediaData $mediaData, ISubtitleStream $stream);
    public function importFile();
}
