<?php
namespace core\media\subtitle;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\source\ISource;
use core\media\language\ILanguage;
class Repository implements IRepository
{
    private $subtitle;
    private $query;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $query,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->query = $query;
        $this->migration = $migration;
    }

    public function create(
        ISource $source,
        ILanguage $language,
        string $file
    ): ISubtitle
    {
        $subtitles = $this->query->byFile($file)->run();
        
        if (!empty($subtitles)) {
            throw new exception\RepeatedSubtitleCreation();
        }

        $create = $this->query->create(
            $source->id(),
            $language->id(),
            $file,
            0
        );

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->query->delete($id)->query()
        );

        return $this->read($id);
    }
    
    public function read(int $id): ISubtitle
    {
        $rows = $this->query->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\SubtitleNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function update(ISubtitle $subtitle)
    {
        $update = $this->query->update(
            $subtitle->id(), 
            $subtitle->source()->id(),
            $subtitle->language()->id(),
            $subtitle->file(),
            $subtitle->phraseNumber()
        );
        
        $old = $this->read($subtitle->id());
        
        $revert = $this->query->update(
            $old->id(), 
            $subtitle->source()->id(),
            $subtitle->language()->id(),
            $old->file(),
            $old->phraseNumber()
        );
        
        $update->run();
        
        $this->migration->append(
            $update->query(),
            $revert->query()
        );
    }

    public function ofVideo(int $videoId): array
    {
        return $this->factory->many(
            $this->query->ofVideo($videoId)->run()
        );
    }

    public function ofSource(int $sourceId): array
    {
        return $this->factory->many(
            $this->query->ofSource($sourceId)->run()
        );
    }
}
