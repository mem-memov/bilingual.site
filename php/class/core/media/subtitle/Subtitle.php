<?php
namespace core\media\subtitle;
use core\media\source\ISource;
use core\media\language\ILanguage;
use core\media\phrase\IPhrase;
use core\media\language\IRepository as ILanguageRepository;
use core\media\source\IRepository as ISourceRepository;
use core\media\phrase\IRepository as IPhraseRepository;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\ISubtitleStream;
class Subtitle implements ISubtitle
{
    private $command;
    private $sources;
    private $languages;
    private $phrases;
    private $id;
    private $sourceId;
    private $languageId;
    private $file;
    private $phraseNumber;

    public function __construct(
        command\IFactory $command,
        ISourceRepository $sources,
        ILanguageRepository $languages,
        IPhraseRepository $phrases,
        int $id,
        int $sourceId,
        int $languageId,
        string $file,
        int $phraseNumber
    ) {
        $this->command = $command;
        $this->sources = $sources;
        $this->languages = $languages;
        $this->phrases = $phrases;
        $this->id = $id;
        $this->sourceId = $sourceId;
        $this->languageId = $languageId;
        $this->file = $file;
        $this->phraseNumber = $phraseNumber;
    }
    
    public function id(): int
    {
        return $this->id;
    }

    public function source(): ISource
    {
        return $this->sources->read($this->sourceId);
    }
    
    public function language(): ILanguage
    {
        return $this->languages->read($this->languageId);
    }

    public function file(): string
    {
        return $this->file;
    }
    
    public function phraseNumber(): int
    {
        return $this->phraseNumber;
    }
    
    public function phrases(): array
    {
        return $this->phrases->ofSubtitle($this->id, true);
    }
    
    public function nextPhrases(IPhrase $phrase): array
    {
        return $this->phrases->ofSubtitleAt($this->id, $phrase->orderNumber() + 1);
    }
    
    public function addPhrase(IPhrase $phrase)
    {
        $this->phraseNumber++;
    }

    public function importStream(IMediaData $mediaData, ISubtitleStream $stream)
    {
        $this->command->importStream($this, $mediaData, $stream);
    }
    
    public function importFile()
    {
        $this->command->importFile($this);
    }
}