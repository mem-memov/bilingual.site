<?php
namespace core\media\subtitle\command;
use core\IFactory as ICore;
use core\media\subtitle\ISubtitle;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\ISubtitleStream;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICore $core
    ) {
        $this->core = $core;
    }
    
    public function importStream(ISubtitle $subtitle, IMediaData $mediaData, ISubtitleStream $stream)
    {
        $command = new ImportStream(
                $this->core->service()->mediaEditor(),
                $this->core->configuration()->directory()->copy(),
                $subtitle,
                $mediaData,
                $stream,
                $this
        );

        $command->execute();
    }

    public function importFile(ISubtitle $subtitle)
    {
        $command = new ImportFile(
            $subtitle,
            $this->core->media()->subtitle(),
            $this->core->media()->phrase(),
            $this->core->configuration()->directory()->copy()
        );
        
        $command->execute();
    }
}
