<?php
namespace core\media\subtitle\command;
use core\media\subtitle\ISubtitle;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\ISubtitleStream;
interface IFactory
{
    public function importStream(ISubtitle $subtitle, IMediaData $mediaData, ISubtitleStream $stream);
    public function importFile(ISubtitle $subtitle);
}
