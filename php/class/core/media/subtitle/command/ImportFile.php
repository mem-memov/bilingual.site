<?php
namespace core\media\subtitle\command;
use core\entity\ICommand;
use core\media\subtitle\ISubtitle;
use core\media\subtitle\IRepository as ISubtitleRepository;
use core\media\phrase\IRepository as IPhraseRepository;
class ImportFile implements ICommand
{
    private $subtitle;
    private $subtitles;
    private $phrase;
    private $copyDirectory;
    
    public function __construct(
        ISubtitle $subtitle,
        ISubtitleRepository $subtitles,
        IPhraseRepository $phrase,
        string $copyDirectory
    ) {
        $this->subtitle = $subtitle;
        $this->subtitles = $subtitles;

        $this->phrase = $phrase;
        $this->copyDirectory = $copyDirectory;
    }
    
    public function execute()
    {
        $target = $this->copyDirectory . '/' . $this->subtitle->file();

        $text = file_get_contents($target);

        if (strpos($text, "\n\n") !== false) {
            $delimiter = "\n\n";
        } else {
            $delimiter = "\r\n\r\n";
        }
        $blocks = explode($delimiter, $text);

        $phraseOrderNumber = 1;

        foreach ($blocks as $block) {

            $lines = explode("\n", $block);

            if (count($lines) < 3) {
                continue;
            }
            
            $orderNumber = (int)$lines[0];
            $timing = explode(' --> ', $lines[1]);
            
            $start = preg_split('/(:|,){1}/', $timing[0]);
            if (count($start) < 3) {
                continue;
            }
            if (!isset($start[3])) {
                $start[3] = 0;
            }
            $startMicroseconds = ($start[0] * 3600 + $start[1] * 60 + $start[2]) * 1000 + $start[3];

            $stop = preg_split('/(:|,){1}/', $timing[1]);
            if (!isset($stop[3])) {
                $stop[3] = 0;
            }
            $stopMicroseconds = ($stop[0] * 3600 + $stop[1] * 60 + $stop[2]) * 1000 + $stop[3];
            
            $textLines = array_slice($lines, 2);
            
            $phrase = implode("\n", $textLines);
            
            $phrase = $this->phrase->create(
                $this->subtitle->language(), 
                $this->subtitle,
                $startMicroseconds, 
                $stopMicroseconds, 
                $phrase,
                $phraseOrderNumber
            );
                    
            $phrase->import();
            
            $this->subtitle->addPhrase($phrase);
            $this->subtitles->update($this->subtitle);
            
            $phraseOrderNumber++;
        }
    }
}
