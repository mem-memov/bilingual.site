<?php
namespace core\media\subtitle\command;
use core\entity\ICommand;
use core\service\media\IEditor as IEditorService;
use core\media\subtitle\ISubtitle;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\ISubtitleStream;
class ImportStream implements ICommand
{
    private $editor;
    private $copyDirectory;
    private $subtitle;
    private $mediaData;
    private $stream;
    private $command;
    
    public function __construct(
        IEditorService $editor,
        string $copyDirectory,
        ISubtitle $subtitle, 
        IMediaData $mediaData, 
        ISubtitleStream $stream,
        IFactory $command
    ) {
        $this->editor = $editor;
        $this->copyDirectory = $copyDirectory;
        $this->subtitle = $subtitle;
        $this->mediaData = $mediaData;
        $this->stream = $stream;
        $this->command = $command;
    }
    
    public function execute()
    {
        $target = $this->copyDirectory . '/' . $this->subtitle->file();

        if (!file_exists($target)) {
            $this->editor->extractSubtitle(
                $this->mediaData->file(), 
                $target, 
                $this->stream->index()
            );
        }
        
        $this->command->importFile($this->subtitle);
    }
}
