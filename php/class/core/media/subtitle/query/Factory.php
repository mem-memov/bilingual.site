<?php
namespace core\media\subtitle\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\subtitle\ITable as ISubtitleTable;
use core\database\table\videoSubtitle\ITable as IVideoSubtitleTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
use core\database\query\IUpdate;
class Factory extends EntityQueryFactory implements IFactory
{
    private $subtitle;
    private $videoSubtitle;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        ISubtitleTable $subtitle,
        IVideoSubtitleTable $videoSubtitle
    ) {
        parent::__construct($database, $filters);
        $this->subtitle = $subtitle;
        $this->videoSubtitle = $videoSubtitle;
    }
    
    public function create(
        int $sourceId,
        int $languageId,
        string $file,
        int $phraseNumber
    ): IInsert 
    {
        $values = $this->subtitle->specifications();
        
        $values[$this->subtitle->sourceId()] = $sourceId;
        $values[$this->subtitle->languageId()] = $languageId;
        $values[$this->subtitle->file()] = $file;
        $values[$this->subtitle->phraseNumber()] = $phraseNumber;

        return $this->crud()->create($this->subtitle, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->subtitle, $id);
    }

    public function update(
        int $id, 
        int $sourceId,
        int $languageId,
        string $file,
        int $phraseNumber
    ): IUpdate
    {
        $values = $this->subtitle->specifications();
        
        $values[$this->subtitle->sourceId()] = $sourceId;
        $values[$this->subtitle->languageId()] = $languageId;
        $values[$this->subtitle->file()] = $file;
        $values[$this->subtitle->phraseNumber()] = $phraseNumber;
        
        return $this->crud()->update($this->subtitle, $id, $values);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->subtitle, $id);
    }
    
    public function byFile(string $file): ISelect
    {
        $filters = $this->subtitle->specifications();
        $filters[$this->subtitle->file()] = $this->filters->is($file);
        
        return $this->filter()->records($this->subtitle, $filters);
    }

    public function ofVideo(int $videoId): ISelect
    {
        return $this->manyToMany()->items(
            $this->subtitle,
            $this->videoSubtitle,
            $this->videoSubtitle->videoId(),
            $videoId
        );
    }

    public function ofSource(int $sourceId): ISelect
    {
        return $this->oneToMany()->items(
            $this->subtitle,
            $this->subtitle->sourceId(),
            $sourceId
        );
    }
}
