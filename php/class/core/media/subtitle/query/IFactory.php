<?php
namespace core\media\subtitle\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
use core\database\query\IUpdate;
interface IFactory
{
    public function create(
        int $sourceId,
        int $languageId,
        string $file,
        int $phraseNumber
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function update(
        int $id, 
        int $sourceId,
        int $languageId,
        string $file,
        int $phraseNumber
    ): IUpdate;
    
    public function delete(int $id): IDelete;
    
    public function byFile(string $file): ISelect;

    public function ofVideo(int $videoId): ISelect;
    
    public function ofSource(int $sourceId): ISelect;
}
