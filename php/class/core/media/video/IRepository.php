<?php
namespace core\media\video;
use core\media\source\ISource;
use core\media\language\ILanguage;
use core\media\audio\IAudio;
use core\media\subtitle\ISubtitle;
interface IRepository
{
    public function create(
        ISource $source,
        ILanguage $language,
        string $file,
        int $width,
        int $height,
        int $duration
    ): IVideo;
    public function read(int $id): IVideo;
    public function addAudio(IVideo $video, IAudio $audio);
    public function addSubtitle(IVideo $video, ISubtitle $subtitle);
    public function ofSource(int $sourceId): array;
}
