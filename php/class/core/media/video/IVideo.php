<?php
namespace core\media\video;
use core\entity\IEntity;
use core\media\source\ISource;
use core\media\language\ILanguage;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IVideoStream;
interface IVideo extends IEntity
{
    public function source(): ISource;
    public function language(): ILanguage;
    public function file(): string;
    public function width(): int;
    public function height(): int;
    public function duration(): int;
    public function audios(): array;
    public function subtitles(): array;
    public function import(IMediaData $mediaData, IVideoStream $videoStream);
    public function cut();
    public function urlSecret(): string;
}
