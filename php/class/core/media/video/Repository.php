<?php
namespace core\media\video;
use core\entity\IFactory;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\source\ISource;
use core\media\language\ILanguage;
use core\media\audio\IAudio;
use core\media\subtitle\ISubtitle;
class Repository implements IRepository
{
    private $factory;
    private $query;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $query,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->query = $query;
        $this->migration = $migration;
    }
    
    public function create(
        ISource $source,
        ILanguage $language, 
        string $file, 
        int $width, 
        int $height, 
        int $duration
    ): IVideo
    {
        $videos = $this->query->byFile($file)->run();

        if (!empty($videos)) {
            throw new exception\RepeatedVideoCreation($file);
        }

        $create = $this->query->create(
            $source->id(),
            $language->id(),
            $file,
            $width,
            $height,
            $duration
        );

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->query->delete($id)->query()
        );
        
        return $this->read($id);
    }
    
    public function read(int $id): IVideo
    {
        $rows = $this->query->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\VideoNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function addAudio(IVideo $video, IAudio $audio)
    {
        $addAudio = $this->query->addAudio($video->id(), $audio->id());
        $removeAudio = $this->query->removeAudio($video->id(), $audio->id());

        $this->migration->append(
            $addAudio->query(),
            $removeAudio->query()
        );

        $addAudio->run();
    }

    public function addSubtitle(IVideo $video, ISubtitle $subtitle)
    {
        $addSubtitle = $this->query->addSubtitle($video->id(), $subtitle->id());
        $removeSubtitle = $this->query->removeSubtitle($video->id(), $subtitle->id());

        $this->migration->append(
            $addSubtitle->query(),
            $removeSubtitle->query()
        );

        $addSubtitle->run();
    }

    public function ofSource(int $sourceId): array
    {
        return $this->factory->many(
            $this->query->ofSource($sourceId)->run()
        );
    }
}
