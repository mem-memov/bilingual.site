<?php
namespace core\media\video;
use core\media\source\IRepository as ISourceRepository;
use core\media\language\IRepository as ILanguageRepository;
use core\media\audio\IRepository as IAudioRepository;
use core\media\subtitle\IRepository as ISubtitleRepository;
use core\media\source\ISource;
use core\media\language\ILanguage;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IVideoStream;
class Video implements IVideo
{
    private $command;
    private $sources;
    private $languages;
    private $audio;
    private $subtitle;
    private $id;
    private $sourceId;
    private $languageId;
    private $file;
    private $width;
    private $height;
    private $duration;
    
    public function __construct(
        command\IFactory $command,
        ISourceRepository $sources,
        ILanguageRepository $languages,
        IAudioRepository $audios,
        ISubtitleRepository $subtitles,
        int $id,
        int $sourceId,
        int $languageId,
        string $file, 
        int $width, 
        int $height, 
        int $duration
    ) {
        $this->command = $command;
        $this->sources = $sources;
        $this->languages = $languages;
        $this->audios = $audios;
        $this->subtitles = $subtitles;
        $this->id = $id;
        $this->sourceId = $sourceId;
        $this->languageId = $languageId;
        $this->file = $file;
        $this->width = $width;
        $this->height = $height;
        $this->duration = $duration;
    }
    
    public function id(): int
    {
        return $this->id;
    }

    public function source(): ISource
    {
        return $this->sources->read($this->sourceId);
    }

    public function language(): ILanguage
    {
        return $this->languages->read($this->languageId);
    }

    public function file(): string
    {
        return $this->file;
    }

    public function width(): int
    {
        return $this->width;
    }

    public function height(): int
    {
        return $this->height;
    }

    public function duration(): int
    {
        return $this->duration;
    }

    public function audios(): array
    {
        return $this->audios->ofVideo($this->id);
    }
    
    public function subtitles(): array
    {
        return $this->subtitles->ofVideo($this->id);
    }
    
    public function import(IMediaData $mediaData, IVideoStream $videoStream)
    {
        $this->command->import($this, $mediaData, $videoStream);
    }
    
    public function cut()
    {
        $this->command->cut($this);
    }
    
    public function urlSecret(): string
    {
        return $this->id . '_' . md5($this->file);
    }
}
