<?php
namespace core\media\video\command;
use core\entity\ICommand;
use core\media\video\IVideo;
use core\media\original\IRepository as IOriginalRepository;
use core\media\foreign\IRepository as IForeignRepository;
class Cut implements ICommand
{
    private $video;
    private $originals;
    private $foreigns;
    
    public function __construct(
        IVideo $video,
        IOriginalRepository $originals,
        IForeignRepository $foreigns
    ) {
        $this->video = $video;
        $this->originals = $originals;
        $this->foreigns = $foreigns;
    }
    
    public function execute()
    {
        $audios = $this->video->audios();

        $originalAudio = null;
        
        foreach ($audios as $audio) {
            if ($audio->language()->id() == $this->video->language()->id()) {
                $originalAudio = $audio;
            }
        }
        
        if (is_null($originalAudio)) {
            throw new \Exception('OriginalAudioNotDetected');
        }
        
        
        
        $subtitles = $this->video->subtitles();

        $originalSubtitle = null;

        foreach ($subtitles as $subtitle) {
            if ($subtitle->language()->id() == $this->video->language()->id()) {
                $originalSubtitle = $subtitle;
            }
        }
        
        if (is_null($originalSubtitle)) {
            throw new \Exception('OriginalSubtitleNotDetected');
        }
        
        $originalExists = $this->originals->exists(
            $this->video->language(),
            $this->video,
            $originalAudio,
            $originalSubtitle
        );

        if ($originalExists) {
            throw new \Exception('Original exists');
        }
        
        $original = $this->originals->create(
            $this->video->language(),
            $this->video,
            $originalAudio,
            $originalSubtitle
        );
        
        $original->cut();
        
        foreach ($subtitles as $foreignSubtitle) {

            if($foreignSubtitle->language()->id() == $originalAudio->language()->id()) {
                continue;
            }
            
            foreach ($audios as $foreignAudio) {
                
                if($foreignAudio->language()->id() != $foreignSubtitle->language()->id()) {
                    continue;
                }
                
                $foreignExists = $this->foreigns->exists(
                    $original,
                    $this->video->language(),
                    $this->video,
                    $originalAudio,
                    $originalSubtitle,
                    $foreignAudio,
                    $foreignSubtitle
                );

                if ($foreignExists) {
                    throw new \Exception('Foreign exists');
                }
                
                $foreign = $this->foreigns->create(
                    $original,
                    $this->video->language(),
                    $this->video,
                    $originalAudio,
                    $originalSubtitle,
                    $foreignAudio,
                    $foreignSubtitle
                );
                
                $foreign->cut();
            }
        }
    }
}
