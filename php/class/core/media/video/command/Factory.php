<?php
namespace core\media\video\command;
use core\IFactory as ICore;
use core\media\video\IVideo;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IVideoStream;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICore $core
    ) {
        $this->core = $core;
    }
    
    public function import(IVideo $video, IMediaData $mediaData, IVideoStream $videoStream)
    {
        $command = new Import(
            $video,
            $mediaData,
            $videoStream,
            $this->core->configuration()->directory()->copy(),
            $this->core->service()->mediaEditor()
        );
        
        $command->execute();
    }
    
    public function cut(IVideo $video)
    {
        $command = new Cut(
            $video,
            $this->core->media()->original(),
            $this->core->media()->foreign()
        );
        
        $command->execute();
    }

}
