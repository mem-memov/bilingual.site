<?php
namespace core\media\video\command;
use core\media\video\IVideo;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IVideoStream;
interface IFactory
{
    public function import(IVideo $video, IMediaData $mediaData, IVideoStream $videoStream);
    public function cut(IVideo $video);
}
