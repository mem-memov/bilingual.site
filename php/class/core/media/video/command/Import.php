<?php
namespace core\media\video\command;
use core\entity\ICommand;
use core\media\video\IVideo;
use core\service\media\mediainfo\IMediaData;
use core\service\media\mediainfo\IVideoStream;
use core\service\media\IEditor as IEditorService;
class Import implements ICommand
{
    private $video;
    private $mediaData;
    private $videoStream;
    private $copyDirectory;
    private $editor;
    
    public function __construct(
        IVideo $video,
        IMediaData $mediaData,
        IVideoStream $videoStream,
        string $copyDirectory,
        IEditorService $editor
    ) {
        $this->video = $video;
        $this->mediaData = $mediaData;
        $this->videoStream = $videoStream;
        $this->copyDirectory = $copyDirectory;
        $this->editor = $editor;
    }
    
    public function execute()
    {
        $target = $this->copyDirectory . '/' . $this->video->file();

        if (!file_exists($target)) {

            $this->editor->extractVideo(
                $this->mediaData->file(),
                $target,
                $this->videoStream->index()
            );
        }
    }
}
