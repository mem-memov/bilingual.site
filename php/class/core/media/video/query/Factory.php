<?php
namespace core\media\video\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\video\ITable as IVideoTable;
use core\database\table\videoAudio\ITable as IVideoAudioTable;
use core\database\table\videoSubtitle\ITable as IVideoSubtitleTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
class Factory extends EntityQueryFactory implements IFactory
{
    private $video;
    private $videoAudio;
    private $videoSubtitle;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        IVideoTable $video,
        IVideoAudioTable $videoAudio,
        IVideoSubtitleTable $videoSubtitle
    ) {
        parent::__construct($database, $filters);
        $this->video = $video;
        $this->videoAudio = $videoAudio;
        $this->videoSubtitle = $videoSubtitle;
    }
    
    public function create(
        int $sourceId,
        int $languageId, 
        string $file, 
        int $width, 
        int $height, 
        int $duration
    ): IInsert 
    {
        $values = $this->video->specifications();
        
        $values[$this->video->sourceId()] = $sourceId;
        $values[$this->video->languageId()] = $languageId;
        $values[$this->video->file()] = $file;
        $values[$this->video->width()] = $width;
        $values[$this->video->height()] = $height;
        $values[$this->video->duration()] = $duration;

        return $this->crud()->create($this->video, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->video, $id);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->video, $id);
    }
    
    public function byFile(string $file): ISelect
    {
        $filters = $this->video->specifications();
        $filters[$this->video->file()] = $this->filters->is($file);
        
        return $this->filter()->records($this->video, $filters);
    }
    
    public function ofSource(int $sourceId): ISelect
    {
        return $this->oneToMany()->items(
            $this->video,
            $this->video->sourceId(),
            $sourceId
        );
    }
    
    public function addAudio(int $videoId, int $audioId): IInsert
    {
        return $this->manyToMany()->add(
            $this->videoAudio,
            $this->videoAudio->videoId(),
            $videoId,
            $this->videoAudio->audioId(),
            $audioId
        );
    }
    
    public function removeAudio(int $videoId, int $audioId): IDelete
    {
        return $this->manyToMany()->remove(
            $this->videoAudio,
            $this->videoAudio->videoId(),
            $videoId,
            $this->videoAudio->audioId(),
            $audioId
        );
    }

    public function addSubtitle(int $videoId, int $subtitleId): IInsert
    {
        return $this->manyToMany()->add(
            $this->videoSubtitle,
            $this->videoSubtitle->videoId(),
            $videoId,
            $this->videoSubtitle->subtitleId(),
            $subtitleId
        );
    }
    
    public function removeSubtitle(int $videoId, int $subtitleId): IDelete
    {
        return $this->manyToMany()->remove(
            $this->videoSubtitle,
            $this->videoSubtitle->videoId(),
            $videoId,
            $this->videoSubtitle->subtitleId(),
            $subtitleId
        );
    }
}
