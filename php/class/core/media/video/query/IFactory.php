<?php
namespace core\media\video\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
interface IFactory
{
    public function create(
        int $sourceId,
        int $languageId, 
        string $file, 
        int $width, 
        int $height, 
        int $duration
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function delete(int $id): IDelete;
    
    public function byFile(string $file): ISelect;
    
    public function ofSource(int $sourceId): ISelect;
    
    public function addAudio(int $videoId, int $audioId): IInsert;
    
    public function removeAudio(int $videoId, int $audioId): IDelete;
    
    public function addSubtitle(int $videoId, int $subtitleId): IInsert;
    
    public function removeSubtitle(int $videoId, int $subtitleId): IDelete;
}
