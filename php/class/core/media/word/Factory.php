<?php
namespace core\media\word;
use core\media\IFactory as IRepositoryFactory;
use core\entity\{IEntity, Factory as EntityFactory};
class Factory extends EntityFactory
{
    private $repositories;
    
    public function __construct(
        IRepositoryFactory $repositories
    ) {
        $this->repositories = $repositories;
    }

    public function one(array $row): IEntity
    {
        return new Word(
            $this->repositories->language(),
            (int)$row['id'],
            (int)$row['language_id'],
            (string)$row['word']
        );
    }
}
