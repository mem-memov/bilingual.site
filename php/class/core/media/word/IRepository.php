<?php
namespace core\media\word;
use core\media\language\ILanguage;
interface IRepository
{
    public function provide(ILanguage $language, string $word): IWord;
    public function create(ILanguage $language, string $word): IWord;
    public function read(int $id): IWord;
    public function ofPhrase(int $phraseId): array;
}
