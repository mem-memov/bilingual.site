<?php
namespace core\media\word;
use core\entity\IEntity;
use core\media\language\ILanguage;
interface IWord extends IEntity
{
    public function language(): ILanguage;
    public function word(): string;
}
