<?php
namespace core\media\word;
use core\entity\IFactory;
use core\service\cache\ICache;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\language\ILanguage;
class Repository implements IRepository
{
    private $factory;
    private $query;
    private $cache;
    private $migration;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $query,
        ICache $cache,
        IMigrationRecording $migration
    ) {
        $this->factory = $factory;
        $this->query = $query;
        $this->cache = $cache;
        $this->migration = $migration;
    }
    
    public function provide(ILanguage $language, string $word): IWord
    {
        $words = $this->query->wordOfLanguage($language->id(), $word)->run();
        
        if (empty($words)) {
            return $this->create($language, $word);
        } else {
            return $this->factory->one($words[0]);
        }
    }
    
    public function create(
        ILanguage $language, 
        string $word
    ): IWord
    {
        $key = 'media.word.'.$word.'.ofLanguage.'.$language->id();
        $words = $this->cache->get($key);
        
        if ($words === false) {
            $words = $this->query->wordOfLanguage($language->id(), $word)->run();
            $this->cache->set($key, $words);
        }

        if (!empty($words)) {
            throw new exception\RepeatedWordCreation($language, $word);
        }

        $create = $this->query->create(
            $language->id(),
            $word
        );

        $id = $create->run();

        $this->migration->append(
            $create->query(),
            $this->query->delete($id)->query()
        );
        
        return $this->read($id);
    }
    
    public function read(int $id): IWord
    {
        $key = 'media.word.'.$id;
        
        $rows = $this->cache->get($key);
        
        if ($rows === false) {
            $rows = $this->query->read($id)->run();
            $this->cache->set($key, $rows);
        }

        if (empty($rows)) {
            throw new exception\WordNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function ofPhrase(int $phraseId): array
    {
        $key = 'media.word.ofPhrase.'.$phraseId;
        
        $rows = $this->cache->get($key);
        
        if ($rows === false) {
            $rows = $this->queries->ofPhrase($phraseId)->run();
            $this->cache->set($key, $rows);
        }
        
        return $this->factory->many($rows);
    }
}
