<?php
namespace core\media\word;
use core\media\language\IRepository as ILanguageRepository;
use core\media\language\ILanguage;
class Word implements IWord
{
    private $languages;
    private $id;
    private $languageId;
    private $word;
    
    public function __construct(
        ILanguageRepository $languages,
        int $id,
        int $languageId,
        string $word
    ) {
        $this->languages = $languages;
        $this->id = $id;
        $this->languageId = $languageId;
        $this->word = $word;
    }
    
    public function id(): int
    {
        return $this->id;
    }

    public function language(): ILanguage
    {
        return $this->languages->read($this->languageId);
    }

    public function word(): string
    {
        return $this->word;
    }
}
