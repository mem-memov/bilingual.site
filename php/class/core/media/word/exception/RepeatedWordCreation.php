<?php
namespace core\media\language\exception;
class RepeatedWordCreation extends Exception
{
    public function __construct(ILanguage $language, string $word)
    {
        parent::__construct('Language ' . $language->language() . '(' . $language->id() . ') , word: ' . $word);
    }
}
