<?php
namespace core\media\word\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\word\ITable as IWordTable;
use core\database\table\phraseWord\ITable as IPhraseWordTable;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
class Factory extends EntityQueryFactory implements IFactory
{
    private $word;
    private $phraseWord;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        IWordTable $word,
        IPhraseWordTable $phraseWord
    ) {
        parent::__construct($database, $filters);
        $this->word = $word;
        $this->phraseWord = $phraseWord;
    }
    
    public function create(
        int $languageId, 
        string $word
    ): IInsert 
    {
        $values = $this->word->specifications();
        
        $values[$this->word->languageId()] = $languageId;
        $values[$this->word->word()] = $word;

        return $this->crud()->create($this->word, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->word, $id);
    }

    public function delete(int $id): IDelete
    {
        return $this->crud()->delete($this->word, $id);
    }
    
    public function wordOfLanguage(int $languageId, string $word): ISelect
    {
        $filters = $this->word->specifications();
        $filters[$this->word->languageId()] = $this->filters->is($languageId);
        $filters[$this->word->word()] = $this->filters->is($word);

        return $this->filter()->records($this->word, $filters);
    }
    
    public function ofPhrase(int $phraseId): ISelect
    {
        return $this->manyToMany()->items(
            $this->word,
            $this->phraseWord,
            $this->phraseWord->phraseId(),
            $phraseId
        );
    }
}
