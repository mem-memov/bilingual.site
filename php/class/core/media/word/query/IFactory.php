<?php
namespace core\media\word\query;
use core\database\query\IDelete;
use core\database\query\IInsert;
use core\database\query\ISelect;
interface IFactory
{
    public function create(
        int $languageId, 
        string $word
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function delete(int $id): IDelete;
    
    public function wordOfLanguage(int $languageId, string $word): ISelect;
    
    public function ofPhrase(int $phraseId): ISelect;
}
