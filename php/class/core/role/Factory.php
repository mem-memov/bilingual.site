<?php
namespace core\role;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;

    private $developer;
    private $user;
    private $cutter;
    private $visitor;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;

        $this->developer = null;
        $this->user = null;
        $this->cutter = null;
        $this->visitor = null;
    }
    
    public function developer(): developer\IFactory
    {
        if (is_null($this->developer)) {
            $this->developer = new developer\Factory($this->core);
        }

        return $this->developer;
    }
    
    public function user(): user\IFactory
    {
        if (is_null($this->user)) {
            $this->user = new user\Factory($this->core);
        }

        return $this->user;
    }
    
    public function cutter(): cutter\IFactory
    {
        if (is_null($this->cutter)) {
            $this->cutter = new cutter\Factory($this->core);
        }

        return $this->cutter;
    }
    
    public function visitor(): visitor\IFactory
    {
        if (is_null($this->visitor)) {
            $this->visitor = new visitor\Factory($this->core);
        }

        return $this->visitor;
    }
}

