<?php
namespace core\role;
interface IAction
{
    public function perform(): array;
}
