<?php
namespace core\role;
interface IFactory
{
    public function developer(): developer\IFactory;
    public function user(): user\IFactory;
    public function cutter(): cutter\IFactory;
    public function visitor(): visitor\IFactory;
}

