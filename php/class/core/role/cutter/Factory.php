<?php
namespace core\role\cutter;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;
    private $media;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;
        $this->media = null;
    }
    
    public function media(): media\IFactory
    {
        if (is_null($this->media)) {
            $this->media = new media\Factory(
                $this->core
            );
        }

        return $this->media;
    }
}
