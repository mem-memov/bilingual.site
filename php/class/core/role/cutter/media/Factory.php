<?php
namespace core\role\cutter\media;
use core\role\IAction;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;
    }

    public function import(): IAction
    {
        $masters = new import\Masters();
        
        return new import\Action(
            $this->core->database()->transaction(),
            $this->core->database()->migration()->recording(),
            $this->core->media()->source(),
            $this->core->configuration()->directory()->master(),
            $this->core->configuration()->directory()->copy()
        );
    }

    public function cut(): IAction
    {
        return new cut\Action(
            $this->core->database()->transaction(),
            $this->core->database()->migration()->recording(),
            $this->core->media()->source()
        );
    }
}

