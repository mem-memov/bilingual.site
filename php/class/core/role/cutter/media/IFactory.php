<?php
namespace core\role\cutter\media;
use core\role\IAction;
interface IFactory
{
    public function import(): IAction;
    public function cut(): IAction;
}

