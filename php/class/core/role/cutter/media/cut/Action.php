<?php
namespace core\role\cutter\media\cut;
use core\service\sql\ITransaction;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\source\IRepository as ISourceRepository;
class Action implements IAction
{
    private $transaction;
    private $migration;
    private $sources;
    
    public function __construct(
        ITransaction $transaction,
        IMigrationRecording $migration,
        ISourceRepository $sources
    ) {
        $this->transaction = $transaction;
        $this->migration = $migration;
        $this->sources = $sources;
    }

    public function perform(): array
    {
        $sources = $this->sources->all();
        
        foreach ($sources as $source) {
            
            $this->transaction->start();
            $this->migration->start('media/' . $source->file() . '/cut');

            $videos = $source->videos(); // one video
            
            foreach ($videos as $video) {
                $video->cut();
            }

            $this->migration->save();
            $this->transaction->commit();
        }        

        return [];
    }
}
