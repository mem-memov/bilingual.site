<?php
namespace core\role\cutter\media\import;
use core\service\sql\ITransaction;
use core\database\migration\IRecording as IMigrationRecording;
use core\media\source\IRepository as ISourceRepository;
class Action implements IAction
{
    private $transaction;
    private $migration;
    private $sources;
    private $masterDirectory;
    private $copyDirectory;
    
    public function __construct(
        ITransaction $transaction,
        IMigrationRecording $migration,
        ISourceRepository $sources,
        string $masterDirectory,
        string $copyDirectory
    ) {
        $this->transaction = $transaction;
        $this->migration = $migration;
        $this->sources = $sources;
        $this->masterDirectory = $masterDirectory;
        $this->copyDirectory = $copyDirectory;
    }

    public function perform(): array
    {
        $directory = new \RecursiveDirectoryIterator($this->masterDirectory, \FilesystemIterator::CURRENT_AS_PATHNAME);
        $iterator = new \RecursiveIteratorIterator($directory);
        $paths = new \RegexIterator($iterator, '/^.+masters\.php$/i', \RecursiveRegexIterator::GET_MATCH);
        
        $sortedPaths = [];
        foreach ($paths as $path) {
            $sortedPaths[] = $path[0];
        }
        
        sort($sortedPaths, SORT_STRING);

        $providers = [];
        foreach ($sortedPaths as $sortedPath) {
            $providers[] = include($sortedPath);
        }
        
        $masters = [];
        foreach ($providers as $provider) {
            $providedMasters = $provider();
            if (!is_array($providedMasters) || empty($providedMasters)) {
                continue;
            }
            $masters = array_merge($masters, $providedMasters);
        }

        foreach ($masters as $masterIndex => $master) {
            $masters[$masterIndex]['video']['path'] = str_replace($this->masterDirectory, '', $master['video']['path']);
            $masters[$masterIndex]['video']['path'] = trim($masters[$masterIndex]['video']['path'], '\/');
        }
        
        foreach ($masters as $master) {

            $copyDirectory = $this->copyDirectory . '/' . dirname($master['video']['path']) . '/' . pathinfo($master['video']['path'], PATHINFO_FILENAME);

            if (!file_exists($copyDirectory)) {
                mkdir($copyDirectory, 0777, true);
            }

            if (isset($master['subtitles']) && !empty($master['subtitles'])) {

                foreach ($master['subtitles'] as $language => $subtitle) {
                    $newSubtitlePath = $copyDirectory . '/' . ucfirst($language) . '.' . pathinfo($subtitle['path'], PATHINFO_EXTENSION);
                    $subtitleText = file_get_contents($subtitle['path']);
                    if (isset($subtitle['encoding'])) {
                        $subtitleText = iconv($subtitle['encoding'], 'UTF-8', $subtitleText);
                    }
                    file_put_contents($newSubtitlePath, $subtitleText);
                }
            }
        }

        foreach ($masters as $master) {

            $this->transaction->start();
            $this->migration->start('media/' . $master['video']['path'] . '/import');

            try {

                $source = $this->sources->create($master['video']['path']);
                $source->import();

                $this->migration->save();
                $this->transaction->commit();
                
            } catch (\Exception $e) {

                $this->transaction->rollback();

                return [
                    'error' => [
                        'type' => get_class($e),
                        'message' => $e->getMessage()
                    ]
                ];
            }
        }

        return [];
    }
}
