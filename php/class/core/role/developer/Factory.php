<?php
namespace core\role\developer;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;

    private $database;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;

        $this->database = null;
    }
    
    public function database(): database\IFactory
    {
        if (is_null($this->database)) {
            $this->database = new database\Factory(
                $this->core
            );
        }

        return $this->database;
    }
}
