<?php
namespace core\role\developer\database;
use core\role\IAction;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;
    private $migration;
    private $schema;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;
        $this->migration = null;
        $this->schema = null;
    }

    public function migration(): migration\IFactory
    {
        if (is_null($this->migration)) {
            $this->migration = new migration\Factory(
                $this->core
            );
        }

        return $this->migration;
    }
    
    public function schema(): schema\IFactory
    {
        if (is_null($this->schema)) {
            $this->schema = new schema\Factory(
                $this->core
            );
        }
        
        return $this->schema;
    }
}

