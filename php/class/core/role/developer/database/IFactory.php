<?php
namespace core\role\developer\database;
interface IFactory
{
    public function schema(): schema\IFactory;
    public function migration(): migration\IFactory;
}

