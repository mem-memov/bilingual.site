<?php
namespace core\role\developer\database\migration;
use core\role\IAction;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;
    }

    public function listAll(): IAction
    {
        return new listAll\Action(
            $this->core->database()->migration()
        );
    }

    public function current(): IAction
    {
        return new current\Action(
            $this->core->database()->migration()
        );
    }

    public function apply(): IAction
    {
        return new apply\Action(
            $this->core->database()->migration()
        );
    }

    public function show(): IAction
    {
        return new show\Action(
            $this->core->database()->migration()
        );
    }
}
