<?php
namespace core\role\developer\database\migration;
use core\role\IAction;
interface IFactory
{
    public function listAll(): IAction;
    public function current(): IAction;
    public function apply(): IAction;
    public function show(): IAction;
}
