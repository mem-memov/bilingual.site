<?php
namespace core\role\developer\database\migration\apply;
use core\service\sql\ITransaction;
use core\database\migration\IFacade as IMigration;
use core\database\migration\exception\MigrationsNotUnique;
class Action implements IAction
{
    private $migration;
    private $targetMigrationName;
    
    public function __construct(
        IMigration $migration
    ) {
        $this->migration = $migration;
        $this->targetMigrationName = '';
    }
    
    public function setTargetMigration(string $name): IAction
    {
        $this->targetMigrationName = $name;
        
        return $this;
    }
    
    public function perform(): array
    {
        try {

            $queries = $this->migration->migrate($this->targetMigrationName);

            return [
                'queries' => $queries
            ];

        } catch (MigrationsNotUnique $e) {

            return [
                'error' => [
                    'type' => 'MigrationsNotUnique',
                    'message' => $e->getMessage()
                ]
            ];
        }
    }
}
