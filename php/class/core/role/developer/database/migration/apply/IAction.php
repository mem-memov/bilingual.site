<?php
namespace core\role\developer\database\migration\apply;
use core\role\IAction as IBaseAction;
interface IAction extends IBaseAction
{
    public function setTargetMigration(string $name): IAction;
}
