<?php
namespace core\role\developer\database\migration\current;
use core\database\migration\IFacade as IMigration;
class Action implements IAction
{
    private $migration;
    
    public function __construct(
        IMigration $migration
    ) {
        $this->migration = $migration;
    }

    public function perform(): array
    {
        return [
            'current' => $this->migration->getCurrent()
        ];
    }
}
