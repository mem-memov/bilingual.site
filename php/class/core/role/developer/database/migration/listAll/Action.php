<?php
namespace core\role\developer\database\migration\listAll;
use core\database\migration\IFacade as IMigration;
class Action implements IAction
{
    private $migration;
    
    public function __construct(
        IMigration $migration
    ) {
        $this->migration = $migration;
    }

    public function perform(): array
    {
        return [
            'migrations' => $this->migration->getAll()
        ];
    }
}
