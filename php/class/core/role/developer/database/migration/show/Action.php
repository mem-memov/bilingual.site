<?php
namespace core\role\developer\database\migration\show;
use core\database\migration\IFacade as IMigration;
class Action implements IAction
{
    private $migration;
    private $migrationKey;
    
    public function __construct(
        IMigration $migration
    ) {
        $this->migration = $migration;
        $this->migrationKey = null;
    }

    public function setMigrationKey(string $migrationKey): IAction
    {
        $this->migrationKey = $migrationKey;

        return $this;
    }

    public function perform(): array
    {
        return [
            'migration' => $this->migration->viewMigration($this->migrationKey)
        ];
    }
}
