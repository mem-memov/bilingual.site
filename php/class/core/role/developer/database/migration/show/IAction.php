<?php
namespace core\role\developer\database\migration\show;
use core\role\IAction as IBaseAction;
interface IAction extends IBaseAction
{
    public function setMigrationKey(string $migrationKey): IAction;
}
