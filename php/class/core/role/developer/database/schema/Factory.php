<?php
namespace core\role\developer\database\schema;
use core\role\IAction;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $coreFactory;
    
    public function __construct(
        ICoreFactory $coreFactory
    ) {
        $this->coreFactory = $coreFactory;
    }
    
    public function view(): IAction
    {
        return new view\Action(
            $this->coreFactory->database()->schema()
        );
    }
}
