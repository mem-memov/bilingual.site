<?php
namespace core\role\developer\database\schema;
use core\role\IAction;
interface IFactory
{
    public function view(): IAction;
}
