<?php
namespace core\role\developer\database\schema\view;
use core\database\ISchema;
class Action implements IAction
{
    private $schema;
    
    public function __construct(
        ISchema $schema
    ) {
        $this->schema = $schema;
    }
    
    public function perform(): array
    {
        return $this->schema->view();
    }
}

