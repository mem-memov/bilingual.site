<?php
namespace core\role\user;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;

    private $authentication;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;

        $this->authentication = null;
    }
    
    public function authentication(): authentication\IFactory
    {
        if (is_null($this->authentication)) {
            $this->authentication = new authentication\Factory(
                $this->core
            );
        }

        return $this->authentication;
    }
}
