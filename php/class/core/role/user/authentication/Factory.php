<?php
namespace core\role\user\authentication;
use core\role\IAction;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;
    }
    
    public function login(): IAction
    {
        return new login\Action(
            $this->core->user()->authentication()
        );
    }
    
    public function loginByHash(): IAction
    {
        return new loginByHash\Action(
            $this->core->user()->authentication()
        );
    }
    
    public function changePassword(): IAction
    {
        return new changePassword\Action(
            $this->core->user()->authentication()
        );
    } 
}
