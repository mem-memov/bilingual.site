<?php
namespace core\role\user\authentication;
use core\role\IAction;
interface IFactory
{
    public function login(): IAction;
    public function loginByHash(): IAction;
    public function changePassword(): IAction;
    public function byEmail(string $email): array;
}
