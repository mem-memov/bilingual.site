<?php
namespace core\role\user\authentication\changePassword;
use core\user\authentication\IRepository as IAuthenticationRepository;
class Action implements IAction
{
    private $authentications;
    private $id;
    private $password;
    
    public function __construct(
        IAuthenticationRepository $authentications
    ) {
        $this->authentications = $authentications;
        $this->id = null;
        $this->password = null;
    }

    public function id(int $id): IAction
    {
        $this->id = $id;
        
        return $this;
    }

    public function password(string $password): IAction
    {
        $this->password = $password;
        
        return $this;
    }
    
    public function perform(): array
    {
        try {
            
            $authetnication = $this->authentications->read($this->id);
            $authetnication->changePassword($this->password);
            
        } catch (\core\user\authentication\exception\Exception $e) {
            
            $error = substr(strrchr(get_class($e), '\\'), 1);
            
            return [
                'error' => $error
            ];
            
        }

        return [];
    }
}
