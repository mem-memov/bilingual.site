<?php
namespace core\role\user\authentication\changePassword;
use core\role\IAction as IBaseAction;
interface IAction extends IBaseAction
{
    public function id(int $id): IAction;
    public function password(string $password): IAction;
}
