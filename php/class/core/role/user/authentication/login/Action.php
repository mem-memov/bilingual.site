<?php
namespace core\role\user\authentication\login;
use core\user\authentication\IRepository as IAuthenticationRepository;
class Action implements IAction
{
    private $authentications;
    private $email;
    private $password;
    
    public function __construct(
        IAuthenticationRepository $authentications
    ) {
        $this->authentications = $authentications;
        $this->email = null;
        $this->password = null;
    }
    
    public function email(string $email): IAction
    {
        $this->email = $email;
        
        return $this;
    }
    
    public function password(string $password): IAction
    {
        $this->password = $password;
        
        return $this;
    }
    
    public function perform(): array
    {
        $authentications = $this->authentications->byEmail($this->email);
        
        if (count($authentications) > 1) {
            throw new \Exception('Multiple users with the same email');
        }
        
        if (!empty($authentications)) {
            
            $authentication = $authentications[0];
            
            try {
                
                $authentication->verifyPassword($this->password);
                
            } catch (\core\user\authentication\exception\WrongPassword $e) {

                if (!is_null($authentication->restoreHash()) && !is_null($authentication->restoreDate())) {

                    $now = new \DateTime();
                    $timePassed = $now->diff($authentication->restoreDate());
                    $minutes = $timePassed->format('%i');

                    //if ($minutes > 5) {
                        $authentication->restorePassword();
                    //}
                }
                
                $authentication->restorePassword();
                
                return [
                    'error' => 'WrongPassword'
                ];
                
            } catch (\core\user\authentication\exception\Exception $e) {

                $error = substr(strrchr(get_class($e), '\\'), 1);

                return [
                    'error' => $error
                ];

            }
            
        } else {
            
            try {
                
                $authentication = $this->authentications->create($this->email, $this->password);
                
            } catch (\core\user\authentication\exception\Exception $e) {

                $error = substr(strrchr(get_class($e), '\\'), 1);

                return [
                    'error' => $error
                ];

            }
            
        }

        return [
            'id' => $authentication->id(),
            'email' => $authentication->email()
        ];
    }
}
