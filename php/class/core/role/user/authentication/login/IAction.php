<?php
namespace core\role\user\authentication\login;
use core\role\IAction as IBaseAction;
interface IAction extends IBaseAction
{
    public function email(string $email): IAction;
    public function password(string $password): IAction;
}
