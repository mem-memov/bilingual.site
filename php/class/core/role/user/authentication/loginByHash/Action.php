<?php
namespace core\role\user\authentication\loginByHash;
use core\user\authentication\IRepository as IAuthenticationRepository;
class Action implements IAction
{
    private $authentications;
    private $hash;
    
    public function __construct(
        IAuthenticationRepository $authentications
    ) {
        $this->authentications = $authentications;
        $this->hash = null;
    }
    
    public function hash(string $hash): IAction
    {
        $this->hash = $hash;
        
        return $this;
    }
    
    public function perform(): array
    {
        $authentications = $this->authentications->byRestoreHash($this->hash);
        
        if (count($authentications) > 1) {
            throw new \Exception('Multiple users with the same restore hash');
        }

        if (empty($authentications)) {
            
            return [
                'error' => 'HashNotFound'
            ];
            
        }

        $authentication = $authentications[0];

        $now = new \DateTime();
        $timePassed = $now->diff($authentication->restoreDate());
        $hours = $timePassed->format('%h');

        if ($hours > 2) {
            
            $authentication->clearRestoreHash();
            return [
                'error' => 'StaleHash'
            ];
            
        }
        
        $authentication->clearRestoreHash();

        return [
            'id' => $authentication->id(),
            'email' => $authentication->email()
        ];
    }
}
