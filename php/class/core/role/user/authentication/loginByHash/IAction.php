<?php
namespace core\role\user\authentication\loginByHash;
use core\role\IAction as IBaseAction;
interface IAction extends IBaseAction
{
    public function hash(string $hash): IAction;
}
