<?php
namespace core\role\visitor;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;

    private $lesson;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;

        $this->lesson = null;
    }
    
    public function lesson(): lesson\IFactory
    {
        if (is_null($this->lesson)) {
            $this->lesson = new lesson\Factory(
                $this->core
            );
        }

        return $this->lesson;
    }
}
