<?php
namespace core\role\visitor\lesson;
use core\role\IAction;
use core\IFactory as ICoreFactory;
class Factory implements IFactory
{
    private $core;
    
    public function __construct(
        ICoreFactory $core
    ) {
        $this->core = $core;
    }
    
    public function teach(): IAction
    {
        return new teach\Action(
            $this->core->media()->cut()
        );
    }
}

