<?php
namespace core\role\visitor\lesson;
use core\role\IAction;
interface IFactory
{
    public function teach(): IAction;
}

