<?php
namespace core\role\visitor\lesson\teach;
use core\media\cut\IRepository as ICutRepository;
class Action implements IAction
{
    private $cut;
    
    public function __construct(
        ICutRepository $cut
    ) {
        $this->cut = $cut;
    }

    public function perform(): array
    {
        $cuts = $this->cut->all();

        $cutCount = count($cuts);
        
        if ($cutCount == 0) {
            return [
                'clips' => []
            ];
        }

        $cutIndex = rand(0, $cutCount-1);
        
        $cut = $cuts[$cutIndex];

        $video = $cut->video();
        
        $videoDuration = $video->duration();
        $lessonDuration = 40000;
        
        if ($lessonDuration > $videoDuration) {
            $lessonDuration = $videoDuration;
        }
        
        $lessonStart = rand(0, ($videoDuration - $lessonDuration));
        $lessonStop = $lessonStart + $lessonDuration;

        $originalClips = $cut->originalClips($lessonStart, $lessonStop);
        $foreignClips = $cut->foreignClips($lessonStart, $lessonStop);

        $clips = [];

        foreach ($foreignClips as $foreignClip) {
            $clips[] = [
                'type' => 'foreign',
                'file' => '/asset/cut/' . $foreignClip->file(),
                'phrase' => strip_tags($foreignClip->phrase()->phrase()),
                'start' => $foreignClip->phrase()->start(),
                'stop' => $foreignClip->phrase()->stop(),
                'duration' => $foreignClip->duration()
            ];
        }

        foreach ($originalClips as $originalClip) {
            $clips[] = [
                'type' => 'original',
                'file' => '/asset/cut/' . str_replace('public/', '', $originalClip->file()),
                'phrase' => strip_tags($originalClip->phrase()->phrase()),
                'start' => $originalClip->phrase()->start(),
                'stop' => $originalClip->phrase()->stop(),
                'duration' => $originalClip->duration()
            ];
        }
        
        return [
            'clips' => $clips
        ];
    }
}
