<?php
namespace core\service;
use core\configuration\IFactory as IConfigurationFactory;
class Factory implements IFactory
{
    private $configuration;

    private $database;
    private $cache;
    private $mediaEditor;
    private $task;
    
    public function __construct(
        IConfigurationFactory $configuration
    ) {
        $this->configuration = $configuration;

        $this->database = null;
        $this->cache = null;
        $this->mediaEditor = null;
        $this->task = null;
    }
    
    public function database(): sql\IDatabase
    {
        if (is_null($this->database)) {

            switch ($this->configuration->database()->type()) {
                case 'MySql':

                    $resultFactory = new sql\ResultFactory();

                    $driver = new sql\mySql\MysqliAdapter(
                        $resultFactory,
                        $this->configuration->database()->server(),
                        $this->configuration->database()->port(),
                        $this->configuration->database()->user(),
                        $this->configuration->database()->password(),
                        $this->configuration->database()->database()
                    );

                    $query = new sql\Query($driver);
                    $transaction = new sql\mySql\Transaction($driver);
                    $renderer = new sql\mySql\Renderer($driver);

                    $this->database = new sql\Database($query, $transaction, $renderer);

                    break;
                default:
                    throw new exception\UnknownDatabaseType($this->configuration->database()->type());
                    break;
            }
        }

        return $this->database;
    }
    
    public function cache(): cache\ICache
    {
        if (is_null($this->cache)) {
            switch ($this->configuration->cache()->type()) {
                case 'memcached':
                    $driver = new cache\memcached\Adapter();
                    $this->cache = new cache\Cache($driver);
                    break;
                default:
                    throw new exception\UnknownCacheType($this->configuration->database()->type());
                    break;
            }
        }
        
        return $this->cache;
    }
    
    public function mediaEditor(): media\IEditor
    {
        if (is_null($this->mediaEditor)) {

            $ffmpeg = new media\Ffmpeg();
            $sox = new media\Sox();
            $handBreakCli = new media\HandBreakCli();

            $streamFactory = new media\mediainfo\StreamFactory();
            $mediaDataFactory = new media\mediainfo\MediaDataFactory($streamFactory);
            $mediainfo = new media\mediainfo\Mediainfo($mediaDataFactory);

            $this->mediaEditor = new media\Editor(
                $ffmpeg,
                $sox,
                $handBreakCli,
                $mediainfo
            );
        }

        return $this->mediaEditor;
    }
    
    public function task(): task\IFactory
    {
        if (is_null($this->task)) {

            $queue = new task\rabbitMq\AmqplibAdapter(
                $this->configuration->queue()->server(),
                $this->configuration->queue()->port(),
                $this->configuration->queue()->user(),
                $this->configuration->queue()->password()
            );

            $this->task = new task\Factory(
                $queue
            );;
        }

        return $this->task;
    }
}