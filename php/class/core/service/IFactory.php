<?php
namespace core\service;
interface IFactory
{
    public function database(): sql\IDatabase;
    public function cache(): cache\ICache;
    public function mediaEditor(): media\IEditor;
    public function task(): task\IFactory;
}

