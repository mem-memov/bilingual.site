<?php
namespace core\service\cache;
class Cache implements ICache
{
    private $driver;
    
    public function __construct(
        IDriver $driver
    ) {
        $this->driver = $driver;
    }

    public function get(string $key)
    {
        return $this->driver->get($key);
    }
    
    public function set(string $key, $value)
    {
        $this->driver->set($key, $value);
    }
    
    public function delete(string $key)
    {
        return $this->driver->delete($key);
    }
}
