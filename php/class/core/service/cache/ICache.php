<?php
namespace core\service\cache;
interface ICache
{
    public function get(string $key);
    public function set(string $key, $value);
    public function delete(string $key);
}
