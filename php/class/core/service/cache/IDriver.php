<?php
namespace core\service\cache;
interface IDriver
{
    public function get(string $key);
    public function set(string $key, $value);
    public function delete(string $key);
}
