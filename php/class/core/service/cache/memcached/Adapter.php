<?php
namespace core\service\cache\memcached;
use core\service\cache\IDriver;
class Adapter implements IDriver
{
    private $memcached;
    
    public function __construct(
    ) {
        $this->memcached = null;
    }
    
    public function connect()
    {
        if (is_null($this->memcached)) {
            $this->memcached = new \Memcached();
        }
    }

    public function get(string $key)
    {
        $this->connect();
        
        return $this->memcached->get($key);
    }
    
    public function set(string $key, $value)
    {
        $this->connect();

        $this->memcached->set($key, $value);
    }
    
    public function delete(string $key)
    {
        $this->connect();
        
        return $this->memcached->delete($key);
    }
}
