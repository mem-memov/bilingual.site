<?php
namespace core\service\media;
class BlackBorder implements IBlackBorder
{
    private $width;
    private $height;
    private $offsetX;
    private $offsetY;
    
    public function __construct(
        int $width, 
        int $height, 
        int $offsetX, 
        int $offsetY
    ) {
        $this->width = $width;
        $this->height = $height;
        $this->offsetX = $offsetX;
        $this->offsetY = $offsetY;
    }
    
    public function width(): int
    {
        return $this->width;
    }
    
    public function height(): int
    {
        return $this->height;
    }
    
    public function offsetX(): int
    {
        return $this->offsetX;
    }
    
    public function offsetY(): int
    {
        return $this->offsetY;
    }
}
