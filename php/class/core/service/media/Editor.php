<?php
namespace core\service\media;
class Editor implements IEditor
{
    private $ffmpeg;
    private $sox;
    private $handBreakCli;
    private $mediainfo;
    
    public function __construct(
        IFfmpeg $ffmpeg,
        ISox $sox,
        IHandBreakCli $handBreakCli,
        mediainfo\IMediainfo $mediainfo
    ) {
        $this->ffmpeg = $ffmpeg;
        $this->sox = $sox;
        $this->handBreakCli = $handBreakCli;
        $this->mediainfo = $mediainfo;
    }
    
    public function getMediaData(string $source): mediainfo\IMediaData
    {
        return $this->mediainfo->getMediaData($source);
    }

    public function convertDvd(string $source, string $target)
    {
        $this->handBreakCli->convertDvd($source, $target);
    }
    
    public function extractVideo(string $source, string $target, int $streamIndex)
    {
        return $this->ffmpeg->extractVideo($source, $target, $streamIndex);
    }
    
    public function extractAudio(string $source, string $target, int $streamIndex)
    {
        return $this->ffmpeg->extractAudio($source, $target, $streamIndex);
    }
    
    public function extractSubtitle(string $source, string $target, int $streamIndex)
    {
        return $this->ffmpeg->extractSubtitle($source, $target, $streamIndex);
    }
    
    public function concatenateVideo(array $parts, string $target)
    {
        return $this->ffmpeg->concatenateVideo($parts, $target);
    }

    public function detectBlackBorder(string $video): IBlackBorder
    {
        return $this->ffmpeg->detectBlackBorder($video);
    }
    
    public function cutVideo(string $source, string $target, int $start, int $duration, int $height)
    {
        return $this->ffmpeg->cutVideo($source, $target, $start, $duration, $height);
    }
    
    public function cutAudio(string $source, string $target, int $start, int $duration)
    {
        return $this->ffmpeg->cutAudio($source, $target, $start, $duration);
    }
    
    public function mix(string $video, string $audio, string $target)
    {
        return $this->ffmpeg->mix($video, $audio, $target);
    }
}
