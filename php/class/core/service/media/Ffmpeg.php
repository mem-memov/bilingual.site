<?php
namespace core\service\media;
class Ffmpeg implements IFfmpeg
{
    private $samplingLimits;
    
    public function __construct(
    ) {
        $this->samplingLimits = ' -analyzeduration 2147480000 -probesize 2147480000 ';
    }
    
    public function extractVideo(string $source, string $target, int $streamIndex)
    {
        $command = 'ffmpeg ' . 
                //$this->samplingLimits . 
                ' -i ' . escapeshellarg($source) .
                ' -map 0:' . ($streamIndex-1) .
                ' -sn ' . // no subtitles
                ' -an ' . // no audio
                ' -c:v:0 libx264 ' . // output codec
                ' -y ' . // replace existing files
                escapeshellarg($target)
        ;

        $output = [];
        exec($command, $output);
    }
    
    public function extractAudio(string $source, string $target, int $streamIndex)
    {
        $command = 'ffmpeg ' . 
                //$this->samplingLimits . 
                ' -i ' . escapeshellarg($source) .
                ' -map 0:' . ($streamIndex-1) .
                ' -sn ' . // no subtitles
                ' -vn ' . // no video
                ' -c:a:0 libmp3lame -qscale:a 2 ' . // output codec
                ' -y ' . // replace existing files
                escapeshellarg($target)
        ;

        $output = [];
        exec($command, $output);
    }
    
    public function extractSubtitle(string $source, string $target, int $streamIndex)
    {
        $command = 'ffmpeg ' . 
                //$this->samplingLimits . 
                ' -i ' . escapeshellarg($source) .
                ' -map 0:' . ($streamIndex-1) .
                ' -an ' . // no audio
                ' -vn ' . // no video
                ' -c:s:0 srt ' . // output codec
                ' -y ' . // replace existing files
                escapeshellarg($target)
        ;

        $output = [];
        exec($command, $output);
    }
    
    public function concatenateVideo(array $parts, string $target)
    {
        $targetDirectory = dirname($target);
        if (!file_exists($targetDirectory)) {
            mkdir($targetDirectory, 0777, true);
        }

        $files = [];
        
        foreach ($parts as $part) {
            $files[] = 'file \'' . $part . '\'';
        }
        
        $list = $targetDirectory . '/list.txt';
        file_put_contents($list, implode("\n", $files));
        
        $command = 'ffmpeg ' .
                ' -f concat ' . // force input or output file format
                ' -i "' . $list . '"' .
                ' -c copy ' . // copy all the streams without reencoding
                $target
        ;

        $output = [];
        //exec($command, $output);

    }
    
    public function detectBlackBorder(string $video): IBlackBorder
    {
        $command = 'ffmpeg ' . 
                $this->samplingLimits . 
                ' -hide_banner '. // disable irrelevant output
                ' -loglevel info ' .
                ' -ss 60 ' . // start at this time
                ' -t 0.3 ' . // stop after this long in seconds
                ' -i "' . $video . '"' .
                ' -vf cropdetect=24:16:0 ' . // video filter
                ' -f null - ' .// no output file
                ' 2>&1' // redirect from stderr to stdout
        ;

        $output = [];
        exec($command, $output);
        
        $cropSetting = null;
        
        foreach ($output as $line) {
            if (strpos($line, 'cropdetect') !== false) {
                $matches = [];
                preg_match_all('/crop=(-{0,1}\d+:-{0,1}\d+:-{0,1}\d+:-{0,1}\d+)/', $line, $matches);
                if (isset($matches[1][0])) {
                    $cropSetting = $matches[1][0];
                    break;
                }
            }
        }
        
        if (is_null($cropSetting)) {
            // throw exception
        }
        
        list($width, $height, $offsetX, $offsetY) = explode(':', $cropSetting);
        
        $blackBorder = new BlackBorder((int)$width, (int)$height, (int)$offsetX, (int)$offsetY);

        return $blackBorder;
    }
    
    public function cutVideo(string $source, string $target, int $start, int $duration, int $height)
    {
        $command = 'ffmpeg ' . 
                ' -i ' . escapeshellarg($source) .
                ' -ss ' . (float)$start/1000 .
                ' -t ' . (float)$duration/1000 .
                ' -filter:v scale=-2:' . $height . // crop to a given height and avoid "width not divisible by 2"
                ' -y ' . // replace existing files
                escapeshellarg($target)
        ;

        $output = [];
        exec($command, $output);
    }
    
    public function cutAudio(string $source, string $target, int $start, int $duration)
    {
        $command = 'ffmpeg ' . 
                ' -i ' . escapeshellarg($source) .
                ' -ss ' . (float)$start/1000 .
                ' -t ' . (float)$duration/1000 .
                ' -y ' . // replace existing files
                escapeshellarg($target)
        ;

        $output = [];
        exec($command, $output);
    }
    
    public function mix(string $video, string $audio, string $target)
    {
        $command = 'ffmpeg ' . 
                //$this->samplingLimits . 
                ' -i ' . escapeshellarg($video) .
                ' -i ' . escapeshellarg($audio) .
                ' -y ' . // replace existing files
                //' -movflags faststart ' .
                escapeshellarg($target)
        ;

        $output = [];
        exec($command, $output);
    }
}
