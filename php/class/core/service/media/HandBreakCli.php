<?php
namespace core\service\media;
class HandBreakCli implements IHandBreakCli
{
    public function __construct(
    ) {

    }
    
    public function convertDvd(string $source, string $target)
    {
        if (!is_dir($source)) {
            throw new \Exception('DVD source path must be a directory with VOB files.');
        }

        $targetDirectory = dirname($target);
        if (!file_exists($targetDirectory)) {
            mkdir($targetDirectory, 0777, true);
        }
      
        if (substr($target, strlen($target)-4) !== '.mp4') {
            throw new \Exception('DVD target file must have mp4 extension.');
        }

        $command = 'HandBrakeCLI ' .
                ' -i "' . $source . '"' . // input directory
                ' -o "' . $target . '"' . // output file
                ' --markers ' . // chapter markers
                ' --all-audio ' . // all audio tracks
                ' --all-subtitles ' // all subtitle tracks
        ;

        $output = [];
        exec($command, $output);
    }
}
