<?php
namespace core\service\media;
interface IBlackBorder
{
    public function width(): int;
    public function height(): int;
    public function offsetX(): int;
    public function offsetY(): int;
}
