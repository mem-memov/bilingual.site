<?php
namespace core\service\media;
interface IEditor
{
    public function getMediaData(string $source): mediainfo\IMediaData;
    public function convertDvd(string $source, string $target);
    public function extractVideo(string $source, string $target, int $streamIndex);
    public function extractAudio(string $source, string $target, int $streamIndex);
    public function extractSubtitle(string $source, string $target, int $streamIndex);
    public function concatenateVideo(array $parts, string $target);
    public function detectBlackBorder(string $video): IBlackBorder;
    public function cutVideo(string $source, string $target, int $start, int $duration, int $height);
    public function cutAudio(string $source, string $target, int $start, int $duration);
    public function mix(string $video, string $audio, string $target);
}
