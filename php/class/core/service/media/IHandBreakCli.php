<?php
namespace core\service\media;
interface IHandBreakCli
{
    public function convertDvd(string $source, string $target);
}
