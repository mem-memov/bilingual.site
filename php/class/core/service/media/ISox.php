<?php
namespace core\service\media;
interface ISox
{
    public function cutAudio(string $source, string $target, int $start, int $duration);
}
