<?php
namespace core\service\media;
class Sox implements ISox
{
    public function __construct(
    ) {
        
    }
    
    public function cutAudio(string $source, string $target, int $start, int $duration)
    {
        $command = 'sox ' . 
                ' -norm ' . // normalize volume
                escapeshellarg($source) .
                ' ' . escapeshellarg($target) .
                ' trim ' . (float)$start/1000 . ' ' . (float)$duration/1000
        ;

        $output = [];
        exec($command, $output);
    }
}
