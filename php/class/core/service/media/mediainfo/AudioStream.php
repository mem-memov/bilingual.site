<?php
namespace core\service\media\mediainfo;
class AudioStream implements IAudioStream
{
    private $data;
    
    public function __construct(array $data)
    {
        $this->data = $data;
    }
    
    public function index(): int
    {
        return (int)$this->data['ID'][0];
    }

    public function start(): int
    {
        return (int)$this->data['Delay'][0];
    }

    public function duration(): int
    {
        return (int)$this->data['Duration'][0];
    }
    
    public function language(): string
    {
        return $this->data['Language'][1];
    }
}
