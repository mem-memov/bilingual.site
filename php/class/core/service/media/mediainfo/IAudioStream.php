<?php
namespace core\service\media\mediainfo;
interface IAudioStream
{
    public function index(): int;
    public function start(): int;
    public function duration(): int;
    public function language(): string;
}
