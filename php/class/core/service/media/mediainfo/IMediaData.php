<?php
namespace core\service\media\mediainfo;
interface IMediaData
{
    public function file(): string;
    public function duration(): int;
    public function video(): array;
    public function audio(): array;
    public function subtitle(): array;
}
