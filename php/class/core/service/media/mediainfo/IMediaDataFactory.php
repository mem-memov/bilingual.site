<?php
namespace core\service\media\mediainfo;
interface IMediaDataFactory
{
    public function make(array $data): IMediaData;
}
