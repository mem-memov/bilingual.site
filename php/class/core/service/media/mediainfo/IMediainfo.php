<?php
namespace core\service\media\mediainfo;
interface IMediainfo
{
    public function getMediaData(string $source): IMediaData;
}
