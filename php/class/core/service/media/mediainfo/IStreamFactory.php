<?php
namespace core\service\media\mediainfo;
interface IStreamFactory
{
    public function video(array $data): IVideoStream;
    public function audio(array $data): IAudioStream;
    public function subtitle(array $data): ISubtitleStream;
}
