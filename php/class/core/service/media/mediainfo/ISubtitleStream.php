<?php
namespace core\service\media\mediainfo;
interface ISubtitleStream
{
    public function index(): int;
    public function language(): string;
}
