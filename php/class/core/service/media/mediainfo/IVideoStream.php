<?php
namespace core\service\media\mediainfo;
interface IVideoStream
{
    public function index(): int;
    public function width(): int;
    public function height(): int;
    public function duration(): int;
    public function bitRate(): int;
    public function frameRate(): int;
    public function language(): string;
}
