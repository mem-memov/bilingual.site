<?php
namespace core\service\media\mediainfo;
class MediaData implements IMediaData
{
    private $stream;
    private $data;
    
    public function __construct(
        IStreamFactory $stream,
        array $data
    ) {
        $this->stream = $stream;
        $this->data = $data;
    }
    
    public function file(): string
    {
        $tracks = $this->data['File']['track'];

        foreach ($tracks as $track) {
            
            if ($track['@attributes']['type'] == 'General') {
                break;
            }
            
        }
        
        return $track['Complete_name'];
    }
    
    public function duration(): int
    {
        $tracks = $this->data['File']['track'];

        foreach ($tracks as $track) {
            
            if ($track['@attributes']['type'] == 'General') {
                break;
            }
            
        }
        
        return (int)$track['Duration'][0];
    }
    
    public function video(): array
    {
        $tracks = $this->data['File']['track'];
        
        $video = [];
        
        foreach ($tracks as $track) {
            
            if ($track['@attributes']['type'] == 'Video') {
                $video[] = $this->stream->video($track);
            }
            
        }
        
        return $video;
    }
    
    public function audio(): array
    {
        $tracks = $this->data['File']['track'];
        
        $audio = [];
        
        foreach ($tracks as $track) {
            
            if ($track['@attributes']['type'] == 'Audio') {
                $audio[] = $this->stream->audio($track);
            }
            
        }
        
        return $audio;
    }
    
    public function subtitle(): array
    {
        $tracks = $this->data['File']['track'];
        
        $subtitles = [];
        
        foreach ($tracks as $track) {
            
            if ($track['@attributes']['type'] == 'Text') {
                $subtitles[] = $this->stream->subtitle($track);
            }
            
        }
        
        return $subtitles;
    }
}
