<?php
namespace core\service\media\mediainfo;
class MediaDataFactory implements IMediaDataFactory
{
    private $stream;
    
    public function __construct(
        IStreamFactory $stream
    ) {
        $this->stream = $stream;
    }

    public function make(array $data): IMediaData
    {
        return new MediaData($this->stream, $data);
    }
}
