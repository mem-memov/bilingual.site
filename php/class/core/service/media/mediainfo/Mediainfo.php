<?php
namespace core\service\media\mediainfo;
class Mediainfo implements IMediainfo
{
    private $mediaData;
    
    public function __construct(
        IMediaDataFactory $mediaData
    ) {
        $this->mediaData = $mediaData;
    }
    
    public function getMediaData(string $source): IMediaData
    {
        $logFile = dirname($source) . '/mediainfo';
        
        $mediainfo = trim(shell_exec('type -P mediainfo'));
        if (empty($mediainfo)){
            die('<h1>Mediainfo is not available</h1>');
        } 
        
        $command = 'mediainfo ' .
                ' --Output=XML ' .
                ' --Full ' .
                escapeshellarg($source)
        ;
        
        putenv('LANG=en_US.UTF-8');
        $output = shell_exec($command);


        $xml = simplexml_load_string($output);
        $json = json_encode($xml);
        $data = json_decode($json, true);

        return $this->mediaData->make($data);
    }

}
