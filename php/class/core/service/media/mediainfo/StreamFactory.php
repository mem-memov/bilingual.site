<?php
namespace core\service\media\mediainfo;
class StreamFactory implements IStreamFactory
{
    public function __construct(
    ) {
        
    }
    
    public function video(array $data): IVideoStream
    {
        return new VideoStream($data);
    }
    
    public function audio(array $data): IAudioStream
    {
        return new AudioStream($data);
    }
    
    public function subtitle(array $data): ISubtitleStream
    {
        return new SubtitleStream($data);
    }
}
