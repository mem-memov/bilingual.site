<?php
namespace core\service\media\mediainfo;
class SubtitleStream implements ISubtitleStream
{
    private $data;
    
    public function __construct(array $data)
    {
        $this->data = $data;
    }
    
    public function index(): int
    {
        return (int)$this->data['ID'][0];
    }
    
    public function language(): string
    {
        return $this->data['Language'][1];
    }
}
