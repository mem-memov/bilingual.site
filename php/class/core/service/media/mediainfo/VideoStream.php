<?php
namespace core\service\media\mediainfo;
class VideoStream implements IVideoStream
{
    private $data;
    
    public function __construct(array $data)
    {
        $this->data = $data;
    }
    
    public function index(): int
    {
        return (int)$this->data['ID'][0];
    }
    
    public function width(): int
    {
        return (int)$this->data['Width'][0];
    }
    
    public function height(): int
    {
        return (int)$this->data['Height'][0];
    }
    
    public function duration(): int
    {
        return (int)$this->data['Duration'][0];
    }
    
    public function bitRate(): int
    {
        return (int)$this->data['Nominal_bit_rate'][0];
    }
    
    public function frameRate(): int
    {
        return (int)(floatval($this->data['Frame_rate'][0]) * 1000);
    }
    
    public function language(): string
    {
        return $this->data['Language'][1];
    }
}
