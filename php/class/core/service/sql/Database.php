<?php
declare(strict_types=1);
namespace core\service\sql;
class Database implements IDatabase
{
    private $query;
    private $transaction;
    private $renderer;
    
    public function __construct(
        IQuery $query, 
        ITransaction $transaction,
        IRenderer $renderer
    ) {
        $this->query = $query;
        $this->transaction = $transaction;
        $this->renderer = $renderer;
    }

    public function query(): IQuery
    {
        return $this->query;
    }
    
    public function transaction(): ITransaction
    {
        return $this->transaction;
    }
    
    public function renderer(): IRenderer
    {
        return $this->renderer;
    }
}

