<?php
declare(strict_types=1);
namespace core\service\sql;
interface IDatabase
{
    public function query(): IQuery;
    public function transaction(): ITransaction;
    public function renderer(): IRenderer;
}

