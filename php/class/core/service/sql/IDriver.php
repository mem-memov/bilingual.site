<?php
declare(strict_types=1);
namespace core\service\sql;
/**
 * The driver connects to an SQL database.
 */
interface IDriver
{
    /**
     * It opens a database connection.
     * @throws \core\service\sql\exception\PhpExtensionMisssing
     * @throws \core\service\sql\exception\ConnectionNotOpened
     */
    public function open();
    
    /**
     * It closes an open database connection.
     * @throws \core\service\sql\exception\ConnectionNotClosed
     */
    public function close();
    
    /**
     * It adds special characters around table and field names.
     * @param string $value
     */
    public function name(string $name): string;
    
    /**
     * It escapes a string to be put into query as a value.
     * @param string $value
     */
    public function escape(string $value): string;
    
    /**
     * It queries the database.
     * @throws \core\service\sql\exception\QueryFailed
     */
    public function query(string $query): IResult;
}

