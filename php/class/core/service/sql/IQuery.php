<?php
declare(strict_types=1);
namespace core\service\sql;
interface IQuery
{
    public function insert(string $query): int;
    public function delete(string $query): int;
    public function update(string $query): int;
    public function select(string $query, array $types = []): array;
    public function apply(string $query);
}

