<?php
declare(strict_types=1);
namespace core\service\sql;
interface IRenderer
{
    public function asTable($table): string;
    public function asField($table, $field): string;
    public function asText(string $value): string;
    public function asDate(\DateTime $value): string;
    public function asBoolean(bool $value): string;
    public function asInteger(int $value): string;
    public function asNull(): string;
    public function isNull(): string;
}
