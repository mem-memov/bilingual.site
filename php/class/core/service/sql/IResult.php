<?php
declare(strict_types=1);
namespace core\service\sql;

interface IResult
{
    public function rows(): array;
    public function lastId(): int;
    public function affectedCount(): int;
}