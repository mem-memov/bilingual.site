<?php
declare(strict_types=1);
namespace core\service\sql;
/**
 * The factory is used by database driver adapters to create a query result.
 */
interface IResultFactory
{
    /**
     * It creates a new query result.
     * @throws \core\service\sql\exception\ResultInvalid
     */
    public function result(
        array $rows = null, 
        int $lastId = null, 
        int $affectedCount = null
    ): IResult;
}