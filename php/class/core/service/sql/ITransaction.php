<?php
declare(strict_types=1);
namespace core\service\sql;
interface ITransaction
{
    public function start();
    public function rollback();
    public function commit();
}

