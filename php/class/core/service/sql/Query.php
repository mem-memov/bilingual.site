<?php
namespace core\service\sql;
class Query implements IQuery
{
    private $driver;
    
    public function __construct(IDriver $driver) {
        $this->driver = $driver;
    }
    
    public function insert(string $query): int 
    {
        $result = $this->driver->query($query);
        return $result->lastId();
    }
    
    public function delete(string $query): int
    {
        $result = $this->driver->query($query);
        return $result->affectedCount();
    }
    
    public function update(string $query): int
    {
        $result = $this->driver->query($query);
        return $result->affectedCount();
    }
    
    public function select(string $query, array $types = []): array
    {
        $result = $this->driver->query($query);
        
        $rows = $result->rows();

        if (!empty($types)) {
            foreach ($rows as $index => $row) {
                foreach ($row as $field => $value) {
                    if (array_key_exists($field, $types)) {
                        switch ($types[$field]) {
                            case 'integer':
                                $rows[$index][$field] = (int)$value;
                                break;
                            case 'boolean':
                                $rows[$index][$field] = (bool)$value;
                                break;
                            default:
                                throw new exception\UnknownCastType($types[$field]);
                                break;
                        }
                    }
                }
            }
        }

        return $rows;
    }
    
    public function apply(string $query)
    {
        $this->driver->query($query);
    }
}

