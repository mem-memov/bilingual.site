<?php
declare(strict_types=1);
namespace core\service\sql;

class Result implements IResult
{
    private $rows;
    private $lastId;
    private $affectedCount;
    
    public function __construct(array $rows = null, int $lastId = null, int $affectedCount = null)
    {
        $this->rows = $rows;
        $this->lastId = $lastId;
        $this->affectedCount = $affectedCount;
    }
    
    public function rows(): array
    {
        return $this->rows;
    }
    
    public function lastId(): int
    {
        return $this->lastId;
    }
    
    public function affectedCount(): int
    {
        return $this->affectedCount;
    }
}

