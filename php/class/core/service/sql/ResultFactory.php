<?php
declare(strict_types=1);
namespace core\service\sql;
class ResultFactory implements IResultFactory
{
    public function result(
        array $rows = null, 
        int $lastId = null, 
        int $affectedCount = null
    ): IResult
    {
        if (!is_null($rows) && (!is_null($lastId) || !is_null($affectedCount))) {
            throw new exception\ResultInvalid('Selecting rows. The driver must provide no insert, update or delete parameters.');
        }
        
        if (!is_null($lastId) && (!is_null($rows) || !is_null($affectedCount))) {
            throw new exception\ResultInvalid('Inserting a row. The driver must provide no select, update or delete parameters.');
        }
        
        if (!is_null($affectedCount) && (!is_null($lastId) || !is_null($rows))) {
            throw new exception\ResultInvalid('Deleting or updating rows. The driver must provide no select or insert parameters.');
        }
        
        return new Result($rows, $lastId, $affectedCount);
    }
}


