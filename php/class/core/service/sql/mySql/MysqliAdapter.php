<?php
declare(strict_types=1);
namespace core\service\sql\mySql;
use core\service\sql\{IDriver, IResultFactory, IResult};
use core\service\sql\exception\{PhpExtensionMisssing, ConnectionNotOpened, ConnectionNotClosed, QueryFailed};
class MysqliAdapter implements IDriver
{
    private $resultFactory;
    
    private $server;
    private $port;
    private $user;
    private $password;
    private $database;
    
    private $connection;
    
    public function __construct(
        IResultFactory $resultFactory, 
        string $server, 
        string $port, 
        string $user, 
        string $password, 
        string $database
    ) {
        $this->resultFactory = $resultFactory;
        
        $this->server = $server;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
        
        $this->connection = null;
    }
    
    public function open()
    {
        if (is_null($this->connection)) {

            try {
                $this->connection = new \mysqli(
                        $this->server,
                        $this->user,
                        $this->password,
                        $this->database,
                        intval($this->port)
                );
            } catch (\Exception $e) {
                throw new PhpExtensionMisssing('mysqli');
            }

            if (!is_null($this->connection->connect_error)) {
                // NULL is returned if no error occurred.
                // http://php.net/manual/en/mysqli.connect-error.php
                throw new ConnectionNotOpened(
                   'server: ' . $this->server .
                   ' port: ' . $this->port .
                   ' database: ' . $this->database
                );
            }
            
            $this->query('SET NAMES utf8 COLLATE utf8_unicode_ci;');
        }
    }
    
    public function close()
    {
        if (!is_null($this->connection)) {
            
            $isSuccess = $this->connection->close();

            if (!$isSuccess) {
                // Returns TRUE on success or FALSE on failure. 
                // http://php.net/manual/en/mysqli.close.php
                throw new ConnectionNotClosed(
                    'server: ' . $this->server .
                    ' port: ' . $this->port .
                    ' database: ' . $this->database
                );
            }
            
        }
    }
    
    public function name(string $name): string
    {
        $this->open();
        
        return '`' . $name . '`';
    }

    public function escape(string $value): string
    {
        $this->open();
        
        return $this->connection->real_escape_string($value);
    }

    public function query(string $query): IResult
    {
        $this->open();

        $result = $this->connection->query($query);
        
        if ($result === false) {
            throw new QueryFailed($query);
        }

        // selected rows
        if ($result instanceof \mysqli_result) {
            
            $rows = [];

            while ($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
            
            $result->free();

        } else {
            
            $rows = null;
            
        }

        // last inserted ID
        // http://php.net/manual/en/mysqli.insert-id.php
        if (is_null($rows)) {

            if (is_string($this->connection->insert_id)) {
                // If the number is greater than maximal int value, mysqli_insert_id() will return a string.
                throw new QueryFailed($query);
            }
            
            if ($this->connection->insert_id == 0) {
                // Returns zero if there was no previous query on the connection or if the query did not update an AUTO_INCREMENT value.
                $lastId = null;
            } else {
                // The value of the AUTO_INCREMENT field that was updated by the previous query.
                $lastId = $this->connection->insert_id;
            }
            
        } else {
            
            $lastId = null;
            
        }
        
        // count rows updated or deleted
        // http://php.net/manual/en/mysqli.affected-rows.php
        if (is_null($rows) && is_null($lastId)) {
            
            if ($this->connection->affected_rows == -1) {
                throw new QueryFailed($query);
            }
            
            $affectedCount = $this->connection->affected_rows;
            
        } else {
            
            $affectedCount = null;
            
        }

        return $this->resultFactory->result($rows, $lastId, $affectedCount);

    }
}

