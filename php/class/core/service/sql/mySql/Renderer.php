<?php
declare(strict_types=1);
namespace core\service\sql\mySql;
use core\service\sql\{IDriver, IRenderer};
class Renderer implements IRenderer
{
    private $driver;
    
    public function __construct(IDriver $driver)
    {
        $this->driver = $driver;
    }
    
    public function asTable($table): string
    {
        return $this->driver->name((string)$table);
    }
    
    public function asField($table, $field): string
    {
        return $this->asTable($table) . '.' . $this->driver->name((string)$field);
    }
    
    public function asText(string $value): string
    {
        return '"' . $this->driver->escape($value) . '"';
    }
    
    public function asDate(\DateTime $value): string
    {
        return '"' . $this->driver->escape($value->format('Y-m-d H:i:s')) . '"';
    }
    
    public function asBoolean(bool $value): string
    {
        return $value ? '1' : '0';
    }
    
    public function asInteger(int $value): string
    {
        return (string)$value;
    }
    
    public function asNull(): string
    {
        return 'NULL';
    }
    
    public function isNull(): string
    {
        return 'IS NULL';
    }
}

