<?php
declare(strict_types=1);
namespace core\service\sql\mySql;
use core\service\sql\{IDriver, ITransaction};
class Transaction implements ITransaction
{
    private $driver;
    
    public function __construct(IDriver $driver)
    {
        $this->driver = $driver;
    }
    
    public function start()
    {
        $this->driver->query('START TRANSACTION;');
    }
    
    public function rollback()
    {
        $this->driver->query('ROLLBACK;');
    }
    
    public function commit()
    {
        $this->driver->query('COMMIT;');
    }
}

