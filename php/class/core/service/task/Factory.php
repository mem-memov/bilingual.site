<?php
namespace core\service\task;
class Factory implements IFactory
{
    private $queue;
    
    public function __construct(
        IQueue $queue
    ) {
        $this->queue = $queue;
    }
    
    public function mail(): IMail
    {
        return new Mail($this->queue, 'mailer');
    }
}
