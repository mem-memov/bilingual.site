<?php
namespace core\service\task;
interface IFactory
{
    public function mail(): IMail;
}
