<?php
namespace core\service\task;
interface IMail extends ITask
{
    public function receiver(string $receiver): IMail;
    public function template(string $template): IMail;
    public function data(array $data): IMail;
}
