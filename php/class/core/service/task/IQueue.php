<?php
namespace core\service\task;
interface IQueue
{
    public function open();
    public function enqueueTask(string $queue, array $task);
    public function close();
}
