<?php
namespace core\service\task;
class Mail implements IMail
{
    private $queue;
    private $queueName;
    private $receiver;
    private $template;
    private $data;
    
    public function __construct(
       IQueue $queue,
       string $queueName
    ) {
        $this->queue = $queue;
        $this->queueName = $queueName;
        $this->receiver = null;
        $this->template = null;
        $this->data = null;
    }

    public function receiver(string $receiver): IMail
    {
        $this->receiver = $receiver;
        
        return $this;
    }
    
    public function template(string $template): IMail
    {
        $this->template = $template;
        
        return $this;
    }
    
    public function data(array $data): IMail
    {
        $this->data = $data;
        
        return $this;
    }
    
    public function enqueue()
    {
        $this->queue->enqueueTask($this->queueName, [
            'receiver' => $this->receiver,
            'template' => $this->template,
            'data' => $this->data
        ]);
    }
}
