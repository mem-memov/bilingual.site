<?php
namespace core\service\queue\exception;
class ConnectionNotOpened extends Exception
{
    public function __construct(string $server, string $port, $message)
    {
        parent::_construct('Connection to queue server ' . $server . ':' . $port . ' could not be opened. ' . $message);
    }
}
