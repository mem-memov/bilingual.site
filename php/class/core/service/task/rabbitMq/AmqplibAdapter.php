<?php
namespace core\service\task\rabbitMq;
use core\service\task\IQueue;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use core\service\task\exception\ConnectionNotOpened;
class AmqplibAdapter implements IQueue
{
    private $server;
    private $port;
    private $user;
    private $password;
    
    private $connection;
    private $channel;
    
    public function __construct(
        string $server, 
        string $port, 
        string $user, 
        string $password
    ) {
        $this->server = $server;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        
        $this->connection = null;
        $this->channel = null;
    }
    
    public function open()
    {
        if (is_null($this->connection)) {

            try {
                
                $this->connection = new AMQPStreamConnection(
                        $this->server,
                        intval($this->port),
                        $this->user,
                        $this->password
                );
                
            } catch (\Exception $e) {
                
                throw new ConnectionNotOpened($this->server, $this->port, $e->getMessage());
                
            }
            
            $this->channel = $this->connection->channel();
        }
    }

    public function enqueueTask(string $queue, array $task)
    {
        $this->open();
        
        $this->channel->queue_declare(
            $queue, 
            false, 
            true, 
            false, 
            false
        );
        
        $message = new AMQPMessage(
            json_encode($task),
            array('delivery_mode' => 2) // make message persistent
        );
        
        $this->channel->basic_publish(
            $message, 
            '', 
            $queue
        );
    }
    
    public function close()
    {
        if (!is_null($this->channel)) {
            $this->channel->close();
        }
        
        if (!is_null($this->connection)) {
            $this->connection->close();
        }
    }
}
