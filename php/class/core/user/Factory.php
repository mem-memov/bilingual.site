<?php
namespace core\user;
use core\IFactory as ICore;
class Factory implements IFactory
{
    private $core;

    private $authentication;
    
    public function __construct(
        ICore $core
    ) {
        $this->core = $core;

        $this->authentication = null;
    }
    
    public function authentication(): authentication\IRepository
    {
        if (is_null($this->authentication)) {

            $password = new authentication\Password(
                $this->core->service()->mailer()
            );

            $query = new authentication\query\Factory(
                $this->core->database()->query(),
                $this->core->database()->filters()
                $this->core->database()->tables()->authentication()
            );

            $command = new authentication\command\Factory(
                $this->core,
                $password
            );

            $factory = new authentication\Factory(
                $this,
                $command
            );
            
            $this->authentication = new authentication\Repository(
                $factory,
                $query,
                $password
            );
        }

        return $this->authentication;
    }
}
