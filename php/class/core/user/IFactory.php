<?php
namespace core\user;
interface IFactory
{
    public function authentication(): authentication\IRepository;
}
