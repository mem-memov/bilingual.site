<?php
namespace core\user\authentication;
class Authentication implements IAuthentication
{
    private $commands;
    private $id;
    private $email;
    private $password;
    private $restoreDate;
    private $restoreHash;
    private $confirmHash;
    private $registrationDate;
    
    public function __construct(
        command\IFactory $commands,
        int $id,
        string $email,
        string $password,
        \DateTime $restoreDate = null,
        string $restoreHash = null,
        string $confirmHash,
        \DateTime $registrationDate
    ) {
        $this->commands = $commands;
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->restoreDate = $restoreDate;
        $this->restoreHash = $restoreHash;
        $this->confirmHash = $confirmHash;
        $this->registrationDate = $registrationDate;
    }
    
    public function id(): int
    {
        return $this->id;
    }
    
    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function restoreDate()//: \Date|null
    {
        return $this->restoreDate;
    }

    public function restoreHash()//: string|null
    {
        return $this->restoreHash;
    }
    
    public function confirmHash(): string
    {
        return $this->confirmHash;
    }

    public function registrationDate(): \DateTime
    {
        return $this->registrationDate;
    }
    
    public function verifyPassword(string $password)
    {
        $self =& $this;
        
        $this->commands->verifyPassword($password, $this);
    }
    
    public function restorePassword()
    {
        $self =& $this;
        
        $this->commands->restorePassword($this, function($restoreDate, $restoreHash) use (&$self) {
            $this->restoreDate = $restoreDate;
            $this->restoreHash = $restoreHash;
        });
    }
    
    public function clearRestoreHash()
    {
        $this->commands->clearRestoreHash($this, function($restoreDate, $restoreHash) use (&$self) {
            $this->restoreDate = $restoreDate;
            $this->restoreHash = $restoreHash;
        });
    }
    
    public function changePassword(string $newPassword)
    {
        $this->commands->changePassword($newPassword, $this, function($password) use (&$self) {
            $this->password = $password;
        });
    }
}
