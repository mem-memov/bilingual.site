<?php
namespace core\user\authentication;
use core\user\IFactory as IRepositoryFactory;
use core\entity\{IEntity, Factory as EntityFactory};
class Factory extends EntityFactory
{
    private $repositories;
    private $commands;
    
    public function __construct(
        IRepositoryFactory $repositories,
        command\IFactory $commands
    ) {
        $this->repositories = $repositories;
        $this->commands = $commands;
    }

    public function one(array $row): IEntity
    {
        return new Authentication(
            $this->commands,
            (int)$row['id'],
            (string)$row['email'],
            (string)$row['password'],
            !empty($row['restore_date']) ? \DateTime::createFromFormat('Y-m-d H:i:s', $row['restore_date']) : null,
            (string)$row['restore_hash'],
            (string)$row['confirm_hash'],
            \DateTime::createFromFormat('Y-m-d H:i:s', $row['registration_date'])
        );
    }
}
