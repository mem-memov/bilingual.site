<?php
namespace core\user\authentication;
use core\entity\IEntity;
interface IAuthentication extends IEntity
{
    public function email(): string;
    public function password(): string;
    public function restoreDate();//: \Date|null;
    public function restoreHash();//: string|null;
    public function confirmHash(): string;
    public function registrationDate(): \DateTime;
    public function verifyPassword(string $password);
    public function restorePassword();
    public function clearRestoreHash();
    public function changePassword(string $newPassword);
}
