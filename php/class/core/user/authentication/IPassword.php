<?php
namespace core\user\authentication;
interface IPassword {
    public function validate(string $password);
    public function hash(string $password): string;
    public function verify(string $password, string $hash): bool;
}
