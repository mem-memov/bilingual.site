<?php
namespace core\user\authentication;
interface IRepository
{
    public function create(string $email, string $password): IAuthentication;
    public function read(int $id): IAuthentication;
    public function update(IAuthentication $authentication);
    public function byEmail(string $email): array;
    public function byRestoreHash(string $restoreHash): array;
}
