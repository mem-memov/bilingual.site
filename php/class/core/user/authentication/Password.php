<?php
namespace core\user\authentication;
class Password implements IPassword 
{
    public function validate(string $password)
    {
        // Using the PASSWORD_BCRYPT as the algorithm, will result in the password parameter being truncated to a maximum length of 72 characters. 
        if (strlen($password) > 72) {
            throw new exception\PasswordTooLong();
        }
        
        if (strlen($password) < 6) {
            throw new exception\PasswordTooShort();
        }
        
        $currentCaracter = '';
        $characterCount = 0;
        foreach (str_split($password) as $character) {
            if ($currentCaracter != $character) {
                $currentCaracter = $character;
                $characterCount = 1;
            } else {
                $characterCount++;
                if ($characterCount > 2) {
                    throw new exception\PasswordContainsRepeatingCharacters();
                }
            }
        }

        if (!preg_match('/^[!-~]+$/', $password)) {
            throw new exception\PasswordWithWrongCharacters();
        }
    }

    public function hash(string $password): string
    {
        $this->validate($password);
        
        $hash = password_hash($password, PASSWORD_BCRYPT);
        
        if ($hash === false) {
            throw new exception\PasswordNotHashed();
        }
        
        return $hash;
    }
    
    public function verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
