<?php
namespace core\user\authentication;
use core\entity\IFactory;
class Repository implements IRepository
{
    private $factory;
    private $query;
    private $password;
    
    public function __construct(
        IFactory $factory,
        query\IFactory $query,
        IPassword $password
    ) {
        $this->factory = $factory;
        $this->query = $query;
        $this->password = $password;
    }
    
    public function create(string $email, string $password): IAuthentication
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new exception\EmailNotValid();
        }
        
        $authetnications = $this->query->byEmail($email)->run();
        
        if (!empty($authetnications)) {
            throw new exception\UserAlreadyExsists();
        }
        
        $id = $this->query->create(
            $email,
            $this->password->hash($password),
            null,
            null,
            $this->password->hash($email . rand(1, 1000000)),
            new \DateTime()
        )->run();

        return $this->read($id);
    }
    
    public function read(int $id): IAuthentication
    {
        $rows = $this->query->read($id)->run();
        
        if (empty($rows)) {
            throw new exception\UserNotFound($id);
        }

        return $this->factory->one($rows[0]);
    }
    
    public function update(IAuthentication $authentication)
    {
        $this->query->update([
                $authentication->id(),
                $authentication->email(),
                $authentication->password(),
                $authentication->restoreDate(),
                $authentication->restoreHash(),
                $authentication->confirmHash(),
                $authentication->registrationDate()
            ])
            ->run();
    }
    
    public function byEmail(string $email): array
    {
        return $this->factory->many(
            $this->query->byEmail($email)->run()
        );
    }
    
    public function byRestoreHash(string $restoreHash): array
    {
        return $this->factory->many(
            $this->query->byRestoreHash($restoreHash)->run()
        );
    }
}
