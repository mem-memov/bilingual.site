<?php
namespace core\user\authentication\command;
use core\entity\ICommand;
use core\user\authentication\IAuthentication;
use core\user\authentication\IPassword as IPasswordTool;
use core\user\authentication\IRepository as IAuthenticationRepository;
class ChangePassword implements ICommand
{
    private $newPassword;
    private $password;
    private $authentication;
    private $update;
    private $passwordTool;
    private $authentications;
    
    public function __construct(
        string $newPassword,
        IAuthentication $authentication,
        callable $update,
        IPasswordTool $passwordTool,
        IAuthenticationRepository $authentications
    ) {
        $this->newPassword = $newPassword;
        $this->authentication = $authentication;
        $this->update = $update;
        $this->passwordTool = $passwordTool;
        $this->authentications = $authentications;
    }

    public function execute()
    {
        $hash = $this->passwordTool->hash($this->newPassword);
        call_user_func_array($this->update, [$hash]);
        $this->authentications->update($this->authentication);
    }
}
