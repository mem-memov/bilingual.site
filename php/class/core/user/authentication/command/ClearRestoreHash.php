<?php
namespace core\user\authentication\command;
use core\entity\ICommand;
use core\user\authentication\IAuthentication;
use core\user\authentication\IRepository as IAuthenticationRepository;
class ClearRestoreHash implements \core\entity\ICommand
{
    private $authentication;
    private $update;
    private $authentications;
    
    public function __construct(
        IAuthentication $authentication,
        callable $update,
        IAuthenticationRepository $authentications
    ) {
        $this->authentication = $authentication;
        $this->update = $update;
        $this->authentications = $authentications;
    }

    public function execute()
    {
        call_user_func_array($this->update, [null, null]);
        $this->authentications->update($this->authentication);
    }
}
