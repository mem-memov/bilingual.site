<?php
namespace core\user\authentication\command;
use core\IFactory as ICore;
use core\user\authentication\IAuthentication;
use core\user\authentication\IPassword as IPasswordTool;
class Factory implements IFactory
{
    private $core;
    private $passwordTool;
    
    public function __construct(
        ICore $core,
        $passwordTool
    ) {
        $this->core = $core;
        $this->passwordTool = $passwordTool;
    }
    
    public function verifyPassword(
        string $password, 
        IAuthentication $authentication
    ) {
        $command = new VerifyPassword(
            $password,
            $authentication,
            $this->passwordTool
        );
        
        $command->execute();
    }
    
    public function restorePassword(
        IAuthentication $authentication, 
        callable $update
    ) {
        $command = new RestorePassword(
            $authentication,
            $update,
            $this->passwordTool,
            $this->core->service()->task(),
            $this->core->user()->authentication(),
            $this->core->configuration()->server()->name()
        );
        
        $command->execute();
    }
    
    public function clearRestoreHash(
        IAuthentication $authentication, 
        callable $update
    ) {
        $command = new ClearRestoreHash(
            $authentication,
            $update,
            $this->core->user()->authentication()
        );
        
        $command->execute();
    }
    
    public function changePassword(
        string $newPassword, 
        IAuthentication $authentication, 
        callable $update
    ) {
        $command = new ChangePassword(
            $newPassword,
            $authentication,
            $update,
            $this->passwordTool,
            $this->core->user()->authentication()
        );
        
        $command->execute();
    }
}
