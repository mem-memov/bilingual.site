<?php
namespace core\user\authentication\command;
use core\user\authentication\IAuthentication;
interface IFactory
{
    public function verifyPassword(
        string $password, 
        IAuthentication $authentication
    );
    public function restorePassword(
        IAuthentication $authentication, 
        callable $update
    );
    public function clearRestoreHash(
        IAuthentication $authentication, 
        callable $update
    );
    public function changePassword(
        string $newPassword, 
        IAuthentication $authentucation, 
        callable $update
    );
}
