<?php
namespace core\user\authentication\command;
use core\entity\ICommand;
use core\user\authentication\IAuthentication;
use core\user\authentication\IPassword as IPasswordTool;
use core\service\task\IFactory as ITaskFactory;
use core\user\authentication\IRepository as IAuthenticationRepository;
use core\user\authentication\exception\WrongPassword;
class RestorePassword implements ICommand
{
    private $authentication;
    private $update;
    private $passwordTool;
    private $task;
    private $authentications;
    private $serverName;
    
    public function __construct(
        IAuthentication $authentication,
        callable $update,
        IPasswordTool $passwordTool,
        ITaskFactory $task,
        IAuthenticationRepository $authentications,
        string $serverName
    ) {
        $this->authentication = $authentication;
        $this->update = $update;
        $this->passwordTool = $passwordTool;
        $this->task = $task;
        $this->authentications = $authentications;
        $this->serverName = $serverName;
    }

    public function execute()
    {
        $restoreDate = new \DateTime();
        $restoreHash = $this->passwordTool->hash(time().rand(1,10000));

        call_user_func_array($this->update, [$restoreDate, $restoreHash]);

        $this->authentications->update($this->authentication);

        $url = 'http://' . $this->serverName . '/user/authentication/password/?key=' . $this->authentication->restoreHash();

        $this->task->mail()
            ->receiver($this->authentication->email())
            ->template('user/authentication/password')
            ->data([
                'url' => $url
            ])
            ->enqueue();
    }
}
