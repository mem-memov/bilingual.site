<?php
namespace core\user\authentication\command;
use core\entity\ICommand;
use core\user\authentication\IAuthentication;
use core\user\authentication\IPassword as IPasswordTool;
use core\user\authentication\exception\WrongPassword;
class VerifyPassword implements ICommand
{
    private $password;
    private $authentication;
    private $passwordTool;
    
    public function __construct(
        string $password,
        IAuthentication $authentication,
        IPasswordTool $passwordTool
    ) {
        $this->password = $password;
        $this->authentication = $authentication;
        $this->passwordTool = $passwordTool;
    }

    public function execute()
    {
        if (!$this->passwordTool->verify($this->password, $this->authentication->password())) {
            throw new WrongPassword();
        }
    }
}
