<?php
namespace core\user\authentication\query;
use core\entity\query\Factory as EntityQueryFactory;
use core\service\sql\IQuery as IDatabase;
use core\database\filter\IFactory as IFilterFactory;
use core\database\table\authentication\ITable as IAuthenticationTable;
use core\database\query\IInsert;
use core\database\query\ISelect;
use core\database\query\IUpdate;
class Factory extends EntityQueryFactory implements IFactory
{
    private $authentication;
    
    public function __construct(
        IDatabase $database,
        IFilterFactory $filters,
        IAuthenticationTable $authentication
    ) {
        parent::__construct($database, $filters);
        $this->authentication = $authentication;
    }
    
    public function create(
        string $email, 
        string $password, 
        \DateTime $restoreDate, 
        string $restoreHash,
        string $confirmHash, 
        \DateTime $registrationDate
    ): IInsert 
    {
        $values = $this->authentication->specifications();
        
        $values[$this->authentication->email()] = $email;
        $values[$this->authentication->password()] = $password;
        $values[$this->authentication->restoreDate()] = $restoreDate;
        $values[$this->authentication->restoreHash()] = $restoreHash;
        $values[$this->authentication->confirmHash()] = $confirmHash;
        $values[$this->authentication->registrationDate()] = $registrationDate;

        return $this->crud()->create($this->authentication, $values);
    }
    
    public function read(int $id): ISelect
    {
        return $this->crud()->read($this->authentication, $id);
    }

    public function update(
        int $id, 
        string $email, 
        string $password, 
        \DateTime $restoreDate, 
        string $restoreHash,
        string $confirmHash, 
        \DateTime $registrationDate
    ): IUpdate
    {
        $values = $this->authentication->specifications();
        
        $values[$this->authentication->email()] = $email;
        $values[$this->authentication->password()] = $password;
        $values[$this->authentication->restoreDate()] = $restoreDate;
        $values[$this->authentication->restoreHash()] = $restoreHash;
        $values[$this->authentication->confirmHash()] = $confirmHash;
        $values[$this->authentication->registrationDate()] = $registrationDate;
        
        return $this->crud()->update($this->authentication, $id, $values);
    }
    
    public function byEmail(string $email): array
    {
        $filters = $this->authentication->specifications();
        $filters[$this->authentication->email()] = $this->filters->is($email);
        
        return $this->filter()->records($this->authentication, $filters);
    }
    
    public function byRestoreHash(string $restoreHash): array
    {
        $filters = $this->authentication->specifications();
        $filters[$this->authentication->restoreHash()] = $this->filters->is($restoreHash);
        
        return $this->filter()->records($this->authentication, $filters);
    }
}
