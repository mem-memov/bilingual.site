<?php
namespace core\user\authentication\query;
use core\entity\query\Factory as IEntityQueryFactory;
use core\database\query\ISelect;
interface IFactory extends IEntityQueryFactory
{
    public function create(
        string $email, 
        string $password, 
        \DateTime $restoreDate, 
        string $restoreHash,
        string $confirmHash, 
        \DateTime $registrationDate
    ): IInsert;
    
    public function read(int $id): ISelect;
    
    public function update(
        int $id, 
        string $email, 
        string $password, 
        \DateTime $restoreDate, 
        string $restoreHash,
        string $confirmHash, 
        \DateTime $registrationDate
    ): IUpdate;
    
    public function byEmail(string $email): array;
    
    public function byRestoreHash(string $restoreHash): array;
}
