<?php
namespace mailer;
class Facade implements IFacade
{
    private $mails;
    private $task;
    
    public function __construct(
        IMailFactory $mails,
        service\task\IQueue $task
    ) {
        $this->mails = $mails;
        $this->task = $task;
    }
    
    public function __destruct()
    {
        $this->task->close();
    }

    public function send()
    {
        $mails = $this->mails;
        
        $callback = function($message) use ($mails) {
            
            echo $message->body . "\n";
            
            $task = json_decode($message->body, true);

            $mail = $mails->make();
            
            $mail->receiver($task['receiver'])
                    ->template($task['template'])
                    ->data($task['data'])
                    ->send();
            
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        };
        
        $this->task->open();
        $this->task->dequeueTasks('mailer', $callback);
    }
}
