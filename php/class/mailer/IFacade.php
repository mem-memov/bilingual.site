<?php
namespace mailer;
interface IFacade
{
    public function send();
}
