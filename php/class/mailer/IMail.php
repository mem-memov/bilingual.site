<?php
namespace mailer;
interface IMail
{
    public function receiver(string $receiver): IMail;
    public function template(string $template): IMail;
    public function data(array $data): IMail;
}
