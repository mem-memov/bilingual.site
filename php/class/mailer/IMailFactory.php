<?php
namespace mailer;
interface IMailFactory
{
    public function make(): IMail;
}
