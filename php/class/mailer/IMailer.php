<?php
namespace mailer;
interface IMailer
{
    public function send(string $to, string $subject, string $message);
}
