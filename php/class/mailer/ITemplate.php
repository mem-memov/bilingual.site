<?php
namespace mailer;
interface ITemplate
{
    public function render(): string;
}
