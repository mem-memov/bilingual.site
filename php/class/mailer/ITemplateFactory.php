<?php
namespace mailer;
interface ITemplateFactory
{
    public function subject(string $template, array $data): ITemplate;
    public function message(string $template, array $data): ITemplate;
}
