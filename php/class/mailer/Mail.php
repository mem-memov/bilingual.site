<?php
namespace mailer;
class Mail implements IMail
{
    private $mailer;
    private $templates;
    private $queueName;
    private $receiver;
    private $template;
    private $data;
    
    public function __construct(
       IMailer $mailer,
       ITemplateFactory $templates,
       string $queueName
    ) {
        $this->mailer = $mailer;
        $this->templates = $templates;
        $this->queueName = $queueName;
        $this->receiver = null;
        $this->template = null;
        $this->data = null;
    }
    
    public function queue(): string
    {
        return $this->queueName;
    }

    public function receiver(string $receiver): IMail
    {
        $this->receiver = $receiver;
        
        return $this;
    }
    
    public function template(string $template): IMail
    {
        $this->template = $template;
        
        return $this;
    }
    
    public function data(array $data): IMail
    {
        $this->data = $data;
        
        return $this;
    }
    
    public function send()
    {
        $subject = $this->templates->subject($this->template, $this->data)->render();
        $message = $this->templates->message($this->template, $this->data)->render();
        
        $this->mailer->send($this->receiver, $subject, $message);
    }
}
