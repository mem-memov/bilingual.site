<?php
namespace mailer;
class MailFactory implements IMailFactory
{
    private $mailer;
    private $templates;
    
    public function __construct(
       IMailer $mailer,
       ITemplateFactory $templates
    ) {
        $this->mailer = $mailer;
        $this->templates = $templates;
    }

    public function make(): IMail
    {
        return new Mail($this->mailer, $this->templates, 'mailer');
    }
}
