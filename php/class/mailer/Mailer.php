<?php
namespace mailer;
use mailer\configuration\IMailer as IMailerConfiguration;
class Mailer implements IMailer
{
    private $configuration;
    private $mailer;
    
    public function __construct(
        IMailerConfiguration $configuration
    ) {
        $this->configuration = $configuration;
        $this->mailer = null;
    }
    
    public function send(string $to, string $subject, string $message) {
        
        if (is_null($this->mailer)) {
            
            $this->mailer = new \PHPMailer;
            //$this->mailer->SMTPDebug = 3;                               // Enable verbose debug output

            $this->mailer->isSMTP();                                      // Set mailer to use SMTP
            $this->mailer->Host = $this->configuration->server();  // Specify main and backup SMTP servers
            $this->mailer->SMTPAuth = true;                               // Enable SMTP authentication
            $this->mailer->Username = $this->configuration->user();                 // SMTP username
            $this->mailer->Password = $this->configuration->password();                           // SMTP password
            //$this->mailer->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $this->mailer->Port = $this->configuration->port();  
            $this->mailer->setFrom($this->configuration->email(), $this->configuration->name());
            
        }

        $this->mailer->addAddress($to);
        $this->mailer->Subject = $subject;
        $this->mailer->Body = $message;
        $this->mailer->AltBody = strip_tags($message);

        if(!$this->mailer->send()) {
            throw new exception\MessageNotMailed($this->mailer->ErrorInfo);
        }
    }
}
