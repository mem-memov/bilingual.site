<?php
namespace mailer;
class Template implements ITemplate
{
    private $path;
    private $data;
    
    public function __construct(
        string $path,
        array $data
    ) {
        $this->path = $path;
        $this->data = $data;
    }

    public function render(): string
    {
        if (!file_exists($this->path)) {
            throw new exception\TemplateNotFound($this->path);
        }
        
        $data = $this->data;
        
        ob_start();
        require $this->path;
        $content = ob_get_clean();
        
        return $content;
    }
}
