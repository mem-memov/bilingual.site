<?php
namespace mailer;
class TemplateFactory implements ITemplateFactory
{

    public function __construct(
    ) {
        
    }

    public function subject(string $template, array $data): ITemplate
    {
        $path = __DIR__ . '/../../mail/' . $template . '/subject.php';
        
        return new Template($path, $data);
    }
    
    public function message(string $template, array $data): ITemplate
    {
        $path = __DIR__ . '/../../mail/' . $template . '/body.php';
        
        return new Template($path, $data);
    }
}
