<?php
namespace mailer\configuration;
class Extractor implements IExtractor
{
    public function __construct(
    ) {
        
    }
    
    public function extractArrayByKey(string $key, array $configuration): array
    {
        if (!isset($configuration[$key])) {
            throw new exception\ConfigurationMissing($key);
        }
        
        if (!is_array($configuration[$key])) {
            throw new exception\WrongConfigurationType($key, 'array', $configuration[$key]);
        }
        
        return $configuration[$key];
    }
    
    public function extractStringByKey(string $key, array $configuration): string
    {
        if (!isset($configuration[$key])) {
            throw new exception\ConfigurationMissing($key);
        }
        
        if (!is_string($configuration[$key])) {
            throw new exception\WrongConfigurationType($key, 'string', $configuration[$key]);
        }
        
        return $configuration[$key];
    }
}
