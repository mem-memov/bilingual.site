<?php
namespace mailer\configuration;
class Factory implements IFactory
{
    private $extractor;
    private $configuration;
    private $mailer;
    private $queue;
    
    public function __construct(
        IExtractor $extractor, 
        array $configuration
    ) {
        $this->extractor = $extractor;
        $this->configuration = $configuration;
        $this->mailer = null;
        $this->queue = null;
    }
    
    public function mailer(): IMailer
    {
        if (is_null($this->mailer)) {
            
            $configuration = $this->extractor->extractArrayByKey('mailer', $this->configuration);
            
            $this->mailer = new Mailer(
                $this->extractor, 
                $configuration
            );
            
        }
        
        return $this->mailer;
    }
    
    public function queue(): IQueue
    {
        if (is_null($this->queue)) {
            
            $configuration = $this->extractor->extractArrayByKey('queue', $this->configuration);
            
            $this->queue = new Queue(
                $this->extractor, 
                $configuration
            );
        }
        
        return $this->queue;
    }
}

