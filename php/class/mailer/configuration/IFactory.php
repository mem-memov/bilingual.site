<?php
namespace mailer\configuration;
interface IFactory
{
    public function mailer(): IMailer;
    public function queue(): IQueue;
}

