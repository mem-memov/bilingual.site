<?php
namespace mailer\configuration;
interface IMailer
{
    public function server(): string;
    public function port(): int;
    public function user(): string;
    public function password(): string;
    public function email(): string;
    public function name(): string;
}
