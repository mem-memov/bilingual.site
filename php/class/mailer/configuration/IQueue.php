<?php
namespace mailer\configuration;
interface IQueue
{
    public function server(): string;
    public function port(): int;
    public function user(): string;
    public function password(): string;
}
