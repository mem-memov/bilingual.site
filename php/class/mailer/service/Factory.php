<?php
namespace mailer\service;
use mailer\configuration\IFactory as IConfigurationFactory;
class Factory implements IFactory
{
    private $configuration;
    private $task;
    
    public function __construct(
        IConfigurationFactory $configuration
    ) {
        $this->configuration = $configuration;
        $this->task = null;
    }

    public function task(): task\IQueue
    {
        if (is_null($this->task)) {
            
            $queue = new task\rabbitMq\AmqplibAdapter(
                $this->configuration->queue()->server(), 
                $this->configuration->queue()->port(), 
                $this->configuration->queue()->user(), 
                $this->configuration->queue()->password()
            );
            
            $this->task = $queue;
            
        }
        
        return $this->task;
    }
}