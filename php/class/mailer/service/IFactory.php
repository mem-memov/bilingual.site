<?php
namespace mailer\service;
interface IFactory
{
    public function task(): task\IQueue;
}

