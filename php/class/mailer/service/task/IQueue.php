<?php
namespace mailer\service\task;
interface IQueue
{
    public function open();
    public function dequeueTasks(string $queueName, callable $callback);
    public function close();
}
