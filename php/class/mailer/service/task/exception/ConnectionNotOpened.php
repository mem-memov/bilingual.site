<?php
namespace mailer\service\task\exception;
class ConnectionNotOpened extends Exception
{
    public function __construct(string $server, string $port, $message)
    {
        parent::__construct('Connection to queue server ' . $server . ':' . $port . ' could not be opened. ' . $message);
    }
}
