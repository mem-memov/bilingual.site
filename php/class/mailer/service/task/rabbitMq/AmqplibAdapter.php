<?php
namespace mailer\service\task\rabbitMq;
use mailer\service\task\IQueue;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use mailer\service\task\exception\ConnectionNotOpened;
class AmqplibAdapter implements IQueue
{
    private $server;
    private $port;
    private $user;
    private $password;
    
    private $connection;
    private $channel;
    
    public function __construct(
        string $server, 
        string $port, 
        string $user, 
        string $password
    ) {
        $this->server = $server;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        
        $this->connection = null;
        $this->channel = null;
    }
    
    public function open()
    {
        if (is_null($this->connection)) {

            try {
                
                $this->connection = new AMQPStreamConnection(
                        $this->server,
                        intval($this->port),
                        $this->user,
                        $this->password
                );
                
            } catch (\Exception $e) {
                
                throw new ConnectionNotOpened($this->server, $this->port, $e->getMessage());
                
            }
            
            $this->channel = $this->connection->channel();
        }
    }

    public function dequeueTasks(string $queueName, callable $callback)
    {
        $this->channel->queue_declare($queueName, false, true, false, false);

        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume($queueName, '', false, false, false, false, $callback);

        while(count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }
    
    public function close()
    {
        if (!is_null($this->channel)) {
            $this->channel->close();
        }
        
        if (!is_null($this->connection)) {
            $this->connection->close();
        }
    }
}
