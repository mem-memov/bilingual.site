<?php return [
    'server' => [
        'name' => 'bilingual.site'
    ],
    'directory' => [
        'root' => __DIR__ . '/../..',
        'master' => __DIR__ . '/../../media/master',
        'copy' => __DIR__ . '/../../media/copy',
        'cut' => __DIR__ . '/../../web/asset/cut'
    ],
    'database' => [
        'type' => 'MySql',
        'server' => '127.0.0.1',
        'port' => '3306',
        'user' => 'root',
        'password' => '',
        'database' => 'bilingual_site',
        'migrations' => __DIR__ . '/../../migration'
    ],
    'cache' => [
        'type' => 'memcached',
        'servers' => [
            [
                'server' => '127.0.0.1',
                'port' => '11211'
            ]
        ]
    ],
    'queue' => [
        'type' => 'RabbitMQ',
        'server' => '127.0.0.1',
        'port' => '5672',
        'user' => 'guest',
        'password' => 'guest'
    ],
    'mailer' => [
        'server' => 'mail.bilingual.site',
        'port' => '25',
        'user' => 'robot',
        'password' => 'r1o2b3o4t',
        'email' => 'robot@bilingual.site',
        'name' => 'Bilingual Robot'
    ]
];

