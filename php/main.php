<?php
gc_enable();
/*
function __autoload($class)
{
    if (class_exists($class)) { // skip php classes
        return;
    }
    
    $file = __DIR__ . '/class/' . ltrim(str_replace('\\', '/', $class), '/') . '.php';
    if (!file_exists($file)) {
        throw new Exception('Class file not found ' . $file);
    }
    
    require_once($file);
}
*/
require __DIR__ . '/vendor/autoload.php';

$configuration = require_once('configuration/developer.php');
$factory = new \Factory($configuration);

register_shutdown_function(function() {
    gc_collect_cycles();
});

return $factory->controller();


