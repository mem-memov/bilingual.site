<div class="card m-x-1 m-y-1">
    <div class="card-header"><?=$data['title']?></div>
    
    <div class="card-block">

        <?php  foreach($data['errors']['email'] as $error): ?>
        <div class="alert alert-danger" role="alert"><?=$error?></div>
        <?php endforeach; ?>

        <?php  foreach($data['errors']['password'] as $error): ?>
        <div class="alert alert-danger" role="alert"><?=$error?></div>
        <?php endforeach; ?>

        <form class="p-y-2 p-x-1" action="/user/authentication/login/" method="post">
            <div class="form-group row">
                <label for="loginInputEmail" class="col-sm-2 form-control-label">Почта</label>
                <div class="col-sm-10 <?=!empty($data['errors']['email'])?'has-danger':''?>">
                    <input id="loginInputEmail" name="email" type="email" value="<?=$data['email']?>" class="form-control <?=!empty($data['errors']['email'])?'form-control-danger':''?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="loginInputPassword" class="col-sm-2 form-control-label">Пароль</label>
                <div class="col-sm-10 <?=!empty($data['errors']['password'])?'has-danger':''?>">
                    <input id="loginInputPassword" name="password" type="password" class="form-control <?=!empty($data['errors']['password'])?'form-control-danger':''?>" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<?=$data['passwordHelp']?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="submit" class="btn btn-secondary">Отправить</button>
                </div>
            </div>
        </form>
    </div>
</div>
