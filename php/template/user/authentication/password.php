<div class="card m-x-1 m-y-1">
    <div class="card-header"><?=$data['title']?></div>
    
    <div class="card-block">

        <?php  if(isset($data['loginError'])): ?>
            <div class="alert alert-danger" role="alert"><?=$data['loginError']?></div>
            <a href="/user/authentication/login/" class="btn btn-primary"><?=$data['loginErrorButton']?></a>
        <?php elseif (!empty($data['success'])): ?>    
            <div class="alert alert-success" role="alert"><?=$data['success']?></div>
            <a href="/user/" class="btn btn-primary"><?=$data['successButton']?></a>
        <?php else: ?>
        
            <?php  foreach($data['errors'] as $error): ?>
            <div class="alert alert-danger" role="alert"><?=$error?></div>
            <?php endforeach; ?>

            <form class="p-y-2 p-x-1" action="/user/authentication/password/" method="post">
                <div class="form-group row">
                    <label for="loginInputEmail" class="col-sm-2 form-control-label">Новый пароль</label>
                    <div class="col-sm-10">
                        <input id="loginInputPassword" name="password" type="password" class="form-control" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<?=$data['passwordHelp']?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="loginInputPassword" class="col-sm-2 form-control-label">Пароль ещё раз</label>
                    <div class="col-sm-10">
                        <input id="loginInputPassword2" name="password2" type="password" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="submit" class="btn btn-primary">Отправить</button>
                    <a href="/user/" class="btn btn-secondary">В меню</a>
                    </div>
                </div>
            </form>
        
        <?php endif; ?>

    </div>
</div>
