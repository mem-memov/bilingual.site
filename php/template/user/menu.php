<div class="container-fluid">
    <div class="card-columns m-y-1">
        
        <div class="card">
            <div class="card-header">Пользователь</div>
            <div class="card-block">
                <a href="#" class="btn btn-secondary btn-block text-xs-left"><i class="fa fa-university"></i> Уроки</a>
                <a href="#" class="btn btn-secondary btn-block text-xs-left"><i class="fa fa-book"></i> Словарь</a>
            </div>
        </div>

        <div class="card">
            <div class="card-header">Материалы</div>
            <div class="card-block">
                <a href="#" class="btn btn-secondary btn-block text-xs-left"><i class="fa fa-video-camera"></i> Фильмы</a>
                <a href="#" class="btn btn-secondary btn-block text-xs-left"><i class="fa fa-search"></i> Слова</a>
            </div>
        </div>

        <div class="card">
            <div class="card-header">Прочее</div>
            <div class="card-block">
                <a href="/user/authentication/password/" class="btn btn-secondary btn-block text-xs-left"><i class="fa fa-key"></i> Сменить пароль</a>
                <a href="/user/authentication/logout/" class="btn btn-secondary btn-block text-xs-left"><i class="fa fa-sign-out"></i> Выход</a>
            </div>
        </div>

    </div>
</div>
