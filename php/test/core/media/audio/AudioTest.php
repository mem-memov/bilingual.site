<?php
namespace core\media\audio;
class AudioTest extends \PHPUnit_Framework_TestCase
{
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $commands;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $sources;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $languages;
    
    protected $id;
    protected $sourceId;
    protected $languageId;
    protected $file;
    protected $duration;

    protected function setUp()
    {
        $this->commands = $this->getMockBuilder('\core\media\audio\command\IFactory')
            ->getMock();

        $this->sources = $this->getMockBuilder('\core\media\source\IRepository')
            ->getMock();
        
        $this->languages = $this->getMockBuilder('\core\media\language\IRepository')
            ->getMock();
        
        $this->id = 5;
        $this->languageId = 1;
        $this->sourceId = 2;
        $this->file = 'path/to/file';
        $this->duration = 3000;
    }

    public function testItGivesItsId()
    {
        $audio = new Audio(
            $this->commands, 
            $this->sources,
            $this->languages,
            $this->id,
            $this->sourceId,
            $this->languageId,
            $this->file,
            $this->duration
        );

        $result = $audio->id();

        $this->assertEquals($result, $this->id);
    }

    public function testItGivesItsSource()
    {
        $audio = new Audio(
            $this->commands, 
            $this->sources,
            $this->languages,
            $this->id,
            $this->sourceId,
            $this->languageId,
            $this->file,
            $this->duration
        );
        
        $source = $this->getMockBuilder('\core\media\source\ISource')
            ->getMock();
        
        $this->sources->expects($this->once())
            ->method('read')
            ->with($this->sourceId)
            ->willReturn($source);

        $result = $audio->source();

        $this->assertEquals($result, $source);
    }

    public function testItGivesItsLanguage()
    {
        $audio = new Audio(
            $this->commands, 
            $this->sources,
            $this->languages,
            $this->id,
            $this->sourceId,
            $this->languageId,
            $this->file,
            $this->duration
        );
        
        $language = $this->getMockBuilder('\core\media\language\ILanguage')
            ->getMock();
        
        $this->languages->expects($this->once())
            ->method('read')
            ->with($this->languageId)
            ->willReturn($language);

        $result = $audio->language();

        $this->assertEquals($result, $language);
    }
    
    public function testItGivesItsFile()
    {
        $audio = new Audio(
            $this->commands, 
            $this->sources,
            $this->languages,
            $this->id,
            $this->sourceId,
            $this->languageId,
            $this->file,
            $this->duration
        );

        $result = $audio->file();

        $this->assertEquals($result, $this->file);
    }
    
    public function testItGivesItsDuration()
    {
        $audio = new Audio(
            $this->commands, 
            $this->sources,
            $this->languages,
            $this->id,
            $this->sourceId,
            $this->languageId,
            $this->file,
            $this->duration
        );

        $result = $audio->duration();

        $this->assertEquals($result, $this->duration);
    }

    public function testItImports()
    {
        $audio = new Audio(
            $this->commands, 
            $this->sources,
            $this->languages,
            $this->id,
            $this->sourceId,
            $this->languageId,
            $this->file,
            $this->duration
        );
        
        $mediaData = $this->getMockBuilder('\core\service\media\mediainfo\IMediaData')
            ->getMock();
        
        $audioStream = $this->getMockBuilder('\core\service\media\mediainfo\IAudioStream')
            ->getMock();
        
        $this->commands->expects($this->once())
            ->method('import')
            ->with($audio, $mediaData, $audioStream);

        $audio->import($mediaData, $audioStream);
    }
}
