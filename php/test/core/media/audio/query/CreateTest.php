<?php
namespace core\media\audio\query;
class CreateTest extends \PHPUnit_Framework_TestCase
{    
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $database;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $audio;
    
    protected $values;
    protected $query;

    protected function setUp()
    {
        $this->database = $this->getMockBuilder('\core\service\sql\IQuery')->getMock();
        $this->audio = $this->getMockBuilder('\core\database\table\audio\IAudio')->getMock();
        
        $this->values = [
            'audio.source_id' => 1,
            'audio.language_id' => 2,
            'audio.file' => '"path/to/file"',
            'audio.duration' => 3000
        ];
        
        $this->audio->expects($this->once())
                ->method('name')
                ->willReturn('audio');
        
        $this->audio->expects($this->once())
                ->method('fields')
                ->willReturn(implode(', ', array_keys($this->values)));
        
        $this->audio->expects($this->once())
                ->method('values')
                ->willReturn(implode(', ', $this->values));
        
        $this->query = 'INSERT INTO audio (audio.source_id, audio.language_id, audio.file, audio.duration) VALUES (1, 2, "path/to/file", 3000);';
    }

    public function testItBuildsQuery()
    {
        $create = new Create(
            $this->database, 
            $this->audio,
            $this->values['audio.source_id'],
            $this->values['audio.language_id'],
            $this->values['audio.file'],
            $this->values['audio.duration']
        );

        $query = $create->query();

        $this->assertEquals($query, $this->query);
    }

    public function testItRunsQuery()
    {
        $create = new Create(
            $this->database, 
            $this->audio,
            $this->values['audio.source_id'],
            $this->values['audio.language_id'],
            $this->values['audio.file'],
            $this->values['audio.duration']
        );
        
        $id = 1;

        $this->database->expects($this->once())
            ->method('insert')
            ->with($this->query)
            ->willReturn($id)
        ;

        $result = $create->run();

        $this->assertEquals($result, $id);
    }
}
