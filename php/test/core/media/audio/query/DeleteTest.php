<?php
namespace core\media\audio\query;
class DeleteTest extends \PHPUnit_Framework_TestCase
{
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $database;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $audio;
    
    protected $id;
    protected $query;
    
    protected function setUp()
    {
        $this->database = $this->getMockBuilder('\core\service\sql\IQuery')->getMock();
        $this->audio = $this->getMockBuilder('\core\database\table\audio\IAudio')->getMock();
        
        $this->id = 5;
        
        $this->audio->expects($this->once())
            ->method('name')
            ->willReturn('audio');

        $this->audio->expects($this->once())
            ->method('idIs')
            ->with($this->id)
            ->willReturn('audio.id = ' . $this->id);
        
        $this->query = 'DELETE FROM audio WHERE audio.id = 5;';
    }

    public function testItBuildsQuery()
    {
        $delete = new Delete(
            $this->database, 
            $this->audio,
            $this->id
        );

        $query = $delete->query();

        $this->assertEquals($query, $this->query);
    }

    public function testItRunsQuery()
    {
        $delete = new Delete(
            $this->database, 
            $this->audio,
            $this->id
        );

        $rowsAffected  = 1;

        $this->database->expects($this->once())
            ->method('delete')
            ->with($this->query)
            ->willReturn($rowsAffected)
        ;

        $result = $delete->run();

        $this->assertEquals($result, $rowsAffected);
    }
}
