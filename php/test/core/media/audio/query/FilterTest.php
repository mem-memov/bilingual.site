<?php
namespace core\media\audio\query;
class FilterTest extends \PHPUnit_Framework_TestCase
{
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $database;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $audio;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $videoAudio;
    
    protected $sourceIds;
    protected $languageIds;
    protected $files;
    protected $videoIds;
    
    protected $query;

    protected function setUp()
    {
        $this->database = $this->getMockBuilder('\core\service\sql\IQuery')->getMock();
        $this->audio = $this->getMockBuilder('\core\database\table\audio\IAudio')->getMock();
        $this->videoAudio = $this->getMockBuilder('\core\database\table\videoAudio\IVideoAudio')->getMock();
        
        $this->sourceIds = [1];
        $this->languageIds = [40];
        $this->files = ['path/to/file'];
        $this->videoIds = [100];
        
        $this->audio->expects($this->once())
            ->method('name')
            ->willReturn('audio');
        
        $this->audio->expects($this->once())
            ->method('id')
            ->willReturn('id');

        $this->audio->expects($this->once())
            ->method('fields')
            ->willReturn('language_id, source_id, file, duration');

        $this->audio->expects($this->once())
            ->method('conditions')
            ->with($this->sourceIds, $this->languageIds, $this->files, [])
            ->willReturn([
                'audio.source_id IN (1)', 
                'audio.source_id IN (40)',
                'audio.source_id IN ("path/to/file")',
                'audio.source_id IN (100)'
            ]);

        $this->audio->expects($this->once())
            ->method('leftJoin')
            ->with($this->videoAudio)
            ->willReturn('LEFT OUTER JOIN video_audio ON(video_audio.id = audio.id)');
        
        $this->videoAudio->expects($this->once())
            ->method('conditions')
            ->with($this->videoIds, [])
            ->willReturn(['video_audio.video_id IN (100)']);
        
        $this->query = 'SELECT id, language_id, source_id, file, duration FROM audio LEFT OUTER JOIN video_audio ON(video_audio.id = audio.id) WHERE TRUE AND audio.source_id IN (40) AND audio.source_id IN ("path/to/file") AND audio.source_id IN (100);';
    }

    public function testItBuildsQuery()
    {
        $filter = new Filter(
            $this->database,
            $this->audio,
            $this->videoAudio,
            $this->sourceIds,
            $this->languageIds,
            $this->files,
            $this->videoIds
        );

        $query = $filter->query();

        $this->assertEquals($query, $this->query);
    }

    public function testItRunsQuery()
    {
        $filter = new Filter(
            $this->database,
            $this->audio,
            $this->videoAudio,
            $this->sourceIds,
            $this->languageIds,
            $this->files,
            $this->videoIds
        );

        $audios = [];

        $this->database->expects($this->any())
            ->method('select')
            ->with($this->query)
            ->willReturn($audios)
        ;

        $result = $filter->run();

        $this->assertEquals($result, $audios);
    }
}
