<?php
namespace core\media\audio\query;
class ReadTest extends \PHPUnit_Framework_TestCase
{
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $database;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $audio;
    
    protected $id;
    protected $query;
    
    protected function setUp()
    {
        $this->database = $this->getMockBuilder('\core\service\sql\IQuery')->getMock();
        $this->audio = $this->getMockBuilder('\core\database\table\audio\IAudio')->getMock();
        
        $this->id = 5;
        
        $this->audio->expects($this->once())
            ->method('name')
            ->willReturn('audio');
        
        $this->audio->expects($this->once())
            ->method('id')
            ->willReturn('id');

        $this->audio->expects($this->once())
            ->method('fields')
            ->willReturn('language_id, source_id, file, duration');

        $this->audio->expects($this->once())
            ->method('idIs')
            ->with($this->id)
            ->willReturn('audio.id = ' . $this->id);
        
        $this->query = 'SELECT id, language_id, source_id, file, duration FROM audio WHERE audio.id = 5;';
    }

    public function testItBuildsQuery()
    {
        $read = new Read(
            $this->database, 
            $this->audio,
            $this->id
        );

        $query = $read->query();

        $this->assertEquals($query, $this->query);
    }

    public function testItRunsQuery()
    {
        $read = new Read(
            $this->database, 
            $this->audio,
            $this->id
        );

        $rows = [
            [1, 5, 7, 'path/to/file', 3000]
        ];

        $this->database->expects($this->any())
            ->method('select')
            ->with($this->query)
            ->willReturn($rows)
        ;

        $result = $read->run();

        $this->assertEquals($result, $rows);
    }
}
