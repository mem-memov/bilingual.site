<?php
namespace core\media\clip\query;
class CreateTest extends \PHPUnit_Framework_TestCase
{    
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $database;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clip;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clipCutId;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clipLanguageId;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clipPhraseId;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clipPhraseDelay;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clipStart;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clipStop;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clipDuration;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $clipFile;
    
    protected $cutId;
    protected $languageId;
    protected $phraseId;
    protected $phraseDelay;
    protected $start;
    protected $stop;
    protected $duration;
    protected $file;

    protected function setUp()
    {
        $this->database = $this->getMockBuilder('\core\service\sql\IQuery')->getMock();
        $this->clip = $this->getMockBuilder('\core\database\table\clip\IClip')->getMock();
        $this->clipCutId = $this->getMockBuilder('\core\database\table\clip\field\ICutId')->getMock();
        $this->clipLanguageId = $this->getMockBuilder('\core\database\table\clip\field\ILanguageId')->getMock();
        $this->clipPhraseId = $this->getMockBuilder('\core\database\table\clip\field\IPhraseId')->getMock();
        $this->clipPhraseDelay = $this->getMockBuilder('\core\database\table\clip\field\IPhraseDelay')->getMock();
        $this->clipStart = $this->getMockBuilder('\core\database\table\clip\field\IStart')->getMock();
        $this->clipStop = $this->getMockBuilder('\core\database\table\clip\field\IStop')->getMock();
        $this->clipDuration = $this->getMockBuilder('\core\database\table\clip\field\IDuration')->getMock();
        $this->clipFile = $this->getMockBuilder('\core\database\table\clip\field\IFile')->getMock();
        
        $this->cutId = 1;
        $this->languageId = 2;
        $this->phraseId = 3;
        $this->phraseDelay = 20;
        $this->start = 1500;
        $this->stop = 1700;
        $this->duration = 200;
        $this->file = 'path/to/file';
        
        $this->clip->expects($this->once())
            ->method('name')
            ->willReturn('clip');

        $this->clipCutId
            ->expects($this->once())
            ->method('name')
            ->willReturn('cut_id');
        $this->clipCutId
            ->expects($this->once())
            ->method('value')
            ->with($this->cutId)
            ->willReturn((string)$this->cutId);

        $this->clipLanguageId
            ->expects($this->once())
            ->method('name')
            ->willReturn('language_id');
        $this->clipLanguageId
            ->expects($this->once())
            ->method('value')
            ->with($this->languageId)
            ->willReturn((string)$this->languageId);

        $this->clipPhraseId
            ->expects($this->once())
            ->method('name')
            ->willReturn('phrase_id');
        $this->clipPhraseId
            ->expects($this->once())
            ->method('value')
            ->with($this->phraseId)
            ->willReturn((string)$this->phraseId);

        $this->clipPhraseDelay
            ->expects($this->once())
            ->method('name')
            ->willReturn('phrase_delay');
        $this->clipPhraseDelay
            ->expects($this->once())
            ->method('value')
            ->with($this->phraseDelay)
            ->willReturn((string)$this->phraseDelay);

        $this->clipStart
            ->expects($this->once())
            ->method('name')
            ->willReturn('start');
        $this->clipStart
            ->expects($this->once())
            ->method('value')
            ->with($this->start)
            ->willReturn((string)$this->start);

        $this->clipStop
            ->expects($this->once())
            ->method('name')
            ->willReturn('stop');
        $this->clipStop
            ->expects($this->once())
            ->method('value')
            ->with($this->stop)
            ->willReturn((string)$this->stop);

        $this->clipDuration
            ->expects($this->once())
            ->method('name')
            ->willReturn('duration');
        $this->clipDuration
            ->expects($this->once())
            ->method('value')
            ->with($this->duration)
            ->willReturn((string)$this->duration);
        
        $this->clipFile
            ->expects($this->once())
            ->method('name')
            ->willReturn('file');
        $this->clipFile
            ->expects($this->once())
            ->method('value')
            ->with($this->file)
            ->willReturn('"' . $this->file . '"');


        
        $this->query = '
            INSERT INTO
                clip (
                    cut_id,
                    language_id,
                    phrase_id,
                    phrase_delay,
                    start,
                    stop,
                    duration,
                    file
                )
            VALUES 
                (
                    1,
                    2,
                    3,
                    20,
                    1500,
                    1700,
                    200,
                    "path/to/file"
                )
            ;
        ';
    }

    public function testItBuildsQuery()
    {
        $create = new Create(
            $this->database,
            $this->clip,
            $this->clipCutId,
            $this->clipLanguageId,
            $this->clipPhraseId,
            $this->clipPhraseDelay,
            $this->clipStart,
            $this->clipStop,
            $this->clipDuration,
            $this->clipFile,
            $this->cutId,
            $this->languageId,
            $this->phraseId,
            $this->phraseDelay,
            $this->start,
            $this->stop,
            $this->duration,
            $this->file
        );

        $query = $create->query();

        $this->assertEquals($query, $this->query);
    }

    public function testItRunsQuery()
    {
        $create = new Create(
            $this->database,
            $this->clip,
            $this->clipCutId,
            $this->clipLanguageId,
            $this->clipPhraseId,
            $this->clipPhraseDelay,
            $this->clipStart,
            $this->clipStop,
            $this->clipDuration,
            $this->clipFile,
            $this->cutId,
            $this->languageId,
            $this->phraseId,
            $this->phraseDelay,
            $this->start,
            $this->stop,
            $this->duration,
            $this->file
        );

        $this->database->expects($this->once())
            ->method('insert')
            ->with($this->query)
            ->willReturn(1)
        ;

        $result = $create->run();

        $this->assertEquals($result, 1);
    }
}
