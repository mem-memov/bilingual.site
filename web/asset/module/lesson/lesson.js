$(function() {
    
    var Clip = function(collection, index) {
        this.index = index;
        this.type = collection[index].type;
        this.file = collection[index].file;
        this.phrase = collection[index].phrase;
        this.start = collection[index].start;
        this.stop = collection[index].stop;
        this.duration = collection[index].duration;
        this.$ = null;
    }
    
    Clip.prototype.append = function($videoContainer) {
        
        var id = 'lesson-clip-' + this.index;

        $videoContainer.append(
            '<video preload="auto" id="' + id + '">' + 
                '<source src="' + this.file + '" type="video/mp4">' + 
            '</video>'
        );

        this.$ = $('#' + id).hide();

    }
    
    Clip.prototype.play = function(visible, after, scope) {
    
        var me = this;
        
        var initialVolume = me.$.get(0).volume;

        me.$.one('ended', function() {

            if (visible) {
                me.$.hide();
            }
            
            me.$.get(0).currentTime  = 0;

            if (typeof after == 'function' && typeof scope == 'object') {
                after.call(scope, me);
            }
        });
        
        if (visible) {
            me.$.show();
        }
        me.$.get(0).play();
    }
    
    Clip.prototype.begin = function(silenced, visible, duration, after, scope) {

        var me = this;
        
        if (duration > me.duration) {
            duration = me.duration;
        }

        me.$.on('timeupdate', function() {

            if (me.$.get(0).currentTime*1000 > duration) {

                me.$.off('timeupdate');

                if (visible) {
                    me.$.hide();
                }
                
                me.$.get(0).pause();
                me.$.get(0).currentTime  = 0;

                if (typeof after == 'function' && typeof scope == 'object') {
                    after.call(scope, me);
                }
            }

        });

        if (visible) {
            me.$.show();
        }
        
        me.$.get(0).play();

        if (silenced) {
            
            var initialVolume = me.$.get(0).volume;
            var step = 20;
            var steps = duration / step;
            var decrease = 1 / steps;

            var fadeout = setInterval(function() {
                var volume = me.$.get(0).volume;
                if (volume > (decrease * 2) ) {
                    me.$.get(0).volume = volume - decrease;
                }
                else {
                    me.$.get(0).volume = initialVolume;
                    clearInterval(fadeout);
                }
            }, 
            step);
        }
    }
    
    Clip.prototype.finish = function(visible, duration, after, scope) {
        
        var me = this;

        me.$.one('ended', function() {

            if (visible) {
                me.$.hide();
            }
            me.$.get(0).currentTime  = 0;

            if (typeof after == 'function' && typeof scope == 'object') {
                after.call(scope, me);
            }
        });
        
        me.$.get(0).currentTime = this.duration > duration ? this.duration - duration : 0;
        if (visible) {
            me.$.show();
        }
        me.$.get(0).play();
    }
    
    var Subtitle = function() {
        this.$container = $('.lesson .subtitle-container').hide();
        this.$originalPhrase = $('.lesson .subtitle-container .original');
        this.$foreignPhrase = $('.lesson .subtitle-container .foreign');
    }
    
    Subtitle.prototype.show = function(originalPhrase, foreignPhrase, after, scope) {


        this.$originalPhrase.text(originalPhrase);
        this.$foreignPhrase.text(foreignPhrase);
        this.$container.show();

        var duration = originalPhrase.length*200;
        
        if (typeof after == 'function' && typeof scope == 'object') {
            var me = this;
            window.setTimeout(function() {
                me.hide();
                after.call(scope);
            }, duration);
        }

    }
    
    Subtitle.prototype.hide = function() {
        
        this.$container.hide();
        this.$originalPhrase.text('');
        this.$foreignPhrase.text('');
        
    }
    
    var Lesson = function() {
        this.subtitle = new Subtitle();
        this.$videoContainer = $('.lesson .video-container');
        this.$message = $('.lesson .message');
        this.clips = [];
        this.numberOfForeignClips = 0;
        this.numberOfOriginalClips = 0;
    }

    Lesson.prototype.load = function(after) {
        
        var me = this;

        $.getJSON(
            '/visitor/lesson/teach',
            function(data) {

                for (var i = 0; i < data.clips.length; i++) {

                    var clip = new Clip(data.clips, i);

                    clip.append(me.$videoContainer);

                    me.clips.push(clip);

                    if (clip.type == 'foreign') {
                        me.numberOfForeignClips++;
                    }
                    if (clip.type == 'original') {
                        me.numberOfOriginalClips++;
                    }
                }

                if (typeof after == 'function') {
                    after(me);
                }
            }
        );
    }
    
    Lesson.prototype.showMessage = function(message, after, scope) {

        this.$message.text(message);
        this.$message.show();

        var duration = message.length*100;
        
        if (typeof after == 'function' && typeof scope == 'object') {
            var me = this;
            window.setTimeout(function() {
                me.hideMessage();
                after.call(scope);
            }, duration);
        }
        
    }
    
    Lesson.prototype.hideMessage = function() {
        this.$message.hide();
    }
    
    Lesson.prototype.firstClip = function(type) {
        
        var firstClip = null;
        
        for (var i=0; i<this.clips.length; i++) {

            if (
                this.clips[i].type === type
                && (!firstClip || this.clips[i].start < firstClip.start)
            ) {
                firstClip = this.clips[i];
            } 
        }
        
        return this.nextClip(firstClip);
    }
    
    Lesson.prototype.siblingClip = function(clip) {
        
        var siblingClip = null;
        
        for (var i=0; i<this.clips.length; i++) {

            if (
                clip.type !== this.clips[i].type
                && this.clips[i].stop > clip.start
                && (!siblingClip ||  (siblingClip.stop - clip.start) > (this.clips[i].stop - clip.start))
            ) {
                siblingClip = this.clips[i];
            } 
        }
        
        return siblingClip;
    }
    
    Lesson.prototype.previousClip = function(clip, type) {
        
        if (typeof type == 'undefined') {
            type = clip.type;
        }

        var previousClip = null;
        
        for (var i=0; i<this.clips.length; i++) {

            if (
                type === this.clips[i].type
                && clip.start > this.clips[i].stop
                && (!previousClip ||  (clip.start - previousClip.stop) > (clip.start - this.clips[i].stop))
            ) {
                previousClip = this.clips[i];
            } 
        }
        
        return previousClip;
        
    }
    
    Lesson.prototype.nextClip = function(clip, type) {
        
        if (typeof type == 'undefined') {
            type = clip.type;
        }

        var nextClip = null;
        
        for (var i=0; i<this.clips.length; i++) {

            if (
                type === this.clips[i].type
                && clip.stop < this.clips[i].start
                && (!nextClip ||  (nextClip.start - clip.stop) > (this.clips[i].start - clip.stop))
            ) {
                nextClip = this.clips[i];
            } 
        }
        
        return nextClip;
        
    }
    
    Lesson.prototype.playSequence = function(clip, length, after, scope) {
        
        if (length <= 0 && typeof after == 'function' && typeof scope == 'object') {
            after.call(scope, clip, clip);
            return;
        }

        clip.play(true, function(clip) {
            
            var nextClip = this.nextClip(clip);

            if (nextClip !== null && length > 0) {
                
                this.playSequence(nextClip, --length, after, scope);

            } else if (typeof after == 'function' && typeof scope == 'object') {
                
                after.call(scope, clip, nextClip);
                
            }
            
        }, this);
        
    }
    
    Lesson.prototype.playSubtitle = function(clip, after, scope) {

        var previousClip = this.previousClip(clip);
        var nextClip = this.nextClip(clip);
        var siblingClip = this.siblingClip(clip);

        previousClip.finish(true, 500, function() {
            clip.play(true, function(clip) {
                if (nextClip !== null) {
                    nextClip.begin(true, false, 500, null, this);
                }
                this.subtitle.show(clip.phrase, siblingClip.phrase, function() {
                    if (typeof after == 'function' && typeof scope == 'object') {
                        after.call(scope, previousClip, clip, nextClip);
                    }
                }, this);
                clip.play(false);
            }, this);
        }, this);
    }
    
    Lesson.prototype.interchangeOriginal = function(offset, numberOfForeignClips, foreignClip, after, scope) {
        
        var length = numberOfForeignClips;
        
        if (foreignClip === null) {
            var foreignClip = this.firstClip('foreign');
            length = numberOfForeignClips - offset;
        }

        this.playSequence(foreignClip, length, function(clip, nextClip) {
            if (nextClip !== null) {
                var originalClip = this.siblingClip(nextClip);
                if (originalClip !== null) {
                    this.playSubtitle(originalClip, function(previousClip, clip, nextClip) {
                        if (nextClip !== null) {
                            var foreignClip = this.siblingClip(nextClip);
                            if (foreignClip !== null) {
                                this.interchangeOriginal(offset, numberOfForeignClips, foreignClip, after, scope);
                            } else if (typeof after == 'function' && typeof scope == 'object') {
                                after.call(scope);
                            }
                        } else if (typeof after == 'function' && typeof scope == 'object') {
                            after.call(scope);
                        }
                    }, this);
                } else if (typeof after == 'function' && typeof scope == 'object') {
                    after.call(scope);
                }
            } else if (typeof after == 'function' && typeof scope == 'object') {
                after.call(scope);
            }
        }, this);
    }
    
    Lesson.prototype.increaceOriginal = function(after, scope) {
        this.showMessage('Прослушайте отрывки на иностранном языке (1/4)', function() {
            this.interchangeOriginal(3, 3, null, function() {
                this.showMessage('Прослушайте отрывки на иностранном языке (2/4)', function() {
                    this.interchangeOriginal(2, 3, null, function() {
                        this.showMessage('Прослушайте отрывки на иностранном языке (3/4)', function() {
                            this.interchangeOriginal(1, 3, null, function() {
                                this.showMessage('Прослушайте отрывки на иностранном языке (4/4)', function() {
                                    this.interchangeOriginal(0, 3, null, function() {
                                        if (typeof after == 'function' && typeof scope == 'object') {
                                            after.call(scope);
                                        }
                                    }, this);
                                }, this);
                            }, this);
                        }, this);
                    }, this);
                }, this);
            }, this);
        }, this);

    }
    
    Lesson.prototype.playOriginal = function(originalClip, after, scope) {
        
        if (originalClip === null) {
            var originalClip = this.firstClip('original');
        }
        
        this.playSubtitle(originalClip, function(previousClip, clip, nextClip) {
            if (nextClip !== null) {

                this.playOriginal(nextClip, after, scope);

            } else if (typeof after == 'function' && typeof scope == 'object') {
                after.call(scope);
            }
        }, this);
    }
    
    var lesson = new Lesson();
    
    lesson.load(function (lesson) {
        
        var foreignClip = lesson.firstClip('foreign');

        lesson.showMessage('Ознакомьтесь с учебным роликом (1 мин)', function() {
            lesson.playSequence(foreignClip, lesson.numberOfForeignClips-1, function() {
                lesson.increaceOriginal(function() {
                    lesson.showMessage('Посмотрите ролик с переводом', function() {
                        lesson.playOriginal(null, function() {
                            lesson.showMessage('Прослушайте ролик целиком на иностранном языке', function() {
                                var originalClip = lesson.firstClip('original');
                                lesson.playSequence(originalClip, lesson.numberOfOriginalClips-1, function() {

                                }, lesson);
                            }, lesson);
                        }, lesson);
                    }, lesson);
                }, lesson);
            }, lesson);
        }, lesson);
        


        
        

    });
    
});
